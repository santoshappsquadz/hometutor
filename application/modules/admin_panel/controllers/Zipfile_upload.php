<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Zipfile_upload extends MX_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('zipfile_upload_model');
		$this->load->library('session');
                if(!$this->session->userdata('adminInfo')){	
                    redirect('login'); 
                 }
	}

	public function index(){

		$this->load->view('zipfile_upload');
	}


	public function Upload_ZIPFile_To_Server(){
		/*$file_dir = base_url('/assets');
		 print_r($_FILES["zipfile"]["tmp_name"]);
		move_uploaded_file($_FILES["zipfile"]["tmp_name"], "$file_dir/");

		if(is_uploaded_file($_FILES['zipfile']['tmp_name'])){

			$zipfilename = basename($_FILES['zipfile']['name']);
			print_R($zipfilename);
			$zip = zip_open($zipfilename);
			print_R($zip);
			if ($zip) {
				while ($zip_entry = zip_read($zip)) {
				$fp = fopen($targetdir . zip_entry_name($zip_entry), "w");
				echo $fp;die;
					if (zip_entry_open($zip, $zip_entry, "r")) {
						$buf = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
						fwrite($fp,"$buf");
						zip_entry_close($zip_entry);
						fclose($fp);
					}
				}
			}
			zip_close($zip);
			//redirect("admin_panel/zipfile_upload");
		}*/
	if(!empty($_FILES['zipfile']['size'])){
		if($_FILES["zipfile"]["name"]) {

			$filename = $_FILES["zipfile"]["name"];
			$source = $_FILES["zipfile"]["tmp_name"];
			$type = $_FILES["zipfile"]["type"];
			$size = $_FILES['zipfile']['size'];

			if(!($_FILES['zipfile']['error'])){
				if($size <= 2000000){
					$name = explode(".", $filename);
					$accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');
					foreach($accepted_types as $mime_type) {
						if($mime_type == $type) {
							$okay = true;
							break;
						}
					}

					$continue = strtolower($name[1]) == 'zip' ? true : false;
					if(!$continue) {
						$message = "The file you are trying to upload is not a .zip file. Please try again.";
					}

					$target_path = "/var/www/html/homeTutor/assets/question_img/".$filename;  // change this to the correct site path
					if(move_uploaded_file($source, $target_path)) {
						$zip = new ZipArchive();
						$x = $zip->open($target_path);
						if ($x === true) {
							$zip->extractTo("/var/www/html/homeTutor/assets/question_img/"); // change this to the correct site path
							$zip->close();

							unlink($target_path);
						}
						/*
						$a = '/var/www/html/homeTutorDev/assets/a/a/';
						$b = '/var/www/html/homeTutorDev/assets/upload/';
						shell_exec("cp -r $a $b");
						if (copy($a,$b)) {
								unlink("/var/www/html/homeTutorDev/assets/".$name[0]);
						} */

						 $this->session->set_flashdata('success', "Your .zip file was uploaded");
						redirect('admin_panel/zipfile_upload');
					}
					else {

						 $this->session->set_flashdata('success', "There was a problem with the upload. Please try again.");
						redirect('admin_panel/zipfile_upload');
					}
				} else{
			$this->session->set_flashdata('success', "File upload max size 2MB");
						redirect('admin_panel/zipfile_upload');
		}

			/* 	$config['upload_path'] = base_url('assets/upload/');
				$config['allowed_types'] = 'zip';
				$config['max_size']    = '10mb';
				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload())
				{
					$error = array('error' => $this->upload->display_errors());
					$this->load->view('upload_form_view', $error);
				}
				else
				{
					$data = array('upload_data' => $this->upload->data());
					$zip = new ZipArchive;
					$file = $data['upload_data']['full_path'];
					chmod($file,0777);
					if ($zip->open($file) === TRUE) {
							$zip->extractTo('/var/www/html/homeTutorDev/assets/upload/');
							$zip->close();
							echo 'ok';
					} else {
							echo 'failed';
					}
					redirect('admin_panel/zipfile_upload', $data);
				} */
		}
		else{
			$this->session->set_flashdata('success', "File upload max size 2MB");
						redirect('admin_panel/zipfile_upload');
		}

	}
}else{
	$this->session->set_flashdata('success', "Please select file");
				redirect('admin_panel/zipfile_upload');
}

	}
}
?>
