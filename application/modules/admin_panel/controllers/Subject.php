<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subject extends MX_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->model('subject_model');
        $this->load->library('session');
        $this->load->library('email');
        if(!$this->session->userdata('adminInfo')){	
	   redirect('login'); 
        }
        
    }
    
    

    public function index() 
    {	
		$obj     =  new subject_model();
		$data    =  array();
		$data['data1'] = $obj->get_board();
		
        $this->load->view('subjectupload',$data);
    }
	
	public function get_class_for_subject(){
		$board_id = $this->input->post('board_id');
		$obj = new subject_model();
		$data = array();
		$data = $obj->get_class_for_subject($board_id);
		echo '<option value="">Select Class</option>';
		foreach($data as $qu2){
		echo "<option value='".$qu2['id']."'>".$qu2['name']."</option>";
		}
	}
	public function get_class_for_subject_topic(){
		$board_id = $this->input->post('board_id');
		$obj = new subject_model();
		$data = array();
		$data = $obj->get_class_for_subject_topic($board_id);
		echo '<option value="">Select Class</option>';
		foreach($data as $qu2){
		echo "<option value='".$qu2['id']."_".$qu2['classId']."'>".$qu2['className']."</option>";
		}
	}
	public function upload_subject() 
    {	
		if($this->input->post()){
			$class_id = $this->input->post('class_id');
			$subject_name = $this->input->post('mytext');
			if($class_id != ''){
				foreach($subject_name as $a){
					if(!empty($a)){
						$obj     =  new subject_model();
						$data    =  array();
						$data    = $obj->upload_subject($class_id,$a);
					}
				}
				if($data){
					$this->session->set_flashdata('success', "successfully upload");
					redirect('admin_panel/subject/all_subject');
				}else{
					$this->session->set_flashdata('success', "Subject Not Upload");
					redirect('admin_panel/Subject');
				}
			}else{
				$this->session->set_flashdata('success', "Subject Not Upload");
				redirect('admin_panel/Subject');
			}
		
		}
	}
	
	
	public function topic_upload() 
    {	
		$obj     =  new subject_model();
		$data    =  array();
		$data['data1'] = $obj->get_board();
		
        $this->load->view('topicupload',$data);
    }
	
	public function get_subject_for_topic() 
    {	
		$class_id = $this->input->post('class_id');
		$class_array = explode('_',$class_id);
		$obj = new subject_model();
		$data = array();
		$data = $obj->get_subject($class_array[1]);
		echo '<option value="">Select Subject</option>';
		foreach($data as $qu2){
		echo "<option value='".$qu2['id']."'>".$qu2['name']."</option>";
		}
    }
	
	public function topic_upload_data(){
		if($this->input->post()){
			$class_id = $this->input->post('class_id');
			$subject_id = $this->input->post('subject_id');
			$topic_name = $this->input->post('mytext');
			if($class_id != '' and $subject_id !=''){
				foreach($topic_name as $a){
					if(!empty($a)){
						$obj     =  new subject_model();
						$data    =  array();
						$data    = $obj->upload_topic($class_id,$subject_id,$a);
					}
				}
				if($data){
					$this->session->set_flashdata('success', "successfully upload");
					redirect('admin_panel/subject/all_topic');
				}else{
					$this->session->set_flashdata('success', "Please Select Option");
					redirect('admin_panel/subject/topic_upload');
				}
				
			}else{
				$this->session->set_flashdata('success', "Please Select Option");
				redirect('admin_panel/subject/topic_upload');
			}
		}
		
	}
	public function all_topic(){
		$obj     =  new subject_model();
		$data    =  array();
		$data['data']    = $obj->all_topic();
		$i=0;
		foreach($data['data'] as $r){
			$class_array = explode('_',$r['class_id']);		
			$this->db->select('ht_subject.name');
			$this->db->where('ht_subject.id',$r['subject_id']);
			$r1 = $this->db->get('ht_subject')->row_array();
			$this->db->select('ht_parentClass.className as class');
			$this->db->where('ht_parentClass.id',$class_array[0]);
			$r2 = $this->db->get('ht_parentClass')->row_array();
			$r['subject'] = $r1['name'];
			$r['class'] = $r2['class'];
			$data['data'][$i] = $r;
			$i++;
		}
		$this->load->view('alltopic',$data);
	} 
	public function edit_topic(){
		$id = $_GET['id'];
		$this->db->select('ht_topics.*,ht_subject.name,ht_parentClass.className as class');
		$this->db->where('ht_topics.id',$id);
		$this->db->from('ht_topics');
		$this->db->join('ht_subject','ht_subject.id = ht_topics.subject_id');
		$this->db->join('ht_parentClass','ht_parentClass.id =SUBSTRING_INDEX(`ht_topics`.`class_id`,"_",1)');
		$data['data'] = $this->db->get()->row_array();
        if (!$_POST) {
            $this->load->view('edit_topic', $data);
        } else {
            $post_data = $_POST;
			unset($post_data['submit']);
            $obj     =  new subject_model();
			$data    =  array();
			$data_in_db   = $obj->edit_topic_in_db($post_data);
			if($data_in_db){
				$this->session->set_flashdata('success', 'topic updated Successfully');
			}else{
				$this->session->set_flashdata('success', 'Record Not updated');
			}
			
            redirect('admin_panel/subject/all_topic');
        }
	}
	/* public function Delete_topic(){
		$id = $_GET['id'];
		$this->db->where('ht_topics.id',$id);
		$this->db->delete('ht_topics');
            redirect('admin_panel/subject/all_topic');
	} */
	
	public function all_subject(){
		$obj     =  new subject_model();
		$data    =  array();
		$data['data']    = $obj->all_subject();
		$i=0;
		foreach($data['data'] as $r){
		$this->db->select('name');
		$this->db->where('id',$r['fkClassId']);
		$r1 = $this->db->get('ht_class')->row_array();
		$r['class'] = $r1['name'];
		$data['data'][$i] = $r;
		$i++;
		}
		$this->load->view('allsubject',$data);
	} 
	
	public function edit_subject(){
		$id = $_GET['id'];
		$this->db->select('ht_subject.*,ht_class.name as class');
		$this->db->where('ht_subject.id',$id);
		$this->db->from('ht_subject');
		$this->db->join('ht_class','ht_class.id = ht_subject.fkClassId');
		$data['data'] = $this->db->get()->row_array();
        if (!$_POST) {
            $this->load->view('edit_subject', $data);
        } else {
            $post_data = $_POST;
			unset($post_data['submit']);
            $obj     =  new subject_model();
			$data    =  array();
			$data_in_db   = $obj->edit_subject_in_db($post_data);
			if($data_in_db){
				$this->session->set_flashdata('success', 'Subject has been update Successfully');
			}else{
				$this->session->set_flashdata('success', 'Subject Not updated');
			}
			
            redirect('admin_panel/subject/all_subject');
        }
	}
}
?>
