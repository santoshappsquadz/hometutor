<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tutor_offer extends MX_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('tutor_offer_model');
		$this->load->library('session');
                if(!$this->session->userdata('adminInfo')){	
	        redirect('login'); 
            }
	}
	
	public function index(){
		$obj = new tutor_offer_model;
		$data = array();
		$data = $obj->all_tutor_offer_in_db();
		$i=0;
		foreach($data as $offer_Data){
			$r = explode(',',$offer_Data['tutor_id']);
			foreach($r as $tutor_id){
				$tutor_name = $obj->get_tutor_name_in_db($tutor_id);
				$tutor_name_array[] = $tutor_name['name'];
			}
			$tutor_name_string = implode(', ',$tutor_name_array);
				$data[$i]['tutor_name'] = $tutor_name_string;
				$tutor_name_array = array();
				$i++;
		}
		$data1['data'] = $data;
		$this->load->view('all_tutor_offers',$data1);
	}
	public function create_offer_for_tutor(){
		$obj = new tutor_offer_model;
		if (!$_POST) {
		$data = array();
		$data['tutor'] = $obj->tutor_name_in_db();
		$this->load->view('create_offers',$data);
		
		}else {
			$data = $_POST;
			$r = implode(',',$data['tutor_id']);
			$data['tutor_id'] = $r;	
            unset($data['submit1']);
			unset($data['example_length']);
			
			if(!empty($data['start_date']) && !empty($data['end_date']) &&!empty($data['offer']) && !empty($data['tutor_id'])){
				$addedmake = $obj->tutor_offer_insert_db($data);
				if ($addedmake) {
					$this->session->set_flashdata('success', 'Offers Create Successfully');
					redirect("admin_panel/tutor_offer");
				}else {
					$this->session->set_flashdata('success', 'Offers Not Create');
					redirect("admin_panel/tutor_offer/create_offer_for_tutor");
				}
			}else{
				$this->session->set_flashdata('success', 'Please Fill All Field');
				redirect("admin_panel/tutor_offer/create_offer_for_tutor");
			}
		
        }
	}
	public function edit_offer_for_tutor(){
		$id = $_GET['id'];
		$obj = new tutor_offer_model;
		if (!$_POST) {
			$data = array();
			$data = $obj->one_offer_in_db($id);
		$data1['edit'] = $data;
		$data1['tutor'] = $obj->tutor_name_in_db();
		$this->load->view('create_offers',$data1);
		
		}else {
			$data = array();
			$data = $_POST;
			unset($data['example_length']);
			if(!empty($data['start_date']) && !empty($data['end_date']) &&!empty($data['offer']) && !empty($data['tutor_id'])){
				$data = $_POST;
				unset($data['example_length']);
				$r = implode(',',$data['tutor_id']);
				$data['tutor_id'] = $r;	
				unset($data['submit1']);
				$addedmake = $obj->tutor_offer_update_db($data);
				if ($addedmake) {
					$this->session->set_flashdata('success', 'Offers Successfully Update');
					redirect("admin_panel/tutor_offer");
				} else {
					$this->session->set_flashdata('success', ' Offers Not Update');
					redirect("admin_panel/tutor_offer");
				}
			}else{
				$this->session->set_flashdata('success', 'Please Fill All Field');
				redirect("admin_panel/tutor_offer/edit_offer_for_tutor?id=".$id );
			}
        }
	}
	public function offer_delete(){
		$id = $_GET['id'];
		$obj = new tutor_offer_model();
		$data = array();
		$data = $obj->tutor_offer_delete_in_db($id);
		if ($data) {
                $this->session->set_flashdata('success', 'Offers Successfully Delete');
                redirect("admin_panel/tutor_offer");
            } else {
                $this->session->set_flashdata('success', 'Offers Not Delete');
                redirect("admin_panel/tutor_offer");
            }
	}
	public function image_upload_how_it_work(){
		$data['data'] = $this->tutor_offer_model->get_data_ht_how_it_work();
		$this->load->view('image_how_it_work',$data);
	}
	public function deleteImage_how_it_work($id)
	{
		$this->db->where('id',$id);
		$res = $this->db->delete('ht_how_it_work');
		if($res ==TRUE)
		{
			$this->session->set_flashdata('success', "Image successfully delete");
		}else{
			$this->session->set_flashdata('success', "Image Not delete");
		}
		redirect('admin_panel/tutor_offer/image_upload_how_it_work'); 
		
	}
	
	public function upload_image_for_how_it_work(){
		if(!empty($_FILES['image']['size'])) {
			$status = $_POST['r1'];
			$filename = $_FILES["image"]["name"];
			$source = $_FILES["image"]["tmp_name"];
			$type = $_FILES["image"]["type"];
			$size = $_FILES['image']['size'];
			$imageFileType = pathinfo($filename,PATHINFO_EXTENSION);
			$target_path = '/var/www/html/homeTutor/assets/image_for_how_it_work'.'/'.$filename;
			if(!($_FILES['image']['error'])){
				/* if (file_exists($target_path)) {
					echo "Sorry, file already exists.";
					$this->session->set_flashdata('success', "Sorry, file already exists.");
				} */
				 if ($_FILES["image"]["size"] > 102400) {
					echo "Sorry, your file is too large.";
					$this->session->set_flashdata('success', "Sorry, your file is too large.");
				}
				else if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				&& $imageFileType != "gif" ) {
					$this->session->set_flashdata('success', "Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
				}else{
					$target_path = '/var/www/html/homeTutor/assets/image_for_how_it_work'.'/'.$filename;
					// change this to the correct site path
					if(move_uploaded_file($source, $target_path)) {
						$image_upload_in_db = $this->tutor_offer_model->image_update_for_how_it_work($filename,$status);
						if($image_upload_in_db){
							$this->session->set_flashdata('success', "Your .file was uploaded");
						 }else{
							 $this->session->set_flashdata('success', "File already exists.");
						 }
					}else{
						$this->session->set_flashdata('success', "File upload Error");
					}
				}
				redirect('admin_panel/tutor_offer/image_upload_how_it_work'); 
			} else{
			$this->session->set_flashdata('success', "File upload max size 100 KB");
						redirect('admin_panel/tutor_offer/image_upload_how_it_work');
			}
		}else{
					$this->session->set_flashdata('success', "Please select file");
					redirect('admin_panel/tutor_offer/image_upload_how_it_work');
		}
		
	}
	
	public function image_upload_why_chooses(){
		$data['data'] = $this->tutor_offer_model->get_data_ht_ht_why_choose();
		$this->load->view('image_why_chooses',$data);
	}
	
	
	public function deleteImage_why_chooses($id)
	{
		$this->db->where('id',$id);
		$res = $this->db->delete('ht_why_choose');
		if($res ==TRUE)
		{
			$this->session->set_flashdata('success', "Image successfully delete");
		}else{
			$this->session->set_flashdata('success', "Image Not delete");
		}
		redirect('admin_panel/tutor_offer/image_upload_why_chooses'); 
		
	}
	
	public function upload_image_for_why_chooses(){
		if(!empty($_FILES['image']['size'])) {
			$status = $_POST['r1'];
			$filename = $_FILES["image"]["name"];
			$source = $_FILES["image"]["tmp_name"];
			$type = $_FILES["image"]["type"];
			$size = $_FILES['image']['size'];
			$imageFileType = pathinfo($filename,PATHINFO_EXTENSION);
			$target_path = '/var/www/html/homeTutor/assets/image_for_why_chooses'.'/'.$filename;
			if(!($_FILES['image']['error'])){
				/* if (file_exists($target_path)) {
					echo "Sorry, file already exists.";
					$this->session->set_flashdata('success', "Sorry, file already exists.");
				} */
				 if ($_FILES["image"]["size"] > 102400) {
					echo "Sorry, your file is too large.";
					$this->session->set_flashdata('success', "Sorry, your file is too large.");
				}
				else if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				&& $imageFileType != "gif" ) {
					$this->session->set_flashdata('success', "Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
				}else{
					// change this to the correct site path
					if(move_uploaded_file($source, $target_path)) {
						$image_upload_in_db = $this->tutor_offer_model->image_update_for_why_chooses($filename,$status);
						 if($image_upload_in_db){
							$this->session->set_flashdata('success', "Your .file was uploaded");
						 }else{
							 $this->session->set_flashdata('success', "File already exists.");
						 }
					}else{
						$this->session->set_flashdata('success', "File upload Error");
					}
				}
				redirect('admin_panel/tutor_offer/image_upload_why_chooses'); 
			} else{
					$this->session->set_flashdata('success', "File upload max size 100 KB");
					redirect('admin_panel/tutor_offer/image_upload_why_chooses');
			}
		}
		 else{
					$this->session->set_flashdata('success', "Please select file");
					redirect('admin_panel/tutor_offer/image_upload_why_chooses');
		}
	}
}

?>