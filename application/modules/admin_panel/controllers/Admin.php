<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MX_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->library('session');
        $this->load->library('email');
        if(!$this->session->userdata('adminInfo')){	
                    redirect('login'); 
                 }

    }



    public function index()
    {
        $this->load->view('login');
    }

    public function login()
    {
        $obj          = new admin_model();
        $email        = $this->input->post('email');
        $password     = md5($this->input->post('password'));
        $results      = $obj->checkAdminExist($email,$password);
        if($results)
        {
            redirect('dashboardNew');
        }
        else
        {
            $this->session->set_flashdata('error','Wrong credentials');
            redirect('admin');
        }

    }

    public function logout()
    {
       $this->session->sess_destroy();
       redirect('admin');
    }
    /*
    public function dashboard()
    {
       $obj                        = new admin_model();
       $data                       = array();
       $data['tutors']             = $obj->getAllTutorsProfile();
       $this->load->view('dashboard',$data);
    } */

      public function dashboardNew()
    {
       $obj                        = new admin_model();
       $data                       = array();
       $data['tutors']             = $obj->getAllTutorsProfile();
       $this->load->view('dashboardNew',$data);
    }

    public function test_ajax(){
       $obj                        = new admin_model();
       $data                       = array();
       $totalData                  = array();
       $requestData= $_REQUEST;


        $columns = array(
        // datatable column index  => database column name

                0 =>'name',
                1 => 'mobile1',
                2=> 'qualification',
             //   3=> 'video',
             //   4=> 'photoId',
                3=> 'id',
                4=> 'isActive',
                5=> 'payments',
                6=> 'incentives',

        );
         $this->db->from('ht_tutor');

        $totalData = $this->db->count_all_results();

        $totalFiltered = $totalData;

         $sql = "SELECT *  ";
        $sql.=" FROM ht_tutor WHERE 1 = 1";

        // getting records as per search parameters
         if( !empty($requestData['search']['value']) ){  $v = $requestData['search']['value'];
          $sql.=" AND name LIKE '".$v."%' ";
          }
          if( !empty($requestData['search']['value']) ){  //salary
          $sql.=" OR qualification LIKE '".$v."%' ";
          }

        $query = $this->db->query($sql)->result();

        $totalFiltered = count($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

        $sql.=" ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";  // adding length

        $result = $this->db->query($sql)->result();
       // $q = $this->db->last_query(); echo $q; die();
        $data = array();


      // while( $row=mysqli_fetch_array($result) ) {  // preparing an array
	foreach ($result as $r) {
        $nestedData=array();
        $nestedData[] = $r->name;
        $nestedData[] = $r->mobile1;
        $nestedData[] = $r->qualification;
       // $nestedData[] = $r->video;
       // $nestedData[] = $r->photoId;
      //  $nestedData[] = ($r->photoId != "") ? "<a href='" . base_url() . "uploads/" . $r->photoId . "' target='_blank'>File</a>" : "";
         $nestedData[] = "<a  href='" . base_url() . "index.php/getTutorDetailsById/" . $r->id . "' >View </a>";
         $status = $r->isActive;
         $id = $r->id;
         if($status==0)
         {
           $nestedData[] =  "<a   href='" . base_url() . "index.php/changeStatus/1/" . $id. "' style='color: #CC0000' >Approve Now</a>";
         }
         else
         {
            $nestedData[] =   "<a  href='" . base_url() . "index.php/changeStatus/0/" . $id . "' >Disapprove Now</a>";
         }
          $nestedData[] = "<a  href='" . base_url() . "index.php/tutorPaymentsRecord/" . $r->id . "' >View </a>";
           $nestedData[] = "<a  href='" . base_url() . "index.php/tutorIncentive/" . $r->id . "' >View</a>";
	$data[] = $nestedData;
}



$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);

echo json_encode($json_data); //die();  // send data as json format


    }

    public function changeStatus($status,$tutorId)
    {
       //echo $status.$tutorId;
       $obj                        = new admin_model();
       $data                       = array();
       $data['id']                 = $tutorId;
       $data['isActive']           = $status;
       $data['modifiedDate']       = date('Y-m-d H:i:s');
       $obj->changeStatus($data);
       if($this->db->affected_rows())
       {
            $tutorInfo              = $obj->getTutorProfileById($tutorId);
            if($status==1)
            {
                $status1 = "approved";
	        $this->session->set_flashdata('success',"Tutor profile has been $status1");
            }
            else
            {
                $status1 = "disapproved";
		$this->session->set_flashdata('success',"Tutor profile has been $status1");
            }
         /*   $this->email->to($tutorInfo['email']);
            $this->email->from('hometutor@support.com');
            $this->email->subject("HomeTutor:Confirmation Admin");
            $this->email->message("Hi ".$tutorInfo['name'].", Admin has $status1 your account,You can login on Hometutor App");
            if($this->email->send())
            {
                $this->session->set_flashdata('success',"Tutor profile has been $status1, Email sent");
            }
            else
            {
                $this->session->set_flashdata('error',"Tutor profile not $status1, Email not send");
            }     
	 */
       }
       else
       {
           $this->session->set_flashdata('error','Tutor profile not approved');
       }
       redirect('dashboardNew');
    }

    public function getTutorDetailsById($tutorId)
    {



        $tutors = getTutorProfile($tutorId);
        $data             = array();
        if(!empty($tutors))
        {
            $data['tutors']   = $tutors;
        }
        else
        {
            $data['tutors']   = array();
        }
        $this->load->view('viewTutor',$data);
    }

    public function saveRating($tutorId)
    {
        $obj                         = new admin_model();
        $rating                      = $this->input->post('rating');
        $data                        = array();
        $data['rating']              = $rating;
        $data['id']                  = $tutorId;
        $data['modifiedDate']        = date('Y-m-d H:i:s');
        $obj->saveRating($data);
        if($this->db->affected_rows())
        {
            $this->session->set_flashdata('success','Rating saved successfully');
        }
        else
        {
            $this->session->set_flashdata('error','Rating not saved');
        }
        redirect('getTutorDetailsById/'.$tutorId);
    }

     public function parentsRecord()
    { //die('Amit');
       $obj                        = new admin_model();
       $data                       = array();
       $data['parents']             = $obj->getAllParentsProfile();
       $this->load->view('parentsDetails',$data);
    }


    public function getParentDetailsById($pId)
    {
        $obj                        = new admin_model();
        $parent = $obj->getParentProfile($pId);

        $data             = array();
        if(!empty($parent))
        {
            $data['parent']   = $parent;
        }
        else
        {
            $data['parent']   = array();
        }
        $this->load->view('viewParent',$data);
    }

     public function approveAmount($rId)
    { //echo $rId; die();

        $update['id'] = $rId;
        $update['verifiedByAdmin'] = 1;
        $obj            = new admin_model();
        $parent = $obj->approveAmount($update);

        $id = $obj->getParentIdByPaymentRecordId($rId);
        redirect('getParentDetailsById/'.$id);
    }






     public function tutorIncentiveView($id)
    {
       $obj                        = new admin_model();
       $data                       = array();
       //$data['tutors']             = $obj->getAllTutorsProfile();
       $this->load->view('tutorIncentives',$data);
    }

    /********************Cont.****************/

     public function tutorIncentive($id)
    {
        $obj                        = new admin_model();
        $incentive = $obj->tutorIncentiveDetails($id);
        //pre($incentive);
        $data             = array();
        if(!empty($incentive))
        {
            $data['incentive']   = $incentive;
        }
        else
        {
            $data['id']   = $id;
        }
        $this->load->view('tutorIncentives',$data);
    }

     public function addIncentives($id)
    {   //echo $id; die();
       $obj                        = new admin_model();
       $data                       = array();
       //$data['tutors']             = $obj->getAllTutorsProfile();

       $incentive = $obj->tutorIncentiveDetails($id);
        //pre($incentive);
        $data             = array();
        if(!empty($incentive))
        {
            $data['incentive']   = $incentive;
        }
        else
        {
            $data['id']   = $id;
        }
       // $this->load->view('tutorIncentives',$data);

       $this->load->view('addIncentive',$data);
    }




    /************Parent Incentives****************/

     public function parentIncentive($id)
    {
        $obj                        = new admin_model();
        $incentive = $obj->parentIncentiveDetails($id);
        //pre($incentive);
        $data             = array();
        if(!empty($incentive))
        {
            $data['incentive']   = $incentive;
        }
        else
        {
            $data['id']   = $id;
        }
        $this->load->view('parentIncentives',$data);
    }

     public function addParentIncentives($id)
    {   //echo $id; die();
       $obj                        = new admin_model();
       $data                       = array();
       //$data['tutors']             = $obj->getAllTutorsProfile();

       $incentive = $obj->parentIncentiveDetails($id);
        //pre($incentive);
        $data             = array();
        if(!empty($incentive))
        {
            $data['incentive']   = $incentive;
        }
        else
        {
            $data['id']   = $id;
        }
       // $this->load->view('tutorIncentives',$data);

       $this->load->view('addParentIncentive',$data);
    }


      public function addParentIncentive($id)
    {// echo $rId; die();
         $obj                        = new admin_model();
       $data = array(
                'fkParentId' =>$id,
                'incentiveAmount' =>$this->input->post('incentiveAmount'),
                'comment' =>$this->input->post('comment')

       );   //pre($data);

        $in  =  $obj->saveParentIncentive($data);

        redirect('parentIncentive/'.$id);
    }
    /********************************************/

      public function tutorPaymentsRecord($id)
    {
        $obj                        = new admin_model();
        $paymentRecord = $obj->tutorPaymentsRecord($id);
        //pre($incentive);
        $data             = array();
        if(!empty($paymentRecord))
        {
            $data['paymentRecord']   = $paymentRecord;
        }
        else
        {
            $data['id']   = $id;
        }
        $this->load->view('tutorPayments',$data);
    }

       public function addTutorPayment($id)
    {
        $obj                        = new admin_model();
        $paymentRecord = $obj->tutorPaymentsRecord($id);
        //pre($incentive);
        $data             = array();
        if(!empty($paymentRecord))
        {
            $data['paymentRecord']   = $paymentRecord;
        }
        else
        {
            $data['id']   = $id;
        }
        $this->load->view('addPayment',$data);
    }




     public function addTutorIncentive($id) 
    {
         $obj                        = new admin_model();
       $data = array(
                'fkTutorId' =>$id,
                'incentiveAmount' =>$this->input->post('incentiveAmount'),
                'comment' =>$this->input->post('comment')
               
       );  
    
        $in  =  $obj->saveIncentive($data);
       
		//Push Work
        $tutorContactDetails  =  $obj->getTutorDeviceToken($id);
        $tutorDeviceToken     =  $tutorContactDetails['deviceToken']; 
        $tutorPhone           =  $tutorContactDetails['mobile1'];
       
        $msg2 = '{"type":64,"message":{"Amount:"' . $data['incentiveAmount'] . '"}}';

        $Tmsg = '"Congratulations. You have received an incentive of Rs"' . $data['incentiveAmount'] . '" . Please check particulars in Incentive section of app."'; 

        sendAndroidPush($tutorDeviceToken , $msg2, "", "", 64);
        sendPushAsSMS($tutorPhone , $Tmsg);
       
        redirect('tutorIncentive/'.$id);
    }


   public function addPayment($id) 
    {   //echo $id; die();
       $obj                        = new admin_model();
       $data                       = array();
       //$data['tutors']             = $obj->getAllTutorsProfile();
       
        $data = array(
                'fkTutorId'        =>$id,
                'comment'           =>$this->input->post('comment'),
                'amount'          =>$this->input->post('amount')
               
       ); 
    
        $in  =  $obj->savePayment($data);
        
        //Push Work
        $tutorContactDetails       =  $obj->getTutorDeviceToken($id);
        $tutorDeviceToken          =  $tutorContactDetails['deviceToken'];
        $tutorPhone                =  $tutorContactDetails['mobile1'];
        
        $msg2 = '{"type":63,"message":{"Amount:"' . $data['amount'] . '"}}';

        $Tmsg = '"We have initiated a credit of Rs   "' . $data['amount'] . '"to your bank account. It will take 3 days to reflect in your bank statement. Please check particulars in Account section of app."'; 

        sendAndroidPush($tutorDeviceToken , $msg2, "", "", 63);
        sendPushAsSMS($tutorPhone , $Tmsg);
  
        redirect('tutorPaymentsRecord/'.$id);
    }


	   public function schemeView()
    {
        $obj                        = new admin_model();
        $result = $obj->schemeView();
        //pre($incentive);
        $data             = array();
        if(!empty($result))
        {
            $data['scheme']   = $result;
        }
        else
        {
          //  $data['id']   = $id;
        }
        $this->load->view('scheme',$data);
    }

       public function schemeActionPage($id)
    {
        $obj                        = new admin_model();
        $result = $obj->singleSchemeView($id);
        //pre($incentive);
        $data             = array();

            $data['scheme']   = $result;

        $this->load->view('updateScheme',$data);
    }

      public function updateScheme($rId)
    { //echo $rId; die();

        $obj                        = new admin_model();
        $data = array(
                'id' =>$rId,
                'scheme' =>$this->input->post('scheme'),
                'price' =>$this->input->post('price'),
                'isActive' =>$this->input->post('isActive'),
                'modifiedDate' =>$this->input->post('modifiedDate')

        );   //pre($data);

        $in  =  $obj->updateScheme($data);
       redirect('schemeView');

    }



	 //---------------Class Termination Approval Begins-------------/

      public function classesTerminationView()
    {
        $obj                        = new admin_model();
        $result = $obj->classesTerminationView();
        //pre($incentive);
        $data             = array();
        if(!empty($result))
        {
            $data['classesTerminationView']   = $result;
        }
        else
        {
          //  $data['id']   = $id;
        }
        $this->load->view('classTerminationApproval',$data);
    }
    public function notification_counter()
    {
        $obj                        = new admin_model();
        $result = $obj->classesTerminationView1();
        //pre($incentive);
        //$data             = array();
        if(!empty($result)){
            $data  = $result;
        }else{
            $data   = 0;
        }

        echo json_encode(array('status'=> true, 'totalLike' => $data));
        die;
    }

  public function cash_payment_notification(){
      $obj                        = new admin_model();
      $result = $obj->cash_payment_notification();
      //pre($incentive);
      //$data             = array();
      if(!empty($result)){
          $data  = $result;
      }else{
          $data   = 0;
      }

      echo json_encode(array('status'=> true, 'totalLike' => $data));
      
      die;
  }



	public function cash_payment_notification_data(){
      $obj                     = new admin_model();
      $result = $obj->cash_payment_notification_data();
      //pre($incentive);
      //$data             = array();
      if(!empty($result)){
          $data['data']  = $result;
      }else{
          $data['data']   = 0;
      }
      $this->load->view('cash_payment_view',$data);
  }

	public function approve_amount($rId)
	{ //echo $rId; die();

     $update['id'] = $rId;
     $update['verifiedByAdmin'] = 1;
     $obj            = new admin_model();
     $parent = $obj->approveAmount($update);

     $id = $obj->getParentIdByPaymentRecordId($rId);
     redirect('admin_panel/admin/cash_payment_notification_data');
	}


     public function changeClassTerminationStatus($status,$status1,$statusEnrolled,$pId,$sId,$tId)
    {
       //echo $status.$tutorId;
       $obj                        = new admin_model();
       $data                       = array();

       $data['fkParentID']                 = $pId;
       $data['fkStudentID']                 = $sId;
       $data['fkTutorId']                 = $tId;
       $data['class_status']       = $status;
       $data1['status']       = $status1;
       $data1['terminate_enrolled']       = $statusEnrolled;

       $obj->changeTerminationStatus($data,$data1);
       if($this->db->affected_rows())
       {
           // $tutorInfo              = $obj->getTutorProfileById($tutorId);
            if($status==6)
            {
                $status1 = "Approved";
                 $this->session->set_flashdata('success',"Termination has been $status1");
            }
            else
            {
                $status1 = "Disapproved";
                 $this->session->set_flashdata('error',"Termination has been $status1");
            }


       }
       else
       {
           $this->session->set_flashdata('error','Tutor profile not approved');
       }

       redirect('classesTermination');
    }

	/* **********************Gaurav**************************** */
	public function csv_upload(){
		$obj     =  new admin_model();
		$data    =  array();
		$data['data1'] = $obj->get_board_for_csv();
		$this->load->view('csvupload',$data);
	}

	public function get_class_for_csv(){
		$board_id = $this->input->post('board_id');
		$obj = new admin_model();
		$data = array();
		$data = $obj->get_class_for_csv($board_id);
		echo '<option value="">Select Class</option>';
		foreach($data as $qu2){
		echo "<option value='".$qu2['id']."_".$qu2['classId']."'>".$qu2['className']."</option>";
		}
	}

	public function get_subject_for_csv(){
		$class_id = $this->input->post('class_id');
		$class_array = explode('_',$class_id);
		$obj = new admin_model();
		$data = array();
		$data = $obj->get_subject_for_csv($class_array[1]);
		echo '<option value="">Select Subject</option>';
		foreach($data as $qu2){
		echo "<option value='".$qu2['id']."'>".$qu2['name']."</option>";
		}
	}

	public function get_topic_for_csv(){
		$subject_id = $this->input->post('subject_id');
		$class_id = $this->input->post('class_id');
		$obj = new admin_model();
		$data = array();
		$data = $obj->get_topic_for_csv($subject_id,$class_id);
		echo "<table><tr><td>Id&nbsp;</td><td>Name</td></tr>";
		foreach($data as $qu2){
		echo '<tr><td>'.$qu2['id'].'&nbsp;&nbsp;</td><td>'.$qu2['topic_name'].'</td></tr>';
		}
		echo "</table>";
	}

	public function upload_csv(){
		if($this->input->post('submit')){
			$board_id = $this->input->post('board_id');
			$class_id = $this->input->post('class_id');
			$subject_id = $this->input->post('subject_id');
			$error_Data = array();
			$error_Data1 = array();
			$error_Data2 = array();
			if($board_id !='' and $class_id !='' and $subject_id !=''){
				if ($_FILES['csv']['size'] > 0) {
					//get the csv file
					$file = $_FILES['csv']['tmp_name'];
					$handle = fopen($file,"r");
					//loop through the csv file and insert into database
					do {
						if(!empty($data)){
							if($data[10]>0 and $data[10]<5 and sizeof($data)==18){
								$topic_exist = array('id' =>$data[3], 'class_id' => $class_id, 'subject_id'=> $subject_id);
								$this->db->where($topic_exist);
								$query = $this->db->get('ht_topics');
								if($query->num_rows() == 1){
								   $obj = new admin_model();
								   $q =  $obj->csv_data_insert($board_id,$class_id,$subject_id,$data);
								   if($q){

									}else{
										$obj = new admin_model();
										$update_data =  $obj->csv_data_update($board_id,$class_id,$subject_id,$data);
										if($update_data){

										}else{
											$error_data2[] = $data[2];
										}
								   }
								}else{
									$error_data1[] = $data[2];
								}
							} else{
							$error_data[] = $data[2];
							}
						 }
					} while ($data = fgetcsv($handle,1000,","));
					if(!empty($error_data) ||!empty($error_data1) ||!empty($error_data2)){
						if(!empty($error_data)){
							$error_data_string = implode(" , ",$error_data);
							$this->session->set_flashdata('success', "Please check the question - $error_data_string");
						}
						if(!empty($error_data1)){
							$error_data_string1 = implode(" , ",$error_data1);
							$this->session->set_flashdata('success1', "Topic not exist this subject - $error_data_string1");
						}
						if(!empty($error_data2)){
							$error_data_string2 = implode(" , ",$error_data2);
							$this->session->set_flashdata('success2', "This Question not update please check the question - $error_data_string2");
						}
						redirect('csv_upload');
					}else{
						$this->session->set_flashdata('success', "csv successfully upload");
						redirect('csv_upload');
					}
				}else{
					$this->session->set_flashdata('success', "Please Check the CSV");
					redirect('csv_upload');
				}
			}else{
					$this->session->set_flashdata('success', "Please Select");
					redirect('csv_upload');
			}
		}
   }






}
