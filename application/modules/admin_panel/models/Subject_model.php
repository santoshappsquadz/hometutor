<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Subject_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();   
    }
	
	function get_board(){
			$this->db->select('*');
			$result = $this->db->get('board')->result_array();
			return $result;
		}
		function get_class_for_subject($data){
			$this->db->select('*');
			$this->db->where('fkBoardId',$data);
			$result = $this->db->get('ht_class')->result_array();
			return $result;
		}
		function get_class_for_subject_topic($data){
			$this->db->select('*');
			$this->db->where('fkBoardId',$data);
			$result = $this->db->get('ht_parentClass')->result_array();
			return $result;
		}
		
		function get_subject($data){
			$this->db->select('*');
			$this->db->where('fkClassId',$data);
			$result = $this->db->get('subject')->result_array();
			return $result;
		}
		function upload_subject($class_id,$subject){
			$data_array = array('fkClassId' => $class_id, 'name'=> $subject);
			$result = $this->db->insert('ht_subject',$data_array);
			return $result;
		}
		
		function upload_topic($class_id,$subject_id,$topic){
			$data_array = array('class_id'=>$class_id, 'subject_id' => $subject_id, 'topic_name'=> $topic);
			$result = $this->db->insert('ht_topics',$data_array);
			return $result;
		}
		function all_topic(){
			$this->db->select('*');
			$result = $this->db->get('ht_topics')->result_array();
			return $result;
		}
		function edit_topic_in_db($data){
			$array_data = array('topic_name' => $data['topic_name']);
			$this->db->where('id',$data['id']);
			$result = $this->db->update('ht_topics',$array_data);
			if($result){
				return true;
			}else{
				return 	false;
			}
		}
		
		function all_subject(){
			$this->db->select('*');
			$result = $this->db->get('ht_subject')->result_array();
			return $result;
		}
		function edit_subject_in_db($data){
			$array_data = array('name' => $data['subject_name']);
			$this->db->where('id',$data['id']);
			$result = $this->db->update('ht_subject',$array_data);
			if($result){
				return true;
			}else{
				return 	false;
			}
		}
}
?>