
<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Login_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function checkAdminExist($email,$password)
    {
        $this->db->where('email',$email);
        $this->db->where('password',$password);
        $result = $this->db->get('admin')->row_array();
        if(!empty($result))
        {
            $this->session->set_userdata('adminInfo',$result);
            return $result;
        }
        else
        {
            return false;
        }
    }
  
   
}
