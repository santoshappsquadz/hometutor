<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Tutor_offer_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();   
    }
	function tutor_name_in_db(){
		$this->db->select('id,name');
		$result = $this->db->get('tutor')->result_array();
		return $result;
	}
	function tutor_offer_insert_db($data){
		$result = $this->db->insert('ht_tutor_offers',$data);
		return $result;
	}
	function all_tutor_offer_in_db(){
		$result = $this->db->get('ht_tutor_offers')->result_array();
		return $result;
	}
	function get_tutor_name_in_db($id){
		$this->db->select('name');
		$this->db->where('id',$id);
		$result = $this->db->get('ht_tutor')->row_array();
		return $result;
	}
	function one_offer_in_db($id){
		$this->db->select('*');
		$this->db->where('id',$id);
		$result = $this->db->get('ht_tutor_offers')->row_array();
		return $result;
	}
	function tutor_offer_delete_in_db($id){
		$this->db->where('id',$id);
		$result = $this->db->delete('ht_tutor_offers');
		return $result;
	}
	function tutor_offer_update_db($data){
		$offer_data = array('start_date' => $data['start_date'],
							  'end_date' => $data['end_date'],
							  'offer' => $data['offer'],
							  'tutor_id' => $data['tutor_id']);
		$this->db->where('id',$data['id']);
		$result = $this->db->update('ht_tutor_offers',$offer_data);
		return $result;
	}
	function image_update_for_how_it_work($data,$status){
		$data = base_url('assets/image_for_how_it_work').'/'.$data;
		$image_data = array('image_path' => $data,'status' => $status);
		$this->db->where($image_data);
		$image_exist = $this->db->get('ht_how_it_work')->num_rows();
		if($image_exist == 0){
			$image_data = array('image_path' => $data,'status' => $status);
			$result = $this->db->insert('ht_how_it_work',$image_data);
			return true;
		}else{
			return false;
		}
	}
	function get_data_ht_how_it_work(){
			$this->db->select('*');
			$result = $this->db->get('ht_how_it_work')->result_array();
			return $result;
	}
	
	function get_data_ht_ht_why_choose(){
			$this->db->select('*');
			$result = $this->db->get('ht_why_choose')->result_array();
			return $result;
	}
	
	function image_update_for_why_chooses($data,$status){
		$data = base_url('assets/image_for_why_chooses').'/'.$data;
		$image_data = array('image_path' => $data,'status' => $status);
		$this->db->where($image_data);
		$image_exist = $this->db->get('ht_why_choose')->num_rows();
		if($image_exist == 0){
			$image_data = array('image_path' => $data,'status' => $status);
			$result = $this->db->insert('ht_why_choose',$image_data);
			return true;
		}else{
			return false;
		}
	}
}
?>	