
<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Admin_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function checkAdminExist($email,$password)
    {
        $this->db->where('email',$email);
        $this->db->where('password',$password);
        //$this->db->where('status',1);
        $result = $this->db->get('admin')->row_array();
        if(!empty($result))
        {
            $this->session->set_userdata('adminInfo',$result);
            return $result;
        }
        else
        {
            return false;
        }
    }
    function commonFxn($result)
    {
        if(!empty($result))
        {
            return $result;
        }
        else
        {
            return array();
        }
    }
     function getAllTutorsCount()
    {
        $results = $this->db->get('tutor')->result_array();
        $totalData = $this->db->count_all_results();

         return $totalData;
    }
    function getAllTutorsProfile()
    {
        $results = $this->db->get('tutor')->result_array();
        if(!empty($results))
        {
            $data                               = array();
            foreach($results as $detail)
            {

                unset($detail['password']);
                $data[$detail['id']]                         = $detail;
                $data[$detail['id']]['coachingDetail']       = $this->getCoachingDetail($detail['id']);
                $data[$detail['id']]['availablity']          = $this->getTutorAvailablity($detail['id']);
                $data[$detail['id']]['coachingLocation']     = $this->getCoachingLocation($detail['id']);
            }

            return $data;
        }
        else
        {
            return false;
        }
    }
    function getTutorAvailablity($tutorId)
    {
        $this->db->select("id,typeOfTution,premise,days,startTime,endTime");
        $this->db->where('fkTutorId',$tutorId);
        $this->db->where('isActive',1);
        $result = $this->db->get('tutorAvailablity')->result_array();
        return $this->commonFxn($result);

    }
    function getCoachingDetail($tutorId)
    {
        $this->db->select("tutorCoachingDetail.id,board.name as boardName,class.Name as className,tutorCoachingDetail.fkSubjectId,tutorCoachingDetail.feesPerHour,tutorCoachingDetail.feesPerMonth");
        $this->db->where('tutorCoachingDetail.fkTutorId',$tutorId);
        $this->db->where('tutorCoachingDetail.isActive',1);
        $this->db->join('board','tutorCoachingDetail.fkBoardId=board.id');
        $this->db->join('class','tutorCoachingDetail.fkClassId=class.id');
        $result = $this->db->get('tutorCoachingDetail')->result_array();
        if(!empty($result))
        {
            $sendArray  = array();
            foreach($result as $data)
            {
                $fkSubjectId = $data['fkSubjectId'];
                unset($data['fkSubjectId']);
                $subjects    = explode(',',$fkSubjectId);
                $sub         = array();
                foreach($subjects as $subject)
                {
                    $sub[] = $this->getSubjectName($subject);
                }
                $sub = implode(',',$sub);
                $sendArray                 = $data;
                $sendArray['subjectName']  = $sub;
            }
            return $this->commonFxn($sendArray);
        }
        else
        {
            return false;
        }

    }
    function getSubjectName($id)
    {
        $this->db->where('id',$id);
        $result = $this->db->get('subject')->row_array();
        return $result['name'];
    }
    function getCoachingLocation($tutorId)
    {
        $this->db->select("id,location,latitude,longitude,radius");
        $this->db->where('fkTutorId',$tutorId);
        $this->db->where('isActive',1);
        $result = $this->db->get('tutorCoachingLocation')->result_array();
        return $this->commonFxn($result);
    }
    function changeStatus($data)
    {
        if($data['isActive'] == 1){
          $this->db->where('id',$data['id']);
          $tutor_data = $this->db->get('tutor')->row_array();
          $pushData = '{"type":21,"message":{"Name":"' . $tutor_data['name'] . '""}}';
          sendAndroidPush($tutor_data['deviceToken'], $pushData, "", "", 21);

          $smsData = '"Congratulations. Your profile is approved and will be visible in search results."';
          $ressmsData = sendPushAsSMS($tutor_data['mobile1'], $smsData);
        }
        $this->db->where('id',$data['id']);
        $this->db->update('tutor',$data);

    }
    function getTutorProfileById($id)
    {

        $this->db->where('id',$id);
        return $this->db->get('tutor')->row_array();
    }
    function saveRating($data)
    {
        $this->db->where('id',$data['id']);
        $this->db->update('tutor',$data);
    }
    function approveAmount($data)
    {
        $this->db->where('id',$data['id']);
        $this->db->update('ht_parentPaymentRecord',$data);
    }
     function getParentIdByPaymentRecordId($rId)
    {
        $this->db->where('id',$rId);
        $result = $this->db->get('ht_parentPaymentRecord')->row_array();
        return $result['fkParentId'];
    }
     function getTutorDeviceToken($id)       
    {
        $this->db->where('id', $id);      
        $resultTutor = $this->db->get('ht_tutor')->row_array();
        
        
        if($resultTutor)
        {          
           return $resultTutor ;
        }
        else
        {
           return false; 
        }
  }
    

 /*   function getClassesAmountDetails($id)
    {

       // $this->db->distinct();
        $this->db->select('ht_tutorPaymentTotal.*,ht_tutor.profileImage,ht_tutor.gender,ht_tutor.dob,ht_tutor.name as tutorName,ht_parent_student.name as studentName' );
        $this->db->join('ht_tutor','ht_tutor.id=ht_tutorPaymentTotal.fkTutorId');
        // $this->db->join('ht_tutorCoachingDetail','ht_tutorCoachingDetail.fkTutorId=ht_tutorPaymentTotal.fkTutorId','right');
        $this->db->join('ht_parent_student','ht_parent_student.id=ht_tutorPaymentTotal.fkStudentId');
        $this->db->where('ht_tutorPaymentTotal.fkParentId', $id);
        $result = $this->db->get('ht_tutorPaymentTotal')->result_array();
        $count =count( $result);
        //pre($result);die();

       // for($i=0;$i<=$count;$i++)
        foreach($result as $res)
        {
            $tutorId = $res['fkTutorId'];
            $subjectId = $res['fkSubjectId'];
            $this->db->select('ht_tutorCoachingDetail.feesPerHour,ht_tutorCoachingDetail.feesPerMonth' );
            $this->db->where('fkTutorId', $tutorId);
            $this->db->where('fkSubjectId', $subjectId);
            $result1 = $this->db->get('ht_tutorCoachingDetail')->row_array();
            $res['feePerHour'] = $result1['feesPerHour'];
            $res['feesPerMonth'] = $result1['feesPerMonth'];
            $resultFinal[] = $res;
        }

        if($result)
        {
           return $resultFinal ;
        }
        else
        {
           return false;
        }
}                         */



   function getAccountDetails($id)
    {


        $this->db->select('ht_parentPaymentRecord.id,ht_tutor.name as tutorName,ht_parentPaymentRecord.date,ht_parentPaymentRecord.verifiedByAdmin,ht_parentPaymentRecord.particulars,ht_parentPaymentRecord.fkSubjectId,ht_parentPaymentRecord.fkStudentId,ht_parentPaymentRecord.fkTutorId,ht_parentPaymentRecord.amount,ht_parent_student.name as studentName,ht_parentPaymentRecord.paymentStatus' );
       // $this->db->select('ht_parentPaymentRecord.fkTutorId,ht_tutor.name as tutorName,ht_parentPaymentRecord.fkParentId' );
        $this->db->join('ht_tutor','ht_tutor.id=ht_parentPaymentRecord.fkTutorId');
        $this->db->join('ht_parent_student','ht_parent_student.id=ht_parentPaymentRecord.fkStudentId');
        $this->db->join('ht_subject','ht_subject.id=ht_parentPaymentRecord.fkSubjectId');
        $this->db->where('ht_parentPaymentRecord.fkParentId', $id);
       // $this->db->where('ht_parentPaymentRecord.fkTutorId', $document['tutorId']);
        $result = $this->db->get('ht_parentPaymentRecord')->result_array();

        if($result)
        {
           return $result ;
        }
        else
        {
           return false;
        }
}

  function getAllParentsProfile()
    {
        $results = $this->db->get('parent')->result_array();
        if(!empty($results))
        {
            $data                               = array();
            foreach($results as $detail)
            {
                unset($detail['password']);
                $data[$detail['id']]                         = $detail;
                $data[$detail['id']]['Due Payments :']  = $this->getClassesAmountDetails($detail['id']);
                $data[$detail['id']]['Paid Payments :']    = $this->getAccountDetails($detail['id']);
            }
            return $data;
        }
        else
        {
            return false;
        }
    }

   function getParentProfile($pId)
    {
        $this->db->where('id',$pId);
        $results = $this->db->get('parent')->row_array();
        //pre($pId);
        //pre($results);
        if(!empty($results))
        {
            unset($results['id']);
            unset($results['password']);
            unset($results['mobile1']);
            unset($results['address2']);
            unset($results['pin']);
            unset($results['latitude']);
            unset($results['longitude']);
            unset($results['referalCode']);
            unset($results['socialType']);
            unset($results['fbId']);
            unset($results['googleId']);
            unset($results['isProfileComplete']);
            unset($results['deviceToken']);
            unset($results['deviceType']);
            unset($results['deviceType']);
            unset($results['isDeleted']);
            unset($results['modifiedDate']);
            unset($results['createdDate']);
            unset($results['isActive']);


            $results['Due Payments']  = $this->getClassesAmountDetails($pId);
            $results['Paid Payments']    = $this->getAccountDetails($pId);
            return $results;
        }
        else
        {
            return false;
        }
    }






           function getClassesAmountDetails($id)
    {
          $last_day_this_month  = date('Y-m-t');

        $this->db->select('ht_ParentDuePayment.*,ht_tutor.profileImage,ht_tutor.gender,ht_tutor.dob,ht_tutor.name as tutorName,ht_parent_student.name as studentName' );
        $this->db->join('ht_tutor','ht_tutor.id=ht_ParentDuePayment.fkTutorId');
        // $this->db->join('ht_tutorCoachingDetail','ht_tutorCoachingDetail.fkTutorId=ht_tutorPaymentTotal.fkTutorId','right');
        $this->db->join('ht_parent_student','ht_parent_student.id=ht_ParentDuePayment.fkStudentId');
        $this->db->where('ht_ParentDuePayment.fkParentId', $id);
        $result = $this->db->get('ht_ParentDuePayment')->result_array();
	//pre($result); die();
	$count =count( $result);

        foreach($result as $res)
        {

	    $this->db->where('id', $res['fkSubjectId']);
	    $resultSub = $this->db->get('ht_subject')->row_array();


            $res['age'] =  date('Y')-$res['dob'];
	    $monthEnrolledDays = $res['Number_class']; //echo $monthEnrolledDays; die();
            $tutorId = $res['fkTutorId'];
            $subjectId = $res['fkSubjectId'];
            $studentId = $res['fkStudentId'];
            $parentId = $res['fkParentId'];
            $dueDate = $res['dueDate'];
           // $this->db->select('ht_tutorCoachingDetail.fees' );
           // $this->db->join('ht_subject','ht_subject.id=ht_tutorCoachingDetail.fkSubjectId');
            $this->db->where('fkTutorId', $tutorId);
	     $this->db->where('fkParentId', $parentId);
	     $this->db->where('fkRequestId',  $studentId);

            $result1 = $this->db->get('ht_offers')->row_array();
	  // $q = $this->db->last_query(); echo $q; die();
	   // $q = $this->db->last_query(); echo $q; die();
           // pre( $result1); die();
           //////////////////per hour days calculation//////////////////////
            $this->db->where('fkTutorId', $tutorId);
            $this->db->where('fkStudentID', $studentId);
            $this->db->where('fkParentID',  $parentId);
             $this->db->where('subject_id',  $subjectId);
             $this->db->where('class_date >=', $dueDate);
            $resultPerHourDays = $this->db->get('ht_class_schedule')->result_array();
           // $q =$this->db->last_query(); echo $q; die();
            $countPerHourDays = count( $resultPerHourDays);
          //  $countPerHourDays = 7;
            ////////////////////////////////////////////////////////////////
            $monthDays = date('t');
            $date1 = date_create($res['dueDate']);
            $date2 =  date_create($last_day_this_month); //pre($date2); die();
            $diff=date_diff($date1,$date2); //echo  $diff->format('%a');
            $fees = $diff->format('%a');   //echo  $fees;die();
            $enrolledDays = $fees+1; // echo $enrolledDays; die();
             // pre($fees);die();


            $feeMonth = $result1['fees']; //echo $monthDays; die();
            if($res['paymentMode']== 1)
            {
	     if($res['Number_class'] > 0)
	     {$dueAmount = $result1['fees']/$monthEnrolledDays;
             $dueAmountMonthly = round($dueAmount*$countPerHourDays);
             $res['dueAmount'] = $dueAmountMonthly;}
	     else{$res['dueAmount'] = 0;}

           }
            elseif($res['paymentMode']== 0)
            {
             $dueAmount = round($result1['fees']*$countPerHourDays);
             $res['dueAmount'] = $dueAmount;
            }
             $res['subjectName'] = $resultSub['name'];
             $res['fees']        = $result1['fees'];
           //  $res['feesPerHour'] = $result1['fees'];

            $resultFinal[] = $res;
        }

        if($result)
        {
           return $resultFinal ;
        }
        else
        {
           return false;
        }
}




     function saveIncentive($data)
    {
         $this->db->order_by("id","DESC");
        $this->db->where('fkTutorId',$data['fkTutorId']);
        $result = $this->db->get('tutorIncentiveRecord')->row_array();

        $TotalAmount =  $result['total'] + $data['incentiveAmount'];
        $data['date'] = date('Y-m-d');
        $data['total'] = $TotalAmount;
        $this->db->insert('tutorIncentiveRecord', $data);

    }

      function savePayment($data)
    {
        $data['date'] = date('Y-m-d');
        $this->db->insert('adminPaidPayments', $data);

    }

      function tutorIncentiveDetails($id)
    { //echo $id;
        $this->db->where('fkTutorId',$id);
        $result = $this->db->get('ht_tutorIncentiveRecord')->result_array();

        return $result;//pre($result);
    }

     function parentIncentiveDetails($id)
    {// echo $id; die();
        $this->db->where('fkParentId',$id);
        $result = $this->db->get('ht_parentIncentiveRecieved')->result_array();

        return $result;//pre($result);
    }

	    function saveParentIncentive($data)
    {
         $this->db->order_by("id","DESC");
        $this->db->where('fkParentId',$data['fkParentId']);
        $result = $this->db->get('ht_parentIncentiveRecieved')->row_array();

        $TotalAmount =  $result['total'] + $data['incentiveAmount'];
        $data['date'] = date('Y-m-d');
        $data['total'] = $TotalAmount;
        $this->db->insert('ht_parentIncentiveRecieved', $data);

    }

       function tutorPaymentsRecord($id)
    { //echo $id;
        $this->db->where('fkTutorId',$id);
        $result = $this->db->get('ht_adminPaidPayments')->result_array();

        return $result;//pre($result);
    }

       function tutorPaymentDetails($id)
    { //echo $id;
        $this->db->where('fkTutorId',$id);
        $result = $this->db->get('ht_adminPaidPayments')->result_array();

        return $result;//pre($result);
    }


	    function schemeView()
    {

        $result = $this->db->get('ht_scheme')->result_array();

        return $result;//pre($result);
    }

        function singleSchemeView($id)
    {
        $this->db->where('id',$id);
        $result = $this->db->get('ht_scheme')->row_array();

        return $result;//pre($result);
    }

     function updateScheme($data)
    {
        $this->db->where('id',$data['id']);
        $this->db->update('ht_scheme',$data);
    }


     function classesTerminationView()
    {
        $terminationStatus = 4;
        $this->db->distinct();
        $this->db->select("ht_class_schedule.fkParentID,ht_class_schedule.fkStudentID,ht_class_schedule.fkTutorId,ht_class_schedule.class_status,ht_class_schedule.statusData1,ht_tutor.name as tutorName,ht_parent.name as parentName,ht_parent_student.name as studentName");
        $this->db->join('ht_tutor','ht_tutor.id=ht_class_schedule.fkTutorId');
        $this->db->join('ht_parent','ht_parent.id=ht_class_schedule.fkParentID');
        $this->db->join('ht_parent_student','ht_parent_student.id=ht_class_schedule.fkStudentID');
        $this->db->where('class_status',$terminationStatus);
        $result = $this->db->get('ht_class_schedule')->result_array();
       // $q = $this->db->last_query(); echo $q; die();
      //  pre($result);
        return $result;//pre($result);
    }
    function classesTerminationView1()
    {
       $terminationStatus = 4;
       $this->db->distinct();
       $this->db->select("ht_class_schedule.fkParentID,ht_class_schedule.fkStudentID,ht_class_schedule.fkTutorId,ht_class_schedule.class_status,ht_class_schedule.statusData1,ht_tutor.name as tutorName,ht_parent.name as parentName,ht_parent_student.name as studentName");
       $this->db->join('ht_tutor','ht_tutor.id=ht_class_schedule.fkTutorId');
       $this->db->join('ht_parent','ht_parent.id=ht_class_schedule.fkParentID');
       $this->db->join('ht_parent_student','ht_parent_student.id=ht_class_schedule.fkStudentID');
       $this->db->where('class_status',$terminationStatus);
       $result = $this->db->get('ht_class_schedule')->num_rows();
      // $q = $this->db->last_query(); echo $q; die();
     //  pre($result);
       return $result;//pre($result);
    }

    function cash_payment_notification(){
        $result =  $this->db->query("SELECT * FROM ht_parentPaymentRecord WHERE paymentStatus = 0 and verifiedByAdmin = 0")->num_rows();
        return $result;
    }

	
	  function cash_payment_notification_data(){
      $this->db->select('ht_parentPaymentRecord.id,ht_tutor.name as tutorName,ht_parentPaymentRecord.date,ht_parentPaymentRecord.verifiedByAdmin,ht_parentPaymentRecord.particulars,ht_parentPaymentRecord.fkSubjectId,ht_parentPaymentRecord.fkStudentId,ht_parentPaymentRecord.fkTutorId,ht_parentPaymentRecord.amount,ht_parent_student.name as studentName,ht_parentPaymentRecord.paymentStatus' );
     // $this->db->select('ht_parentPaymentRecord.fkTutorId,ht_tutor.name as tutorName,ht_parentPaymentRecord.fkParentId' );
      $this->db->join('ht_tutor','ht_tutor.id=ht_parentPaymentRecord.fkTutorId');
      $this->db->join('ht_parent_student','ht_parent_student.id=ht_parentPaymentRecord.fkStudentId');
      $this->db->join('ht_subject','ht_subject.id=ht_parentPaymentRecord.fkSubjectId');
      $this->db->where('ht_parentPaymentRecord.verifiedByAdmin', 0);
      $this->db->where('ht_parentPaymentRecord.paymentStatus', 0);
     // $this->db->where('ht_parentPaymentRecord.fkTutorId', $document['tutorId']);
      $result = $this->db->get('ht_parentPaymentRecord')->result_array();
        if(!empty($result)){
          return $result;
        }else{
          return false;
        }
    }

	
     function changeTerminationStatus($data,$data1)
    {
        $this->db->where('fkParentID',$data['fkParentID']);
        $this->db->where('fkStudentID',$data['fkStudentID']);
        $this->db->where('fkTutorId',$data['fkTutorId']);
        $this->db->update('ht_class_schedule',$data);

        $this->db->where('fkParentID',$data['fkParentID']);
        $this->db->where('fkParentRequestId',$data['fkStudentID']);
        $this->db->where('fkTutorId',$data['fkTutorId']);
        $this->db->update('ht_parentRequestTutor',$data1);
    }


	/* *********************Gaurav*************************** */

	function get_board_for_csv(){
			$this->db->select('*');
			$result = $this->db->get('board')->result_array();
			return $result;
		}
		function get_class_for_csv($data){
			$this->db->select('*');
			$this->db->where('fkBoardId',$data);
			$result = $this->db->get('ht_parentClass')->result_array();
			return $result;
		}

		function get_subject_for_csv($data){
			$this->db->select('*');
			$this->db->where('fkClassId',$data);
			$result = $this->db->get('subject')->result_array();
			return $result;
		}

		function get_topic_for_csv($data,$class_id){
			$array = array('class_id'=>$class_id,'subject_id'=>$data);
			$this->db->select('*');
			$this->db->where($array);
			$result = $this->db->get('ht_topics')->result_array();
			return $result;
		}

		function csv_data_insert($board_id,$class_id,$subject_id,$data){
				$this->db->where('question_id',$data[2]);
				$check_question = $this->db->get('ht_question_master');
				if($check_question->num_rows() > 0){
					return false;
				}else{
					if(!empty($data[13])){
						$data[13] = base_url('assets/question_img/'.$data[13]);
					}
					$insert_data = array('board_id' => $board_id,
			                     'class_id' => $class_id,
								 'subject_id' => $subject_id,
								 'chapter_no' => $data[0],
					           'chapter_name' => $data[1],
					            'question_id' => $data[2],
								'topic_id' => $data[3],
								'focus_area' => $data[4],
								'question' => $data[5],
								'answer_one' =>$data[6],
								'answer_two' =>$data[7],
								'answer_three' =>$data[8],
								'answer_four' =>$data[9],
								'valid_answer' =>$data[10],
								'solution' =>$data[11],
								'type' =>$data[12],
								'jpeg_file' =>base_url('/assets/question_img').'/'.$data[13],
								'level' =>$data[14],
								'set_key' =>$data[15],
								'status' =>$data[16],
								'source' =>$data[17]
								);
					$q = $this->db->insert('ht_question_master',$insert_data);
					return true;
				}

		}

		function csv_data_update($board_id,$class_id,$subject_id,$data){
			$update_data = array('board_id' => $board_id,'class_id' => $class_id,'subject_id' => $subject_id,'chapter_no' => $data[0],
					'chapter_name' => $data[1],'topic_id' => $data[3],'focus_area' => $data[4],
					'question' => $data[5],'answer_one' =>$data[6],'answer_two' =>$data[7],'answer_three' =>$data[8],
					'answer_four' =>$data[9],'valid_answer' =>$data[10],'solution' =>$data[11],'type' =>$data[12],
					'jpeg_file' =>$data[13],'level' =>$data[14],'set_key' =>$data[15],'status' =>$data[16],
					'source' =>$data[17]);
			$this->db->where('question_id',$data[2]);
			$result = $this->db->update('ht_question_master',$update_data);
			return $result;
		}


}
