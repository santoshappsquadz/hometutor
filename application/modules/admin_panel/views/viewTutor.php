<?php $this->load->view('header'); ?>
<body>
<div id="wrapper">
     <!-- Navigation -->
        <nav class="top1 navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                <a class="navbar-brand" href="index.html">
                    Welcome <?php echo $this->session->userdata('adminInfo')['email']; ?>
                </a>
            </div>
            <!-- /.navbar-header -->
            <ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
	        		<!--a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-comments-o"></i><span class="badge">4</span></a>
	        		<ul class="dropdown-menu">
						<li class="dropdown-menu-header">
							<strong>Messages</strong>
							<div class="progress thin">
							  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
							    <span class="sr-only">40% Complete (success)</span>
							  </div>
							</div>
						</li>
						<li class="avatar">
							<a href="#">
								<img src="images/1.png" alt=""/>
								<div>New message</div>
								<small>1 minute ago</small>
								<span class="label label-info">NEW</span>
							</a>
						</li>
						<li class="avatar">
							<a href="#">
								<img src="images/2.png" alt=""/>
								<div>New message</div>
								<small>1 minute ago</small>
								<span class="label label-info">NEW</span>
							</a>
						</li>
						<li class="avatar">
							<a href="#">
								<img src="images/3.png" alt=""/>
								<div>New message</div>
								<small>1 minute ago</small>
							</a>
						</li>
						<li class="avatar">
							<a href="#">
								<img src="images/4.png" alt=""/>
								<div>New message</div>
								<small>1 minute ago</small>
							</a>
						</li>
						<li class="avatar">
							<a href="#">
								<img src="images/5.png" alt=""/>
								<div>New message</div>
								<small>1 minute ago</small>
							</a>
						</li>
						<li class="avatar">
							<a href="#">
								<img src="images/pic1.png" alt=""/>
								<div>New message</div>
								<small>1 minute ago</small>
							</a>
						</li>
						<li class="dropdown-menu-footer text-center">
							<a href="#">View all messages</a>
						</li>	
	        		</ul-->
	      		</li>
			    <li class="dropdown">
                               
	        		<!--a href="#" class="dropdown-toggle avatar" data-toggle="dropdown"><img src="<?php echo base_url("assets/images/1.png"); ?>" alt=""/><span class="badge">9</span></a>
	        		<ul class="dropdown-menu">
						<li class="dropdown-menu-header text-center">
							<strong>Account</strong>
						</li>
						<li class="m_2"><a href="#"><i class="fa fa-bell-o"></i> Updates <span class="label label-info">42</span></a></li>
						<li class="m_2"><a href="#"><i class="fa fa-envelope-o"></i> Messages <span class="label label-success">42</span></a></li>
						<li class="m_2"><a href="#"><i class="fa fa-tasks"></i> Tasks <span class="label label-danger">42</span></a></li>
						<li><a href="#"><i class="fa fa-comments"></i> Comments <span class="label label-warning">42</span></a></li>
						<li class="dropdown-menu-header text-center">
							<strong>Settings</strong>
						</li>
						<li class="m_2"><a href="#"><i class="fa fa-user"></i> Profile</a></li>
						<li class="m_2"><a href="#"><i class="fa fa-wrench"></i> Settings</a></li>
						<li class="m_2"><a href="#"><i class="fa fa-usd"></i> Payments <span class="label label-default">42</span></a></li>
						<li class="m_2"><a href="#"><i class="fa fa-file"></i> Projects <span class="label label-primary">42</span></a></li>
						<li class="divider"></li>
						<li class="m_2"><a href="#"><i class="fa fa-shield"></i> Lock Profile</a></li>
						<li class="m_2"><a href="#"><i class="fa fa-lock"></i> Logout</a></li>	
	        		</ul-->
	      		</li>
			</ul>
			<!--form class="navbar-form navbar-right">
              <input type="text" class="form-control" value="Search..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search...';}">
            </form-->
            <div class="navbar-default sidebar" role="navigation">
                <?php $this->load->view('sidebar'); ?> 
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <div id="page-wrapper">
        <div class="col-md-12 graphs">
	   <div class="xs">
  	 <h3>Tutors Details</h3>
  	<div class="bs-example4" data-example-id="contextual-table">
    <table class="table">
       <?php
       if($this->session->flashdata('success'))
       {
           echo "<span class='alert alert-success'>".$this->session->flashdata('success')."</span>";
       }
       elseif($this->session->flashdata('error'))
       {
           echo "<span class='alert alert-danger'>".$this->session->flashdata('error')."</span>";
       }
       ?>
      <thead>
        <tr>
          <th>Feild Name</th>
          <th>Data</th>         
        </tr>
      </thead>
      <tbody>
          
        <?php if(!empty($tutors)){ $count=0;?> 
           <?php foreach($tutors as $key=>$detail): ?>
          <?php
          $total =(1+$count++);
          if($total%2==0)
          {
              $class='info';
          }
          else
          {
             $class='warning'; 
          }
          ?>
            <tr class="<?php echo $class;?>">
                <td><?php echo "<h5>".ucfirst($key)."</h5>" ?></td>  
                <td>
                 <?php if(!is_array($detail)){?>   
                    <?php  echo $detail?> 
                  
                <?php if($key=='photoId'||$key=='profileImage'){?>
                    <a href="<?php echo $detail; ?>"><img class="img-responsive" src="<?php  echo $detail;?>" width="100" height="100"/></a>
                    <?php }
                    else{ ?>
       
                    <?php } ?>
                    <?php if($key=='video'){ ?>
                    <video width="200" controls>                  
                      <source src="<?php echo $detail;?>" type="video/mp4">
                      <source src="<?php echo $detail;?>" type="video/ogg">
                      Your browser does not support HTML5 video.
                    </video>
                    <?php } ?>
                    <?php 
                    if($key=='qualificationCertificate')
                    { 
                        $certificates = explode(',',$detail);
                        foreach($certificates as $image)
                        {
                            if($image)
                            {
                            ?>   
                                <a href="<?php echo $image;?>"><img class="img-responsive" src="<?php echo $image;?>" width="70" height="70"/></a>
                            <?php 
                            }
                        }
                    }else{ ?>
                    
                 <?php }}  /* else{ ?>
                <?php  
                    if($key="coachingDetail")
                    {
                       if(!empty($detail))
                       {  //pre($detail);
                           ?>
                            <table class="table">

                                <thead>
                                  <tr>
                                    <th>Feild Name</th>
                                    <th>Data</th>         
                                  </tr>
                                </thead>
                                <tbody>
                               <tbody>
                           <?php
                            $count1=0;
                           foreach($detail as $data)
                           {
                                $total =(1+$count1++);
                                if($total%2==0)
                                {
                                    $class='danger';
                                }
                                else
                                {
                                   $class='warning'; 
                                }
                                foreach($data as $key=>$datas)
                                {
                                    
                                    ?>
                                    <tr class="<?php echo $class;?>">
                                        <td><?php echo "<h5>".ucfirst($key)."</h5>";?></td>
                                        <td><?php echo $datas;?> </td>
                                    </tr>
                                    <?php
                                }
                           }
                           ?>
                                    </tbody>
                            </table>
                            <?php           
                       }
                    }
                    elseif($key="availablity")
                    {
                       if(!empty($detail))
                       {
                           
                       }
                    }
                    elseif($key="coachingLocation")
                    {
                       if(!empty($detail))
                       {
                           
                       }
                    }
                      
                ?>
                 <?php } */  ?>
                </td>              
            </tr>
            <?php endforeach;?>
            
            
 <?php   // Coaching Details // ?>
               
 <?php if($tutors['coachingDetail']){  $count=0;  //pre($parent['Due Payments']); ?>
          						
          <tr >
              <td><button  type="button" class="btn btn-info" data-toggle="collapse" data-target="#toggle1">Coaching Details</button>
              </td></tr>
          <tr>
              <td>
                  <table class="table collapse" id = "toggle1" >

                              <thead>
                                  <tr ">
                                    <th style="width: 100px">Board Name</th>
                                    <th>Class Name</th>         
                                    <th>Fees Per Hour</th>         
                                    <th>Fees Per Month</th>  
                                    <th>Subject Name</th> 
                                  </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($tutors['coachingDetail'] as $x){?>
                                     <?php
                                    $total =(1+$count++);
                                    if($total%2==0)
                                    {
                                        $class='info';
                                    }
                                    else
                                    {
                                       $class='warning'; 
                                    }

                                    ?>
                                    <tr class="<?php echo $class;// if(!empty($parent['Due Payments'])){ echo 'hi'; }?>">
                                <td><?php echo $x['boardName'];?></td>
                                <td><?php echo $x['className'];?></td>
                                <td><?php echo $x['feesPerHour'];?></td>
                                <td><?php echo $x['feesPerMonth'];?></td>
                                <td><?php echo $x['subjectName'];?></td>
                        </tr>                               
                                    <?php } ?>
                                </tbody>
                                </table>
              </td>
          </tr
          
          <?php } ?>
            
  <?php   // Availablity // ?>         
            
            
     <?php if($tutors['availablity']){  $count=0;  //pre($parent['Due Payments']); ?>
          						
          <tr >
              <td><button  type="button" class="btn btn-info" data-toggle="collapse" data-target="#toggle2">Availablity</button>
              </td></tr>
          <tr>
              <td>
                  <table class="table collapse" id = "toggle2" >

                              <thead>
                                  <tr ">
                                    <th style="width: 100px">Type Of Tution</th>
                                    <th>Premise</th>         
                                    <th>Days</th>         
                                    <th>Start Time</th>  
                                    <th>End Time</th>
                                    <th>Day Nr</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($tutors['availablity'] as $x){?>
                                     <?php
                                    $total =(1+$count++);
                                    if($total%2==0)
                                    {
                                        $class='info';
                                    }
                                    else
                                    {
                                       $class='warning'; 
                                    }

                                    ?>
                                    <tr class="<?php echo $class;// if(!empty($parent['Due Payments'])){ echo 'hi'; }?>">
                                <td><?php echo $x['typeOfTution'];?></td>
                                <td><?php echo $x['premise'];?></td>
                                <td><?php echo $x['days'];?></td>
                                <td><?php echo $x['startTime'];?></td>
                                <td><?php echo $x['endTime'];?></td>
                                <td><?php echo $x['day_nr'];?></td>
 </tr>                               
                                    <?php } ?>
                                </tbody>
                                </table>
              </td>
          </tr
          
          <?php } ?>       
            
   <?php   // Coaching Location // ?>             
            
          <?php if($tutors['coachingLocation'] || empty($tutors['coachingLocation'])){  $count=0;  //pre($parent['Due Payments']); ?>
          						
          <tr >
              <td><button  type="button" class="btn btn-info" data-toggle="collapse" data-target="#toggle3">Coaching Location</button>
              </td></tr>
          <tr>
              <td>
                  <table class="table collapse" id = "toggle3" >

                              <thead>
                                  <tr ">
                                    <th style="width: 100px">Location</th>
                                    <th>Radius</th>         
                                    
                                  </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($tutors['coachingLocation'] as $x){?>
                                     <?php
                                    $total =(1+$count++);
                                    if($total%2==0)
                                    {
                                        $class='info';
                                    }
                                    else
                                    {
                                       $class='warning'; 
                                    }

                                    ?>
                                    <tr class="<?php echo $class;// if(!empty($parent['Due Payments'])){ echo 'hi'; }?>">
                                <td><?php echo $x['location'];?></td>
                                <td><?php echo $x['radius'];?></td>
                          
 </tr>                               
                                    <?php } ?>
                                </tbody>
                                </table>
              </td>
          </tr
          
          <?php } ?>       
            
            
            
            
            
            
            
            <tr>
                <td><h3>Save Rating</h3></td>
                <td>
                    <form action="<?php echo site_url('saveRating/'.$this->uri->segment(2));?>" method="POST">
                        <input type="text" name="rating" />
                    <button class="btn btn_5 btn-lg btn-info" type="submit">Save Rating</button>
                    </form>
                </td>
            </tr>
        <?php }else{ ?>
            
            <tr class="active">
                <td colspan="3">No tutors found</td>
            </tr>
            
        <?php }?>
      </tbody>
    </table>
   </div>	
  </div>
<?php $this->load->view('footer'); ?> 
