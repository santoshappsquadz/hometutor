<?php $this->load->view('header'); ?>
<body>
<div id="wrapper">
     <!-- Navigation -->
        <nav class="top1 navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                <a class="navbar-brand" href="index.html">
                    Welcome <?php echo $this->session->userdata('adminInfo')['email']; ?>
                </a>
            </div>
            <!-- /.navbar-header -->
            <ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
	        		<!--a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-comments-o"></i><span class="badge">4</span></a>
	        		<ul class="dropdown-menu">
						<li class="dropdown-menu-header">
							<strong>Messages</strong>
							<div class="progress thin">
							  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
							    <span class="sr-only">40% Complete (success)</span>
							  </div>
							</div>
						</li>
						<li class="avatar">
							<a href="#">
								<img src="images/1.png" alt=""/>
								<div>New message</div>
								<small>1 minute ago</small>
								<span class="label label-info">NEW</span>
							</a>
						</li>
						<li class="avatar">
							<a href="#">
								<img src="images/2.png" alt=""/>
								<div>New message</div>
								<small>1 minute ago</small>
								<span class="label label-info">NEW</span>
							</a>
						</li>
						<li class="avatar">
							<a href="#">
								<img src="images/3.png" alt=""/>
								<div>New message</div>
								<small>1 minute ago</small>
							</a>
						</li>
						<li class="avatar">
							<a href="#">
								<img src="images/4.png" alt=""/>
								<div>New message</div>
								<small>1 minute ago</small>
							</a>
						</li>
						<li class="avatar">
							<a href="#">
								<img src="images/5.png" alt=""/>
								<div>New message</div>
								<small>1 minute ago</small>
							</a>
						</li>
						<li class="avatar">
							<a href="#">
								<img src="images/pic1.png" alt=""/>
								<div>New message</div>
								<small>1 minute ago</small>
							</a>
						</li>
						<li class="dropdown-menu-footer text-center">
							<a href="#">View all messages</a>
						</li>	
	        		</ul-->
	      		</li>
			    <li class="dropdown">
                               
	        		<!--a href="#" class="dropdown-toggle avatar" data-toggle="dropdown"><img src="<?php echo base_url("assets/images/1.png"); ?>" alt=""/><span class="badge">9</span></a>
	        		<ul class="dropdown-menu">
						<li class="dropdown-menu-header text-center">
							<strong>Account</strong>
						</li>
						<li class="m_2"><a href="#"><i class="fa fa-bell-o"></i> Updates <span class="label label-info">42</span></a></li>
						<li class="m_2"><a href="#"><i class="fa fa-envelope-o"></i> Messages <span class="label label-success">42</span></a></li>
						<li class="m_2"><a href="#"><i class="fa fa-tasks"></i> Tasks <span class="label label-danger">42</span></a></li>
						<li><a href="#"><i class="fa fa-comments"></i> Comments <span class="label label-warning">42</span></a></li>
						<li class="dropdown-menu-header text-center">
							<strong>Settings</strong>
						</li>
						<li class="m_2"><a href="#"><i class="fa fa-user"></i> Profile</a></li>
						<li class="m_2"><a href="#"><i class="fa fa-wrench"></i> Settings</a></li>
						<li class="m_2"><a href="#"><i class="fa fa-usd"></i> Payments <span class="label label-default">42</span></a></li>
						<li class="m_2"><a href="#"><i class="fa fa-file"></i> Projects <span class="label label-primary">42</span></a></li>
						<li class="divider"></li>
						<li class="m_2"><a href="#"><i class="fa fa-shield"></i> Lock Profile</a></li>
						<li class="m_2"><a href="#"><i class="fa fa-lock"></i> Logout</a></li>	
	        		</ul-->
	      		</li>
			</ul>
			<!--form class="navbar-form navbar-right">
              <input type="text" class="form-control" value="Search..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search...';}">
            </form-->
            <div class="navbar-default sidebar" role="navigation">
                <?php $this->load->view('sidebar'); ?> 
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <div id="page-wrapper">
        <div class="col-md-12 graphs">
	   <div class="xs">
  	 <div class="col-md-12 graphs">
	   <div class="xs">
  	 <h3>Payment Details</h3>
  	<div class="bs-example4" data-example-id="contextual-table">
   
       <?php
       if($this->session->flashdata('success'))
       {
           echo "<span class='alert alert-success'>".$this->session->flashdata('success')."</span>";
       }
       elseif($this->session->flashdata('error'))
       {
           echo "<span class='alert alert-danger'>".$this->session->flashdata('error')."</span>";
       }
       ?>
     
          
        
          
          <?php
          $count = 0;
          $total =(1+$count++);
          if($total%2==0)
          {
              $class='info';
          }
          else
          {
             $class='warning'; 
          }
         
          ?><?php   if(isset($paymentRecord)){   //pre($parent['Due Payments']); ?>
          						
           <!-- class="table collapse"  id = "toggle1"             <td><button type="button" class="btn btn-info" data-toggle="collapse"  data-target="#toggle1">All Payments </button></td>   -->
              <td>
                  <table class="table"  >

                              <thead>
                                  <tr >
                                    <th style="width: 100px">Amount</th>
                                    <th>Date</th>         
                                    <th>Comment</th>         
                                             
                                  </tr>
                                </thead>
                                <tbody> 
                                    <?php  foreach($paymentRecord as $x){?>
                                     <?php  $id = $x['fkTutorId'];// echo $id;
                                    $total =(1+$count++);
                                    if($total%2==0)
                                    {
                                        $class='info';
                                    }
                                    else
                                    {
                                       $class='warning'; 
                                    }

                                    ?>
                                    <tr class="<?php echo $class;?>">
                               
                                <td><?php echo $x['amount'];?></td>
                                <td><?php echo $x['date'];?></td>
                                <td><?php echo $x['comment'];?></td>
 </tr>                               
 
                                    <?php } ?>
                                </tbody>
                  </table>
                                <tr><td><button onclick="window.location.href='<?php echo site_url('addTutorPayment/'.$id)?>';" class="btn btn-warning warning_22" type="button"  style="float: right;">Pay Now</button></td><td></tr>
              </td>
          </tr
         
          
          <?php }else{ ?> 
          <tr class="active">
                <td colspan="3">No Payment Till Now</td>
                        <tr><td><button onclick="window.location.href='<?php  echo site_url('addTutorPayment/'.$id)?>';" class="btn btn-warning warning_22" type="button" style="float: right;">Pay Now</button></td><td></tr>
          </tr>   <?php } ?>
          
         

   </div>	
  </div>
<?php $this->load->view('footer'); ?> 
