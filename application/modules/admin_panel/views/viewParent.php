<?php $this->load->view('header'); ?>
<body>
<div id="wrapper">
     <!-- Navigation -->
        <nav class="top1 navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                <a class="navbar-brand" href="index.html">
                    Welcome <?php echo $this->session->userdata('adminInfo')['email']; ?>
                </a>
            </div>
            <!-- /.navbar-header -->
            <ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
	        		
	      		</li>
			    <li class="dropdown">
                               
	        		
	      		</li>
			</ul>
			<!--form class="navbar-form navbar-right">
              <input type="text" class="form-control" value="Search..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search...';}">
            </form-->
            <div class="navbar-default sidebar" role="navigation">
                <?php $this->load->view('sidebar'); ?> 
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <div id="page-wrapper">
        <div class="col-md-12 graphs">
	   <div class="xs">
  	 <h3>Parent Details</h3>
  	<div class="bs-example4" data-example-id="contextual-table">
    <table class="table">
       <?php
       if($this->session->flashdata('success'))
       {
           echo "<span class='alert alert-success'>".$this->session->flashdata('success')."</span>";
       }
       elseif($this->session->flashdata('error'))
       {
           echo "<span class='alert alert-danger'>".$this->session->flashdata('error')."</span>";
       }
       ?>
      <thead>
        <tr>
          <th>Feild Name</th>
          <th>Data</th>         
        </tr>
      </thead>
      <tbody>
          
        <?php //pre($parent); 
        if(!empty($parent)){ $count=0; ?>
           <?php foreach($parent as $key=>$detail): ?>
          <?php
          $total =(1+$count++);
          if($total%2==0)
          {
              $class='info';
          }
          else
          {
             $class='warning'; 
          }
         
          ?><?php if(!is_array($detail)){?>
            <tr class="<?php echo $class;// if(!empty($parent['Due Payments'])){ echo 'hi'; }?>">
                <td><?php echo "<h5>".ucfirst($key)."</h5>" ?></td>  
                <td>
                 <?php  echo $detail; ?> 
                  
                <?php if($key=='photoId'||$key=='profileImage'){?>
                    <a href="<?php echo $detail;?>"><img class="img-responsive" src="<?php echo $detail;?>" width="100" height="100"/></a>
                    <?php }
                    
                    /*else{ ?>
                <?php  

                       if(!empty($detail))
                       {
						   $colla = rand(234234,234234234234);
                           ?>
							  <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo<?php echo $colla;?>">View full details </button>
							  <div id="demo<?php echo $colla;?>" class="collapse">
								
							  
                            <table class="table">

                              <thead>
                                  <tr>
                                    <th>Feild Name</th>
                                    <th>Data</th>         
                                  </tr>
                                </thead>
                                <tbody>
                               <tbody>
                           <?php
                            $count1=0;
                           
                           // echo $key."<br>";
                            if($key=="Paid Payments"){ $varApprove = 1; }
                           foreach($detail as $data)
                           { 
                                $total =(1+$count1++);
                                if($total%2==0)
                                {
                                    $class='danger';
                                }
                                else
                                {
                                   $class='warning'; 
                                }
                                $ttl = count($data);
                                $i=1;
                                $v1 = 0;
                                //pre($data); die();
                                foreach($data as $key=>$datas)
                                {
                                    if($i==1){ $id = $datas;}
                                    
                                    else
                                    {
                                     if($key=="verifiedByAdmin" and $datas==1){ $v1 = 1;}

          
                                    ?>
                                   
                                   <tr class="<?php echo $class;?>" >
                                        <td><?php echo "<h5>".ucfirst($key)."</h5>";?></td>
                                        <td><?php echo $datas;?> </td>
                                   </tr>
                                    
                                    <?php
                                    if($i==$ttl)
                                    {?><tr class="<?php echo $class;?>">
                                        
                                        
                                    
                                         <?php if(isset($varApprove)){ ?>
                                        <td><?php echo "<h5>Action</h5>";?> </td>
                                        <?php if($v1==1){ ?>
                                        <td><button onclick="window.location.href='#'" class="btn btn-warning warning_22" type="button">Approved</button></td></tr>
                                        <?php } else { ?>
                                    <td><button onclick="window.location.href='<?php echo site_url('approveAmount/'.$id)?>';" class="btn btn_5 btn-lg btn-success warning_1" type="button">Approve<?php //echo $id; ?></button></td></tr>
                                        
                          
                                        
                                    <?php }} }
                                    
                                   
                                } $i = $i+1;}
                                ?> <?php
                           }
                           ?>
                                    </tbody>
                            </table>
		  </div>
                            <?php           
                       }
                   
                      
                ?>
                 <?php }*/?>
                </td>              
          </tr><?php } ?>
            <?php endforeach;?>
          <?php if($parent['Due Payments']){  $count=0;  //pre($parent['Due Payments']); ?>
          						
          <tr >
              <td><button  type="button" class="btn btn-info" data-toggle="collapse" data-target="#toggle1">Due Payments </button>
              </td></tr>
          <tr>
              <td>
                  <table class="table collapse" id = "toggle1" >

                              <thead>
                                  <tr ">
                                    <th style="width: 100px">Due Date</th>
                                    <th>Due Amount</th>         
                                    <th>Student Name</th>         
                                    <th>Subject Name</th>         
                                  </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($parent['Due Payments'] as $x){?>
                                     <?php
                                    $total =(1+$count++);
                                    if($total%2==0)
                                    {
                                        $class='info';
                                    }
                                    else
                                    {
                                       $class='warning'; 
                                    }

                                    ?>
                                    <tr class="<?php echo $class;// if(!empty($parent['Due Payments'])){ echo 'hi'; }?>">
                                <td><?php echo $x['dueDate'];?></td>
                                <td><?php echo $x['dueAmount'];?></td>
                                <td><?php echo $x['studentName'];?></td>
                                <td><?php echo $x['subjectName'];?></td>
 </tr>                               
                                    <?php } ?>
                                </tbody>
                                </table>
              </td>
          </tr
          
          <?php } ?>
          
            <?php if($parent['Paid Payments']){  $varApprove = 1;  $count=0;// pre($parent['Paid Payments']);?>
          
          <tr >
              <td><button  type="button" class="btn btn-info" data-toggle="collapse" data-target="#toggle2">Paid Payments </button>
              </td></tr>
          <tr>
              <td>
                  <table class="table collapse" id = "toggle2">

                              <thead>
                                  <tr ">
                                    <th>Tutor Name</th>
                                    <th>Date</th>         
                                    <th>Amount</th>        
                                    <th>Student Name</th> 
 				    <th>Payment Mode</th>
                                     <th>Admin Verified</th>
                                     <th>Payment Status</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $i=1;
                                    $v1 = 0; 
                                    //pre($parent['Paid Payments']); die();
                                    foreach($parent['Paid Payments'] as $key=> $x){?>
                                     <?php
                                     //if($key=="verifiedByAdmin" and $x==1){ $v1 = 1;}
                                     //if($i==3) { pre($x['id']); die(); }
                                      if($i==1){ $id = $x['id']; }
                                    $total =(1+$count++);
                                    if($total%2==0)
                                    {
                                        $class='info';
                                    }
                                    else
                                    {
                                       $class='warning'; 
                                    }
                                    if($key=="verifiedByAdmin" and $x==1){ $v1 = 1;}
                                    ?>
                                    <tr class="<?php echo $class;// if(!empty($parent['Due Payments'])){ echo 'hi'; }?>">
                                <td><?php echo $x['tutorName'];?></td>
                                <td><?php echo $x['date'];?></td>
                                <td><?php echo $x['amount'];?></td>
                                <td><?php echo $x['studentName'];?></td>
				<td><?php if($x['paymentStatus']==1){echo "Online";}else{echo "By Cash";}?></td>
                                <td><?php echo $x['verifiedByAdmin'];?></td>
                                 <?php if(isset($varApprove)){ ?>
                                        
                                        <?php if($x['verifiedByAdmin']==1){ ?>
                                        <td><button class="btn btn-warning warning_22">Approved</button></td></tr>
                                        <?php } else { ?>
                                    <td><button onclick="window.location.href='<?php echo site_url('approveAmount/'.$x['id'])?>';" class="btn btn_5 btn-lg btn-success warning_1" type="button">Approve<?php //echo $id; ?></button></td></tr>
                                        
                          
                                        
                                    <?php }} ?>
 </tr>                               
                                    <?php $i = $i+1; } ?>
                                </tbody>
                                </table>
              </td>
          </tr
          
          <?php } ?>
          
         
        <?php }else{ ?>
            
            <tr class="active">
                <td colspan="3">No tutors found</td>
            </tr>
            
        <?php }?>
      </tbody>
    </table>
   </div>	
  </div>
            <script>
              $(document).ready(function() {
                  //$('.info').hide();
                  // $('.warning').hide();
                 
              });
            
            </script>
<?php $this->load->view('footer'); ?> 
