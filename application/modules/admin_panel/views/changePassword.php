<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$this->load->view('header.php');
?>
<div id="page-wrapper">
    <h1 style="margin:0 auto;">Change Password</h1><hr>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-2">
                </div>
                <div class="col-sm-6" style="background-color: rgb(6, 217, 149); border-radius: 7px">
                   
                    <form  action="<?= base_url() .'index.php/admin/passwordChange' ?>" method="post" onsubmit="return validate_Pwd()">
                         
                        <div class="form-group">
                            <label for="exampleInputEmail1"><h5>Old Password</h5></label>
                            <input type="password" class="form-control" id="exampleInputEmail1" placeholder="Email" name="pwd" required/>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1"><h5>New Password</h5></label>
                            <input type="password" class="form-control" id="Password1" name="pwd1" placeholder="Password" required/>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1"><h5>Confirm Password</h5></label>
                            <input type="password" class="form-control" id="Password2" name="pwd2" placeholder="Password" required/>
                        </div>
                        <div class="clearfix"></div>
                        <input type="hidden" name="user" value="<?= $user ?>">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function validate_Pwd() {
        var password1 = $('#Password1').val();
        var password2 = $('#Password2').val();
        if(password1 == password2) {
            return true;
        } else {
            alert('Reset password not match');
            return false;
        }
    }
</script>
<?php
$this->load->view('footer.php');
