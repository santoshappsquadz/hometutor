<?php $this->load->view('header');?>
<body>
<?php
$sn = 1;
$perpage = 15;
$start = 0;
?>
<div id="wrapper">
     <!-- Navigation -->
        <nav class="top1 navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                <a class="navbar-brand" href="index.html">
                    Welcome <?php echo $this->session->userdata('adminInfo')['email']; ?>
                </a>
            </div>
           
            <div class="navbar-default sidebar" role="navigation">
                <?php $this->load->view('sidebar'); ?> 
				
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <div id="page-wrapper">
        <div class="col-md-12 graphs">
	   <div class="xs">
	    <?php 
       if($this->session->flashdata('success'))
       {
           echo "<span class='alert alert-success'>".$this->session->flashdata('success')."</span></br>";
       }
	   ?>
  	   <h3>All Topic</h3>
  	<div class="bs-example4" data-example-id="contextual-table">
		<thead>
		<table cellpadding="10" cellspacing="0" width="100%" border="0" bgcolor="#FFF" class="table" id="example" >
                        <tr class="">
                            <th width="5%">S.N.</th>
							<th width="5%">Start Date</th>                                                
                            <th width="11%">End Date</th>                          
                            <th width="11%">Offer</th>                          
                            <th width="11%">Totur Name</th>
							<th width="11%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 	foreach ($data as $w) { ?>
                                <td><?php echo $sn + $start; ?>.</td>   
								<td><?php echo ucfirst($w['start_date']); ?>&nbsp;</td> 
                                <td><?php echo ucfirst($w['end_date']); ?>&nbsp;</td>                               
                                <td><?php echo ucfirst($w['offer']); ?>&nbsp;</td>                               
                                <td><?php echo ucfirst($w['tutor_name']); ?>&nbsp;</td>
                                <td>
                                    

                                    <a href="<?php echo base_url("index.php/admin_panel/tutor_offer/edit_offer_for_tutor?id=$w[id]");    ?>">
                                        <img src="<?php echo base_url('assets/images/edit.jpg')    ?>" alt="Edit" title="Edit"/></a>
										<a href="<?php echo base_url("index.php/admin_panel/tutor_offer/offer_delete?id=$w[id]");    ?>">
                                        <img src="<?php echo base_url('assets/images/delete.jpg')    ?>" alt="Delete" title="Delete"/></a>
                                </td>

                            </tr>

                            <?php
                            $sn++;
                        }
                        ?>
      </tbody>
	  </table>
    </div>	
  </div>
<?php $this->load->view('footer'); ?> 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
