<?php $this->load->view('header'); ?>
<body id="login">
  <div class="login-logo">
    
  </div>
  <h2 class="form-heading">Home Tutor Admin Login</h2>
  <div class="app-cam">
       <?php
       if($this->session->flashdata('error'))
       {
           echo "<span class='alert alert-danger'>".$this->session->flashdata('error')."</span>";
       }
       ?>
        <form action="<?php echo site_url('adminLogin')?>" method="post">
            <input type="email" class="text" name="email" placeholder="Email" required>
            <input type="password" name="password" placeholder="Password" required>
            <div class="submit"><input type="submit"  value="Login"></div>
	</form>
  </div>
<?php $this->load->view('footer'); ?>   