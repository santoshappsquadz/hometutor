<?php $this->load->view('header');?>
<body>
<div id="wrapper">
     <!-- Navigation -->
        <nav class="top1 navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                <a class="navbar-brand" href="index.html">
                    Welcome <?php echo $this->session->userdata('adminInfo')['email']; ?>
                </a>
            </div>
           
            <div class="navbar-default sidebar" role="navigation">
                <?php $this->load->view('sidebar'); ?> 
				
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <div id="page-wrapper">
        <div class="col-md-12 graphs">
	   <div class="xs">
	    <?php 
       if($this->session->flashdata('success'))
       {
           echo "<span class='alert alert-success'>".$this->session->flashdata('success')."</span></br>";
       }
	   ?>
  	   <h3>Edit Subject</h3>
  	<div class="bs-example4" data-example-id="contextual-table">
      <thead>
	  <tbody>

		  <form action="" method="post" name="form1" id="form1">
		<div class="row">
			<label style="color:darkblack; font-family: verdana; font-size:130%;">Class:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $data['class'];?></label>
			<input type="hidden" name="fkClassId" value="<?php  echo $data['fkClassId']; ?>">
			<input type="hidden" name="id" value="<?php  echo $data['id']; ?>">
		</div></br>
		<div class="row">
		<label style="color:darkblack; font-family: verdana; font-size:130%;">Subject:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
		<input type="text" name="subject_name" <?php if($data['name']) { $r = $data['name'];} else { $r = ''; }?> value="<?php echo $r; ?>"></div>
			</br>
		<div class="row">	
	  <input type="submit" name="submit" value="Submit" />
	  </div>
      </tbody>
	   </form>
   </div>	
  </div> 
  
  
  
  
<?php $this->load->view('footer'); ?> 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>


