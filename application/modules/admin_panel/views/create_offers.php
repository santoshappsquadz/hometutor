<?php $this->load->view('header');?>
<?php //echo '<pre>'; print_r($tutor); die;?>
<?php error_reporting(0);?>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<link href="<?php echo base_url('assets/css/demo.css')?>" rel='stylesheet' type='text/css' />
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript"  src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

<body>
<div id="wrapper">
     <!-- Navigation -->
        <nav class="top1 navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                <a class="navbar-brand" href="index.html">
                    Welcome <?php echo $this->session->userdata('adminInfo')['email']; ?>
                </a>
            </div>
           
            <div class="navbar-default sidebar" role="navigation">
                <?php $this->load->view('sidebar'); ?> 
				
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <div id="page-wrapper">
        <div class="col-md-12 graphs">
	   <div class="xs">
	    <?php 
       if($this->session->flashdata('success'))
       {
           echo "<span class='alert alert-success'>".$this->session->flashdata('success')."</span></br>";
       }
	   ?>
  	   <h3>Create Offer</h3>
  	<div class="bs-example4" data-example-id="contextual-table">
      <thead>
	  <tbody>

		  <form action="" method="post" name="form1" id="form1" >
		  <?php if(!empty($edit)){
			  echo '<input type ="hidden" value ="'.$edit['id'].'" name= "id">';
		  } ?>
		<div class="row"> 
<div class="col-md-6">Start Date: <input type="text" name="start_date" value="<?php echo isset($edit['start_date']) ? $edit['start_date'] : ""; ?>"id="datestart"></div>			
<div class="col-md-6">End Date: <input type="text" name="end_date" value="<?php echo isset($edit['end_date']) ? $edit['end_date'] : ""; ?>" id="dateend"></div>	
</div>	</br>
		<div class="row">
			<label style="color:darkblack; font-family: verdana; font-size:130%;">Offer:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<textarea id="editor" name="offer" rows="10" cols="80" ><?php echo isset($edit['offer']) ? $edit['offer'] : ""; ?>
                    </textarea>
		</div></br>
		<table id="example" class="display select" cellspacing="0" width="100%">
		<thead>
            <tr>
						<th><input id= "select1" value="1" type="checkbox"></th>
						<th>Tutor Name</th>
			</tr></thead>
			<tbody>
				
				<?php  foreach($tutor as $tutor1){?>
				<tr>
					<td><input type="checkbox" id = "tutor_id" class="multiselect" <?php if(strstr($edit['tutor_id'],$tutor1['id'])) {?> checked="checked" <?php } else{}?> name="tutor_id[]" value="<?php echo $tutor1['id']?>" ></td>
					<td><?php echo $tutor1['name'];?></td>
				</tr>
				<?php }?>
				<span id="spnameError" class="error" name="spnameError" style="display:none;"></span>
			</tbody>
		</table>
        
	  <input type="button" name="submit1" class="btn btn-success" id="submit1" value="Submit" />
      </tbody>
	   </form>
   </div>	
  </div> 
  
  
  
  
<?php $this->load->view('footer'); ?> 
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
  
  <script>
  $( function() {
    $( "#datestart" ).datepicker();
  } );
  </script>
  <script>
  $( function() {
    $( "#dateend" ).datepicker();
  } );
  </script>
  <script>
jQuery(document).ready(function() {
$("#all_select").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
});;
});
</script>

<script src ="<?php echo base_url('assets/ckeditor/ckeditor.js')?>"></script>
<script>
  
</script>
<script>
CKEDITOR.replace( 'editor');
</script>
<script>
 /* $('#example').DataTable({
   'columnDefs': [
      {
         'targets': 0,
         'checkboxes': {
            'selectRow': true
         }
      }
   ],
   'select': {
      'style': 'multi'
   },
   'order': [[1, 'asc']]
});  */
$('#example').DataTable();
</script>
<script>

   $("#select1").on('click', function(e){
      if(this.checked){
         $('#example tbody input[type="checkbox"]:not(:checked)').trigger('click');
      } else {
         $('#example tbody input[type="checkbox"]:checked').trigger('click');
      }
   });
 
 </script>
<script>
$("#submit1").click(function () 
{ 
    var startTime = $('#datestart').val();   
    var endTime   = $('#dateend').val(); 
	
    if (startTime >= endTime) 
    {
        alert('End date always greater then start date.');
		
    } else {
		
		document.getElementById("form1").submit();
	}
	
});
</script>