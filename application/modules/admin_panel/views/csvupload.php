<?php $this->load->view('header');?>
<body>
<div id="wrapper">
     <!-- Navigation -->
        <nav class="top1 navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                <a class="navbar-brand" href="index.html">
                    Welcome <?php echo $this->session->userdata('adminInfo')['email']; ?>
                </a>
            </div>
           
            <div class="navbar-default sidebar" role="navigation">
                <?php $this->load->view('sidebar'); ?> 
				
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <div id="page-wrapper">
        <div class="col-md-12 graphs">
	   <div class="xs">
	    <?php 
       if($this->session->flashdata('success'))
       {
           echo "<span class='alert alert-success'>".$this->session->flashdata('success')."</span></br>";
       }
	     if($this->session->flashdata('success1'))
       {
           echo "<span class='alert alert-success'>".$this->session->flashdata('success1')."</span></br>";
       }
	     if($this->session->flashdata('success2'))
       {
           echo "<span class='alert alert-success'>".$this->session->flashdata('success2')."</span></br>";
       }
	   ?>
  	   <h3>Csv Upload</h3>
  	<div class="bs-example4" data-example-id="contextual-table">
    <table class="table">
      <thead>
	  <tbody>
		  <form action="admin_panel/admin/upload_csv" method="post" enctype="multipart/form-data" name="form1" id="form1">
		<div class="row">
			<label style="color:darkblack; font-family: verdana; font-size:130%;">Board:&nbsp;&nbsp;&nbsp;&nbsp;</label>
			<select name="board_id" id="board-list" class="demoInputBox" onChange="getclass(this.value);">
			<option value="">Select Board</option>
			<?php
			foreach($data1 as $board) {
			?>
			<option value="<?php echo $board["id"]; ?>"><?php echo $board["name"]; ?></option>
			<?php
			}
			?>
			</select>
		</div></br>
		<div class="row">
			<label style="color:darkblack; font-family: verdana; font-size:130%;">Class:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
			<select name="class_id" id="Class-list" class="demoInputBox" onclick ="getsubject(this.value);">
			<option value="">Select Class</option>
			</select>
		</div></br>
			<div class="row">
			<label style="color:darkblack; font-family: verdana; font-size:130%;">Subject:&nbsp;&nbsp;</label>
			<select name="subject_id" id="Subject-list" class="demoInputBox" onclick ="gettopic(this.value,document.getElementById('Class-list').value);">
			<option value="">Select Subject</option>
			</select>
		</div></br>
		<div class="row">
			<label style="color:darkblack; font-family: verdana; font-size:130%;">Topic:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<label id="Topic-list" style="color:darkblack; font-family: verdana; font-size:130%;"></label>
		</div><br/>
	<div class="row">	
	  <label style="color:darkblack; font-family: verdana; font-size:130%;">Choose your file: &nbsp;&nbsp;&nbsp;&nbsp;</label><input name="csv" type="file" id="csv" />
	 </div> </br>
	  <input type="submit" name="submit" value="Submit" />
	</form> 
      </tbody>
    </table>
   </div>	
  </div>
<?php $this->load->view('footer'); ?> 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
function getclass(val) {
dataString="board_id="+val;
	$.ajax({
            url: "admin_panel/admin/get_class_for_csv",
            type: "POST",
            data: dataString,
            success: function(data)
             {
              $('#Class-list').html(data);
             },
        });
}
</script>
<script>
function getsubject(val) {
dataString="class_id="+val;
	$.ajax({
            url: "admin_panel/admin/get_subject_for_csv",
            type: "POST",
            data: dataString,
            success: function(data)
             {
              $('#Subject-list').html(data);
             },
        });
}
</script>
<script>
function gettopic(val,id) {
	$.ajax({
            url: "admin_panel/admin/get_topic_for_csv",
            type: "POST",
			data: {subject_id: val, class_id: id},
            success: function(data)
             {
              $('#Topic-list').html(data);
             },
        });
}
</script>
