<?php $this->load->view('header');?>
<body>
<div id="wrapper">
     <!-- Navigation -->
        <nav class="top1 navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                <a class="navbar-brand" href="index.html">
                    Welcome <?php echo $this->session->userdata('adminInfo')['email']; ?>
                </a>
            </div>
           
            <div class="navbar-default sidebar" role="navigation">
                <?php $this->load->view('sidebar'); ?> 
				
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <div id="page-wrapper">
        <div class="col-md-12 graphs">
	   <div class="xs">
	    <?php 
       if($this->session->flashdata('success'))
       {
           echo "<span class='alert alert-success'>".$this->session->flashdata('success')."</span></br>";
       }
	   ?>
  	   <h3>Topic Upload</h3>
  	<div class="bs-example4" data-example-id="contextual-table">
      <thead>
	  <tbody>
		  <form action="topic_upload_data" method="post" enctype="multipart/form-data" name="form1" id="form1">
		<div class="row">
			<label style="color:darkblack; font-family: verdana; font-size:130%;">Board:&nbsp;&nbsp;&nbsp;&nbsp;</label>
			<select id="board-list" class="demoInputBox" onChange="getclass(this.value);">
			<option value="">Select Board</option>
			<?php
			foreach($data1 as $board) {
			?>
			<option value="<?php echo $board["id"]; ?>"><?php echo $board["name"]; ?></option>
			<?php
			}
			?>
			</select>
		</div></br>
		<div class="row">
			<label style="color:darkblack; font-family: verdana; font-size:130%;">Class:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
			<select id="Class-list" name = "class_id" class="demoInputBox" onclick ="getsubject(this.value);">
			<option value="">Select Class</option>
			</select>
		</div></br>
			<div class="row">
			<label style="color:darkblack; font-family: verdana; font-size:130%;">Subject:&nbsp;&nbsp;</label>
			<select name="subject_id" id="Subject-list" class="demoInputBox">
			<option value="">Select Subject</option>
			</select>
		</div></br>
		<div class="row">
		<label style="color:darkblack; font-family: verdana; font-size:130%;">Topic:&nbsp;&nbsp;</label>
		</div>
		<div class="input_fields_wrap">
			 <button class="add_field_button">Add More Fields</button>
			<div><input type="text" name="mytext[]"></div>
		</div> </br>
	  <input type="submit" name="submit" value="Submit" />
	</form> 
      </tbody>
    </div>	
  </div>
<?php $this->load->view('footer'); ?> 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
function getclass(val) {
dataString="board_id="+val;
	$.ajax({
            url: "get_class_for_subject_topic",
            type: "POST",
            data: dataString,
            success: function(data)
             {
              $('#Class-list').html(data);
             },
        });
}
</script>
<script>
function getsubject(val) {
dataString="class_id="+val;
	$.ajax({
            url: "get_subject_for_topic",
            type: "POST",
            data: dataString,
            success: function(data)
             {
              $('#Subject-list').html(data);
             },
        });
}
</script>
<script>
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
   
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div><input type="text" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>'); //add input box
        }
    });
   
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })

	
});
</script>
