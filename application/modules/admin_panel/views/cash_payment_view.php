<?php $this->load->view('header');?>
<body>
<?php
$sn = 1;
$perpage = 15;
$start = 0;
?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<link href="<?php echo base_url();?>assets/css/demo.css" rel='stylesheet' type='text/css' />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript"  src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<div id="wrapper">
     <!-- Navigation -->
        <nav class="top1 navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brand" href="index.html">
                    Welcome <?php echo $this->session->userdata('adminInfo')['email']; ?>
                </a>
            </div>

            <div class="navbar-default sidebar" role="navigation">
                <?php $this->load->view('sidebar'); ?>

                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <div id="page-wrapper">
        <div class="col-md-12 graphs">
	   <div class="xs">
  	   <h3>All Cash Payments</h3>
  	<div class="bs-example4" data-example-id="contextual-table">
		<table cellpadding="10" cellspacing="0" width="100%" border="0" bgcolor="#FFF" class="table" id="all_tutor" >
					<thead>
                        <tr class="">
                          <th>Tutor Name</th>
                          <th>Date</th>
                          <th>Amount</th>
                          <th>Student Name</th>
                          <th>Payment Mode</th>
                          <th>Payment Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($data)){
                          foreach ($data as $x) { ?>
                                <?php if($sn%2 == 0){
                                  echo '<tr class="info">';
                                }else{
                                  echo '<tr class="warning">';
                                }
                                  ?>

                                  <td><?php echo $x['tutorName'];?></td>
                                  <td><?php echo $x['date'];?></td>
                                  <td><?php echo $x['amount'];?></td>
                                  <td><?php echo $x['studentName'];?></td>
                                  <td><?php if($x['paymentStatus']==1){echo "Online";}else{echo "By Cash";}?></td>
                                  <td><a href="<?php echo site_url('admin_panel/admin/approve_amount/'.$x['id']) ; ?>" class="btn btn_5 btn-lg btn-success warning_1">Approve</a></td>

                            </tr>

                            <?php
                            $sn++;
                        }
                        }
                      ?>
      </tbody>
	  </table>
    </div>
  </div>
<?php $this->load->view('footer'); ?>
<script>
$(document).ready(function(){
    $('#all_tutor').DataTable();
});
</script>
