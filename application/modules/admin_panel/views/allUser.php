<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$this->load->view('header.php');
?>
<div id="page-wrapper" class="container_16">
    <div class="grid_16">
        <h3>All User<a href="javascript:history.go(-1)" class="add-sec" style="float:right"><strong>Back</strong></a></h3><hr/> 
        <div id="content">
            <div class="nodes index" style="background-color: rgb(6, 217, 149); border-radius: 7px">
             <!-- <h2>Manage User <a href="<?php //echo base_url("admin/add");?>" class="add-sec"><strong>Add User</strong></a></h2> -->
                <table cellpadding="10" cellspacing="0" width="100%" border="0" bgcolor="#FFF" class="display" id="example" >
                    <thead>
                        <tr class="hello">
                            <th width="5%">S.N.</th>
                            <th width="10%">FUll Name</th>
                            <th width="30%">Email</th>
                            <th width="35%">Phone</th>
                            <th width="20%">Action</th> 
                        </tr>
                    </thead>
                    <?php
                    $sno = 0;

                    foreach ($users as $array) {
                        $sno++;
                        ?>
                        <tbody>
                        <td><?= $sno ?></td>
                        <td><?= $array['fullName'] ?></td>
                        <td><?= $array['email'] ?></td>
                        <td><?= $array['phone'] ?></td>
                        <td>
                            <a href="<?= base_url() . 'index.php/admin/view?id='. $array['id'] ?>" >view |</a>
                            <a href="" >delete</a>

                        </td>


                        </tbody>
                    <?php } ?>

                </table>
                <br>
                <br/>
            </div>
        </div>
    </div>
    <div id="pagination">
<ul class="tsc_pagination">

<!-- Show pagination links -->
<?php foreach ($links as $link) {
echo  $link;
} ?>
</ul>
</div>
    <div class="clear">&nbsp;</div>
   
</div>


<!-- Show pagination links -->

<?php
$this->load->view('footer.php');
