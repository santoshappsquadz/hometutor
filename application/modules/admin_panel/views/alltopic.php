<?php $this->load->view('header');?>
<body>
<?php
$sn = 1;
$perpage = 15;
$start = 0;
?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<link href="<?php echo base_url();?>assets/css/demo.css" rel='stylesheet' type='text/css' />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript"  src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<div id="wrapper">
     <!-- Navigation -->
        <nav class="top1 navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                <a class="navbar-brand" href="index.html">
                    Welcome <?php echo $this->session->userdata('adminInfo')['email']; ?>
                </a>
            </div>
           
            <div class="navbar-default sidebar" role="navigation">
                <?php $this->load->view('sidebar'); ?> 
				
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <div id="page-wrapper">
        <div class="col-md-12 graphs">
	   <div class="xs">
	    <?php 
       if($this->session->flashdata('success'))
       {
           echo "<span class='alert alert-success'>".$this->session->flashdata('success')."</span></br>";
       }
	   ?>
  	   <h3>All Topic</h3>
  	<div class="bs-example4" data-example-id="contextual-table">
		<table cellpadding="10" cellspacing="0" width="100%" border="0" bgcolor="#FFF" class="table" id="all_tutor" > 
					<thead>
                        <tr class="">
                            <th width="5%">S.N.</th>
							<th width="5%">Class</th>                                                
                            <th width="11%">Subject</th>                          
                            <th width="11%">Topic</th>
                            <th width="5%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 	foreach ($data as $w) { ?>
						<?php if($sn%2 == 0){
							echo '<tr class="info">';
						}else{
							echo '<tr class="warning">';
						}
							?>
							
                                <td><?php echo $sn + $start; ?>.</td>   
								<td><?php echo ucfirst($w['class']); ?>&nbsp;</td> 
                                <td><?php echo ucfirst($w['subject']); ?>&nbsp;</td>                               
                                <td><?php echo ucfirst($w['topic_name']); ?>&nbsp;</td>
                                <td>
                                    

                                    <a href="<?php echo base_url("index.php/admin_panel/subject/edit_topic?id=$w[id]");    ?>">
                                        <img src="<?php echo base_url('assets/images/edit.jpg')?>" alt="Edit" title="Edit"/></a>
                                </td>

                            </tr>

                            <?php
                            $sn++;
                        }
                        ?>
      </tbody>
	  </table>
    </div>	
  </div>
<?php $this->load->view('footer'); ?> 

<script>
$(document).ready(function(){
    $('#all_tutor').DataTable();
});
</script>