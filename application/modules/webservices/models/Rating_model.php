<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Rating_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }



    function addRating($document)
        {
            //$this->db->where('id', $document['id']);
            $result=$this->db->insert('rating', $document);

            $data  = $this->isUser($document);
            $msg1 = '{"type":51,"message":"Congratulations. You are reviewed by a parent. Check your reviews in app."}';
            $Tmsg = 'Congratulations. You are reviewed by a parent. Check your reviews in app.';

            sendAndroidPush($data['deviceToken'], $msg1,"","",51);
            sendPushAsSMS($data['mobile1'], $Tmsg);

            if(!empty($result))
            {
                return $result;
            }
            else
            {
                return false;
            }
        }

    function isUser($json)
    {
        $this->db->where('id',$json['tutorId']);
        $result = $this->db->get('tutor')->row_array();
        if(!empty($result))
        {
            return $result;
        }
        else
        {
            return false;
        }

    }

    function getRating($document)
        {
            //$this->db->distinct('id');
            //$this->db->group_by('parent_student.fkParentId');
            $this->db->select('parent.id as parentId,parent.name as parentName,email,review,rating,parentId,tutorId,created');
            $this->db->where('tutorId', $document['tutorId']);
            $this->db->join('parent','parent.id = rating.parentId');
            //$this->db->join('parent_student','parent_student.fkParentId = rating.parentId');
            $result=$this->db->get('rating')->result_array();
            //pre($result);
            foreach($result as $res)
            {
              $res['childName'] = $this->childNameByParentId($res);
              $final[] = $res;
            }
            if(!empty($result))
            {
                return $final;
            }
            else
            {
                return false;
            }
        }

        function childNameByParentId($document)
        {
          $this->db->where('fkParentId', 8);//$document['parentId']);
          $result=$this->db->get('parent_student')->row_array();
        
          if(!empty($result))
          {
            return $result['name'];
          }
          else {
            return array();
          }
        }


        function avgRating($document)
        {
           $this->db->select('AVG(rating)');
         //   $this->db->join('parent','parent.id = rating.parentId');
            $this->db->where('tutorId', $document['tutorId']);
            $result=$this->db->get('rating')->row_array();
            //    pre($result); die();
            if(!empty($result))
            {
                return $result;
            }
            else
            {
                return false;
            }
        }

     function getRatingByParentId($document)
        {
            $this->db->where('parentId', $document['parentId']);
            $result=$this->db->get('rating')->result_array();


            //pre($result); die();
            if(!empty($result))
            {
                return $result;
            }
            else
            {
                return false;
            }
        }


    function getAllRating($document)
        {
            $this->db->where('parentId', $document['parentId']);
            $this->db->where('tutorId', $document['tutorId']);
            $this->db->order_by("id","desc");
            $result=$this->db->get('rating')->result_array();
            //pre($result); die();
            if(!empty($result))
            {
                return $result;
            }
            else
            {
                return false;
            }
        }

}
