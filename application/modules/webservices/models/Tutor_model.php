<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Tutor_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getTutorProfile($tutorId) {
        $this->db->select("tutor.*,tutor.qualification as qualificationId,"
                                 . "(CASE WHEN ht_qualifications.name='Other' THEN ht_tutor.other_qualification
                            WHEN ht_qualifications.name!='' THEN ht_qualifications.name
                            ELSE '' END) as  qualification," 
                . "(YEAR(now())-ht_tutor.dob) as age,IF(avg(ht_rating.rating)!='',cast(avg(ht_rating.rating) as decimal(10,1)),0.0) as rating");
        $this->db->where('tutor.id', $tutorId);
        $this->db->join('qualifications', 'qualifications.id=tutor.qualification', 'left');
        $this->db->join('ht_rating', 'ht_rating.tutorId=tutor.id', 'left');
        $result = $this->db->get('tutor')->row_array(); 
        
        if (!empty($result)) {
            $data = array();
            unset($result['password']);
            $data = $result;
            $data['coachingDetail'] = $this->getCoachingDetail($tutorId);
            $data['availablity'] = $this->getTutorAvailablity($tutorId);
            $data['coachingLocation'] = $this->getCoachingLocation($tutorId);
            //$data['boardsClassesSubjects']= $this->getAllBoardsClassesSubjects();
            return $data;
        } else {
            return false;
        }
    }

    function commonFxn($result) {
        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
    }

    function getTutorAvailablity($tutorId) {
        $this->db->select("id,typeOfTution as typeOfTutionId ,premise as premiseId,(CASE  WHEN typeOfTution=1 THEN 'Any'
                              WHEN typeOfTution=2 THEN 'Individual'     
                              ELSE 'Group' END) as typeOfTution,                         
                            (CASE  WHEN premise=1 THEN 'Any'
                                  WHEN premise=2 THEN 'Parent'     
                                  ELSE 'Tutor' END) as premise,days, 
            DATE_FORMAT(startTime, '%l:%i %p') as startTime, DATE_FORMAT(endTime, '%l:%i %p') as endTime,case days
            when 'Mon' then 1
            when 'Tue' then 2
            when 'Wed' then 3
            when 'Thu' then 4
            when 'Fri' then 5
            when 'Sat' then 6
            when 'Sun' then 7
            end as day_nr");
        $this->db->where('fkTutorId', $tutorId);
        $this->db->where('isActive', 1);
        $this->db->order_by('day_nr,startTime');
        $result = $this->db->get('tutorAvailablity')->result_array();
        return $this->commonFxn($result);
    }

    function getCoachingDetail($tutorId) {
        $this->db->select("tutorCoachingDetail.id,tutorCoachingDetail.fkBoardId as boardId,board.name as boardName,tutorCoachingDetail.fkClassId as classId,class.Name as className,tutorCoachingDetail.fkSubjectId,tutorCoachingDetail.feesPerHour,tutorCoachingDetail.feesPerMonth");
        $this->db->where('tutorCoachingDetail.fkTutorId', $tutorId);
        $this->db->where('tutorCoachingDetail.isActive', 1);
        $this->db->join('board', 'tutorCoachingDetail.fkBoardId=board.id', 'left');
        $this->db->join('class', 'tutorCoachingDetail.fkClassId=class.id', 'left');
        $this->db->order_by('tutorCoachingDetail.fkClassId');
        $result = $this->db->get('tutorCoachingDetail')->result_array();
        if (!empty($result)) {
            $sendArray = array();
            foreach ($result as $data) {
                $fkSubjectId = $data['fkSubjectId'];
                unset($data['fkSubjectId']);
                $subjects = explode(',', $fkSubjectId);
                $sub = array();
                $subID = array();
                foreach ($subjects as $subject) {
                    $sub[] = $this->getSubjectName($subject);
                    $subID = $subject;
                }
                $sub = implode(',', $sub);
                $sendArray[$data['id']] = $data;
                $sendArray[$data['id']]['subjectName'] = $sub;
                $sendArray[$data['id']]['subjectID'] = $subID;
            }
            $sendArray = array_values($sendArray);
            return $this->commonFxn($sendArray);
        } else {
            return false;
        }
    }

    function getSubjectName($id) {
        $this->db->where('id', $id);
        $result = $this->db->get('subject')->row_array();
        return $result['name'];
    }

    function getCoachingLocation($tutorId) {
        $this->db->select("id,location,latitude,longitude,radius");
        $this->db->where('fkTutorId', $tutorId);
        $this->db->where('isActive', 1);
        $result = $this->db->get('tutorCoachingLocation')->result_array();
        return $this->commonFxn($result);
    }

    function updateTutorProfile($data) {
        $tabName = $data['tabName'];
        unset($data['tabName']);
        $this->db->where('id', $data['id']);
        $this->db->update($tabName, $data);
    }

    function deleteRecordFromTab($data) {
        $tabName = $data['tabName'];
        unset($data['tabName']);
        $this->db->where('id', $data['id']);
        if ($tabName == 2) {
            $this->db->update("tutorCoachingDetail", $data);
        } elseif ($tabName == 3) {
            $this->db->update("tutorAvailablity", $data);
        } elseif ($tabName == 4) {
            $this->db->update("tutorCoachingLocation", $data);
        }
    }

    function tutorSignUp($document) {
        $coachingDetails = $document['coachingDetail'];
        $availablitys = $document['availablity'];
        $coachingLocations = $document['coachingLocation'];
        unset($document['coachingDetail']);
        unset($document['availablity']);
        unset($document['coachingLocation']);
        if ($document['id'])/* Tutor Edit Profile Start */ {
            unset($document['createdDate']);
            unset($document['password']);
            unset($document['referalCode']);
            unset($document['referalBy']);
            unset($document['isActive']);
            $document['modifiedDate'] = date('Y-m-d H:i:s');
            $this->db->where('id', $document['id']);
            $this->db->update('tutor', $document);
             /**************Update push send**************************/
            $tutorInfo = $this->getTutorProfile($document['id']);
            $pushData = '{"type":40,"message":{"Name":"' . $tutorInfo['name'] . '"}}';           
            sendAndroidPush($tutorInfo['deviceToken'], $pushData, "", "", 40);            
            $smsData = 'Your profile is updated.' ;
            $ressmsData = sendPushAsSMS($tutorInfo['mobile1'], $smsData);
            /*****************End****************************/
            
            if (!empty($coachingDetails)) {
                $this->db->delete('tutorCoachingDetail', array('fkTutorId' => $document['id']));
                foreach ($coachingDetails as $coachingDetail) {
                    $data = array();
                    $data['fkTutorId'] = $document['id'];
                    $data['fkBoardId'] = $coachingDetail['board'];
                    $data['fkClassId'] = $coachingDetail['class'];
                    $data['fkSubjectId'] = $coachingDetail['subject'];
                    $data['feesPerHour'] = $coachingDetail['feesPerHour'];
                    $data['feesPerMonth'] = $coachingDetail['feesPerMonth'];
                    $data['isActive'] = 1;
                    $data['createdDate'] = date('Y-m-d H:i:s');
                    $this->db->insert('tutorCoachingDetail', $data);
                }
            }
            if (!empty($availablitys)) {
                $this->db->delete('tutorAvailablity', array('fkTutorId' => $document['id']));
                foreach ($availablitys as $availablity) {
                    $no_Of_days = explode(',', $availablity['days']);
                    if (count($no_Of_days) > 1) {
                        foreach ($no_Of_days as $day) {
                            $data = array();
                            $data['fkTutorId'] = $document['id'];
                            $data['typeOfTution'] = $availablity['typeOfTution'];
                            $data['premise'] = $availablity['premise'];
                            $data['days'] = $day;
                            $data['premise'] = $availablity['premise'];
                            $data['startTime'] = date('H:i:s', strtotime($availablity['startTime']));
                            $data['endTime'] = date('H:i:s', strtotime($availablity['endTime']));
                            $data['isActive'] = 1;
                            $data['createdDate'] = date('Y-m-d H:i:s');
                            $this->db->insert('tutorAvailablity', $data);
                        }
                    } else {
                        $data = array();
                        $data['fkTutorId'] = $document['id'];
                        $data['typeOfTution'] = $availablity['typeOfTution'];
                        $data['premise'] = $availablity['premise'];
                        $data['days'] = $availablity['days'];
                        $data['premise'] = $availablity['premise'];
                        $data['startTime'] = date('H:i:s', strtotime($availablity['startTime']));
                        $data['endTime'] = date('H:i:s', strtotime($availablity['endTime']));
                        $data['isActive'] = 1;
                        $data['createdDate'] = date('Y-m-d H:i:s');
                        $this->db->insert('tutorAvailablity', $data);
                    }
                }
            }
            if (!empty($coachingLocations)) {
                $this->db->delete('tutorCoachingLocation', array('fkTutorId' => $document['id']));
                foreach ($coachingLocations as $coachingLocation) {
                    $data = array();
                    $data['fkTutorId'] = $document['id'];
                    $data['location'] = $coachingLocation['location'];
                    $data['radius'] = $coachingLocation['radius'];
                    $data['latitude'] = $coachingLocation['latitude'];
                    $data['longitude'] = $coachingLocation['longitude'];
                    $data['isActive'] = 1;
                    $data['createdDate'] = date('Y-m-d H:i:s');
                    $this->db->insert('tutorCoachingLocation', $data);
                }
            }
        }/* Tutor Edit Profile End */ else/* Tutor Register Profile Start */ {
            $this->db->insert('tutor', $document);
            $insertId = $this->db->insert_id();
            /******************push notification****************/
            $TutorInfo = $this->getTutorProfile($insertId);
            $pushData = '{"type":20,"message":{"Name":"' . $TutorInfo['name'] . '""}}';           
            sendAndroidPush($TutorInfo['deviceToken'], $pushData, "", "", 20);
            
            $smsData = '"You have succesfully registered. Your profile is under process for approval."';            
            $ressmsData = sendPushAsSMS($TutorInfo['mobile1'], $smsData);
            /******************end********************************/
            
            if (!empty($coachingDetails)) {
                foreach ($coachingDetails as $coachingDetail) {
                    $data = array();
                    $data['fkTutorId'] = $insertId;
                    $data['fkBoardId'] = $coachingDetail['board'];
                    $data['fkClassId'] = $coachingDetail['class'];
                    $data['fkSubjectId'] = $coachingDetail['subject'];
                    $data['feesPerHour'] = $coachingDetail['feesPerHour'];
                    $data['feesPerMonth'] = $coachingDetail['feesPerMonth'];
                    $data['isActive'] = 1;
                    $data['createdDate'] = date('Y-m-d H:i:s');
                    $this->db->insert('tutorCoachingDetail', $data);
                }
            }
            if (!empty($availablitys)) {
                foreach ($availablitys as $availablity) {
                    $no_Of_days = explode(',', $availablity['days']);
                    if (count($no_Of_days) > 1) {
                        foreach ($no_Of_days as $day) {

                            $data = array();
                            $data['fkTutorId'] = $insertId;
                            $data['typeOfTution'] = $availablity['typeOfTution'];
                            $data['premise'] = $availablity['premise'];
                            $data['days'] = $day;
                            $data['premise'] = $availablity['premise'];
                            $data['startTime'] = date('H:i:s', strtotime($availablity['startTime']));
                            $data['endTime'] = date('H:i:s', strtotime($availablity['endTime']));
                            $data['isActive'] = 1;
                            $data['createdDate'] = date('Y-m-d H:i:s');
                            $this->db->insert('tutorAvailablity', $data);
                        }
                    } else {
                        $data = array();
                        $data['fkTutorId'] = $insertId;
                        $data['typeOfTution'] = $availablity['typeOfTution'];
                        $data['premise'] = $availablity['premise'];
                        $data['days'] = $availablity['days'];
                        $data['premise'] = $availablity['premise'];
                        $data['startTime'] = date('H:i:s', strtotime($availablity['startTime']));
                        $data['endTime'] = date('H:i:s', strtotime($availablity['endTime']));
                        $data['isActive'] = 1;
                        $data['createdDate'] = date('Y-m-d H:i:s');
                        $this->db->insert('tutorAvailablity', $data);
                    }
                }
            }
            if (!empty($coachingLocations)) {
                foreach ($coachingLocations as $coachingLocation) {
                    $data = array();
                    $data['fkTutorId'] = $insertId;
                    $data['location'] = $coachingLocation['location'];
                    $data['radius'] = $coachingLocation['radius'];
                    $data['latitude'] = $coachingLocation['latitude'];
                    $data['longitude'] = $coachingLocation['longitude'];
                    $data['isActive'] = 1;
                    $data['createdDate'] = date('Y-m-d H:i:s');
                    $this->db->insert('tutorCoachingLocation', $data);
                }
            }
            return $insertId;
        }/* Tutor Register Profile End */
    }

    function tutorLogin($document) {
        $this->db->where('password', $document['password']);
        $this->db->where("(email='" . $document['emailOrPhone'] . "' OR mobile1='" . $document['emailOrPhone'] . "')");
        $result = $this->db->get('tutor')->row_array();
        //echo $this->db->last_query();
        if (!empty($result)) {
            $data = array();
            $data['deviceToken'] = $document['deviceToken'];
            $data['deviceType'] = $document['deviceType'];
            $this->db->where('id', $result['id']);
            $this->db->update('tutor', $data);
            //echo $this->db->last_query();
            return array('tutorId' => $result['id']);
        } else {
            $this->db->where('email', $document['emailOrPhone']);
            $this->db->or_where('mobile1', $document['emailOrPhone']);
            $this->db->where('password!=', $document['password']);
            $result = $this->db->get('tutor')->row_array();
            if (!empty($result)) {
                return 2; //Wrong credentail.
            } else {
                return 3; //User not exist
            }
        }
    }

    function isEmailOrMobileAlreadyExist($emailOrMobile) {
        $this->db->where('email', $emailOrMobile);
        $this->db->or_where('mobile1', $emailOrMobile);
        $result = $this->db->get('tutor')->row_array();
        if (!empty($result)) {
            return $result['id'];
        } else {
            return false;
        }
    }

    function isEmailAlreadyExist($email) {
        $this->db->where('email', $email);
        $result = $this->db->get('tutor')->row_array();
        if (!empty($result)) {
            return true;
        } else {
            return false;
        }
    }

    function isMobileAlreadyExist($mobile) {
        $this->db->where('mobile1', $mobile);
        $result = $this->db->get('tutor')->row_array();
        if (!empty($result)) {
            return true;
        } else {
            return false;
//            $this->db->where('mobile2',$mobile);
//            $result = $this->db->get('tutor')->row_array();
//            if(!empty($result))
//            {
//               return true; 
//            }
//            else
//            {
//                return false;
//            }
        }
    }
    /***********************check Refer Code Exist or not************/
     function checkReferCode($referalBy) {
        $this->db->select('id');
        $this->db->where('referalCode', $referalBy);
        $Tresult = $this->db->get('ht_tutor')->row_array();  
        
        $this->db->select('id');
        $this->db->where('referalCode', $referalBy);
        $Presult = $this->db->get('ht_parent')->row_array();
        $result=count($Tresult)+count($Presult);
        if ($result>0) {
            return true;
        } else {
            return false;
        }
    }
   /*****************END*****************************************/
    
    /***********************Get Refer Data************/
     function checkReferCodeData($referalBy) {
        $this->db->select('id,name');
        $this->db->where('referalCode', $referalBy);
        $Tresult = $this->db->get('ht_tutor')->row_array();           
        $this->db->select('id,name');
        $this->db->where('referalCode', $referalBy);
        $Presult = $this->db->get('ht_parent')->row_array();
        $result=count($Tresult)+count($Presult);
        if ($result>0) {
           if(!empty($Tresult)){
            $Referdata=  $Tresult;
        }else{
            $Referdata=  $Presult;
        }
            return $Referdata;
        } else {
            return false;
        }
    }
   /*****************END*****************************************/
    
    function isGoogleIdExist($googleId) {
        $this->db->where('googleId', $googleId);
        $result = $this->db->get('tutor')->row_array();
        if (!empty($result)) {
            return $result['id'];
        } else {
            return false;
        }
    }

    function isFbIdExist($fbId) {
        $this->db->where('fbId', $fbId);
        $result = $this->db->get('tutor')->row_array();
        if (!empty($result)) {
            return $result['id'];
        } else {
            return false;
        }
    }

    function changePassword($data) {
        $this->db->where('id', $data['id']);
        $this->db->update('tutor', $data);
    }

    function getAllBoards() {
        $this->db->select('id as boardId,name as boardName');
        $this->db->where('isActive', 1);
        return $this->db->get('board')->result_array();
    }

    function getAllBoardsClassesSubjects() {
        $this->db->select('id as boardId,name as boardName');
        $this->db->where('isActive', 1);
        $results = $this->db->get('board')->result_array();
        if (!empty($results)) {
            $sendArray = array();
            foreach ($results as $data) {
                $sendArray[$data['boardId']]['boardId'] = $data['boardId'];
                $sendArray[$data['boardId']]['boardName'] = $data['boardName'];
                $classData = $this->getAllClassesByBoardId($data['boardId']);
                $sendArray[$data['boardId']]['classes'] = $classData;
            }
            $sendArray = array_values($sendArray);
            return $sendArray;
        }
    }

    function getAllClassesByBoardId($boardId) {
        $this->db->select('id as classId,name as className');
        $this->db->where('fkBoardId', $boardId);
        $results = $this->db->get('class')->result_array();
        if (!empty($results)) {
            $sendArray = array();
            foreach ($results as $data) {
                $sendArray[$data['classId']]['classId'] = $data['classId'];
                $sendArray[$data['classId']]['className'] = $data['className'];
                $subjectData = $this->getAllSubjectsByClassId($data['classId']);
                $sendArray[$data['classId']]['Subjects'] = $subjectData;
            }
            $sendArray = array_values($sendArray);
            return $sendArray;
        }
    }

    function getAllSubjectsByClassId($classId) {
        $this->db->select('id as subjectId,name as subjectName');
        $this->db->where('fkClassId', $classId);
        return $this->db->get('subject')->result_array();
    }

    function getAllStates() {
        $this->db->distinct();
        $this->db->select('state as stateName');
        return $this->db->get('cityState')->result_array();
    }

    function getAllCityByStateName($stateName) {
        $this->db->select('id as cityId,name as cityName');
        $this->db->like('state', $stateName, 'both');
        return $this->db->get('cityState')->result_array();
    }

    function getAllQualifications() {
        $this->db->select('id as qualificationId,name as qualificationName');
        return $this->db->get('qualifications')->result_array();
    }

    function getTabsData($document) {

        $this->db->distinct();
        $this->db->select("parentRequestTutor.id as parentRequestTutorId,parent_student.isRequestClosed,if(ht_parentRequestTutor.fkTutorId=ht_parent_student.closedExceptTutorId,1,0) as statusAccepted,"
                . "if(ht_parentRequestTutor.status=5,1,0) as statusReject,"
                . "parent_student.latitude,parent_student.longitude,parentRequestTutor.fkParentId as parentId,parent_student.name,"
                . "parentRequestTutor.fkParentRequestId as requestId,subject.name as subjectName,"
                . "parent.mobile1 as mobile,getParentClass(ht_parent_student.class) as className,"
                . "parent_student.location,parent_student.preferredTutionDays,"
                . "DATE_FORMAT(ht_parent_student.startTime, '%l:%i %p') as startTime,ht_parent_student.startTime as startClass,ht_parent_student.endTime as endClass,"
                . "(CASE  WHEN ht_parent_student.tutionPremises=1 THEN 'Any'
                          WHEN ht_parent_student.tutionPremises=2 THEN 'Parent'     
                          ELSE 'Tutor' END) as tutionPremises,"
                . "(CASE  WHEN ht_parentRequestTutor.status=0 THEN 'Move to In Process'
                          WHEN ht_parentRequestTutor.status=1 THEN 'Connected'     
                          WHEN ht_parentRequestTutor.status=2 THEN 'Demo Scheduled'     
                          WHEN ht_parentRequestTutor.status=3 THEN 'Demo Done' 
                          WHEN (ht_parentRequestTutor.status=4 and ht_parentRequestTutor.statusOffer=0) THEN 'Offer Pending' 
                          WHEN (ht_parentRequestTutor.status=4 and ht_parentRequestTutor.statusOffer=1) THEN 'Offer Accepted'                           
                          WHEN (ht_parentRequestTutor.status=4 and ht_parentRequestTutor.statusOffer=2) THEN 'Offer Rejected'                              
                          ELSE 'Request Rejected' END) as statusType,"
                . "(CASE  WHEN ht_parent_student.tutionType=1 THEN 'Any'
                          WHEN ht_parent_student.tutionType=2 THEN 'Individual'     
                          ELSE 'Group' END) as tutionType");
        if ($document['tabName'] == 1) {
            $this->db->where('parentRequestTutor.status', 0);
            $this->db->where('parentRequestTutor.isParentDeselectedTutor!=', 1);
        } elseif ($document['tabName'] == 2) {
            $this->db->where('parentRequestTutor.status!=', 0);
            $this->db->where('parentRequestTutor.statusOffer!=', 1);
        } elseif ($document['tabName'] == 3) {
            $this->db->where('parentRequestTutor.status', 4);
            $this->db->where('parentRequestTutor.statusOffer', 1);
        }
        $this->db->where('parentRequestTutor.fkTutorId', $document['id']);
        $this->db->where('parentRequestTutor.statusCancelOrDelete', 0);
        $this->db->join('parent', "parent.id=parentRequestTutor.fkParentId");
        $this->db->join('parent_student', "parent_student.id=parentRequestTutor.fkParentRequestId");
        $this->db->join('subject', 'subject.id=parent_student.subject');
        $this->db->order_by('parentRequestTutor.statusOffer');
        $this->db->order_by('statusReject');
        $this->db->order_by('parentRequestTutor.createdDate', 'desc');
        $result = $this->db->get('parentRequestTutor')->result_array();
        //echo $this->db->last_query();
        return $result;
    }

    function tutorOfferToParent($document) {
        $this->db->insert('offers', $document);
        return $this->db->insert_id();
    }

    function getReasonById($reasonId) {
        $this->db->where('id', $reasonId);
        return $this->db->get('reasons')->row_array();
    }

    function tutorUpdateStatus($document) {
        if ($document['status'] == 4) {
            $data = array();
            $data['fkRequestId'] = $document['statusData']['requestId'];
            $data['fkParentId'] = $document['statusData']['parentId'];
            $data['fkTutorId'] = $document['statusData']['tutorId'];
            $data['mode'] = $document['statusData']['mode'];
            $data['fees'] = $document['statusData']['fees'];
            $data['duration'] = $document['statusData']['duration'];
            $data['start'] = $document['statusData']['start'];
            $data['days'] = $document['statusData']['days'];
            $data['time'] = date('H:i:s', strtotime($document['statusData']['time']));
            $data['location'] = $document['statusData']['location'];
            $data['type'] = $document['statusData']['type'];
            $data['isActive'] = 1;
            $data['createdDate'] = date('Y-m-d H:i:s');
            $this->db->insert('offers', $data);
            $insert_id = $this->db->insert_id();
            $document['statusData'] = $insert_id;
            
           
            
        }
        $this->db->where('fkTutorId', $document['fkTutorId']);
        $this->db->where('fkParentId', $document['fkParentId']);
        $this->db->where('fkParentRequestId', $document['fkParentRequestId']);
        $this->db->where('id', $document['id']);
        $this->db->update('parentRequestTutor', $document);
        
        /********************Push notification***************/
       if ($this->db->affected_rows() && $document['status'] == 1) {//CONNECTED
            $tutorInfo = $this->getTutorProfile($document['fkTutorId']);
            $reasonInfo = $this->getReasonById($document['statusData']);
            //pre($reasonInfo);
            $this->load->model('parent_model');
            $obj = new parent_model;
            $result = $obj->getRequestDetail($document['fkParentRequestId']);
            $parentInfo = $obj->getParentProfile($document['fkParentId']);
            //parent
            $msg1 = '{"type":22,"message":{"tutorName":"' . $tutorInfo['name'] . '",'
                    . '"Class"     :"' . $result['className'] . '",'
                    . '"Subject"   :"' . $result['subjectName'] . '"}}';
            $msg = '" "' . $tutorInfo['name'] . '" has connected with you for your tuition request. Class : '
                    . 'Class   :"' . $result['className'] . '",'
                    . 'Subject :"' . $result['subjectName'] . '" "';
            sendAndroidPush($parentInfo['deviceToken'], $msg1, "", "", 22);
            $res = sendPushAsSMS($parentInfo['mobile1'], $msg);
          //  tutor
            $Tmsg1 = '{"type":23,"message":{"ParentName":"'.$parentInfo['name'].'",'
                    . '"Class"     :"' . $result['className'] . '",'
                    . '"Subject"   :"' . $result['subjectName'] . '"}}';
            $Tmsg = '" You have changed status of tuition request of"' . $parentInfo['name'] . '"for Class :  '
                    . 'Class   :"' . $result['className'] . '",'
                    . 'Subject :"' . $result['subjectName'] . '" to Connected."';
            
            sendAndroidPush($tutorInfo['deviceToken'], $Tmsg1, "", "", 23);
            sendPushAsSMS($tutorInfo['mobile1'], $Tmsg);
            if ($res['success']) {
                return 1;
            } else {
                return 0;
            }
        }
        
         elseif ($this->db->affected_rows() && $document['status'] == 2) {//Demo scheduled
             
            $data = array(); 
            
            $statusDatas=explode(" ",$document['statusData']);
            $time=$statusDatas[1].$statusDatas[2];           
            $data['start'] = $statusDatas[0];
            $data['time'] = $time;           
             
            $tutorInfo = $this->getTutorProfile($document['fkTutorId']);
            $reasonInfo = $this->getReasonById($document['statusData']);
            //pre($reasonInfo);
            $this->load->model('parent_model');
            $obj = new parent_model;
            $result = $obj->getRequestDetail($document['fkParentRequestId']);
            $parentInfo = $obj->getParentProfile($document['fkParentId']);
            //parent
            $msg1 = '{"type":24,"message":{"TutorName":"'.$tutorInfo['name'].'",'
                    . '"Class"     :"' . $result['className'] . '",'
                    . '"Subject"   :"' . $result['subjectName'] . '",'
                    . '"Date"       :"' . $data['start'] . '",'                
                    . '"Time"      :"' . $data['time'] . '"}}';
            $msg = '"'. $tutorInfo['name'] . '"has scheduled a demo at '
                    . 'Date/time:"' . $data['start'] . '"/"' . $data['time']  . '",'
                    . 'for your tuition request. Class :"' . $result['className'] . '",'
                    . 'Subject :"' . $result['subjectName'] . '".';
            
            sendAndroidPush($parentInfo['deviceToken'], $msg1, "", "", 24);
            $res = sendPushAsSMS($parentInfo['mobile1'], $msg);
          //  tutor
            $Tmsg1 = '{"type":25,"message":{"ParentName":"' . $parentInfo['name'] . '",'
                    . '"Class"     :"' . $result['className'] . '",'
                    . '"Subject"   :"' . $result['subjectName'] . '",'
                    . '"Date"       :"' . $data['start'] . '",'                
                    . '"Time"      :"' . $data['time'] . '"}}';
            $Tmsg = '"You have scheduled demo of tuition request by"' . $parentInfo['name'] . '"for '
                    . 'Class   :"' . $result['className'] . '",'
                    . 'Subject :"' . $result['subjectName'] . '" ,'
                    . 'at Date/time:"' .  $data['start']  . '"/"' . $data['time']  . '"."';
            sendAndroidPush($tutorInfo['deviceToken'], $Tmsg1, "", "", 25);
            sendPushAsSMS($tutorInfo['mobile1'], $Tmsg);
            if ($res['success']) {
                return 1;
            } else {
                return 0;
            }
        }
        
         elseif ($this->db->affected_rows() && $document['status'] == 3) {//Demo Done
             
            $data = array();          
            $statusDatas=explode(" ",$document['statusData']);
            $time=$statusDatas[1].$statusDatas[2];           
            $data['start'] = $statusDatas[0];
            $data['time'] = $time;     
             
            $tutorInfo = $this->getTutorProfile($document['fkTutorId']);
            $reasonInfo = $this->getReasonById($document['statusData']);
            //pre($reasonInfo);
            $this->load->model('parent_model');
            $obj = new parent_model;
            $result = $obj->getRequestDetail($document['fkParentRequestId']);
            $parentInfo = $obj->getParentProfile($document['fkParentId']);
            //parent
            $msg1 = '{"type":26,"message":{"TutorName":"' . $tutorInfo['name'] . '",'
                    . '"Class"     :"' . $result['className'] . '",'
                    . '"Subject"   :"' . $result['subjectName'] . '",'
                    . '"Date"       :"' . $data['start'] . '",'                
                    . '"Time"      :"' . $data['time'] . '"}}';
            $msg = '" "' . $tutorInfo['name'] . '"has completed a demo'
                    . 'at Date/time:"'. $data['start'] . '"/"' . $data['time'] . '",'
                    . 'for your tuition request. Class :"' . $result['className'] . '",'
                    . 'Subject :"' . $result['subjectName'] . '"."';
            sendAndroidPush($parentInfo['deviceToken'], $msg1, "", "", 26);
            $res = sendPushAsSMS($parentInfo['mobile1'], $msg);
          //  tutor
            $Tmsg1 = '{"type":27,"message":{"ParentName":"' . $parentInfo['name'] . '",'
                    . '"Class"     :"' . $result['className'] . '",'
                    . '"Subject"   :"' . $result['subjectName'] . '",'
                    . '"Date"       :"' . $data['start'] . '",'                
                    . '"Time"      :"' . $data['time'] . '"}}';
            $Tmsg = '"You have completed a demo of tuition request by"' . $parentInfo['name'] . '"for Class :  '
                    . 'Class   :"' . $result['className'] . '",'
                    . 'Subject :"' . $result['subjectName'] . '" ,'
                    . 'at Date/time:"' . $data['start'] . '"/"' . $data['time'] . '"."';
            sendAndroidPush($tutorInfo['deviceToken'], $Tmsg1, "", "", 27);
            sendPushAsSMS($tutorInfo['mobile1'], $Tmsg);
            
            if ($res['success']) {
                return 1;
            } else {
                return 0;
            }
        }
        
         elseif ($this->db->affected_rows() && $document['status'] == 4) {//offer
            $tutorInfo = $this->getTutorProfile($document['fkTutorId']);
            $reasonInfo = $this->getReasonById($document['statusData']);
            //pre($reasonInfo);
            $this->load->model('parent_model');
            $obj = new parent_model;
            $result = $obj->getRequestDetail($document['fkParentRequestId']);
            $parentInfo = $obj->getParentProfile($document['fkParentId']);
                  //parent
            $msg1 = '{"type":28,"message":{"TutorName":"'.$tutorInfo['name'].'",'
                    . '"Class"     :"' . $result['className'] . '",'
                    . '"Subject"   :"' . $result['subjectName'] . '"}}';
            $msg = '"Congratulations. You have received an offer for tuition from "' . $tutorInfo['name'] . '" '
                    . 'for your tuition request. Class :"' . $result['className'] . '",'
                    . 'Subject :"' . $result['subjectName'] . '"."';
            sendAndroidPush($parentInfo['deviceToken'], $msg1, "", "", 28);
            $res = sendPushAsSMS($parentInfo['mobile1'], $msg);
          //  tutor
            $Tmsg1 = '{"type":29,"message":{"ParentName":"' . $parentInfo['name'] . '"}}';
            $Tmsg = '"You have successfully offered your quotation for tuition request of"' . $parentInfo['name'] . '". Good Luck."';
            sendAndroidPush($tutorInfo['deviceToken'], $Tmsg1, "", "", 29);
            sendPushAsSMS($tutorInfo['mobile1'], $Tmsg);
            
            if ($res['success']) {
                return 1;
            } else {
                return 0;
            }
        }
        
         elseif ($this->db->affected_rows() && $document['status'] == 5) {//Reject
            $tutorInfo = $this->getTutorProfile($document['fkTutorId']);
            $reasonInfo = $this->getReasonById($document['statusData']);
            //pre($reasonInfo);
            $this->load->model('parent_model');
            $obj = new parent_model;
            $result = $obj->getRequestDetail($document['fkParentRequestId']);
            $parentInfo = $obj->getParentProfile($document['fkParentId']);
            //parent
            $msg1 = '{"type":30,"message":{"TutorName":"' . $tutorInfo['name'] . '"}}';
            $msg = '"Unfortunately, the tutor "' . $tutorInfo['name'] . '".will not be able to provide tuition as per your request. Please select other tutors from the search more option."';
            sendAndroidPush($parentInfo['deviceToken'], $msg1, "", "", 30);
            $res = sendPushAsSMS($parentInfo['mobile1'], $msg);
            //tutor
            $Tmsg1 = '{"type":31,"message":{"parentName":"' . $parentInfo['name'] . '",'
                    . '"Class"     :"' . $result['className'] . '",'
                    . '"Subject"   :"' . $result['subjectName'] . '"}}';
            $Tmsg = '"You have rejected tuition request of "' . $parentInfo['name'] . '" '                  
                    . 'for Class :"' . $result['className'] . '",'
                    . 'Subject :"' . $result['subjectName'] . '" We will contact you shortly to understand the reasons."';
            sendAndroidPush($tutorInfo['deviceToken'], $Tmsg1, "", "", 31);
            $res = sendPushAsSMS($tutorInfo['mobile1'], $Tmsg);
            
            if ($res['success']) {
                return 1;
            } else {
                return 0;
            }
        }        
        elseif ($this->db->affected_rows()) {
            return 1;
        } else {
            return 0;
        }
        
        /*********************End****************************/
        
        
        
      /*  if ($this->db->affected_rows() && $document['status'] == 5) {
            $tutorInfo = $this->getTutorProfile($document['fkTutorId']);
            $reasonInfo = $this->getReasonById($document['statusData']);
            //pre($reasonInfo);
            $this->load->model('parent_model');
            $obj = new parent_model;
            $result = $obj->getRequestDetail($document['fkParentRequestId']);
            $parentInfo = $obj->getParentProfile($document['fkParentId']);
            $msg1 = '{"type":2,"message":{"location":"' . $result['location'] . '",'
                    . '"Class"     :"' . $result['className'] . '",'
                    . '"Subject"   :"' . $result['subjectName'] . '",'
                    . '"Day"       :"' . $result['preferredTutionDays'] . '",'
                    . '"Reason"    :"' . $reasonInfo['name'] . '",'
                    . '"Time"      :"' . $result['startTime'] . '"}}';
            $msg = '"Your request for a tuition has been rejected by tutor due to reason:"' . $reasonInfo['name'] . '" . Request details: '
                    . 'location:"' . $result['location'] . '", '
                    . 'Class   :"' . $result['className'] . '",'
                    . 'Subject :"' . $result['subjectName'] . '" ,'
                    . 'Day/time:"' . $result['preferredTutionDays'] . '"/"' . $result['startTime'] . '"./n Please connect using Hometutor App"';
            sendAndroidPush($parentInfo['deviceToken'], $msg1, "", "", 2);
            $res = sendPushAsSMS($parentInfo['mobile1'], $msg);
            if ($res['success']) {
                return 1;
            } else {
                return 0;
            }
        } elseif ($this->db->affected_rows()) {
            return 1;
        } else {
            return 0;
        }*/
    }

    function getAllReasons($data) {
        $this->db->select("id as reasonId,name as reasonName");
        $this->db->where('reasonsType', $data['tabName']);
        $this->db->where('isParent', $data['isParent']);
        return $this->db->get('reasons')->result_array();
    }

    /*     * ***********************santosh(08-08-2016)************************************* */

    function getTopicNameById($data) {
        $this->db->select("topic_name");
        $this->db->where('id', $data);
        return $this->db->get('ht_topics')->row_array();
    }

    /* Get Topic name by id */

    public function insert_topic_in_db($data) {
        $result = $this->db->insert('ht_assignment_master', $data);
        return $result;
    }

    function tutorClassDone($document) {
        $student = $document['fkStudentID'];
        $topic = $document['topic'];
        $topicStatus = $document['topic_status'];
        unset($document['fkStudentID']);
        unset($document['topic']);
        unset($document['topic_status']);
        /* Tutor class done  Start */
        $no_student = explode(',', $student);
        if (count($no_student) > 0) {
            foreach ($no_student as $no_studentdata) {
                $document['fkStudentID'] = $no_studentdata;
                $classdocument['class_status'] = 1;
                $classdocument['modifiedDate'] = date("Y-m-d H:i:s");
                $this->db->where('id', $document['classScheduledID']);
                $this->db->update('ht_class_schedule', $classdocument);             
                $insertId = $document['classScheduledID'];

                /* start Push notification */

                $classScheduled = $this->getStudentScheduledById($insertId);
                //print_r($classScheduled);die;
                // for Parent notification
                if (!empty($topic)) {
                    $no_topic1 = explode(',', $topic);
                    if (count($no_topic1) > 0) {
                        foreach ($no_topic1 as $key => $topicData1) {
                            $TopicName = $this->getTopicNameById($topicData1);
                        }
                    }
                }

                $allTopic = implode(',', $TopicName);
                
                $msg1 = '{"type":14,"message":{"StudentName":"'. $classScheduled['studentName'] .'",'
                        . '"Subject"   :"' . $classScheduled['SubjectName'] . '",'
                        . '"Date"       :"' . $classScheduled['class_date'] . '",'
                        . '"Time"       :"' . $classScheduled['start_time'] . '",'
                        . '"Topic"      :"' . $allTopic . '"}}';
               $msg = '"Tuition session of "'. $classScheduled['studentName'] .'"'
                       . 'Subject   :"' . $classScheduled['SubjectName'] . '",'
                       . 'Date   :"' . $classScheduled['class_date']  . '",'
                       . 'Time   :"' . $classScheduled['start_time']  .'",is completed. Topics covered '     
                       . 'Topic :"' . $allTopic. '" ."';
               
                sendAndroidPush($classScheduled['parentDeviceToken'], $msg1, "", "", 14);
                sendPushAsSMS($classScheduled['parentMobile'], $msg);

                // for tutor notification
                $Tmsg2 = '{"type":15,"message":{"Name":"' . $classScheduled['studentName'] . '",'
                        . '"at Date"       :"' . $classScheduled['class_date'] . '",'
                        . '"Time"   :"' . $classScheduled['start_time'] . '"}}';
                $Tmsg = '"You have marked Tuition Session done for "'.$classScheduled['studentName'].'" at '
                        . 'Date  "' . $classScheduled['class_date'] . '",'
                        . 'Time  "' . $classScheduled['start_time'] .'". "';
                sendAndroidPush($classScheduled['TutorDeviceToken'], $Tmsg2, "", "", 15);
                sendPushAsSMS($classScheduled['TutorMobile'], $Tmsg); 
                /* End Push Notification */
            
                /* Tutor topic done  Start */
                if (!empty($topic)) {
                    $no_topic = explode(',', $topic);
                    $no_topicStatus = explode(',', $topicStatus);
                    if (count($no_topic) > 0) {
                        foreach ($no_topic as $key => $topicData) {

                            $data = array();
                            $data['enrolled_id'] = $insertId;
                            $data['topic'] = $topicData;
                            $data['fkTutorId'] = $document['fkTutorId'];
                            $data['fkParentID'] = $document['fkParentID'];
                            $data['fkStudentID'] = $document['fkStudentID'];
                            $data['subject_id'] = $document['subject_id'];
                            $data['topic_status'] = $no_topicStatus[$key];
                            //$data['createdDate'] = date('Y-m-d H:i:s');
                            $data['createdDate'] = $document['class_date'];
                            $res = $this->db->insert('ht_class_done_topic', $data);
                            //echo $this->db->last_query();die;
                            $checkTopicStatus = $this->checkTopicStatus($topicData);//check for complete class
                            $checkAssingment = $this->checkAssingment($data);//check for dublicate assgnment
                            if (!empty($checkTopicStatus) && $no_topicStatus[$key]==1 && empty($checkAssingment)) {
                                $dataAssig = array();
                                $dataAssig['topic_id'] = $topicData;
                                $dataAssig['student_id'] = $document['fkStudentID'];
                                $this->db->insert('ht_assignment_master', $dataAssig);
                            }
                        }
                    }
                }
            }
        }

        return $insertId;
        /* Tutor class done  End */
    }
    
    /*******************************get subject*************/
     function checkTopicStatus($data) {
        $this->db->select('*');
        $this->db->where('id', $data);
        $this->db->where('status', 0);
        $result = $this->db->get('ht_topics')->row_array();
        return $result;
    }
    /*******************************End******************/
    
     /*******************************get check Assingment dublicate*************/
     function checkAssingment($data) {
       
        $this->db->select('*');
        $this->db->where('student_id', $data['fkStudentID']);
        $this->db->where('topic_id', $data['topic']);
        $result = $this->db->get('ht_assignment_master')->row_array();
         // pre($result);die;
        return $result;
    }
    /*******************************End******************/

    function getSubjectTopic($data) {
        $this->db->select("*");
        $this->db->where('class_id', $data['class_id']);
        $this->db->where('subject_id', $data['subject_id']);
        return $this->db->get('ht_topics')->result_array();
    }

/* Tutor Topic End */

    function getEnrolledClass($data) {
        //$curdate = date("Y-m-d");
        $curdate = date("Y-m-d h:i:s"); 
        $this->db->select('parent_student.name,parent_student.location,parent_student.latitude,'
                . 'parent_student.longitude,parent_student.class classID,ht_subject.name as subjectName ,ht_class_schedule.*,'
                . 'DATE_FORMAT( ht_class_schedule.class_date, "%D %b") as classDate,'
                . 'CONCAT(ht_class_schedule.class_date," ",ht_class_schedule.start_time) as CheckDate,'
                . 'DATE_FORMAT( ht_class_schedule.start_time, "%h:%i %p") as start_time');
        $this->db->where('ht_class_schedule.fkTutorId', $data['fkTutorId']);
       // $this->db->where("ht_class_schedule.class_date>='$curdate'");
        $this->db->where("(ht_class_schedule.class_status=0");
        $this->db->or_where("ht_class_schedule.class_status=4)");
        $this->db->join('parent_student', 'ht_class_schedule.fkStudentID=parent_student.id');
        $this->db->join('ht_subject', 'ht_subject.id=ht_class_schedule.subject_id');
        $this->db->order_by('ht_class_schedule.class_date ASC');
       // $this->db->group_by('ht_class_schedule.fkStudentID');
        $this->db->order_by('ht_class_schedule.start_time ASC'); 
        $this->db->having("CheckDate>='" . $curdate . "'");
        $result=$this->db->get('ht_class_schedule')->result_array();
        $new_array = array();
      if(!empty($result)){ 
                 foreach($result as $val){
                     if(!in_array($val['fkStudentID'],$new_array)){
                         $new_array[] = $val['fkStudentID'];
                         $final[] = $val;
                     }
                 } 
                // pre($final);die;
       return $final;
      }else{
          return array();
      }
    }

/* Tutor enrolled class End */
    
    
    

    function getclassScheduledById($data) {
        $this->db->select('*');
        $this->db->where('id', $data);
        $result = $this->db->get('ht_class_schedule')->row_array();
        return $result;
    }

/* Get class Scheduled data by ID End */

    function getStudentScheduledById($data) {
        $this->db->select('ht_class_schedule.*,ht_tutor.name as TutorName,ht_tutor.mobile1 as TutorMobile,parent_student.name as studentName,ht_subject.name as SubjectName,ht_parent.deviceToken as parentDeviceToken,ht_parent.mobile1 as parentMobile,ht_tutor.deviceToken as TutorDeviceToken');
        $this->db->join('ht_tutor', "ht_tutor.id=ht_class_schedule.fkTutorId");
        $this->db->join('parent_student', "parent_student.id=ht_class_schedule.fkStudentID");
        $this->db->join('ht_parent', "ht_parent.id=ht_class_schedule.fkParentID");
        $this->db->join('ht_subject', "ht_subject.id=ht_class_schedule.subject_id");
        $this->db->where('ht_class_schedule.id', $data);
        $result = $this->db->get('ht_class_schedule')->row_array();
       // echo $this->db->last_query();die;
        return $result;
    }

    /* Get class Scheduled data by ID End */

    function tutorClassRescheduled($data) {
        // print_r($data);die;
        $classdocument['class_status'] = 2;
        $classdocument['status'] = $data['status'];
        $classdocument['modifiedDate'] = date("Y-m-d H:i:s");
        $this->db->where('id', $data['classScheduledID']);
        $classScheduledID = $data['classScheduledID'];
        $result = $this->db->update('ht_class_schedule', $classdocument);
        $classScheduled = $this->getclassScheduledById($data['classScheduledID']);

        if ($result) {
            $data = array(
                'fkParentID' => $classScheduled['fkParentID'],
                'fkStudentID' => $classScheduled['fkStudentID'],
                'fkTutorId' => $classScheduled['fkTutorId'],
                'class_date' => $data['class_date'],
                'start_time' => $data['start_time'],
                'class_duration' => $classScheduled['class_duration'],
                'subject_id' => $classScheduled['subject_id'],
                'className' => $classScheduled['className'],
                'boardName' => $classScheduled['boardName'],
                'created_date' => date("Y-m-d H:i:s")
            );

            $this->db->insert('ht_class_schedule', $data);

            // for Parent notification
            $notificationScheduled = $this->getStudentScheduledById($classScheduledID);  // push notification   

            $msg1 = '{"type":12,"message":{"Name":"' . $notificationScheduled['studentName'] . '",'
                    . '"Subject"   :"' . $notificationScheduled['SubjectName'] . '",'
                    . '"Date"       :"' . $notificationScheduled['class_date'] . '",'
                    . '"RescheduledDate"       :"' . $data['class_date'] . '",'
                    . '"RescheduledTime"       :"' . $data['start_time'] . '",'
                    . '"Tutor"      :"' . $notificationScheduled['TutorName'] . '"}}';
            
            $pmsg = 'Tuition session of ' . $notificationScheduled['studentName'] . '/'
                    . 'Subject '.  $notificationScheduled['SubjectName'] . '/'
                    . 'Date/time: ' . $notificationScheduled['class_date']  . '/' . $notificationScheduled['start_time'] . ''
                    . ' is rescheduled to  Date/time: ' . $data['class_date'] . '/' . $data['start_time'] . ''
                    . ' by ' . $notificationScheduled['TutorName']. '.';
            
            sendAndroidPush($notificationScheduled['parentDeviceToken'], $msg1, "", "", 12);
            sendPushAsSMS($notificationScheduled['parentMobile'], $pmsg);
            // for tutor notification
            $msg2 = '{"type":13,"message":{"Name":"' . $notificationScheduled['studentName'] . '",'
                    . '"atDate"     :"' . $notificationScheduled['class_date'] . '",'
                    . '"atTime"     :"' . $notificationScheduled['start_time'] . '",'
                    . '"ToDate"     :"' . $data['class_date'] . '",'
                    . '"ToTime"     :"' . $data['start_time'] . '"}}';
            
            $Tmsg = 'You have Rescheduled Tuition Session for ' . $notificationScheduled['studentName'] . ''
                    . ' at Date/time: ' . $notificationScheduled['class_date']  . '/' . $notificationScheduled['start_time'] . ''
                    . ' To Date/time: ' . $data['class_date'] . '/' . $data['start_time'] . '';
            
            sendAndroidPush($notificationScheduled['TutorDeviceToken'], $msg2, "", "", 13);
            sendPushAsSMS($notificationScheduled['TutorMobile'], $Tmsg);
        }
        return $result;
    }

/* Tutor enrolled Rescheduled class End */

    function tutorClassCancel($data) {
        $classdocument['class_status'] = 3;
        $classdocument['statusData2'] = $data['statusData2'];
        $classdocument['statusData1'] = 2;
        $classdocument['modifiedDate'] = date("Y-m-d H:i:s");
        $this->db->where('id', $data['classScheduledID']);
        $result = $this->db->update('ht_class_schedule', $classdocument);
        if ($result) {
            $classScheduled = $this->getStudentScheduledById($data['classScheduledID']);
            // for Parent notification

            $msg1 = '{"type":10,"message":{"StudentName":"' . $classScheduled['studentName'] . '",'
                    . '"Subject"   :"' . $classScheduled['SubjectName'] . '",'
                    . '"Date"       :"' . $classScheduled['class_date'] . '",'
                    . '"Tutor"      :"' . $classScheduled['TutorName'] . '"}}';
            
            $pmsg = 'Tuition session of ' . $classScheduled['studentName'] . '/'
                    . 'Subject '.  $classScheduled['SubjectName'] . '/'
                    . 'Date/time: ' . $classScheduled['class_date']  . '/' . $classScheduled['start_time'] . ''                 
                    . ' is Cancelled by ' . $classScheduled['TutorName']. '.';
           
            sendAndroidPush($classScheduled['parentDeviceToken'], $msg1, "", "", 10);
            sendPushAsSMS($classScheduled['parentMobile'], $pmsg);
            // for tutor notification
            $msg2 = '{"type":11,"message":{"StudentName":"' . $classScheduled['studentName'] . '",'
                    . '"Date"       :"' . $classScheduled['class_date'] . '",'
                    . '"Time"   :"' . $classScheduled['start_time'] . '"}}';
            
            $Tmsg = 'You have Cancelled Tuition Session for ' . $classScheduled['studentName'] . ''
                    . ' at Date/time: ' . $classScheduled['class_date']  . '/' . $classScheduled['start_time'] . '';
           
            sendAndroidPush($classScheduled['TutorDeviceToken'], $msg2, "", "", 11);
            sendPushAsSMS($classScheduled['TutorMobile'], $Tmsg);
        }
        return $result;
    }

/* Tutor enrolled Rescheduled class End */

    function myEnrolledClass($document) {
        $this->db->distinct();
        $this->db->select("parentRequestTutor.fkParentRequestId as requestId,parentRequestTutor.terminate_enrolled,parent_student.isRequestClosed,if(ht_parentRequestTutor.fkTutorId=ht_parent_student.closedExceptTutorId,1,0) as statusAccepted,"
                . "if(ht_parentRequestTutor.status=5,1,0) as statusReject,"
                . "parent_student.latitude,parent_student.longitude,parentRequestTutor.fkParentId as parentId,parent_student.name,"
                . "subject.name as subjectName,"
                . "parent.mobile1 as mobile,getParentClass(ht_parent_student.class) as className,ht_offers.days,"
                . "DATE_FORMAT(ht_offers.time, '%l:%i %p') as startTime,DATE_FORMAT(ht_offers.start, '%Y-%m %d') as startDate,ht_offers.fees,"
                . "(CASE  WHEN ht_offers.mode=0 THEN 'Monthly'                             
                          ELSE 'Hourly' END) as feePaymode,"
                . "(CASE  WHEN ht_parentRequestTutor.status=0 THEN 'Move to In Process'
                          WHEN ht_parentRequestTutor.status=1 THEN 'Connected'     
                          WHEN ht_parentRequestTutor.status=2 THEN 'Schedule Demo'     
                          WHEN ht_parentRequestTutor.status=3 THEN 'Demo Done' 
                          WHEN (ht_parentRequestTutor.status=4 and ht_parentRequestTutor.statusOffer=0) THEN 'Offer Pending' 
                          WHEN (ht_parentRequestTutor.status=4 and ht_parentRequestTutor.statusOffer=1) THEN 'Offer Accepted'                           
                          WHEN (ht_parentRequestTutor.status=4 and ht_parentRequestTutor.statusOffer=2) THEN 'Rejected'                              
                          ELSE 'Request Rejected' END) as statusType,"
                . "(CASE  WHEN ht_offers.location=1 THEN ht_tutorCoachingLocation.location
                          WHEN ht_offers.location=2 THEN ht_parent_student.location     
                          ELSE 'Other' END) as Location,"
                . "(CASE  WHEN ht_offers.type=2 THEN 'Individual'
                          WHEN ht_offers.type=3 THEN 'Group'     
                          ELSE 'Any' END) as tutionType");

        if ($document['tabName'] == 1) {
            $this->db->where('parentRequestTutor.status', 4);
            $this->db->where('parentRequestTutor.statusOffer', 1);
        } elseif ($document['tabName'] == 2) {
            $this->db->where('parentRequestTutor.status', 6);
            $this->db->where('parentRequestTutor.statusOffer', 1);
        }

        $this->db->where('parentRequestTutor.fkTutorId', $document['id']);
        $this->db->where('parentRequestTutor.statusCancelOrDelete', 0);
        $this->db->join('parent', "parent.id=parentRequestTutor.fkParentId");
        $this->db->join('parent_student', "parent_student.id=parentRequestTutor.fkParentRequestId");
        $this->db->join('subject', 'subject.id=parent_student.subject');
        $this->db->join('ht_offers', 'ht_offers.fkRequestId=parentRequestTutor.fkParentRequestId');
        $this->db->join('ht_tutorCoachingLocation', 'ht_tutorCoachingLocation.fkTutorId=parentRequestTutor.fkTutorId');
       

        $this->db->order_by('parent_student.name', 'ASC');
        $this->db->group_by('parentRequestTutor.fkParentRequestId');
        $result = $this->db->get('parentRequestTutor')->result_array();
        /***********************get status if delete enrolled by tutor*********/
   
         /**********************ENd**************************/
        $i=0;
        foreach( $result as $curdate){
        $result[$i]['newOfferDate'] = date('Y-m-d', mktime(0, 0, 0, date('m')+1, 1, date('Y')));
        $i++; }

        return $result;
    }

    function viewEnrolledClassHistory($document) {
        $result = array();
        $this->db->select("parent_student.id as studentId,"
                . "parent_student.name,subject.name as subjectName,"
                . "getParentClass(ht_parent_student.class) as className,"
                . "DATE_FORMAT(ht_offers.time, '%l:%i %p') as startTime,DATE_FORMAT(ht_offers.start, '%Y-%m %d') as startDate,ht_offers.days"
        );
        $this->db->where('parent_student.id', $document);
        $this->db->join('subject', 'subject.id=parent_student.subject');
        $this->db->join('ht_offers', 'ht_offers.fkRequestId=parent_student.id');
        $result = $this->db->get('parent_student')->row_array();
        //start complete hours
        $this->db->select("SUM(class_duration) as CompletedHours");
        $this->db->where('fkStudentID', $document);
        $this->db->where('class_status', 1);
        $result1 = $this->db->get('ht_class_schedule')->row_array();
        $result['CompletedHour'] = $result1['CompletedHours'];
        return $result;
    }

    public function get_assignment_detail($id) {
        $student_id = $id;
        $this->db->select('ht_assignment_master.*,(SELECT count(*) as total_count FROM ht_assignment_master_student where `assignment_id` = ht_assignment_master.id ) as count,
  (select topic_name from ht_topics where id = ht_assignment_master.topic_id) as topic_name,
  (select name from ht_parent_student where id = ht_assignment_master.student_id) as student_name,
  ht_subject.name as subject_name');
        $this->db->where('ht_assignment_master.student_id', $student_id);
        $this->db->from('ht_assignment_master');
        $this->db->join('ht_topics', 'ht_topics.id=ht_assignment_master.topic_id');
        $this->db->join('ht_subject', 'ht_topics.subject_id=ht_subject.id');
        $result = $this->db->get()->result_array();
        foreach ($result as $a) {
            if ($a['count'] > 0) {
                $result_data[] = $a;
            }
        }
        if (!empty($result_data)) {
            return $result_data;
        } else {
            return false;
        }
    }

    public function get_assignment_full_detail($id) {
        $this->db->where('ht_assignment_master_student.assignment_id', $id);
        $result = $this->db->get('ht_assignment_master_student')->result_array();
        if (!empty($result)) {
            return $result;
        } else {
            return false;
        }
    }

    /* Get student data by ID End */

    function sendMessagesBy($data) {
        $result = $this->db->insert('ht_message', $data);
        return $result;
    }

/* Send Message End */

    function getMessagesBy($data) {
        if ($data['isParent'] == 0) {
            $this->db->select('ht_message.id as messageId,ht_message.fkFromUserId as TutorId,ht_message.fkToStuId as StudentId, ht_parent_student.name as studentName,getParentClass(ht_parent_student.class) as className,ht_tutor.name,ht_tutor.profileImage,ht_tutor.mobile1 as Phone,ht_parent_student.location as Address,ht_message.messgae as Message,(YEAR(now())-ht_tutor.dob) as age,ht_subject.name as subject');
            $this->db->join('ht_tutor', "ht_tutor.id=ht_message.fkFromUserId");
            $this->db->join('ht_parent_student', "ht_parent_student.id=ht_message.fkToStuId");
            $this->db->join('ht_subject', "ht_subject.id=ht_parent_student.subject");
            $this->db->join('ht_tutorCoachingLocation', "ht_tutorCoachingLocation.fkTutorId=ht_message.fkFromUserId");
            $this->db->where('ht_message.fkToUserId', $data['fkToUserId']); //tutor send message to parent than find all Inbox message (Inbox for parent)
            $this->db->where('ht_message.isParent', 0);
            $this->db->where('ht_message.isdeleted', 0);
            $this->db->order_by('ht_message.id', 'DESC');
            $this->db->group_by('ht_message.fkToStuId ');
            $result['message'] = $this->db->get('ht_message')->result_array();
            // echo $this->db->last_query(); die;

            /**************start enrolled****************************************** */
            $this->db->distinct();
            $this->db->select("parentRequestTutor.fkTutorId as TutorId,parentRequestTutor.fkParentRequestId as StudentId,ht_parent_student.name as studentName,getParentClass(ht_parent_student.class) as className,ht_tutor.name,ht_tutor.profileImage,ht_tutor.mobile1 as Phone,ht_parent_student.location as Address,(YEAR(now())-ht_tutor.dob) as age,ht_subject.name as subject");
            $this->db->join('ht_tutor', "ht_tutor.id=parentRequestTutor.fkTutorId");
            $this->db->join('ht_parent_student', "ht_parent_student.id=parentRequestTutor.fkParentRequestId");
            $this->db->join('ht_subject', "ht_subject.id=ht_parent_student.subject");
            $this->db->join('ht_tutorCoachingLocation', "ht_tutorCoachingLocation.fkTutorId=parentRequestTutor.fkTutorId");
            $this->db->where('parentRequestTutor.fkParentId', $data['fkToUserId']);
            $this->db->where('parentRequestTutor.status', 4);
            $this->db->where('parentRequestTutor.statusOffer', 1);
            $this->db->where('parentRequestTutor.terminate_enrolled!=2');
            $results = $this->db->get('parentRequestTutor')->result_array();
            if (!empty($results)) {
                $result['enrolled'] = $results;
            }

            return $result;
        }
        if ($data['isParent'] == 1) {
            $this->db->select('ht_message.id as messageId,ht_message.fkFromUserId as ParentId,ht_message.fkToStuId as StudentId,ht_parent_student.name,getParentClass(ht_parent_student.class) as className,ht_parent.mobile1 as Phone,ht_parent.profileImage,ht_parent_student.location as Address,ht_message.messgae as Message,ht_subject.name as subject');
            $this->db->join('ht_parent', "ht_parent.id=ht_message.fkFromUserId");
            $this->db->join('ht_parent_student', "ht_parent_student.id=ht_message.fkToStuId");
            $this->db->join('ht_subject', "ht_subject.id=ht_parent_student.subject");
            $this->db->where('ht_message.fkToUserId', $data['fkToUserId']); //Parent send message to Tutor than find all Inbox message (Inbox for Tutor)
            $this->db->where('ht_message.isParent', 1);
            $this->db->where('ht_message.isdeleted', 0);
            $this->db->order_by('ht_message.id', 'DESC');
            $this->db->group_by('ht_message.fkToStuId ');
            $result['message'] = $this->db->get('ht_message')->result_array();
            /*************start enrolled****************************************** */
            $this->db->distinct();
            $this->db->select("parentRequestTutor.fkParentId as ParentId,parentRequestTutor.fkParentRequestId as StudentId,ht_parent_student.name,getParentClass(ht_parent_student.class) as className,ht_parent.profileImage,ht_parent.mobile1 as Phone,ht_parent_student.location as Address,ht_subject.name as subject");
            $this->db->join('ht_parent', "ht_parent.id=parentRequestTutor.fkParentId");
            $this->db->join('ht_parent_student', "ht_parent_student.id=parentRequestTutor.fkParentRequestId");
            $this->db->join('ht_subject', "ht_subject.id=ht_parent_student.subject");
            $this->db->where('parentRequestTutor.fkTutorId', $data['fkToUserId']);
            $this->db->where('parentRequestTutor.status', 4);
            $this->db->where('parentRequestTutor.statusOffer', 1);
            $this->db->where('parentRequestTutor.terminate_enrolled!=2');
            $results = $this->db->get('parentRequestTutor')->result_array();
            if (!empty($results)) {
                $result['enrolled'] = $results;
            }

            return $result;
        }
    }

/* Get Inbox Message End  */

    function getSendMessagesBy($data) {
        if ($data['isParent'] == 1) {
            $this->db->select('ht_message.id as messageId,ht_message.fkToUserId as TutorId,ht_message.fkToStuId as StudentId,ht_tutor.name,ht_tutor.profileImage,,ht_tutor.mobile1 as Phone,ht_parent_student.location as Address,ht_message.messgae as Message,(YEAR(now())-ht_tutor.dob) as age,ht_subject.name as subject');
            $this->db->join('ht_tutor', "ht_tutor.id=ht_message.fkToUserId");
            $this->db->join('ht_parent_student', "ht_parent_student.id=ht_message.fkToStuId");
            $this->db->join('ht_subject', "ht_subject.id=ht_parent_student.subject");
            $this->db->where('ht_message.fkFromUserId', $data['fkFromUserId']); //tutor send message to parent than find all send message tutor(Send box for parent)
            $this->db->where('ht_message.isParent', 1);
            $this->db->where('ht_message.isdeleted', 0);
            $this->db->order_by('ht_message.id', 'DESC');
            $this->db->group_by('ht_message.fkToStuId ');
            $result['message'] = $this->db->get('ht_message')->result_array();
            return $result;
        }
        if ($data['isParent'] == 0) {
            $this->db->select('ht_message.id as messageId,ht_message.fkToUserId as ParentId,ht_message.fkToStuId as StudentId,ht_parent_student.name,ht_parent.mobile1 as Phone,ht_parent.profileImage,ht_parent_student.location as Address,ht_message.messgae as Message,ht_subject.name as subject');
            $this->db->join('ht_parent', "ht_parent.id=ht_message.fkToUserId");
            $this->db->join('ht_parent_student', "ht_parent_student.id=ht_message.fkToStuId");
            $this->db->join('ht_subject', "ht_subject.id=ht_parent_student.subject");
            $this->db->where('ht_message.fkFromUserId', $data['fkFromUserId']); //parent send message to Tutor than find all send message parent(Send box for Tutor)
            $this->db->where('ht_message.isParent', 0);
            $this->db->where('ht_message.isdeleted', 0);
            $this->db->order_by('ht_message.id', 'DESC');
            $this->db->group_by('ht_message.fkToStuId ');
            $result['message'] = $this->db->get('ht_message')->result_array();

            return $result;
        }
    }

/* Get Send Message End  */

    function deletedMessages($data) {

        $document['isdeleted'] = 1;
        $this->db->where('fkFromUserId', $data['fkFromUserId']);
        $this->db->where('isParent', $data['isParent']);
        $result = $this->db->update('ht_message', $document);
        return $result;
    }

/* Send Message End */

    function sendPushOfflineMessage($data) {
        
        $this->load->model('parent_model');
        $obj = new parent_model;
        if ($data['isParent'] == 1) {
            //  Notification Send by Parent               
            $parentInfo = $obj->getParentProfile($data['fkFromUserId']);
            $studentInfo = $obj->getRequestDetail($data['fkToStuId']);
            $tutorInfo = $this->getTutorProfile($data['fkToUserId']);

            $msg1 = '{"type":"17","chatID":"' . $data['chatID'] . '","chatID2":"' . $data['chatID2'] . '","StudentName":"' . $studentInfo['name'] . '",,"ParentName":"' . $parentInfo['name'] . '","ParentID":"' . $parentInfo['id'] . '","image":"' . $parentInfo['profileImage'] . '","studentid":"' . $data['fkToStuId'] . '","message":{"name":"' . $parentInfo['name'] . '"}}';
            $result = sendAndroidPush($tutorInfo['deviceToken'], $msg1, "", "", 17);
        }
        if ($data['isParent'] == 0) {
            //  Notification Send by tutor
            $parentInfo = $obj->getParentProfile($data['fkToUserId']);
            $studentInfo = $obj->getRequestDetail($data['fkToStuId']);
            $tutorInfo = $this->getTutorProfile($data['fkFromUserId']);
           $msg2 = '{"type":18,"chatID":"' . $data['chatID'] . '","chatID2":"' . $data['chatID2'] . '","StudentName":"' . $studentInfo['name'] . '","TutorName":"' . $tutorInfo['name'] . '","TutorID":"' . $tutorInfo['id'] . '","image":"' . $tutorInfo['profileImage'] . '","studentid":"' . $data['fkToStuId'] . '","message":{"name":"' . $tutorInfo['name'] . '"}}';
            $result = sendAndroidPush($parentInfo['deviceToken'], $msg2, "", "", 18);
        }
        return true;
    }

/* Tutor enrolled Rescheduled class End */

    function deletEnrolledClassTutor($data) {
        $data['class_status'] = 4;
        $data['statusData1'] = 2;
        $this->db->where('fkParentID', $data['fkParentID']);
        $this->db->where('fkStudentID', $data['fkStudentID']);
        $this->db->where('fkTutorId', $data['fkTutorId']);
        $this->db->where('class_status', 0);
        $result = $this->db->update('ht_class_schedule', $data);
        
        $enrolleddata['terminate_enrolled'] = 1;       
        $this->db->where('fkParentId', $data['fkParentID']);
        $this->db->where('fkParentRequestId', $data['fkStudentID']);
        $this->db->where('fkTutorId', $data['fkTutorId']);
        $enrooledresult = $this->db->update('ht_parentRequestTutor', $enrolleddata);

        // for tutor notification
        $tutorInfo = $this->getTutorProfile($data['fkTutorId']);
        $this->load->model('parent_model');
        $obj = new parent_model;
        $studentInfo = $obj->getRequestDetail($data['fkStudentID']);
        $parentInfo = $obj->getParentProfile($data['fkParentID']);
            $msg1 = '{"type":19,"message":{"StudentName":"' . $studentInfo['name'] . '",'
                . '"Class":"' . $studentInfo['className'] . '",'
                . '"Subject":"' . $studentInfo['subjectName'] . '"}}';
        
            $Tmsg = 'You have sent a request for termination of tuition enrollment of '. $studentInfo['name'] . ' '
                . 'Class ' . $studentInfo['className'] . ' '
                . 'Subject ' . $studentInfo['subjectName'] . ' We will contact you shortly for further processing.';
     
        sendAndroidPush($tutorInfo['deviceToken'], $msg1, "", "", 19);
        sendPushAsSMS($tutorInfo['mobile1'], $Tmsg);
        return $result;
    }

    /* Tutor delete current enrolled class End */

    function deletPreEnrolledClassTutor($data) {
        $updatedata['status'] = 7;
        $this->db->where('fkParentId', $data['fkParentID']);
        $this->db->where('fkParentRequestId', $data['fkStudentID']);
        $this->db->where('fkTutorId', $data['fkTutorId']);
        $result = $this->db->update('ht_parentRequestTutor', $updatedata);
        return $result;
    }

    /* Tutor delete enrolled class End */

  /*********************update offer by mycurrent enrolled*************/
     function getPstudentOffer($document) {  
        $this->db->select('*');
        $this->db->where('fkRequestId',$document);
        $result =  $this->db->get('offers')->row_array();      
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    function getPstudent($data) {
        $result = array();
        $this->db->select("*");
         $this->db->where("id",$data);
        $result = $this->db->get('ht_parent_student')->row_array();       
        return $result;
    }
    
     function getRequestStudent($data) {
        $result = array();
        $this->db->select("*");
        $this->db->where("fkParentId",$data['fkParentId']);
        $this->db->where("fkTutorId",$data['fkTutorId']);
        $this->db->where("fkParentRequestId",$data['fkRequestId']);
        $result = $this->db->get('ht_parentRequestTutor')->row_array();       
        return $result;
    }
    
     function getNewRequestStudent($data) {
        $result = array();
        $this->db->select("*");      
        $this->db->where("fkParentRequestId",$data);
        $result = $this->db->get('ht_parentRequestTutor')->row_array();       
        return $result;
    }
    
    function updateOfferEnrolledTutor($document) {
        $this->db->where('fkParentId', $document['fkParentId']);
        $this->db->where('fkTutorId', $document['fkTutorId']);
        $this->db->where('fkRequestId', $document['fkRequestId']);
        $result = $this->db->update('offers', $document);
       //echo $this->db->last_query();die;
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }
/*********************End Update offer by mycurrent enrolled*************/
    
    /********************Auto Terminate Offer Update************************/
    function getUpdateRequest() {       
        $this->db->select("*");
        $this->db->where("status=4");
        $this->db->where("statusOffer=1");
        $this->db->where("update_enrolled!=0");
        $result = $this->db->get('ht_parentRequestTutor')->result_array();    
        //echo $this->db->last_query();die;
        return $result;
    }
    
      function getstudentOffer($document) {  
        $this->db->select('*');
        $this->db->where('fkRequestId',$document['fkParentRequestId']);
        $this->db->where('fkParentId',$document['fkParentId']);
        $this->db->where('fkTutorId',$document['fkTutorId']);
        $result =  $this->db->get('offers')->row_array();      
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }
    /*************************End*******************************************/
    
    /******************************Reoffer by tutor******************************/
     function reOfferEnrolledTutor($document) {  
        $document['isAccepted'] = 0;
        $this->db->where('fkRequestId',$document['fkRequestId']);
        $this->db->where('fkParentId',$document['fkParentId']);
        $this->db->where('fkTutorId',$document['fkTutorId']);
        $this->db->where('isAccepted!=1');
        $result =  $this->db->update('ht_offers',$document);
       
        if ($this->db->affected_rows()) {
        $this->db->where('fkParentRequestId',$document['fkRequestId']);
        $this->db->where('fkParentId',$document['fkParentId']);
        $this->db->where('fkTutorId',$document['fkTutorId']);    
        $documentReq['statusOffer'] = 0;
        $result1 =  $this->db->update('ht_parentRequestTutor',$documentReq);  
            return true;
        } else {
            return false;
        }
    }

    /*******************************End**********************************/

     function getSchemeRefer($data) {
        $result = array();
        $this->db->select("id,price,scheme");
        $result['scheme'] = $this->db->get('ht_scheme')->row_array();
        if($data['reffer']==0){
        $tutorInfo = $this->getTutorProfile($data['fkFromTutorId']);//for get refferal code
        $result['ReferalCodeFrom'] = $tutorInfo['referalCode'];
        }else
        {
        $this->load->model('parent_model');    
        $parentInfo = $this->parent_model->getParentProfile($data['fkFromTutorId']);//for get refferal code  
        $result['ReferalCodeFrom'] = $parentInfo['referalCode'];
        }
        
        return $result;
    }
    /*********************for push when referal code used check email tutor*********/
     function getRefferPrice() {
        $result = array();
        $this->db->select("id,price,scheme");
        $result = $this->db->get('ht_scheme')->row_array();
        return $result;
    }
     /***************************Start getTabsDataCount************************************** */
    function getTabsDataCount($document) {
    for($tab=1;$tab<=3;$tab++){//echo $tab;die('gg');
        $this->db->distinct();
        $this->db->select("parentRequestTutor.id as parentRequestTutorId,parent_student.isRequestClosed,if(ht_parentRequestTutor.fkTutorId=ht_parent_student.closedExceptTutorId,1,0) as statusAccepted,"
                . "if(ht_parentRequestTutor.status=5,1,0) as statusReject,"
                . "parent_student.latitude,parent_student.longitude,parentRequestTutor.fkParentId as parentId,parent_student.name,"
                . "parentRequestTutor.fkParentRequestId as requestId,subject.name as subjectName,"
                . "parent.mobile1 as mobile,getParentClass(ht_parent_student.class) as className");
        if ($tab == 1) {
            $this->db->where('parentRequestTutor.status', 0);
            $this->db->where('parentRequestTutor.isParentDeselectedTutor!=', 1);
        } elseif ($tab == 2) {
            $this->db->where('parentRequestTutor.status!=', 0);
            $this->db->where('parentRequestTutor.statusOffer!=', 1);
        } elseif ($tab == 3) {
            $this->db->where('parentRequestTutor.status', 4);
            $this->db->where('parentRequestTutor.statusOffer', 1);
        }
        $this->db->where('parentRequestTutor.fkTutorId', $document['id']);
        $this->db->where('parentRequestTutor.statusCancelOrDelete', 0);
        $this->db->join('parent', "parent.id=parentRequestTutor.fkParentId");
        $this->db->join('parent_student', "parent_student.id=parentRequestTutor.fkParentRequestId");
        $this->db->join('subject', 'subject.id=parent_student.subject');
        $this->db->order_by('parentRequestTutor.statusOffer');
        $this->db->order_by('statusReject');
        $this->db->order_by('parentRequestTutor.createdDate', 'desc');
        $result = $this->db->get('parentRequestTutor')->result_array();
       // echo $this->db->last_query();
          if ($tab == 1) {
           $total['Request']=  count($result);
                     // print_r($total);die('gg');
        } elseif ($tab == 2) {
            $total['offer']=  count($result);
        } elseif ($tab == 3) {
           $total['enrollement']=  count($result);
        }
       
    }
        return $total;
    }
  /***************************End getTabsDataCount************************************** */
    
    /***************************Start sms Send Assignment Complete************************************** */
    function smsSendAssignmentCom($document) {
        // for tutor notification
            $tutorInfo = $this->getTutorProfile($document['fkTutorId']);       
                $msg1 = '{"type":47,"message":{"Assignment":"' . $document['Assignment'] . '",'
                   . '"Attempt":"' . $document['Attempt'] . '",'
                   . '"Date_Time":"' . $document['Date_Time'] . '",'
                   . '"Time_taken":"' . $document['Time_taken'] . '",'
                   . '"Correct_Answer_per":"' . $document['Correct_Answer_per'] . '",'
                   . '"Total_Questions":"' . $document['Total_Questions'] . '",'
                   . '"Attempted_Questions":"' . $document['Attempted_Questions'] . '",'                   
                   . '"Correct_Answers":"' . $document['Correct_Answers'] . '""}}';
        
                $Tmsg = 'Hi,Please check my result.'
                    .PHP_EOL.'Assignment : ' . $document['Assignment'] . ''
                    .PHP_EOL. 'Attempt : ' . $document['Attempt'] . ''
                    .PHP_EOL. 'Date/Time : ' . $document['Date_Time'] . ''
                    .PHP_EOL. 'Time taken : ' . $document['Time_taken'] . ''
                    .PHP_EOL. '% age Correct Answer : ' . $document['Correct_Answer_per'] . ''
                    .PHP_EOL. 'Total Questions : ' . $document['Total_Questions'] . ''
                    .PHP_EOL. 'Attempted Questions : ' . $document['Attempted_Questions'] . ''
                    .PHP_EOL. 'Correct Answers : ' . $document['Correct_Answers'] . '';              
                
   
           sendAndroidPush($tutorInfo['deviceToken'], $msg1, "", "", 47);
           sendPushAsSMS($tutorInfo['mobile1'], $Tmsg);
        return true;
    }
  /***************************End sms Send Assignment Complete*************************************** */


}
