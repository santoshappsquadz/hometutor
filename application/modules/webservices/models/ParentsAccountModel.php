<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Parent_model
 *
 * @author appsquadz
 */
class ParentsAccountModel extends CI_Model 
{
   function __construct()
    {
        parent::__construct();   
    }
    
    
           function getClassesAmountDetails($document)       
    { 

        $this->db->distinct();
        $this->db->select('ht_tutorPaymentTotal.*,ht_tutor.profileImage,ht_tutor.gender,ht_tutor.dob,ht_tutor.name as tutorName,ht_parent_student.name as studentName' );                         
        $this->db->join('ht_tutor','ht_tutor.id=ht_tutorPaymentTotal.fkTutorId'); 
        // $this->db->join('ht_tutorCoachingDetail','ht_tutorCoachingDetail.fkTutorId=ht_tutorPaymentTotal.fkTutorId','right'); 
        $this->db->join('ht_parent_student','ht_parent_student.id=ht_tutorPaymentTotal.fkStudentId');                          
        $this->db->where('ht_tutorPaymentTotal.fkParentId', $document['parentId']);
        $result = $this->db->get('ht_tutorPaymentTotal')->result_array();
        $count =count( $result);
      // pre($result); die();
        
       // for($i=0;$i<=$count;$i++)
        foreach($result as $res)
        {
		$res['age'] =  date('Y')-$res['dob'];
	    $res['dueDate'] = "2016-12-31";
            $tutorId = $res['fkTutorId'];
            $subjectId = $res['fkSubjectId'];
            $this->db->select('ht_tutorCoachingDetail.feesPerHour,ht_tutorCoachingDetail.feesPerMonth,ht_subject.name as subjectName' );
	    $this->db->join('ht_subject','ht_subject.id=ht_tutorCoachingDetail.fkSubjectId');
            $this->db->where('fkTutorId', $tutorId);
            $this->db->where('fkSubjectId', $subjectId);
            $result1 = $this->db->get('ht_tutorCoachingDetail')->row_array();
            $res['feePerHour'] = $result1['feesPerHour'];
            $res['feesPerMonth'] = $result1['feesPerMonth'];
	    $res['subjectName'] = $result1['subjectName'];
	    $res['incentive'] = 10;
	   
            $resultFinal[] = $res;
        }
      
        if($result)
        {          
           return $resultFinal ;
        }
        else
        {
           return false; 
        }
}

             function updateTutorAmountAfterPayment($document)       
    {
 
        $this->db->where('fkParentId', $document['parentId']);
        $this->db->where('fkTutorId', $document['tutorId']);
        $this->db->where('fkStudentId', $document['studentId']);
        $this->db->where('fkSubjectId', $document['fkSubjectId']);
        $result = $this->db->get('ht_tutorPaymentTotal')->row_array();
        $amount = $result['totalAmount'];
        
        $updatedAmount = $amount - $document['amount'];
        
        
        $this->db->set('totalAmount', $updatedAmount);
        $this->db->where('fkParentId', $document['parentId']);
        $this->db->where('fkTutorId', $document['tutorId']);
        $this->db->where('fkStudentId', $document['studentId']);
         $this->db->where('fkSubjectId', $document['fkSubjectId']);
        $updateQuery = $this->db->update('ht_tutorPaymentTotal');
        
        $document1['amount'] = $document['amount'];
        $document1['date'] = $document['date'];
        $document1['particulars'] = $document['particulars'];
        $document1['fkParentId'] = $document['parentId'];
        $document1['fkTutorId'] = $document['tutorId'];
        $document1['fkStudentId'] = $document['studentId'];
        $document1['fkSubjectId'] = $document['fkSubjectId'];
        $this->db->insert('ht_parentPaymentRecord', $document1);
       // $id = $this->db->insert_id();

        if($updateQuery)
        {          
           return true ;
        }
        else
        {
           return false; 
        }
}



     
     function getAccountDetails($document)       
    {

        //$this->db->distinct();
       // $this->db->select("fkTutorId");
        $this->db->select('ht_tutor.name as tutorName,ht_parentPaymentRecord.date,ht_parentPaymentRecord.particulars,ht_parentPaymentRecord.fkSubjectId,ht_parentPaymentRecord.fkStudentId,ht_parentPaymentRecord.fkTutorId,ht_parentPaymentRecord.amount,ht_parent_student.name as studentName,ht_subject.name as subjectName' );                         
       // $this->db->select('ht_parentPaymentRecord.fkTutorId,ht_tutor.name as tutorName,ht_parentPaymentRecord.fkParentId' );                         
        $this->db->join('ht_tutor','ht_tutor.id=ht_parentPaymentRecord.fkTutorId'); 
        $this->db->join('ht_parent_student','ht_parent_student.id=ht_parentPaymentRecord.fkStudentId');
	$this->db->join('ht_subject','ht_subject.id=ht_parentPaymentRecord.fkSubjectId');
        $this->db->where('ht_parentPaymentRecord.fkParentId', $document['parentId']);
       // $this->db->where('ht_parentPaymentRecord.fkTutorId', $document['tutorId']);
        $result = $this->db->get('ht_parentPaymentRecord')->result_array();
       
        if($result)
        {          
           return $result ;
        }
        else
        {
           return false; 
        }
}

    
    
}
