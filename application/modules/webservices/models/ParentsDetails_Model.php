<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Parent_model
 *
 * @author appsquadz
 */
class ParentsDetails_Model extends CI_Model 
{
   function __construct()
    {
        parent::__construct();   
    }
    
    
           function getClassesAmountDetails($document)       
    {    



        $last_day_this_month  = date('Y-m-t'); //echo $last_day_this_month; die();

	
	
    
        $this->db->select('ht_ParentDuePayment.*,ht_tutor.profileImage,ht_tutor.gender,ht_tutor.dob,ht_tutor.name as tutorName,ht_parent_student.name as studentName' );                         
        $this->db->join('ht_tutor','ht_tutor.id=ht_ParentDuePayment.fkTutorId'); 
        // $this->db->join('ht_tutorCoachingDetail','ht_tutorCoachingDetail.fkTutorId=ht_tutorPaymentTotal.fkTutorId','right'); 
        $this->db->join('ht_parent_student','ht_parent_student.id=ht_ParentDuePayment.fkStudentId');                          
        $this->db->where('ht_ParentDuePayment.fkParentId', $document['parentId']);
        $result = $this->db->get('ht_ParentDuePayment')->result_array();
	//pre($result); die();        
	$count =count( $result);
       
        foreach($result as $res)
        {
            
	    $this->db->where('id', $res['fkSubjectId']);
	    $resultSub = $this->db->get('ht_subject')->row_array(); 


            $res['age'] =  date('Y')-$res['dob'];
	    $monthEnrolledDays = $res['Number_class']; //echo $monthEnrolledDays; die();
            $tutorId = $res['fkTutorId'];
            $subjectId = $res['fkSubjectId'];
            $studentId = $res['fkStudentId'];
            $parentId = $res['fkParentId'];
            $dueDate = $res['dueDate'];
	    $Mode = $res['paymentMode']; 
          
            $this->db->where('fkTutorId', $tutorId);
	    $this->db->where('fkParentId', $parentId);
	    $this->db->where('fkRequestId',  $studentId);
	    // $this->db->where('isAccepted',  1);
	   
            $result1 = $this->db->get('ht_offers')->row_array(); 
	 


	    $this->db->where('fkTutorId', $tutorId);
	    $this->db->where('fkParentId', $parentId);
	    $this->db->where('fkParentRequestId',  $studentId);
	    $this->db->where('status',  6);
	    $resultTerminationCheck = $this->db->get('ht_parentRequestTutor')->row_array();

	    if(empty($resultTerminationCheck))
	    {
           //////////////////per hour days calculation//////////////////////
	     $last_day_of_due_month = date('Y-m-t', strtotime($dueDate)); //echo $last_day_of_due_month; die();
            $this->db->where('fkTutorId', $tutorId);
            $this->db->where('fkStudentID', $studentId);
            $this->db->where('fkParentID',  $parentId);
             $this->db->where('subject_id',  $subjectId);
	     $this->db->where('class_status !=',  2);
            if($res['paymentMode'] == 1){ $this->db->where('class_date >', $dueDate);}else{$this->db->where('class_date >=', $dueDate);} //echo $dueDate; die();
	    $this->db->where('class_date <=', $last_day_of_due_month);
            $resultPerHourDays = $this->db->get('ht_class_schedule')->result_array();
           // $q =$this->db->last_query(); echo $q; die();
            $countPerHourDays = count( $resultPerHourDays); //echo $countPerHourDays; die();
         
        ///////////////////Total Months - Monthly Case////////////////////////    
      /*      $this->db->where('fkTutorId', $tutorId);
            $this->db->where('fkStudentID', $studentId);
            $this->db->where('fkParentID',  $parentId);
            $this->db->where('subject_id',  $subjectId);
            $this->db->where('class_date >=', $dueDate);
            $this->db->order_by("id","desc");
            $this->db->limit(1);
            $classLastDate = $this->db->get('ht_class_schedule')->row_array(); //pre($classLastDate); die();
            $enrolledLastDate = $classLastDate['class_date']; //echo $enrolledLastDate; die();
            $dueDate = $res['dueDate']; //echo $dueDate; die();
            
            $d1 = new DateTime($dueDate);
            $d2 = new DateTime($enrolledLastDate);
            $months = 0;

            $d1->add(new \DateInterval('P1M'));
            while ($d1 <= $d2){
                $months ++;
                $d1->add(new \DateInterval('P1M'));
            }
            $enrolledMonths = $months+1;  
        ///////////////////Total Months - Monthly Case////////////////////////       */
 
            $monthDays = date('t'); 
            $date1 = date_create($res['dueDate']); 
            $date2 =  date_create($last_day_this_month); //pre($date2); die();
            $diff=date_diff($date1,$date2); //echo  $diff->format('%a');
            $fees = $diff->format('%a');   //echo  $fees;die();
            $enrolledDays = $fees; //echo $enrolledDays; die(); 
             // pre($fees);die();
             
            $first_day_this_month = date('Y-m-01'); 
            $feeMonth = $result1['fees']; 
            
            $d1 = new DateTime($first_day_this_month);
            $d2 = new DateTime($dueDate);
            $months = 0;

            $d1->add(new \DateInterval('P1M'));
            while ($d1 <= $d2){
                $months ++;
                $d1->add(new \DateInterval('P1M'));
            }
            $enrolledMonths = $months;  //echo $enrolledMonths; 
            if($res['paymentMode']== 1)
            {
                if($res['dueDate'] != $first_day_this_month && $res['dueDate']!= $last_day_this_month)
                {$dueAmount        = $result1['fees']/$monthDays;
                $dueAmountMonthly = round($dueAmount*$enrolledDays);
               
                    if($enrolledMonths >0){$res['dueAmount'] = $feeMonth;}
                    else{$res['dueAmount'] = $dueAmountMonthly;} //echo $dueAmountMonthly; die();
                }
		elseif($res['dueDate']== $last_day_this_month){$res['dueAmount'] = $result1['fees'];}
                else{ //echo $dueDate; die();
                   
                    
                    
                    $res['dueAmount'] = $result1['fees'];
                }
                
            }
            elseif($res['paymentMode']== 0)
            {
		if($res['dueDate']== $last_day_of_due_month)
		{
			$date=date_create($res['dueDate']);
			date_add($date,date_interval_create_from_date_string("1 days")); //pre($dateNew); die();
			$dateAfterDue    = date_format($date,"Y-m-d");
			$dateAfterDueFinal = date('Y-m-t', strtotime($dateAfterDue));
			//echo $dateAfterDueFinal; die();
	
			$this->db->where('fkTutorId', $tutorId);
		        $this->db->where('fkStudentID', $studentId);
		        $this->db->where('fkParentID',  $parentId);
		        $this->db->where('subject_id',  $subjectId);
			$this->db->where('class_status !=',  2);
		        $this->db->where('class_date >=', $dueDate); //echo $dueDate; die();
			$this->db->where('class_date <=', $dateAfterDueFinal);
		        $resultCountAfterDueDate = $this->db->get('ht_class_schedule')->result_array();
		     // $q =$this->db->last_query(); echo $q; die();
		        $CountAfterDueDate = count( $resultCountAfterDueDate);

			
			 $dueAmount = round($result1['fees']*$CountAfterDueDate);
			$res['dueAmount'] = $dueAmount;
		}
           
		 else{ 
		 $dueAmount = round($result1['fees']*$countPerHourDays);
             $res['dueAmount']   = $dueAmount;   }
            }
             $res['subjectName'] = $resultSub['name'];
             $res['fees']        = $result1['fees'];
          
             
             
            $this->db->order_by("id","DESC");
            $this->db->where('fkParentId',$document['parentId']);
            $resultIncentive = $this->db->get('ht_parentIncentiveRecieved')->row_array();
            if($resultIncentive){
			$res['dueIncentive'] = $resultIncentive['total'];
		}
		else{
			$res['dueIncentive'] = "0";
		} 
            
            $resultFinal[] = $res;
        }
	}
      
        if($result)
        {          
           return $resultFinal ;
        }
        else
        {
           return false; 
        }
}
     
     function getParentDeviceToken($document)       
       {
        $this->db->where('id', $document['parentId']);      
        $resultParent = $this->db->get('ht_parent')->row_array();   
        if($resultParent)
        {          
          return $resultParent ;
        }
        else
        {
           return false; 
        }
    }

    function addPayment($document)       
    {
        
      
       $this->db->where('fkTutorId', $document['tutorId']);
        $this->db->where('fkParentId', $document['parentId']);
        $this->db->where('fkRequestId',  $document['studentId']);	   
        $resultsub = $this->db->get('ht_offers')->row_array(); 
        $last_day_this_month  = date('Y-m-t');  
        
        $tutorId = $document['tutorId'];
        $subjectId = $document['fkSubjectId'];
        $this->db->select('ht_tutorCoachingDetail.feesPerHour,ht_tutorCoachingDetail.feesPerMonth' );
        $this->db->where('fkTutorId', $tutorId);
        $this->db->where('fkSubjectId', $subjectId);
        $result1 = $this->db->get('ht_tutorCoachingDetail')->row_array();
        $feesPerMonth = $resultsub['fees'];
	//$feesPerMonth = $result1['feesPerMonth'];
        $feesPerHour = $resultsub['fees'] ;
               
        /////////////////////////////////////////////////
        
        $this->db->select('ht_ParentDuePayment.*,ht_tutor.profileImage,ht_tutor.gender,ht_tutor.dob,ht_tutor.name as tutorName,ht_parent_student.name as studentName' );                         
        $this->db->join('ht_tutor','ht_tutor.id=ht_ParentDuePayment.fkTutorId'); 
        // $this->db->join('ht_tutorCoachingDetail','ht_tutorCoachingDetail.fkTutorId=ht_tutorPaymentTotal.fkTutorId','right'); 
        $this->db->join('ht_parent_student','ht_parent_student.id=ht_ParentDuePayment.fkStudentId');                          
        $this->db->where('ht_ParentDuePayment.fkParentId', $document['parentId']);
        $this->db->where('ht_ParentDuePayment.fkTutorId', $document['tutorId']);
        $this->db->where('ht_ParentDuePayment.fkParentId', $document['parentId']);
        $this->db->where('ht_ParentDuePayment.fkStudentId',  $document['studentId']);
        $dueResult = $this->db->get('ht_ParentDuePayment')->row_array();
        //pre($dueResult); die();
        
        $tutorId = $dueResult['fkTutorId'];
        $subjectId = $dueResult['fkSubjectId'];
        $studentId = $dueResult['fkStudentId'];
        $parentId = $dueResult['fkParentId'];
        $dueDate = $dueResult['dueDate'];
        $totalEnrolledClassesMonth = $dueResult['Number_class'];
        
        
     
        $date1 = date_create($dueResult['dueDate']); 
        $date2 =  date_create($last_day_this_month); //pre($date2); die();
        $diff=date_diff($date1,$date2); //echo  $diff->format('%a');
        $fees = $diff->format('%a');
        $fees1 = $fees +1;  
        $monthDays = date('t'); //echo $fees; die();
      //  pre($fees1); die();
         //////////////////per hour days calculation//////////////////////
        $this->db->where('fkTutorId', $tutorId);
        $this->db->where('fkStudentID', $studentId);
        $this->db->where('fkParentID',  $parentId);
        $this->db->where('subject_id',  $subjectId);
        $this->db->where('class_date >=', $dueDate);
	$this->db->where('class_status !=',  2);
        $resultPerHourDays = $this->db->get('ht_class_schedule')->result_array(); // pre($resultPerHourDays); die();
        $countPerHourDays = count( $resultPerHourDays); 
       // $q =$this->db->last_query(); echo $q; die();
       // echo $countPerHourDays; die();
         //////////////////////////////////////////////
           
        ///////////////////////////////////////////////
        $monthDays = date('t'); 
         if($document['paymentMode']== 1) 
            {
                             
                $dueAmount        = $resultsub['fees']/$monthDays;
                $dueAmountMonthly = round($dueAmount*$countPerHourDays);
                $res['dueAmount'] = $dueAmountMonthly;
 
                $monthlyDueDatePlus = round($document['amount']/$dueAmountMonthly);
             
             
            }
            elseif($document['paymentMode']== 0)
            { 
             $hourlyCalc = $document['amount']/$feesPerHour;
         	$classLastdatefixvalue = $hourlyCalc -1; // echo $classLastdatefixvalue; die();
		if($countPerHourDays  == $hourlyCalc){$NextDueDate = $resultPerHourDays[$classLastdatefixvalue]['class_date'];}
		else{$NextDueDate = $resultPerHourDays[$hourlyCalc]['class_date']; }
            }
       
           // pre($result1); die();     
        
       ///////////////////////////////////////////////////////////////////////////////
       
        $this->db->where('fkParentId', $document['parentId']);
        $this->db->where('fkTutorId', $document['tutorId']);
        $this->db->where('fkStudentId', $document['studentId']);
        $this->db->where('fkSubjectId', $document['fkSubjectId']);
        $this->db->where('paymentMode', $document['paymentMode']);                   
        $this->db->where('ht_ParentDuePayment.fkParentId', $document['parentId']);
        $resultDue = $this->db->get('ht_ParentDuePayment')->row_array();    
        $oldDueDate = $resultDue['dueDate'];// echo $oldDueDate; die();
        
        
    
       // echo $monthlyDueDatePlus; die();
       if($document['paymentMode']== 1)
       {  
            $time = strtotime($dueResult['dueDate']);  //echo $final; die();

            $final = date("Y-m-d", strtotime("+$monthlyDueDatePlus month", $time));// echo $final; die();
            $time1 = strtotime($final); 
            $final1 = date('Y-m-01', strtotime($final));                              
           
            $LastDateCalc = date("Y-m-d", strtotime("+$fees day", $time)); //echo $LastDateCalc; die();
            $feeTurningMonth = date("Y-m-d", strtotime("+$fees1 day", $time)); //echo $feeTurningMonth; die();
            $this->db->query("update ht_ParentDuePayment set dueDate='".$LastDateCalc."' where `fkParentId`='".$document['parentId']."'&& `fkTutorId`='".$document['tutorId']."' && `fkStudentId`='".$document['studentId']."'&& `fkSubjectId`='".$document['fkSubjectId']."'&& `paymentMode`='".$document['paymentMode']."' ");}
       
       else { $this->db->query("update ht_ParentDuePayment set dueDate='".$NextDueDate."' where `fkParentId`='".$document['parentId']."'&& `fkTutorId`='".$document['tutorId']."' && `fkStudentId`='".$document['studentId']."'&& `fkSubjectId`='".$document['fkSubjectId']."'&& `paymentMode`='".$document['paymentMode']."' ");}
       
        $document1['amount'] = $document['amount'];
        $document1['date'] = $document['Date'];
        $document1['particulars'] = $document['particulars'];
        $document1['fkParentId'] = $document['parentId'];
        $document1['fkTutorId'] = $document['tutorId'];
        $document1['fkStudentId'] = $document['studentId'];
        $document1['fkSubjectId'] = $document['fkSubjectId'];
	$document1['paymentMode'] = $document['paymentMode'];
        $document1['paymentStatus'] = $document['paymentStatus'];
	$document1['verifiedByAdmin'] = $document['verifiedByAdmin'];
        $paymentDone = $this->db->insert('ht_parentPaymentRecord', $document1);
        $insert_id = $this->db->insert_id();
        
     
        
      	 //---------------------final push work------------------------//
		
        $this->db->where('id', $document['studentId']);                         
        $studentNameRes = $this->db->get('ht_parent_student')->row_array(); 
        $studentName = $studentNameRes['name'];
        
        $this->db->where('id', $document['fkSubjectId']);
       // $this->db->where('fkClassId', $resultDue['fkClassId']);
        $subjectNameRes = $this->db->get('ht_subject')->row_array(); 
        $subjectName = $subjectNameRes['name']; 
        
        $this->db->where('id', $resultDue['fkClassId']);       
        $classNameRes = $this->db->get('ht_parentClass')->row_array(); 
        $className = $classNameRes['className']; 
        
        
        
        
        $this->db->where('id', $document['parentId']);      
        $resultParent = $this->db->get('ht_parent')->row_array();
        $parentDeviceToken = $resultParent['deviceToken']; 
        $parentPhone = $resultParent['mobile1'];                      
             
                    
      $msg2push = '{"type":"61","message":{"Amount:"' . $document['amount'] . '",Name:"' . $studentName . '",Class: "'. $className .'",Subject: "'. $subjectName .'",Time:"'. $document['particulars'] .'"."}}';     

      $msg2 = 'Thank you for making a payment of Rs "' . $document['amount'] . '". for of "' . $studentName . '", Class : "' . $className . '" , Subject: "' . $subjectName . '" for a duration of "' . $document['particulars'] . '"."';

      sendAndroidPush($parentDeviceToken, $msg2push, "", "", 61);
      sendPushAsSMS($parentPhone, $msg2);  

        if($paymentDone)
        {          
           return true ;
        }
        else
        {
           return false; 
        }
}


     
 function getAccountDetails($document)       
    {

        $resultFinal =array();
	//$this->db->distinct();
	$this->db->order_by('ht_parentPaymentRecord.id','DESC'); 
        $this->db->select('ht_parentPaymentRecord.id,ht_parentPaymentRecord.verifiedByAdmin,ht_tutor.name as tutorName,ht_parentPaymentRecord.date,ht_parentPaymentRecord.particulars,ht_parentPaymentRecord.fkSubjectId,ht_parentPaymentRecord.fkStudentId,ht_parentPaymentRecord.fkTutorId,ht_parentPaymentRecord.amount,ht_parent_student.name as studentName,ht_subject.name as subjectName,ht_offers.mode,ht_offers.fees' );                         
                
        $this->db->join('ht_tutor','ht_tutor.id=ht_parentPaymentRecord.fkTutorId'); 
        $this->db->join('ht_parent_student','ht_parent_student.id=ht_parentPaymentRecord.fkStudentId');
        $this->db->join('ht_subject','ht_subject.id=ht_parentPaymentRecord.fkSubjectId');
	$this->db->join('ht_offers','ht_offers.fkRequestId=ht_parentPaymentRecord.fkStudentId');
        $this->db->where('ht_parentPaymentRecord.fkParentId', $document['parentId']);
	$this->db->where('ht_parentPaymentRecord.verifiedByAdmin', 1);
	$this->db->where('ht_offers.isAccepted', 1);
      
        $result = $this->db->get('ht_parentPaymentRecord')->result_array();
	//pre($result);die();
        foreach($result as $res)
	{
	
	 if($res['mode'] == 0){$res['mode'] = 1;}else{$res['mode'] = 0;}
	$resultFinal[] = $res;
	}
        if($result)
        {          
           return $resultFinal ;
        }
        else
        {
           return false; 
        }
}

function getAccountStudents($document)       
    {

        //$this->db->distinct();
        $this->db->select("ht_parentPaymentRecord.fkStudentId,ht_parentPaymentRecord.fkTutorId,ht_parentPaymentRecord.fkTutorId");
        $this->db->where('ht_parentPaymentRecord.fkTutorId', $document['fkTutorId']);
        $this->db->where('ht_parentPaymentRecord.fkParentId', $document['fkParentId']);
        //$this->db->join('ht_parent_student','ht_parent_student.id=ht_parentPaymentRecord.fkStudentId');
        $result = $this->db->get('ht_parentPaymentRecord')->result_array();
        //$val['students'] = $result1;

        //$final[] = $val;
        
        if($result)
        {          
           return $result ;
        }
        else
        {
           return false; 
        }
}

function getAccountSubjects($document)       
    {

        $this->db->distinct();
        $this->db->select("ht_parentPaymentRecord.fkStudentId,ht_parentPaymentRecord.fkTutorId");
        $this->db->where('ht_parentPaymentRecord.fkTutorId', $document['fkTutorId']);
        $this->db->where('ht_parentPaymentRecord.fkParentId', $document['fkParentId']);
        $this->db->where('ht_parentPaymentRecord.fkStudentId', $document['fkStudentId']);
        //$this->db->join('ht_parent_student','ht_parent_student.id=ht_parentPaymentRecord.fkStudentId');
        $result = $this->db->get('ht_parentPaymentRecord')->result_array();
        //$val['students'] = $result1;

        //$final[] = $val;
        
        if($result)
        {          
           return $result ;
        }
        else
        {
           return false; 
        }
}

function getAccountDetailsold($document)       
    {

        $this->db->distinct();
        $this->db->select("fkTutorId");
        //$this->db->select('ht_tutor.name as tutorName,ht_parentPaymentRecord.date,ht_parentPaymentRecord.particulars,ht_parentPaymentRecord.amount,ht_parent_student.name as studentName' );                         
        $this->db->select('ht_parentPaymentRecord.fkTutorId,ht_tutor.name as tutorName' );                         
        $this->db->join('ht_tutor','ht_tutor.id=ht_parentPaymentRecord.fkTutorId'); 
        //$this->db->join('ht_parent_student','ht_parent_student.id=ht_parentPaymentRecord.fkStudentId');
        $this->db->where('ht_parentPaymentRecord.fkParentId', $document['parentId']);
       // $this->db->where('ht_parentPaymentRecord.fkTutorId', $document['tutorId']);
        $result = $this->db->get('ht_parentPaymentRecord')->result_array();
        
       if($result)
        {
           foreach($result as $val)
           {
                //$this->db->where('ht_parentPaymentRecord.fkSubjectId', $val['fkSubjectId']);
                //$this->db->where('ht_parentPaymentRecord.fkTutorId', $val['fkTutorId']);
                //$this->db->where('ht_parentPaymentRecord.fkParentId', $document['parentId']);
                //
               $this->db->distinct();
                $this->db->select("ht_parentPaymentRecord.fkStudentId,ht_parentPaymentRecord.fkTutorId");
                $this->db->where('ht_parentPaymentRecord.fkTutorId', $val['fkTutorId']);
                $this->db->where('ht_parentPaymentRecord.fkParentId', $document['parentId']);
                //$this->db->join('ht_parent_student','ht_parent_student.id=ht_parentPaymentRecord.fkStudentId');
                $result1 = $this->db->get('ht_parentPaymentRecord')->result_array();
                $val['students'] = $result1;
               
                $final[] = $val;
                
                
                foreach($final as $val1)
                {
                    //pre($val1);
                    
                    $this->db->distinct();
                     $this->db->select("ht_parentPaymentRecord.fkSubjectId");
                     $this->db->where('ht_parentPaymentRecord.fkStudentId', $val1['students']['fkStudentId']);
                     $this->db->where('ht_parentPaymentRecord.fkTutorId', $val1['fkTutorId']);
                     $this->db->where('ht_parentPaymentRecord.fkParentId', $document['parentId']);
                     //$this->db->join('ht_parent_student','ht_parent_student.id=ht_parentPaymentRecord.fkStudentId');
                     $result1 = $this->db->get('ht_parentPaymentRecord')->result_array();
                     $val1['subjects'] = $result1;

                     $final1[] = $val1;

                }
                
           }
           if($final)
           { //pre($final);
               foreach($final as $val1)
                {
                    //pre($val1);
                    
                    $this->db->distinct();
                     $this->db->select("ht_parentPaymentRecord.fkSubjectId");
                     $this->db->where('ht_parentPaymentRecord.fkStudentId', $val1['students']['fkStudentId']);
                     $this->db->where('ht_parentPaymentRecord.fkTutorId', $val1['fkTutorId']);
                     $this->db->where('ht_parentPaymentRecord.fkParentId', $document['parentId']);
                     //$this->db->join('ht_parent_student','ht_parent_student.id=ht_parentPaymentRecord.fkStudentId');
                     $result1 = $this->db->get('ht_parentPaymentRecord')->result_array();
                     $val1['subjects'] = $result1;

                     $final1[] = $val1;

                }
           }
        }

        if($result)
        {          
           return $final1 ;
        }
        else
        {
           return false; 
        }
}




    function parentsPaymentDetails($document)       
    {

      
        $this->db->select('ht_tutor.name as tutorName,ht_parentPaymentRecord.date,ht_parentPaymentRecord.particulars,ht_parentPaymentRecord.fkSubjectId,ht_parentPaymentRecord.fkStudentId,ht_parentPaymentRecord.fkTutorId,ht_parentPaymentRecord.amount,ht_parent_student.name as studentName' );                         
       // $this->db->select('ht_parentPaymentRecord.fkTutorId,ht_tutor.name as tutorName,ht_parentPaymentRecord.fkParentId' );                         
        $this->db->join('ht_tutor','ht_tutor.id=ht_parentPaymentRecord.fkTutorId'); 
        $this->db->join('ht_parent_student','ht_parent_student.id=ht_parentPaymentRecord.fkStudentId');
        $this->db->join('ht_subject','ht_subject.id=ht_parentPaymentRecord.fkSubjectId');
        $this->db->where('ht_parentPaymentRecord.fkParentId', $document['parentId']);
       // $this->db->where('ht_parentPaymentRecord.fkTutorId', $document['tutorId']);
        $result = $this->db->get('ht_parentPaymentRecord')->result_array();
       
        if($result)
        {          
           return $result ;
        }
        else
        {
           return false; 
        }
}

	/*************************Tutor-Account************************/


	         function getTutorAmountDetails($document)       
    {    
         $last_day_this_month  = date('Y-m-t'); 
        $this->db->order_by('dueDate','ASC'); 
        $this->db->select('ht_ParentDuePayment.*,ht_tutor.profileImage,ht_tutor.gender,ht_tutor.dob,ht_tutor.name as tutorName,ht_parent_student.name as studentName,ht_parent_student.location' );                         
        $this->db->join('ht_tutor','ht_tutor.id=ht_ParentDuePayment.fkTutorId'); 
        // $this->db->join('ht_tutorCoachingDetail','ht_tutorCoachingDetail.fkTutorId=ht_tutorPaymentTotal.fkTutorId','right'); 
        $this->db->join('ht_parent_student','ht_parent_student.id=ht_ParentDuePayment.fkStudentId');                          
        $this->db->where('ht_ParentDuePayment.fkTutorId', $document['fkTutorId']);
        $result = $this->db->get('ht_ParentDuePayment')->result_array();
	//$q =$this->db->last_query(); echo $q; die();
	//print_r($result); die();
        $count =count( $result);
       
        $class_status = 1;                                      //////changed 
           // $res['age'] =  date('Y')-$res['dob'];
        foreach($result as $res)
        {
            
	    $this->db->where('id', $res['fkSubjectId']);
	    $resultSub = $this->db->get('ht_subject')->row_array(); 


            $res['age'] =  date('Y')-$res['dob'];
	    $monthEnrolledDays = $res['Number_class']; //echo $monthEnrolledDays; die();
            $tutorId = $res['fkTutorId'];
            $subjectId = $res['fkSubjectId'];
            $studentId = $res['fkStudentId'];
            $parentId = $res['fkParentId'];
            $dueDate = $res['dueDate'];
           // $this->db->select('ht_tutorCoachingDetail.fees' );
           // $this->db->join('ht_subject','ht_subject.id=ht_tutorCoachingDetail.fkSubjectId');
            $this->db->where('fkTutorId', $tutorId);
	    $this->db->where('fkParentId', $parentId);
	    $this->db->where('fkRequestId',  $studentId);
	   
            $result1 = $this->db->get('ht_offers')->row_array(); 
	 

	    $this->db->where('fkTutorId', $tutorId);
	    $this->db->where('fkParentId', $parentId);
	    $this->db->where('fkParentRequestId',  $studentId);
	    $this->db->where('status',  6);
	    $resultTerminationCheck = $this->db->get('ht_parentRequestTutor')->row_array();

	    if(empty($resultTerminationCheck))
	    {
           //////////////////per hour days calculation//////////////////////
	    $last_day_of_due_month = date('Y-m-t', strtotime($dueDate)); 
            $this->db->where('fkTutorId', $tutorId);
            $this->db->where('fkStudentID', $studentId);
            $this->db->where('fkParentID',  $parentId);
             $this->db->where('subject_id',  $subjectId);
		$this->db->where('class_status !=',  2);
            if($res['paymentMode'] == 1){ $this->db->where('class_date >', $dueDate);}else{$this->db->where('class_date >=', $dueDate);}
	    $this->db->where('class_date <=', $last_day_of_due_month);
            $resultPerHourDays = $this->db->get('ht_class_schedule')->result_array();
           // $q =$this->db->last_query(); echo $q; die();
            $countPerHourDays = count( $resultPerHourDays); //echo $countPerHourDays; die();
         
        ///////////////////Total Months - Monthly Case////////////////////////    
      /*      $this->db->where('fkTutorId', $tutorId);
            $this->db->where('fkStudentID', $studentId);
            $this->db->where('fkParentID',  $parentId);
            $this->db->where('subject_id',  $subjectId);
            $this->db->where('class_date >=', $dueDate);
            $this->db->order_by("id","desc");
            $this->db->limit(1);
            $classLastDate = $this->db->get('ht_class_schedule')->row_array(); //pre($classLastDate); die();
            $enrolledLastDate = $classLastDate['class_date']; //echo $enrolledLastDate; die();
            $dueDate = $res['dueDate']; //echo $dueDate; die();
            
            $d1 = new DateTime($dueDate);
            $d2 = new DateTime($enrolledLastDate);
            $months = 0;

            $d1->add(new \DateInterval('P1M'));
            while ($d1 <= $d2){
                $months ++;
                $d1->add(new \DateInterval('P1M'));
            }
            $enrolledMonths = $months+1;  
        ///////////////////Total Months - Monthly Case////////////////////////       */
 
            $monthDays = date('t'); 
            $date1 = date_create($res['dueDate']); 
            $date2 =  date_create($last_day_this_month); //pre($date2); die();
            $diff=date_diff($date1,$date2); //echo  $diff->format('%a');
            $fees = $diff->format('%a');   //echo  $fees;die();
            $enrolledDays = $fees; //echo $enrolledDays; die(); 
             // pre($fees);die();
             
            $first_day_this_month = date('Y-m-01'); 
            $feeMonth = $result1['fees']; 
            
            $d1 = new DateTime($first_day_this_month);
            $d2 = new DateTime($dueDate);
            $months = 0;

            $d1->add(new \DateInterval('P1M'));
            while ($d1 <= $d2){
                $months ++;
                $d1->add(new \DateInterval('P1M'));
            }
            $enrolledMonths = $months;  //echo $enrolledMonths; 
            if($res['paymentMode']== 1)
            {
                if($res['dueDate'] != $first_day_this_month)
                {$dueAmount        = $result1['fees']/$monthDays;
                $dueAmountMonthly = round($dueAmount*$enrolledDays);
               
                    if($enrolledMonths >0){$res['dueAmount'] = $feeMonth;}
                    else{$res['dueAmount'] = $dueAmountMonthly;} //echo $dueAmountMonthly; die();
                }
                else{ //echo $dueDate; die();
                   
                    
                    
                    $res['dueAmount'] = $result1['fees'];
                }
                
            }
            elseif($res['paymentMode']== 0)
            {
             if($res['dueDate']== $last_day_of_due_month)
		{
			$date=date_create($res['dueDate']);
			date_add($date,date_interval_create_from_date_string("1 days")); //pre($dateNew); die();
			$dateAfterDue    = date_format($date,"Y-m-d");
			$dateAfterDueFinal = date('Y-m-t', strtotime($dateAfterDue));
			//echo $dateAfterDueFinal; die();
	
			$this->db->where('fkTutorId', $tutorId);
		        $this->db->where('fkStudentID', $studentId);
		        $this->db->where('fkParentID',  $parentId);
		        $this->db->where('subject_id',  $subjectId);
			$this->db->where('class_status !=',  2);
		        $this->db->where('class_date >', $dueDate); //echo $dueDate; die();
			$this->db->where('class_date <=', $dateAfterDueFinal);
		        $resultCountAfterDueDate = $this->db->get('ht_class_schedule')->result_array();
		     // $q =$this->db->last_query(); echo $q; die();
		        $CountAfterDueDate = count( $resultCountAfterDueDate);

			
			 $dueAmount = round($result1['fees']*$CountAfterDueDate);
			$res['dueAmount'] = $dueAmount;
		}
           
		 else{ 
		 $dueAmount = round($result1['fees']*$countPerHourDays);
             $res['dueAmount']   = $dueAmount;   }  
            }
             $res['subjectName'] = $resultSub['name'];
            // $res['fees']        = $result1['fees'];
            
            // $res['feesPerMonth'] = $result1['fees'];
            // $res['feesPerHour'] = $result1['fees'];
             
              /////////unset/////////////
            unset($res['fkTutorId']);
            unset($res['fkParentId']);
            unset($res['fkStudentId']);
            unset($res['fkSubjectId']);
            unset($res['fkClassId']);
            unset($res['paymentMode']);
            unset($res['profileImage']);
            unset($res['gender']);
            unset($res['dob']);
            unset($res['feesPerMonth']);
            unset($res['feesPerHour']);
             unset($res['id']);
           // unset($detail['password']);

            $resultFinal[] = $res;
            
            
            
         }
        }
      
        if($result)
        {          
           return $resultFinal ;
        }
        else
        {
           return false; 
        }
}
    

        
                function tutorPaymentRecieved($document)       
    {    
	$resultFinal = array();
        $this->db->distinct();
        $this->db->order_by('date','DESC'); 
        $this->db->select('ht_parentPaymentRecord.*,ht_parent_student.name as studentName,ht_subject.name as subjectName ,ht_parent_student.location' );                         
       // $this->db->join('ht_tutor','ht_tutor.id=ht_parentPaymentRecord.fkTutorId'); 
        // $this->db->join('ht_tutorCoachingDetail','ht_tutorCoachingDetail.fkTutorId=ht_tutorPaymentTotal.fkTutorId','right');
	$this->db->join('ht_subject','ht_subject.id=ht_parentPaymentRecord.fkSubjectId'); 
        $this->db->join('ht_parent_student','ht_parent_student.id=ht_parentPaymentRecord.fkStudentId');                          
        $this->db->where('ht_parentPaymentRecord.fkTutorId', $document['fkTutorId']);
	$this->db->where('ht_parentPaymentRecord.verifiedByAdmin', 1);
        $result = $this->db->get('ht_parentPaymentRecord')->result_array();
        $count =count( $result);
       
	/*foreach($result as $res)
  	{
	 $this->db->where('ht_parent_student.id', $res['fkStudentId']);
        $result2 = $this->db->get('ht_parent_student')->row_array();
	$res['location'] = $result2['location'];   */
	//$resultFinal[] = $result;
	//}
        
     
        if($result)
        {          
           return $result ;
        }
        else
        {
           return false; 
        }
     }



	 /************Admin Account Side ************/


         
                function tutorPaymentsByAdmin($document)       
    {    
	$resultFinal = array();
        $mnth = date('m');
        $lastmnth = date('m')-1;
        $thisYear = date("Y"); 
        $this->db->order_by('date','DESC'); 
        $this->db->select('ht_adminPaidPayments.*' );                         
                  
        $this->db->where('ht_adminPaidPayments.fkTutorId', $document['fkTutorId']);
        $result = $this->db->get('ht_adminPaidPayments')->result_array();
        $count =count( $result);
        $resultFinal[]= $result;  
        
       
        $this->db->select('SUM(amount) as currentMonth' );
        $this->db->where('ht_adminPaidPayments.fkTutorId', $document['fkTutorId']);       
        $this->db->where(  'MONTH(date)' , $mnth);
        $result1 = $this->db->get('ht_adminPaidPayments')->row_array();    
        
        $this->db->select('SUM(amount) as lastMonth' );
        $this->db->where('ht_adminPaidPayments.fkTutorId', $document['fkTutorId']);      
        $this->db->where(  'MONTH(date)' , $lastmnth);
        $result2 = $this->db->get('ht_adminPaidPayments')->row_array(); 
      
        $this->db->select('SUM(amount) as thisYear' );
        $this->db->where('ht_adminPaidPayments.fkTutorId', $document['fkTutorId']);       
        $this->db->where(  'YEAR(date)' , $thisYear);
        $result3 = $this->db->get('ht_adminPaidPayments')->row_array(); 
      
     /*	if(empty($result)){
	$result[0]['fkTutorId']  = $document['fkTutorId'];
	$result[0]['amount']     = "0";
	$result[0]['date']       = "";
	$result[0]['comment']    = "";
	$result[0]['thisMonth']  = "0";
	$result[0]['lastMonth']  = "0";
	$result[0]['thisYear']   = "0";
	}  */
         
          
        
        if($result)
        { 	
              if($result1['currentMonth'] != ""){  $result[0]['thisMonth']  = $result1['currentMonth'];}else{  $result[0]['thisMonth']  = "0";}
         if($result2['lastMonth'] != ""){$result[0]['lastMonth']  = $result2['lastMonth'];}else{$result[0]['lastMonth']  = "0";}
         if($result3['thisYear'] != ""){$result[0]['thisYear']   = $result3['thisYear'];}else{$result[0]['thisYear']   = "0";}   
           return $result ;
        }
        else
        {
           return false; 
        }
    }
    
    
    function tutorIncentives($document)       
    {    
        $mnth = date('m');
        $lastmnth = date('m')-1;// echo $lastmnth; die();
        $thisYear = date("Y"); 
        $this->db->order_by('date','DESC'); 
        $this->db->select('ht_tutorIncentiveRecord.*' );                                               
        $this->db->where('ht_tutorIncentiveRecord.fkTutorId', $document['fkTutorId']);
        $result = $this->db->get('ht_tutorIncentiveRecord')->result_array();
        $count =count( $result);
       
        
        $this->db->select('SUM(incentiveAmount) as currentMonth' );
        $this->db->where('ht_tutorIncentiveRecord.fkTutorId', $document['fkTutorId']);       
        $this->db->where(  'MONTH(date)' , $mnth);
        $result1 = $this->db->get('ht_tutorIncentiveRecord')->row_array();
     
       
        $this->db->select('SUM(incentiveAmount) as lastMonth' );
        $this->db->where('ht_tutorIncentiveRecord.fkTutorId', $document['fkTutorId']);      
        $this->db->where(  'MONTH(date)' , $lastmnth);
        $result2 = $this->db->get('ht_tutorIncentiveRecord')->row_array(); 
      
        $this->db->select('SUM(incentiveAmount) as tillNow' );
        $this->db->where('ht_tutorIncentiveRecord.fkTutorId', $document['fkTutorId']);     
        $result3 = $this->db->get('ht_tutorIncentiveRecord')->row_array(); 
      
     
     /*  if(empty($result)){
	$result['fkTutorId']  = $document['fkTutorId'];
	$result['incentiveAmount']  = "0";
	$result['total']  = "0";
	$result['date']  = "";
	$result['comment']  = "";
	$result['thisMonth']  = "0";
	$result['lastMonth']  = "0";
	$result['tillNow']   = "0";
	}  */
	
         
        
   
        if($result)
        {     
	   if($result1['currentMonth'] != ""){  $result[0]['thisMonth']  = $result1['currentMonth'];}else{  $result[0]['thisMonth']  = "0";}
           if($result2['lastMonth'] != ""){$result[0]['lastMonth']  = $result2['lastMonth'];}else{$result[0]['lastMonth']  = "0";}
           if($result3['tillNow'] != ""){$result[0]['tillNow']   = $result3['tillNow'];}else{$result[0]['tillNow']   = "0";}            
           return $result ;
        }
        else
        {
           return false; 
        }
    }



	 
                 function parentIncentiveUsed($document)       
    {    
       
        $this->db->order_by("id","DESC");
        $this->db->where('fkParentId',$document['fkParentId']);
        $result = $this->db->get('ht_parentIncentiveRecieved')->row_array();  
         
        $TotalAmount =  $result['total'] - $document['incentiveAmount']; 
        $document['date'] = date('Y-m-d');
        $document['total'] = $TotalAmount;
        $id = $result['id'];
        
        $this->db->where('id',$id );
        $this->db->where('fkParentId',$document['fkParentId'] );
        $this->db->update('ht_parentIncentiveRecieved',$document);
       
  
        
     
        if($result)
        {          
           return $result ;
        }
        else
        {
           return false; 
        }
    }

    
    
}
