<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Version_detail_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }



    function addVersion($document)
        {
            $checkUserVersion  = $this->checkUserVersion($document);
            if($checkUserVersion){
                $this->db->where('user_id',$document['user_id']);
                $this->db->where('user_type',$document['user_type']);
                $result=$this->db->update('ht_app_version_detail', $document);
                
            }else{
                $result=$this->db->insert('ht_app_version_detail', $document);
            }         
           
            if(!empty($result))
            {
                return $result;
            }
            else
            {
                return false;
            }
        }


    function checkUserVersion($document)
    {
        $this->db->where('user_id',$document['user_id']);
        $this->db->where('user_type',$document['user_type']);
        $result = $this->db->get('ht_app_version_detail')->row_array();
        if(!empty($result))
        {
            return $result;
        }
        else
        {
            return false;
        }

    }

    function checkuser($document)
    {
        if($document['user_type']==0){
        $this->db->where('id',$document['user_id']);
        $result = $this->db->get('ht_tutor')->row_array();  
        }else{
        $this->db->where('id',$document['user_id']);
        $result = $this->db->get('ht_parent')->row_array();    
        }
        if(!empty($result))
        {
            return $result;
        }
        else
        {
            return false;
        }

    }

   

}
