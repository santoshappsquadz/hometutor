
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Parent_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function parentSignUp($document) {
        if ($document['id']) {
            $document['isProfileComplete'] = 1;
            $document['modifiedDate'] = date("Y-m-d H:i:s");
            $this->db->where('id', $document['id']);
            $this->db->update('parent', $document);
            /**************push send**************************/
            $parentInfo = $this->getParentProfile($document['id']);
            $pushData = '{"type":42,"message":{"Name":"' . $parentInfo['name'] . '"}}';           
            sendAndroidPush($parentInfo['deviceToken'], $pushData, "", "", 42);            
            $smsData = 'You profile is updated.' ;
            $ressmsData = sendPushAsSMS($parentInfo['mobile1'], $smsData);
            /*****************End****************************/
        } else {
            
            $this->db->insert('parent', $document);
            /*********************Push Notification***************/
            $parentId= $this->db->insert_id();
            $parentInfo = $this->getParentProfile($parentId);
            $pushData = '{"type":32,"message":{"Name":"' . $parentInfo['name'] . '""}}';           
            sendAndroidPush($parentInfo['deviceToken'], $pushData, "", "", 32);            
            $smsData = '"You have sucessfully registered on itzTutor , A complete home tutoring solution. Find all benefits of using itzTutor at itzTutor.com"' ;      
            $ressmsData = sendPushAsSMS($parentInfo['mobile1'], $smsData);
            /******************End*********************************/
            return $parentId;
            
            
        }
    }

    function parentLogin($document) {
        $this->db->where('password', $document['password']);
        $this->db->where("(email='" . $document['emailOrPhone'] . "' OR mobile1='" . $document['emailOrPhone'] . "')");
        $result = $this->db->get('parent')->row_array();
        //echo $this->db->last_query();
        if (!empty($result)) {
            $data = array();
            $data['deviceToken'] = $document['deviceToken'];
            $data['deviceType'] = $document['deviceType'];
            $this->db->where('id', $result['id']);
            $this->db->update('parent', $data);
            //echo $this->db->last_query();
            return array('parentId' => $result['id']);
        } else {
            $this->db->where('email', $document['emailOrPhone']);
            $this->db->or_where('mobile1', $document['emailOrPhone']);
            $this->db->where('password!=', $document['password']);
            $result = $this->db->get('parent')->row_array();
            if (!empty($result)) {
                return 2; //Wrong credentail.
            } else {
                return 3; //User not exist
            }
        }
    }

    function isEmailOrMobileAlreadyExist($emailOrMobile) {
        $this->db->where('email', $emailOrMobile);
        $this->db->or_where('mobile1', $emailOrMobile);
        $result = $this->db->get('parent')->row_array();
        if (!empty($result)) {
            return $result['id'];
        } else {
            return false;
        }
    }

    function isEmailAlreadyExist($email) {
        $this->db->where('email', $email);
        $result = $this->db->get('parent')->row_array();
        if (!empty($result)) {
            return true;
        } else {
            return false;
        }
    }

    function isMobileAlreadyExist($mobile) {
        $this->db->where('mobile1', $mobile);
        $result = $this->db->get('parent')->row_array();
        if (!empty($result)) {
            return true;
        } else {
            return false;
        }
    }

    function isGoogleIdExist($googleId) {
        $this->db->where('googleId', $googleId);
        $result = $this->db->get('parent')->row_array();
        if (!empty($result)) {
            return $result['id'];
        } else {
            return false;
        }
    }

    function isFbIdExist($fbId) {
        $this->db->where('fbId', $fbId);
        $result = $this->db->get('parent')->row_array();
        if (!empty($result)) {
            return $result['id'];
        } else {
            return false;
        }
    }

    function getParentProfile($parentId) {
        $this->db->where('id', $parentId);
        $result = $this->db->get('parent')->row_array();
        if (!empty($result)) {
            return $result;
        } else {
            return false;
        }
    }

    function getParentMyProfile($parentId) {
        $this->db->where('id', $parentId);
        $result['parent'] = $this->db->get('parent')->row_array();
        $this->db->distinct();
        $this->db->select('stud.name,stud.*,DATE_FORMAT( stud.startTime, "%h:%i %p") as startTime,DATE_FORMAT( stud.endTime, "%h:%i %p") as endTime,brd.name as boardName,sb.name as subjectName,pc.className as className');
        $this->db->from('parent as pr');
        $this->db->join('ht_parent_student as stud', 'pr.id=stud.fkParentId');
        $this->db->join('ht_board as brd', 'brd.id=stud.board');
        $this->db->join('ht_parentClass as pc', 'pc.id=stud.class');
        $this->db->join('ht_subject as sb', 'sb.id=stud.subject');
        $this->db->where('pr.id', $parentId);   
        $this->db->where('stud.isDeleted', 0);
        $this->db->order_by('stud.id','DESC');
         $this->db->group_by('stud.name');
        $result['child'] = $this->db->get()->result_array();
       
       /* $this->db->distinct();
        $this->db->select("parent_student.name,parent_student.id as requestId,board.id as boardId,board.name as boardName,"
                . "parent_student.class as classId,subject.id as subjectId,subject.name as subjectName,"
                . "parent_student.name,parent_student.schoolName,parent_student.studentGender,"
                . "parent_student.tutionType,parent_student.tutionPremises,parent_student.preferredTutionDays,"
                . "DATE_FORMAT(ht_parent_student.startTime, '%l:%i %p') as startTime, "
                . "DATE_FORMAT(ht_parent_student.endTime, '%l:%i %p') as endTime");
         $this->db->where('fkParentId', $parentId);
        $this->db->where('isDeleted', 0);
        $this->db->join('board', 'board.id=parent_student.board');
        $this->db->join('class', 'class.id=parent_student.fkClassId');
        $this->db->join('subject', 'subject.id=parent_student.subject');
        $this->db->order_by('parent_student.createdDate','DESC');
        $result1 = $this->db->get('parent_student')->result_array();
        
         if (!empty($result1)) {
            $sendArray = array();
            foreach ($result1 as $data) {
                $sendArray[$data['name']] = $data;
                $sendArray[$data['name']]['className'] = $this->getParentClass($data['classId']);
            }
            $sendArray = array_values($sendArray);
         }
         $result['child'] =  $sendArray ;*/
        if (!empty($result)) {
            return $result;
        } else {
            return false;
        }
    }

    /*  function editParentProfile($document) {
      $id = $document['parentId'];
      unset($document['parentId']);
      $this->db->where('id', $id);
      $result = $this->db->update("parent", $document);

      if ($result) {
      return true;
      } else {
      return false;
      }
      } */

    function editStudentProfile($document) {
        $Pid = $document['parentId'];
        $Sid = $document['studentId'];
        unset($document['parentId']);
        unset($document['studentId']);
        $this->db->where('id', $Sid);
        $this->db->where('fkParentId', $Pid);
        $result = $this->db->update("ht_parent_student", $document);
        /**************Update push send**************************/
            $getStu['parentId']=$Pid;
            $getStu['studentId']= $Sid;       
            $studentInfo = $this->getStudentProfile($getStu);
            $parentInfo = $this->getParentProfile($Pid);
            $pushData = '{"type":41,"message":{"StudentName":"'. $studentInfo['name'] .'"}}';           
            sendAndroidPush($parentInfo['deviceToken'], $pushData, "", "", 41);            
            $smsData = 'You have updated profile of '.$studentInfo['name'].'' ;
            $ressmsData = sendPushAsSMS($parentInfo['mobile1'], $smsData);
            /*****************End****************************/
        //echo $this->db->last_query();die("hhh");
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function getStudentProfile($document) {
        $this->db->select('*');
        $this->db->where('fkParentId', $document['parentId']);
        $this->db->where('id', $document['studentId']);
        $result = $this->db->get('ht_parent_student')->row_array();
        //echo $this->db->last_query();
        if (!empty($result)) {
            return $result;
        } else {
            return false;
        }
    }

    function deleteParentStudent($document) {
        $updatedata['isDeleted']=1;
        $this->db->where('id', $document['studentId']);
        $this->db->where('fkParentId', $document['parentId']);
        $result = $this->db->update("ht_parent_student",$updatedata);
      
        if ($result) {
           
            return true;
        } else {
            return false;
        }
    }

    function searchTutor($filters) {
        $this->db->join('tutorAvailablity', 'tutorAvailablity.fkTutorId=id.tutor');
        $this->db->join('tutorCoachingDetail', 'tutorCoachingDetail.fkTutorId=id.tutor');
        $this->db->join('tutorCoachingLocation', 'tutorCoachingLocation.fkTutorId=id.tutor');
        return $this->db->get('tutor')->result_array();
    }
    
     function getPstudent($data) {//for pick student
        $result = array();
        $this->db->select("*");
         $this->db->where("id",$data);
        $result = $this->db->get('ht_parent_student')->row_array();       
        return $result;
    }

    function addStudent($document) {
        $requestId = $document['requestId'];
        $counter = $document['output_counter'];
        $gender = $document['gender'];
        $selectmore = $document['selectmore'];
        unset($document['requestId']);
        unset($document['output_counter']);
        unset($document['gender']);
        unset($document['selectmore']);        
        
        if($requestId==''){
        $insert_query = $this->db->insert_string('parent_student', $document);
        $insert_query = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insert_query);
        $this->db->query($insert_query);
        $insert_id = $this->db->insert_id();
        }else{ // for parent pick student and serach       
           
        $getPstudent=$this->getPstudent($requestId);//Get current student detail    
        if($getPstudent['preferredTutionDays']==''){
        $insert_query = $this->db->where('id', $requestId);    
        $insert_query = $this->db->update('parent_student', $document);      
        $insert_id = $requestId;  
        }else
        {
       // $document['update_enrolled']=$requestId;     
        $insert_query = $this->db->insert_string('parent_student', $document);
        $insert_query = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insert_query);
        $this->db->query($insert_query);
        $insert_id = $this->db->insert_id();  
        }
        
        }
        
        
        if($insert_id) {
            $requestId = $insert_id;
        }
        if ($document['tutorCode']) {
            $this->db->distinct('tutor.id');
            $this->db->select("tutorCoachingLocation.location,tutorCoachingLocation.radius,tutor.id,tutor.profileImage,"
                    . "(CASE WHEN ht_qualifications.name='Other' THEN ht_tutor.other_qualification
                            WHEN ht_qualifications.name!='' THEN ht_qualifications.name
                            ELSE '' END) as  qualification,"  
                    . "tutor.name,(Year(now())-ht_tutor.dob) as age,"
                    . "tutor.gender,tutor.teachingExp,tutorCoachingDetail.feesPerMonth,tutorCoachingDetail.feesPerHour,"
                    . "min(floor(((acos(sin((" . $document['latitude'] . "*pi()/180)) * sin((ht_tutorCoachingLocation.latitude*pi()/180))+cos((" . $document['latitude'] . "*pi()/180)) * cos((ht_tutorCoachingLocation.latitude*pi()/180)) * cos(((" . $document['longitude'] . "- ht_tutorCoachingLocation.longitude)*pi()/180))))*180/pi())*60*1.1515*1.609344)) as distance"); //distance in km for miles remove 1.609344 
            $this->db->join('tutorAvailablity', 'tutorAvailablity.fkTutorId=tutor.id', 'left');
            $this->db->join('tutorCoachingDetail', 'tutorCoachingDetail.fkTutorId=tutor.id', 'left');
            $this->db->join('tutorCoachingLocation', 'tutorCoachingLocation.fkTutorId=tutor.id', 'left');
            $this->db->join('qualifications', 'qualifications.id=tutor.qualification', 'left');
            $this->db->where('tutor.referalCode', $document['tutorCode']);
        } else {
            if ($document['preferredTutionDays']) {
                $concat = "FIND_IN_SET(days,'" . $document['preferredTutionDays'] . "') AND ";
            } else {
                $concat = "";
            }


            $startTime = $document['startTime'];
            $endTime = $document['endTime'];
            /* calculate intersect time */
            $sel = " , max( SEC_TO_TIME(GREATEST(0, ( TIME_TO_SEC( LEAST( STR_TO_DATE(  endTime, '%H:%i:%s' ), STR_TO_DATE(  '$endTime', '%H:%i:%s' ) ) ) - TIME_TO_SEC( GREATEST( STR_TO_DATE(  startTime, '%H:%i:%s' ), STR_TO_DATE(  '$startTime', '%H:%i:%s' ) ) ) )))) AS intersect_time , startTime , endTime ";
          
            $this->db->distinct('tutor.id');
            $this->db->select("tutor.id, ht_parentRequestTutor.isParentDeselectedTutor,tutorCoachingLocation.location,tutorCoachingLocation.radius,tutor.profileImage,"
                    . "(CASE WHEN ht_qualifications.name='Other' THEN ht_tutor.other_qualification
                            WHEN ht_qualifications.name!='' THEN ht_qualifications.name
                            ELSE '' END) as  qualification,"  
                    . "tutor.name,(Year(now())-ht_tutor.dob) as age,"
                    . "tutor.gender,tutor.teachingExp,"
                    . "min(floor(((acos(sin((" . $document['latitude'] . "*pi()/180)) * sin((ht_tutorCoachingLocation.latitude*pi()/180))+cos((" . $document['latitude'] . "*pi()/180)) * cos((ht_tutorCoachingLocation.latitude*pi()/180)) * cos(((" . $document['longitude'] . "- ht_tutorCoachingLocation.longitude)*pi()/180))))*180/pi())*60*1.1515*1.609344)) as distance , $sel"); //distance in km for miles remove 1.609344 
            $this->db->join('tutorAvailablity', 'tutorAvailablity.fkTutorId=tutor.id', 'left');
            $this->db->join('tutorCoachingDetail', 'tutorCoachingDetail.fkTutorId=tutor.id', 'left');
            $this->db->join('tutorCoachingLocation', 'tutorCoachingLocation.fkTutorId=tutor.id', 'left');
            $this->db->join('qualifications', 'qualifications.id=tutor.qualification', 'left');
            $this->db->join('ht_parentRequestTutor', 'ht_parentRequestTutor.fkTutorId=tutor.id', 'left');
            $this->db->order_by('tutor.gender');
            $this->db->order_by('tutor.rating', 'desc');
            $this->db->where($concat . "ht_tutorCoachingDetail.fkBoardId=" . $document['board']);
            $this->db->where('tutorCoachingDetail.fkClassId', $document['fkClassId']);
           $this->db->where("(tutorCoachingDetail.fkSubjectId = '{$document['subject']}' or tutorCoachingDetail.fkSubjectId in(1,2,3,4,5,6))");
       
            $this->db->group_by("tutor.id");
            
            /*
             * Filter for All Subjects start
             */
            /* if ($document['fkClassId'] == 1) {
              if ($document['subject'] == 1) {
              $this->db->where_in('tutorCoachingDetail.fkSubjectId', array(7, 15, 21, 29, 49, 89));
              }
              } elseif ($document['fkClassId'] == 2) {
              if ($document['subject'] == 2) {
              $this->db->where_in('tutorCoachingDetail.fkSubjectId', array(8, 16, 22, 30, 50, 90));
              }
              } elseif ($document['fkClassId'] == 3) {
              if ($document['subject'] == 3) {
              $this->db->where_in('tutorCoachingDetail.fkSubjectId', array(9, 17, 23, 31, 51, 91));
              }
              } else {
              $this->db->where('tutorCoachingDetail.fkSubjectId', $document['subject']);
              } */
            /*
             * Filter for All Subjects END
             */
            if ($document['tutionType'] != 1) {
                $this->db->where('(tutorAvailablity.typeOfTution="' . $document["tutionType"] . '" or tutorAvailablity.typeOfTution=1)');
            }
            if ($document['tutionPremises'] != 1) {
                $this->db->where('(tutorAvailablity.premise="' . $document["tutionPremises"] . '" or tutorAvailablity.premise=1)');
            }

            $this->db->where('tutor.isActive', 1);
            $this->db->where("tutor.gender", $gender);
            $this->db->having('distance<=tutorCoachingLocation.radius');
            $this->db->having('intersect_time>="01:00:00"');
        }

        $this->db->limit(5, $counter);
       // $results  = $this->db->get('tutor')->result_array();
        $resultss  = $this->db->get('tutor')->result_array();
        //  echo $this->db->last_query();die;     
        if (!empty($resultss)) {
        foreach($resultss as $val ){
           /************for get tutor rating*********/
            $this->db->select("IF(avg(rating)!='',cast(avg(rating) as decimal(10,1)),0.0) as rating");
            $this->db->where('tutorId',$val['id']);
            $rat = $this->db->get('ht_rating')->row_array();
            $val['rating'] = $rat['rating'];  
            
            /************for get fees per tutor*********/
            $this->db->select("feesPerMonth,feesPerHour");
            $this->db->where('fkSubjectId',$document['subject']);  
            $this->db->where('fkBoardId',$document['board']);
            $this->db->where('fkClassId',$document['fkClassId']);
            $this->db->where('fkTutorId',$val['id']);
            $fee = $this->db->get('ht_tutorCoachingDetail')->row_array();
            if(!empty($fee)){
            $val['feesPerMonth'] = $fee['feesPerMonth'];  
            $val['feesPerHour'] = $fee['feesPerHour'];            
            }else
            {
            $this->db->select("feesPerMonth,feesPerHour");
            $this->db->where_in('fkSubjectId',array(1,2,3,4,5,6));  
            $this->db->where('fkBoardId',$document['board']);
            $this->db->where('fkClassId',$document['fkClassId']);
            $this->db->where('fkTutorId',$val['id']);
            $fee = $this->db->get('ht_tutorCoachingDetail')->row_array();  
            $val['feesPerMonth'] = $fee['feesPerMonth'];  
            $val['feesPerHour'] = $fee['feesPerHour'];   
            }
            
            $results[] = $val;
        }
       
      
        $this->db->select("fkTutorId,isParentDeselectedTutor,fkParentRequestId");
        $this->db->where("fkParentRequestId", $requestId);
        $this->db->where("isParentDeselectedTutor", 0);
        $queryresults = $this->db->get('ht_parentRequestTutor')->result_array();
        if (!empty($queryresults)) {
            foreach ($queryresults as $querytutor) {

                $quer1[] = $querytutor['fkTutorId'];
            }
            $quer = array_unique($quer1);
        } else {
            $quer = array();
        }

        $total_record = count($results);
        if (!empty($results) && $document['tutorCode']) {
            $sendArray = array();
            foreach ($results as $data) {

                $sendArray[$data['id']] = $data;
            }
            $results = $sendArray;
        }

        $sendArray = array();
        if ($selectmore == 1) {
            $sendArray['Selected_tutor'] = $queryresults;
        } else {
            $sendArray['Selected_tutor'] = array();
        }
        $sendArray['total_record'] = $total_record;
       
        if (!empty($results)) {
            foreach ($results as $data) {

                if ($selectmore == 1 && in_array($data['id'], $quer)) {    //1=search selectmore button for
                    $data['isParentDeselectedTutor'] = 0;
                } else {   //New search tutore for
                    $data['isParentDeselectedTutor'] = 2;
                }

                if ($data['gender'] == 0) {
                    $sendArray["male"][] = $data;
                } else {
                    $sendArray["female"][] = $data;
                }
            }
            if (array_key_exists("male", $sendArray)) {
                $male_record = count($sendArray["male"]);
            } else {
                $male_record = 0;
            }
            if (array_key_exists("female", $sendArray)) {
                $female_record = count($sendArray["female"]);
            } else {
                $female_record = 0;
            }
            $sendArray['male_record'] = $male_record;
            $sendArray['female_record'] = $female_record;
            $sendArray['requestId'] = $requestId;
            return $sendArray;
        } else {
            $ListArray=array();
            $ListArray['requestId'] = $requestId;
            return $ListArray;
           // return array();
        }
        
        } else {
            $ListArray=array();
            $ListArray['requestId'] = $requestId;
            return $ListArray;
            //return array();
        }
        
    }

    function checkDuplicateEntryForStudent($document) {
        $this->db->where('name', $document['name']);
        $this->db->where('board', $document['board']);
        $this->db->where('class', $document['class']);
        $this->db->where('subject', $document['subject']);
        return $this->db->get('parent_student')->row_array();
    }

    function getParentClass($classId) {
        $id = explode('_', $classId);
        $this->db->where('id', $id[0]);
        $this->db->where('classId', $id[1]);
        $result = $this->db->get('parentClass')->row_array();
        return $result['className'];
    }

    function pickAStudent($parentId) {
        $this->db->distinct();
        $this->db->select("parent_student.id as requestId,board.id as boardId,board.name as boardName,"
                . "parent_student.class as classId,subject.id as subjectId,subject.name as subjectName,"
                . "parent_student.name,parent_student.schoolName,parent_student.studentGender,"
                . "parent_student.tutionType,parent_student.tutionPremises,parent_student.preferredTutionDays,"
                . "DATE_FORMAT(ht_parent_student.startTime, '%l:%i %p') as startTime, "
                . "DATE_FORMAT(ht_parent_student.endTime, '%l:%i %p') as endTime");
        $this->db->where('fkParentId', $parentId);
        $this->db->where('isDeleted', 0);
        $this->db->join('board', 'board.id=parent_student.board');
        $this->db->join('class', 'class.id=parent_student.fkClassId');
        $this->db->join('subject', 'subject.id=parent_student.subject');
        $result = $this->db->get('parent_student')->result_array();
        if (!empty($result)) {
            $sendArray = array();
            foreach ($result as $data) {
                $sendArray[$data['name']] = $data;
                $sendArray[$data['name']]['className'] = $this->getParentClass($data['classId']);
            }
            $sendArray = array_values($sendArray);
            return $sendArray;
        } else {
            return array();
        }
    }

    function getParentClassesByBoardId($boardId) {
        $this->db->select("concat(id,'_',classId) as classId,className");
        $this->db->where('fkBoardId', $boardId);
        $results = $this->db->get('parentClass')->result_array();
        return $results;
    }

    function getParentSubjectsByClassId($classId) {
        $classes = explode('_', $classId);
        $this->db->select('id as subjectId,name as subjectName');
        $this->db->where('fkClassId', $classes[1]);
        return $this->db->get('subject')->result_array();
    }

    function getTutors($tutorId) {
        $this->db->where('id', $tutorId);
        $this->db->where('isActive', 1);
        $result = $this->db->get('tutor')->row_array();
        return $result;
    }

    function getRequestDetail($requestId) {
        $this->db->select("board.name as boardName,parent_student.name,getParentClass(ht_parent_student.class) as className,ht_parent_student.class as ClassId,"
                . "subject.name as subjectName,subject.id as subjectId,parent_student.location,parent_student.preferredTutionDays,"
                . "DATE_FORMAT(ht_parent_student.startTime, '%l:%i %p') as startTime," . "DATE_FORMAT(ht_parent_student.endTime, '%l:%i %p') as endTime,ht_parent_student.startTime as start,ht_parent_student.endTime as end,");
        $this->db->where('parent_student.id', $requestId);
        $this->db->join('subject', 'subject.id=parent_student.subject');
        $this->db->join('board', 'board.id=parent_student.board');
        $result = $this->db->get('parent_student')->row_array();
        return $result;
    }

    function getOfferDetail($document) {
        $this->db->select("*");
        $this->db->where('fkRequestId', $document['fkParentRequestId']);
        $this->db->where('fkTutorId', $document['fkTutorId']);
        $this->db->where('isAccepted', 1);
        $result = $this->db->get('ht_offers')->row_array();
        return $result;
    }
    
    
     function checkDublicateRequest($data) {
        $this->db->select("*");
        $this->db->where('fkParentId', $data['parentId']);  
        $this->db->where('fkTutorId', $data['tutorId']);  
        $this->db->where('fkParentRequestId', $data['requestId']); 
        $this->db->where('(statusCancelOrDelete', 0); 
        $this->db->or_where('statusCancelOrDelete=1)'); 
        $result = $this->db->get('parentRequestTutor')->row_array();
        //echo $this->db->last_query();die;
        return $result;
    }

    function parentSendRequest($documents) {
       
        //pre($documents);
        if (!empty($documents)) {
            $deviceToken = array();
            $mobilePhone = array();
            foreach ($documents as $data) {
              //  pre($data);die;
                $checkDublicateRequest = $this->checkDublicateRequest($data);//check for send dublicate send request
                //print_r($checkDublicateRequest);die;
                if($data['requestId']!=0 ){                 
                $send = array();
                $send['fkParentId'] = $data['parentId'];
                $send['fkTutorId'] = $data['tutorId'];
                $send['fkParentRequestId'] = $data['requestId'];
                $send['searchQuerystring'] = $data['searchQuerystring'];
                //$send['request_send'] = 1;
                $send['createdDate'] = date('Y-m-d H:i:s');
                $send['status'] = 0;
                if(empty($checkDublicateRequest)){
                $insert_query = $this->db->insert_string('parentRequestTutor', $send);
                $insert_query               = str_replace('INSERT INTO','INSERT IGNORE INTO',$insert_query);
                $this->db->query($insert_query);
                }else
                {           
                            if($checkDublicateRequest['status']==5){//for Resend when tutor Reject request 
                             $statusCancelOrDelete['status'] = 0;  
                             $statusCancelOrDelete['statusData'] ="";
                            }  
                            $statusCancelOrDelete['isParentDeselectedTutor'] = 0;//for Resend desected tutor
                            $statusCancelOrDelete['statusCancelOrDelete'] = 0;
                            $this->db->where('fkParentId', $send['fkParentId']);  
                            $this->db->where('fkTutorId',  $send['fkTutorId']);  
                            $this->db->where('fkParentRequestId', $send['fkParentRequestId']); 
                            //$this->db->where('statusCancelOrDelete', 1); 
                            $this->db->update('parentRequestTutor', $statusCancelOrDelete);
                }
            
                $tutorInfo = $this->getTutors($data['tutorId']);
                $deviceToken[] = $tutorInfo['deviceToken'];
                $tutorName[] = $tutorInfo['name'];
                $mobilePhone[] = $tutorInfo['mobile1'];
                }
                
            }
         
            $mobilePhone = implode(',', $mobilePhone);
        
            $result = $this->getRequestDetail($send['fkParentRequestId']);
            $msg1 = '{"type":1,"message":{"location":"' . $result['location'] . '",'
                    . '"Class"     :"' . $result['className'] . '",'
                    . '"Subject"   :"' . $result['subjectName'] . '",'
                    . '"Day"       :"' . $result['preferredTutionDays'] . '",'
                    . '"Time"      :"' . $result['startTime'] . '"}}';
            $msg = '"You have been selected for a tuition request. Request details: '
                    . 'location:"' . $result['location'] . '", '
                    . 'Class   :"' . $result['className'] . '",'
                    . 'Subject :"' . $result['subjectName'] . '" ,'
                    . 'Day     :"' . $result['preferredTutionDays'] . '" ,'
                    . 'Time    :"' . $result['startTime'] . '".Please connect using itzTutor App"';
            sendAndroidPush($deviceToken, $msg1, "", "", 1);
            sendPushAsSMS($mobilePhone, $msg);
            /*****************************************push notification**************************/
            //parent
            $tutorNames = implode(',', $tutorName);
            $parentInfo = $this->getParentProfile($send['fkParentId']);
            
            $Pmsg1 = '{"type":33,"message":{"student":"' . $result['name'] . '",'
                    . '"tutor"     :"' . $tutorNames . '"}}';
            $Pmsg = '"You have successfully created a new tuition request for '
                    . '"' . $result['name'] . '" and selected'
                    . 'Tutor   :"' . $tutorNames . '"tutors. Selected tutors will contact you shortly."';
            sendAndroidPush($parentInfo['deviceToken'], $Pmsg1, "", "", 33);
            sendPushAsSMS($parentInfo['mobile1'], $Pmsg);
            /*******************************************End**************************************/
            return 1;
        }
    }

   function getTutorByIds($tutorIds, $latitude, $longitude, $parentId, $requestId) {
        //pre($tutorIds);
        $Pdocument['parentId']=$parentId;
        $Pdocument['studentId']=$requestId;
        $parentClass=$this->getStudentProfile($Pdocument);       
        $parentClassID=$parentClass['fkClassId'];
        $this->db->distinct();
        $this->db->select("tutor.id as tutorId,IFNULL((select cast(avg(ht_rating.rating) as decimal(10, 1)) from ht_rating where ht_rating .tutorId = `ht_tutor`.`id`), 0 ) as rating,tutor.profileImage,"
                . "(CASE WHEN ht_qualifications.name='Other' THEN ht_tutor.other_qualification
                            WHEN ht_qualifications.name!='' THEN ht_qualifications.name
                            ELSE '' END) as  qualification,"                
                . "tutor.name,(Year(now())-ht_tutor.dob) as age,"
                . "tutor.gender,tutor.teachingExp"); //distance in km for miles remove 1.609344 
        $this->db->join('tutorAvailablity', 'tutorAvailablity.fkTutorId=tutor.id', 'left');
        $this->db->join('tutorCoachingDetail', 'tutorCoachingDetail.fkTutorId=tutor.id', 'left');
        $this->db->join('tutorCoachingLocation', 'tutorCoachingLocation.fkTutorId=tutor.id', 'left');
        $this->db->join('qualifications', 'qualifications.id=tutor.qualification', 'left');
        $this->db->join('ht_rating', 'ht_rating.tutorId=tutor.id', 'left');
        $this->db->where_in('tutor.id', $tutorIds);
        $this->db->where('tutor.isActive', 1); 
        $result = $this->db->get('tutor')->result_array();
        //echo $this->db->last_query();
        if (!empty($result)) {
            $sendArray = array();
            foreach ($result as $data) {
                $sendArray[$data['tutorId']] = $data;
                $this->db->select("(CASE  WHEN status=0 THEN 'Pending'
                            WHEN status=1 THEN 'Connected'     
                            WHEN status=2 THEN concat('Demo Scheduled By Tutor  on ',statusData)     
                            WHEN status=3 THEN concat('Demo Done on ',statusData) 
                            WHEN status=5 THEN 'Request Rejected' 
                            ELSE '' END) as statusType");
                $this->db->where('fkTutorId', $data['tutorId']);
                $this->db->where('fkParentId', $parentId);
                $this->db->where('fkParentRequestId', $requestId);
                $results = $this->db->get('parentRequestTutor')->row_array();
                // echo $this->db->last_query();
             $sendArray[$data['tutorId']]['statusType'] = $results['statusType'];
             
            /*********************for Distance detail********************/ 
            $this->db->select("min(floor(((acos(sin(('" . $latitude . "' *pi()/180)) * sin((ht_tutorCoachingLocation.latitude*pi()/180))+cos(('" . $latitude . "' *pi()/180)) * cos((ht_tutorCoachingLocation.latitude*pi()/180)) * cos((('" . $longitude . "'- ht_tutorCoachingLocation.longitude)*pi()/180))))*180/pi())*60*1.1515*1.609344)) as distance");
            $this->db->where('fkTutorId',$data['tutorId']);
            $distance = $this->db->get('ht_tutorCoachingLocation')->row_array();      
            $sendArray[$data['tutorId']]['distance'] = $distance['distance']; 
            
              /*********************for fee detail********************/ 
            $this->db->select("feesPerMonth,feesPerHour");
            $this->db->where('fkSubjectId',$parentClass['subject']);  
            $this->db->where('fkBoardId',$parentClass['board']);
            $this->db->where('fkClassId',$parentClassID);
            $this->db->where('fkTutorId',$data['tutorId']);
            $fee = $this->db->get('ht_tutorCoachingDetail')->row_array();          
            if(!empty($fee)){            
             $sendArray[$data['tutorId']]['feesPerMonth'] = $fee['feesPerMonth'];  
             $sendArray[$data['tutorId']]['feesPerHour'] = $fee['feesPerHour'];       
            }else
            {
            $this->db->select("feesPerMonth,feesPerHour");
            $this->db->where("(tutorCoachingDetail.fkSubjectId = '{$parentClass['subject']}' or tutorCoachingDetail.fkSubjectId in(1,2,3,4,5,6))"); 
            $this->db->where('fkBoardId',$parentClass['board']);
            $this->db->where('fkClassId',$parentClassID);
            $this->db->where('fkTutorId',$data['tutorId']);
            $fee = $this->db->get('ht_tutorCoachingDetail')->row_array();  
          
             $sendArray[$data['tutorId']]['feesPerMonth'] = $fee['feesPerMonth'];  
             $sendArray[$data['tutorId']]['feesPerHour'] = $fee['feesPerHour']; 
            }
             if($fee['feesPerMonth']!=''){
              $sendArray[$data['tutorId']]['feesPerMonth'] = $fee['feesPerMonth'];     
             }else{
              $sendArray[$data['tutorId']]['feesPerMonth'] = "0";     
             }
               if($fee['feesPerHour']!=''){
              $sendArray[$data['tutorId']]['feesPerHour'] = $fee['feesPerHour'];     
             }else{
              $sendArray[$data['tutorId']]['feesPerHour'] = "0";     
             }
             /*********************End for fee detail********************/    
                
            }
            if (!function_exists('build_sorter')) {

                function build_sorter($key) {
                    return function ($a, $b) use ($key) {
                        return strnatcmp($a[$key], $b[$key]);
                    };
                }

            }

            usort($sendArray, build_sorter('statusType'));
            $sendArray = array_values($sendArray);
           
           
            return $sendArray;
        } else {
            return array();
        }
    }

    function getAllOfferByRequestIds($parentId, $requestId, $board, $class, $subject) {
        $this->db->select("if(ht_offers.isAccepted=2,1,0) as isOfferRejected,if(ht_offers.isAccepted=1,2,0) as isOfferAccepted,tutor.id as tutorId,tutor.profileImage,offers.id as offerId,(CASE  WHEN ht_offers.location=1 THEN 'Tutor'
                                  WHEN ht_offers.location=2 THEN 'Student'     
                              ELSE 'Other' END) as  premises,if(ht_offers.type=2,'Individual','Group') as type,if(ht_offers.mode=0,'Monthly','Hourly') as mode,"
                . "offers.fees,offers.start as startDate,offers.days,DATE_FORMAT(ht_offers.time, '%l:%i %p') as startTime");
        $this->db->join('tutor', 'tutor.id=offers.fkTutorId');
        $this->db->where('offers.fkRequestId', $requestId);
        $this->db->where('offers.fkParentId', $parentId);
        $this->db->order_by('offers.isAccepted');
        $result = $this->db->get('offers')->result_array();
        if (!empty($result)) {
            $sendArray = array();
            foreach ($result as $data) {
                $sendArray[$data['offerId']] = $data;
                $sendArray[$data['offerId']]['boardName'] = $board;
                $sendArray[$data['offerId']]['className'] = $class;
                $sendArray[$data['offerId']]['subjectName'] = $subject;
            }
            $sendArray = array_values($sendArray);
            return $sendArray;
        } else {
            return array();
        }
    }

    function getTabsDataForParent($document) {
        $this->db->select("parentRequestTutor.isParentDeselectedTutor,parentRequestTutor.statusCancelOrDelete as statusCancel,(CASE  WHEN ht_parent_student.tutionType=1 THEN 'Any'
                              WHEN ht_parent_student.tutionType=2 THEN 'Individual'     
                              ELSE 'Group' END) as tutionType,                         
                            (CASE  WHEN ht_parent_student.tutionPremises=1 THEN 'Any'
                                  WHEN ht_parent_student.tutionPremises=2 THEN 'Parent'     
                                  ELSE 'Tutor' END) as tutionPremises,
                            parentRequestTutor.statusCancelOrDelete,parentRequestTutor.fkParentId as parentId,parentRequestTutor.fkTutorId as tutorId,parentRequestTutor.searchQuerystring as searchQuerystring,
                            group_concat(DISTINCT ht_parentRequestTutor.fkTutorId) as tutorIds,parentRequestTutor.fkParentRequestId as requestId,
                            parent_student.latitude,parent_student.longitude");
        $this->db->group_by('parentRequestTutor.fkParentRequestId');
        $this->db->order_by('parentRequestTutor.statusOffer');
        $this->db->order_by('parentRequestTutor.isParentDeselectedTutor	');
        $this->db->order_by('parentRequestTutor.createdDate', 'desc');
        $this->db->where('parentRequestTutor.fkParentId', $document['fkParentId']);
        if ($document['tabName'] == 1) {
            $this->db->where('parentRequestTutor.status!=', 4);
            $this->db->where('parentRequestTutor.isParentDeselectedTutor!=', 1);
        } elseif ($document['tabName'] == 2) {
            $this->db->where('parentRequestTutor.status', 4);
           // $this->db->where('parentRequestTutor.statusOffer!=', 1);
            //$this->db->where('parent_student.isRequestClosed', 0);
        } elseif ($document['tabName'] == 3) {
            $this->db->where('parentRequestTutor.status', 4);
            $this->db->where('parentRequestTutor.statusOffer', 1);
        }
        $this->db->join('parent_student', 'parent_student.id=parentRequestTutor.fkParentRequestId', 'left');
        $results = $this->db->get('parentRequestTutor')->result_array();
      
        if (!empty($results)) {
            $sendArray = array();
            foreach ($results as $data) {
                $arrayIds = explode(',', $data['tutorIds']);
                $getRequestDetail = $this->getRequestDetail($data['requestId']);
                $sendArray[$data['requestId']]['requestId'] = $data['requestId'];
                $sendArray[$data['requestId']]['parentId'] = $data['parentId'];
                $sendArray[$data['requestId']]['tutionPremises'] = $data['tutionPremises'];
                $sendArray[$data['requestId']]['tutionType'] = $data['tutionType'];
                //$sendArray[$data['requestId']]['statusType']         = $data['statusType'];
                $sendArray[$data['requestId']]['statusCancelOrDelete'] = $data['statusCancelOrDelete'];
                //$sendArray[$data['requestId']]['request_send'] = $data['request_send'];
                $sendArray[$data['requestId']]['searchQuerystring'] = $data['searchQuerystring'];
                $sendArray[$data['requestId']]['name'] = $getRequestDetail['name'];
                $sendArray[$data['requestId']]['className'] = $getRequestDetail['className'];
                $sendArray[$data['requestId']]['subjectName'] = $getRequestDetail['subjectName'];
                $sendArray[$data['requestId']]['startTime'] = $getRequestDetail['startTime'];
                $sendArray[$data['requestId']]['endTime'] = $getRequestDetail['endTime'];
                $sendArray[$data['requestId']]['startClass'] = $getRequestDetail['start'];
                $sendArray[$data['requestId']]['endClass'] = $getRequestDetail['end'];
                $sendArray[$data['requestId']]['preferredTutionDays'] = $getRequestDetail['preferredTutionDays'];
                $sendArray[$data['requestId']]['noTutorSelected'] = count($arrayIds);
                $sendArray[$data['requestId']]['checkstatus'] = 0;
                $latitude = $data['latitude'];
                $longitude = $data['longitude'];

                if ($document['tabName'] == 1) {//Myrequest
                    $sendArray[$data['requestId']]['tutors'] = $this->getTutorByIds($arrayIds, $latitude, $longitude, $data['parentId'], $data['requestId']);
                } elseif ($document['tabName'] == 2) {//Offer
                    $sendArray[$data['requestId']]['offers'] = $this->getAllOfferByRequestIds($data['parentId'], $data['requestId'], $getRequestDetail['boardName'], $getRequestDetail['className'], $getRequestDetail['subjectName']);
                     $offer=$sendArray[$data['requestId']]['offers'];                 
                    if(!empty($offer)){
                    foreach($offer as $key =>$offervale){                      
                        if($offervale['isOfferAccepted']==2){
                            $sendArray[$data['requestId']]['checkstatus'] = 1;
                        }
                    }
                  }
                } elseif ($document['tabName'] == 3) {//enrolled
                    $sendArray[$data['requestId']]['enrolled'] = $this->getTutorByIds($arrayIds, $latitude, $longitude, $data['parentId'], $data['requestId']);
                }
                //echo $this->db->last_query();
            }
            $sendArray = array_values($sendArray);
            return $sendArray;
        } else {
            return array();
        }
    }

   function updateCancelOrDelete($document) {
        $tutorID=$document['fkTutorId'];
        unset($document['fkTutorId']);
        $this->db->where('fkParentId', $document['fkParentId']);
        $this->db->where('fkParentRequestId', $document['fkParentRequestId']);
        $this->db->where('fkTutorId', $tutorID);
        $this->db->update("parentRequestTutor", $document);
    }

    function updateOfferStatus($document) { 
        $this->db->where('status', 4);
        $this->db->where('fkParentRequestId', $document['fkParentRequestId']);
        $this->db->where('statusData', $document['statusData']);
        $result= $this->db->update("parentRequestTutor", $document);
       // echo $this->db->last_query();die;  
        if($this->db->affected_rows()){
       /*$offer['isAccepted']=$document['statusOffer'];
        $this->db->where('fkRequestId', $document['fkParentRequestId']);
        $this->db->where('fkTutorId', $document['fkTutorId']);      
        $updateoffer=$this->db->update("ht_offers", $offer);*/
       
        return true;
        }else{           
            return false;
        }        
       
    }


    function getReasonsName($reasonId) {
        $this->db->select('name as reasonName');
        $this->db->where('id', $reasonId);
        $result = $this->db->get('reasons')->row_array();
        return $result['reasonName'];
    }

    function deSelectTutor($data) {
        $this->db->where('fkParentRequestId', $data['fkParentRequestId']);
        $this->db->where('fkParentId', $data['fkParentId']);
        $this->db->where('fkTutorId', $data['fkTutorId']);
        $this->db->where('statusOffer!=', 1);
        $result = $this->db->update('parentRequestTutor', $data);
    }

    function isTutorCodeExist($tutorCode) {
        $this->db->where('referalCode', $tutorCode);
        $results = $this->db->get('tutor')->row_array();
        if (!empty($results)) {
            return true;
        } else {
            return false;
        }
    }

    /********************************************************santosh************************************ */

    function getStudentEnrolled($data) {
        //$curdate = date("Y-m-d");
        $curdate = date("Y-m-d h:i:s");        
        $this->db->select('ht_tutor.id as tutorId,'
                . '(select ht_offers.fees from ht_offers where ht_offers.fkTutorId=ht_tutor.id and ht_offers.fkRequestId=ht_class_schedule.fkStudentID) as fees,'
                . '(CASE WHEN (select ht_offers.mode from ht_offers where ht_offers.fkTutorId=ht_tutor.id and ht_offers.fkRequestId=ht_class_schedule.fkStudentID)=0 THEN "Monthly" ELSE "Hourly" END)as feesMode,'
                . 'ht_tutor.name as tutorName,ht_tutor.gender as tutorGender,'
                . 'ht_tutor.dob as tutorDOB,ht_tutor.profileImage as tutorProfileImage,parent_student.name as studentName,'
                . '(select ht_offers.days from ht_offers where ht_offers.fkTutorId=ht_tutor.id and ht_offers.fkRequestId=ht_class_schedule.fkStudentID) as TutionDays,'               
                . 'ht_class_schedule.*,'
                . 'DATE_FORMAT( ht_class_schedule.start_time, "%h:%i %p") as start_time,'
                . 'DATE_FORMAT( ht_class_schedule.class_date, "%a %D %b") as classDate,'
                . 'CONCAT(ht_class_schedule.class_date," ",ht_class_schedule.start_time) as CheckDate,'
                . 'ht_subject.name as subjectName,ht_ParentDuePayment.dueDate');
        $this->db->where('ht_class_schedule.fkParentID', $data['fkParentID']);
        //$this->db->where("ht_class_schedule.class_date >='" . $curdate . "'");
        $this->db->where("(ht_class_schedule.class_status=0");
        $this->db->or_where("ht_class_schedule.class_status=4)");        
        $this->db->order_by('ht_class_schedule.class_date ASC');
        $this->db->order_by('ht_class_schedule.start_time ASC');        
       // $this->db->group_by('ht_class_schedule.fkStudentID');
        $this->db->having("CheckDate>='" . $curdate . "'");
        $this->db->join('ht_tutor', 'ht_class_schedule.fkTutorId=ht_tutor.id');
        $this->db->join('parent_student', 'ht_class_schedule.fkStudentID=parent_student.id');
        $this->db->join('ht_subject', 'ht_class_schedule.subject_id=ht_subject.id');       
        $this->db->join('ht_offers', 'ht_offers.fkRequestId=ht_class_schedule.fkStudentID');  
        $this->db->join('ht_ParentDuePayment', 'ht_ParentDuePayment.fkStudentId=ht_class_schedule.fkStudentID');  
        $result_data = $this->db->get('ht_class_schedule')->result_array();
         //echo  $this->db->last_query();die;
      // pre($result_data);        
        $new_array = array();
      if(!empty($result_data)){ 
                 foreach($result_data as $val){
                     if(!in_array($val['fkStudentID'],$new_array)){
                         $new_array[] = $val['fkStudentID'];
                         $final[] = $val;
                     }
                 }
         
        foreach($final as $r){
            $this->db->select('count(ht_topics.id) as assignmentList');
            $this->db->where('ht_assignment_master.student_id', $r['fkStudentID']);
            $this->db->where('ht_assignment_master.status', 0);
            $this->db->from('ht_assignment_master ');
            $this->db->where('ht_topics.subject_id', $r['subject_id']);
            $this->db->join('ht_topics', 'ht_topics.id= ht_assignment_master.`topic_id');
            $result = $this->db->get()->row_array();
               //  echo $this->db->last_query();
           //print_r($result);
            if($result){
                $r['assignment'] = $result['assignmentList'];
                $final_result[] =  $r;
            }else{
                $r['assignment'] = 0;
                $final_result[] =  $r;
            }
        }       
      return $final_result;
      }else{
          return false; 
      }
    }


    /* Parent  enrolled class End */

    function cancelEnrolledClass($data) {
        $data['class_status'] = 3;
        $data['statusData1'] = 1;
        $classScheduledID = $data['classScheduledID'];
        unset($data['classScheduledID']);
        $data['modifiedDate'] = date("Y-m-d H:i:s");
        $this->db->where('id', $classScheduledID);
        $this->db->where('fkParentID', $data['fkParentID']);
        $this->db->where('fkStudentID', $data['fkStudentID']);
        $this->db->where('fkTutorId', $data['fkTutorId']);
        return $this->db->update('ht_class_schedule', $data);
    }

    /* Parent cancel enrolled class End */

    function terminateEnrolledClass($data) {
     
        $data['class_status'] = 4;
        $data['statusData1'] = 1;
        $this->db->where('fkParentID', $data['fkParentID']);
        $this->db->where('fkStudentID', $data['fkStudentID']);
        $this->db->where('fkTutorId', $data['fkTutorId']);
        $this->db->where('class_status', 0);
        $result = $this->db->update('ht_class_schedule', $data);
       
        return $result;
    }

    /* Parent Terminate enrolled class End */

    function getAllReasonsParent($data) {
        $this->db->select("id as reasonId,name as reasonName");
        $this->db->where('reasonsType', $data['tabName']);
        $this->db->where('isParent', $data['isParent']);
        return $this->db->get('reasons')->result_array();
    }

    /* Parent Reason class End */
    
     function getStudSchedulerHist($data) {
        $this->db->select("GROUP_CONCAT(DISTINCT ht_topics.topic_name) as Topic,DATE_FORMAT(ht_class_done_topic.createdDate, '%D %b %Y') as date");
        $this->db->join('ht_topics', 'ht_topics.id=ht_class_done_topic.topic');
       // $this->db->where('topic_status', 1);
        $this->db->where('fkStudentID', $data['fkStudentID']);
        $this->db->group_by('date');
        $this->db->order_by('date');
        return $this->db->get('ht_class_done_topic')->result_array();
    }

    /* Parent Reason class End */
    
    function addPstudent($document) {
        $insert_query = $this->db->insert_string('parent_student', $document);
        $insert_query = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insert_query);
        $this->db->query($insert_query);
        $insert_id = $this->db->insert_id();
         /**************Update push send**************************/
            $getStu['parentId']= $document['fkParentId'];
            $getStu['studentId']= $insert_id;       
            $studentInfo = $this->getStudentProfile($getStu);
            $parentInfo = $this->getParentProfile($document['fkParentId']);
            $pushData = '{"type":43,"message":{"StudentName":"'. $studentInfo['name'] .'"}}';           
            sendAndroidPush($parentInfo['deviceToken'], $pushData, "", "", 43);            
            $smsData = 'You have added profile of '.$studentInfo['name'].'' ;
            $ressmsData = sendPushAsSMS($parentInfo['mobile1'], $smsData);
          /*****************End****************************/
        if ($insert_id) {
            return $requestId = $insert_id;
        }
         else {
            return array();
        }
    }
      
    /*****************************Get Tab Data**************************************************************/
      function getTabsDataParentCount($document) {
       for($tab=1;$tab<=3;$tab++){
         
        $this->db->select("parentRequestTutor.isParentDeselectedTutor,parentRequestTutor.statusCancelOrDelete as statusCancel,
                            parentRequestTutor.statusCancelOrDelete,parentRequestTutor.fkParentId as parentId,parentRequestTutor.searchQuerystring as searchQuerystring,
                            group_concat(DISTINCT ht_parentRequestTutor.fkTutorId) as tutorIds,parentRequestTutor.fkParentRequestId as requestId,
                            parent_student.latitude,parent_student.longitude");
        $this->db->group_by('parentRequestTutor.fkParentRequestId');
        $this->db->order_by('parentRequestTutor.statusOffer');
        $this->db->order_by('parentRequestTutor.isParentDeselectedTutor	');
        $this->db->order_by('parentRequestTutor.createdDate', 'desc');
        $this->db->where('parentRequestTutor.fkParentId', $document['fkParentId']);
        if ($tab == 1) {
            $this->db->where('parentRequestTutor.status!=', 4);
            $this->db->where('parentRequestTutor.isParentDeselectedTutor!=', 1);
        } elseif ($tab == 2) {
            $this->db->where('parentRequestTutor.status', 4);
           // $this->db->where('parentRequestTutor.statusOffer!=', 1);
           // $this->db->where('parent_student.isRequestClosed', 0);
        } elseif ($tab == 3) {
            $this->db->where('parentRequestTutor.status', 4);
            $this->db->where('parentRequestTutor.statusOffer', 1);
        }
        $this->db->join('parent_student', 'parent_student.id=parentRequestTutor.fkParentRequestId', 'left');
        $results = $this->db->get('parentRequestTutor')->result_array();
       // print_r($results);
        if ($tab == 1) {
           $total['Request']=  count($results);                    
        } elseif ($tab == 2) {
            $total['offer']=  count($results);
        } elseif ($tab == 3) {
           $total['enrollement']=  count($results);
        }
       
    }
      return $total;
     
    }

    /*****************************End Get Tab Data********************************************************/
    
        
    /**************************Start GenrateAutoScheduler************************************** */
     function genrateAutoScheduler($document) {
        $this->db->distinct();
        $this->db->select("parentRequestTutor.fkParentId as parentId,"
                . "parentRequestTutor.fkParentRequestId as requestId,"
                . "parentRequestTutor.fkTutorId as tutorId,");
     
            $this->db->where('parentRequestTutor.status', 4);
            $this->db->where('parentRequestTutor.statusOffer', 1);
      
        $this->db->where('parentRequestTutor.statusCancelOrDelete', 0);  
        $this->db->join('parent_student', "parent_student.id=parentRequestTutor.fkParentRequestId");
        $this->db->order_by('parentRequestTutor.createdDate', 'desc');
        $result = $this->db->get('parentRequestTutor')->result_array();      
        return $result;
    }
/****************************End GenrateAutoScheduler************************************************* */
    
     /**************************Start CheckScheduler************************************** */
     function CheckScheduler($document) {
        
        $this->db->select("*");      
        $this->db->where('fkParentID', $document['fkParentID']);
        $this->db->where('fkStudentID', $document['fkStudentID']); 
        $this->db->where('fkTutorId', $document['fkTutorId']); 
        $this->db->where('class_date', $document['class_date']); 
        $result = $this->db->get('ht_class_schedule')->row_array();      
        return $result;
    }
/****************************End CheckScheduler************************************************* */
    
         /**************************Start PaymentDueDate************************************** */
     function CheckPaymentDueDate($document) {        
        $this->db->select("*");      
        $this->db->where('fkParentID', $document['fkParentId']);
        $this->db->where('fkStudentID', $document['fkStudentId']); 
        $this->db->where('fkTutorId', $document['fkTutorId']);       
        $result = $this->db->get('ht_ParentDuePayment')->row_array();      
        return $result;
    }
/****************************End PaymentDueDate************************************************* */
    
      /**************************Get Request AllTutor when  one offer accept************************************** */
     function RequestAllTutor($document) {
        $this->db->select("fkTutorId"); 
        $this->db->where('fkParentRequestId', $document['fkParentRequestId']); 
        $this->db->where('statusOffer',0);  
        $this->db->where('statusData!=',$document['statusData']);  
        $result = $this->db->get('ht_parentRequestTutor')->result_array();      
        return $result;
    }
/****************************End ************************************************ */
       

/*********************************************************************santosh work end************************* */
    
     function getParentSchemeRefer($data) {
        $result = array();
        $this->db->select("id,price,scheme");
        $result['scheme'] = $this->db->get('ht_scheme')->row_array();
        $ParentInfo = $this->getParentProfile($data['fkFromTutorId']);
        $result['ReferalCodeFrom'] = $ParentInfo['referalCode'];
        return $result;
    }



    /*     * ****************gaurav 27-7-2016*************** */
    /*
     * get student assignment
     */

    public function get_assignment($id) {
        $this->db->select('ht_parent_student.id, ht_assignment_master.id as assignment_id,ht_assignment_master.topic_id,ht_assignment_master.reassign_state,ht_assignment_master.status, ht_topics.topic_name,ht_topics.subject_id,ht_subject.name as subject_name, ht_parent_student.name');
        $this->db->where('ht_parent_student.fkParentId', $id);
        $this->db->from('ht_parent_student');
        $this->db->join('ht_assignment_master', 'ht_assignment_master.student_id = ht_parent_student.id');
        $this->db->join('ht_topics', 'ht_topics.id=ht_assignment_master.topic_id');
        $this->db->join('ht_subject', 'ht_topics.subject_id=ht_subject.id');
        $result = $this->db->get()->result_array();
        return $result;
    }

    /*
     * get student assignment question
     */

    public function get_question($id) {
        $question_id = array();
        $question_total = 10;
        $question_count = 0;
        $this->db->select('ht_assignment_master.id,(SELECT count(*) as total_count FROM ht_assignment_master_student where `assignment_id` = ht_assignment_master.id ) as count');
        $this->db->where($id);
        $result1 = $this->db->get('ht_assignment_master')->row_array();
        if ($result1['count'] > 0) {
            $data_reassign = array('reassign_state' => '1');
            $this->db->where($id);
            $this->db->update('ht_assignment_master', $data_reassign);
        }
        if (!empty($result1)) {
            $this->db->select('board,class,subject');
            $this->db->where('id', $id['student_id']);
            $student_record = $this->db->get('ht_parent_student')->row_array();
          if(!empty($student_record)){
            $query_array = array('board_id' => $student_record['board'], 'class_id' => $student_record['class'], 'subject_id' => $student_record['subject'], 'topic_id' => $id['topic_id'], 'status' => 'v');
            $this->db->select('id as question_id, set_key');
            $this->db->where($query_array);
            $this->db->order_by("set_key", "asc");
            $r = $this->db->get('ht_question_master')->result_array();
            if (!empty($r)) {
                $key_array = array();
                foreach ($r as $c) {
                    if (!in_array($c['set_key'], $key_array)) {
                        $key_array[] = $c['set_key'];
                    }
                }
                $final_array = array();
                foreach ($r as $pre) {
                    foreach ($key_array as $do) {
                        if ($do == $pre['set_key']) {
                            $final_array[$do][] = $pre['question_id'];
                        }
                    }
                }
                if (count($r) > $question_total) {
                    $count = count($final_array);
                    $set_count = floor($question_total / $count);
                    if ($set_count == 0) {
                        $set_count = 1;
                    }
                    while ($question_count < $question_total) {
                        $detail = $this->question_id_detail($final_array, $set_count, $question_count, $question_total, $question_id);
                        $question_id = $detail['question_id'];
                        $question_count = $detail['question_count'];
                    }
                } else {
                    foreach ($final_array as $q) {
                        foreach ($q as $j) {
                            $detail['question_id'][] = $j;
                        }
                    }
                }
                $result = array();
                foreach ($detail['question_id'] as $question) {
                    $this->db->where('id', $question);
                    $result[] = $this->db->get('ht_question_master')->row_array();
                }
                return $result;
            } else {
                return false;
            }
          }else{
            return false;
          }
        } else {
            return false;
        }
    }


    function question_id_detail($final_array, $set_count, $question_count, $question_total, $question_id) {

        foreach ($final_array as $q) {
            for ($i = 0; $i < $set_count; $i++) {
                shuffle($q);
                $random_keys = array_rand($q, 1);
                $random_value = $q[$random_keys];

                if (!in_array($random_value, $question_id)) {
                    $question_id[] = $q[$random_keys];
                    $question_count = $question_count + 1;
                }
                if ($question_count == $question_total) {
                    break;
                }
            }
            if ($question_count == $question_total) {
                break;
            }
        }

        $detail = array();
        $detail['question_id'] = $question_id;
        $detail['question_count'] = $question_count;
        return $detail;
    }

    public function set_assignment_download($id) {
        $data = array(
            'status' => $id['status']
        );
        $this->db->where('id', $id['assignment_id']);
        $result = $this->db->update('ht_assignment_master', $data);
        $r = $this->db->affected_rows();
        if ($r) {
            return true;
        } else {
            return false;
        }
    }

    public function set_assignment_complete($record) {
        $this->db->where('assignment_id', $record['assignment_id']);
        $r = $this->db->get('ht_assignment_master_student');
        $total = $r->num_rows();
        if ($total < 3) {
            $data = array(
                'assignment_id' => $record['assignment_id'],
                'completion_time' => $record['completion_time'],
                'completed_date' => $record['completed_date'],
                'score' => $record['score'],
                'total_question' => $record['total_question'],
                'attempted_question' => $record['attempted_question'],
                'correct_answer' => $record['correct_answer']
            );
            $r = $this->db->insert('ht_assignment_master_student', $data);
            if ($r) {
                /******************push notification****************/
                $assignment_id =$record['assignment_id'];
                $push_data = $this->db->query("SELECT ht_tutor.mobile1,ht_tutor.deviceToken,ht_tutor.name as tutor_name ,ht_topics.topic_name,ht_parent_student.name as student_name FROM ht_assignment_master
                inner join ht_topics on ht_topics.id = ht_assignment_master.topic_id
                inner join ht_parent_student on ht_parent_student.id = ht_assignment_master.student_id
                inner join ht_parentRequestTutor on ht_parentRequestTutor.fkParentRequestId = ht_assignment_master.student_id
                inner join ht_tutor on ht_tutor.id = ht_parentRequestTutor.fkTutorId
                where ht_parentRequestTutor.statusOffer = 1
                and ht_assignment_master.id = $assignment_id")->row_array();
                $pushData = '{"type":39,"message":{"StudentName":"' . $push_data['student_name'] . '",'
                . '"TopicName" :"' . $push_data['topic_name'] . '"}}';
                sendAndroidPush($push_data['deviceToken'], $pushData, "", "", 20);
                $smsData = "Your Student ".$push_data['student_name']." has completed an assignment of ".$push_data['topic_name'].".";
                $ressmsData = sendPushAsSMS($push_data['mobile1'], $smsData);
                /******************end********************************/
                $data = array('reassign_state' => '0', 'status' => '2');
                $this->db->where('id', $record['assignment_id']);
                $this->db->update('ht_assignment_master', $data);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function get_assignment_detail($id) {
        $student_id = $id['student_id'];
        $this->db->select('ht_assignment_master.*,(SELECT count(*) as total_count FROM ht_assignment_master_student where `assignment_id` = ht_assignment_master.id ) as count,
  (select topic_name from ht_topics where id = ht_assignment_master.topic_id) as topic_name,
  (select name from ht_parent_student where id = ht_assignment_master.student_id) as student_name,
  ht_subject.name as subject_name');
        $this->db->where('ht_assignment_master.student_id', $student_id);
        $this->db->from('ht_assignment_master');
        $this->db->join('ht_topics', 'ht_topics.id=ht_assignment_master.topic_id');
        $this->db->join('ht_subject', 'ht_topics.subject_id=ht_subject.id');
        $result = $this->db->get()->result_array();
        foreach ($result as $a) {
            if ($a['count'] > 0) {
                $result_data[] = $a;
            }
        }
        if (!empty($result_data)) {
            return $result_data;
        } else {
            return false;
        }
    }

    public function get_assignment_full_detail($id) {
        $this->db->where('ht_assignment_master_student.assignment_id', $id);
        $result = $this->db->get('ht_assignment_master_student')->result_array();
        if (!empty($result)) {
            return $result;
        } else {
            return false;
        }
    }

    /*     * ****************End gaurav 27-7-2016*************** */
}
