<?php
	class Share_tutor_model extends CI_Model{
	function __Construct(){
		parent::__Construct();
	}
	public function get_tutor_mobile_number($data){
		$check_array = array('fkParentRequestId'=>$data,'status' => '4','statusOffer' =>'1');
		$this->db->select('ht_tutor.mobile1');
		$this->db->where($check_array);
		$this->db->from('ht_parentRequestTutor');
		$this->db->join('ht_tutor','ht_tutor.id = ht_parentRequestTutor.fkTutorId');
		$tutor_mobile_no = $this->db->get()->row_array();
		if($tutor_mobile_no){
			return $tutor_mobile_no;
		}else{
			return false;
		}

	}
	function tutor_offer_show_screen($id){
		$result = $this->db->query("select id,offer,start_date,end_date from ht_tutor_offers where FIND_IN_SET($id, tutor_id) order by id desc")->result_array();
		if($result){
			return $result;
		}else{
			return false;
		}

	}
	function image_for_how_it_work($status){
		$this->db->select('*');
		$this->db->where('status',$status);
		$result = $this->db->get('ht_how_it_work')->result_array();
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	function image_for_why_chooses($status){
		$this->db->select('*');
			$this->db->where('status',$status);
		$result = $this->db->get('ht_why_choose')->result_array();
		if($result){
			return $result;
		}else{
			return false;
		}
	}
}
?>
