<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Parents
 *
 * @author appsquadz
 */
class ParentsAccount extends MX_Controller {
    
    function __construct()
    {
       // error_reporting(0);
       parent::__construct();
        $this->load->model('parentsAccountModel'); 
        $this->load->library('email');
        // date_default_timezone_set("Asia/Bankok");
    } 
    
    
    
      
      public function getAccountDetails()
    {           
         
         
         
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();
            $document['parentId']           = $json['parentId'];
            //$document['tutorId']            = $json['tutorId'];
           
          
          
            $isSuccess                      = false;
            $obj                            = new ParentsAccountModel;
           
               
            $results                        = $obj->getAccountDetails($document);
                    
                    if($results)
                    { 
                        
                        foreach($results as $val)
                        {
$val['dueDate'] = "2016-12-31";

                            $box = $val['fkStudentId'].$val['fkSubjectId'].$val['fkTutorId'];
                            if(isset($final[$box])and !empty($final[$box]))
                            {
                                $final[$box][] = $val;
                            }
                            else
                            {
                                $final[$box] = array();
                                $final[$box][] = $val;
                            }
                        }
                        
                        
                        
                        $isSuccess      = true;
                        $message        = "Details Shown Successfully";
                        $data           = array_values($final);
                    }
                    else
                    {
                        $isSuccess      = false;
                        $message        = "Details Couldnt Show ";
                        $data           = array();
                    }

        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }

        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }
    

       
    public function updateTutorAmountAfterPayment()
    {            
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();
            $document['paymentData']        = $json['paymentData'];
           /* $document['tutorId']            = $json['tutorId'];
            $document['studentId']          = $json['studentId'];
            $document['amount']             = $json['amount'];
            $document['date']               = $json['date'];
            $document['particulars']        = $json['particulars']; */
          
          
            $isSuccess                      = false;
            $obj                            = new ParentsAccountModel;
           
              foreach($json['paymentData'] as $data) 
                        {   
                            
                             $results                        = $obj->updateTutorAmountAfterPayment($data);
                        }
            
               
            //$results                        = $obj->updateTutorAmountAfterPayment($document);
                    
                    if($results)
                    {                                          
                        $isSuccess      = true;
                        $message        = "Details Shown Successfully";
                        $data           = $results;
                    }
                    else
                    {
                        $isSuccess      = false;
                        $message        = "Details Couldnt Show ";
                        $data           = array();
                    }

        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }

        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }
    
    
   /*  public function getAccountDetails()
    {            
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();
            $document['parentId']           = $json['parentId'];
            $document['tutorId']            = $json['tutorId'];
           
          
          
            $isSuccess                      = false;
            $obj                            = new ParentsAccountModel;
           
               
            $results                        = $obj->getAccountDetails($document);
                    
                    if($results)
                    {                                          
                        $isSuccess      = true;
                        $message        = "Details Shown Successfully";
                        $data           = $results;
                    }
                    else
                    {
                        $isSuccess      = false;
                        $message        = "Details Couldnt Show ";
                        $data           = array();
                    }

        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }

        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }   */


	public function getClassesAmountDetails()
    {            
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();
            $document['parentId']               = $json['parentId'];
          
          
            $isSuccess                      = false;
            $obj                            = new ParentsAccountModel;
           
               
            $results                        = $obj->getClassesAmountDetails($document);
                    
                    if($results)
                    {                                          
                        $isSuccess      = true;
                        $message        = "Details Shown Successfully";
                        $data           = $results;
                    }
                    else
                    {
                        $isSuccess      = false;
                        $message        = "Details Couldnt Show ";
                        $data           = array();
                    }

        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }

        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }

    
    
    
    
}
