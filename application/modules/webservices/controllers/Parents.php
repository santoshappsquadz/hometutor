<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parents extends MX_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();

        $this->load->model('parent_model');

        $this->load->library('email');
        //$this->load->helper('aes'); 
    }

    /*
     * 
     */

    public function parentsSignUp() {
        $json = file_get_contents('php://input');
        //$post                                   = $this->input->post();         
        if (is_json($json)) {
            $document = array();
            $json = json_decode($json, true);

            $document['password'] = md5($json['password']);
            $document['mobile1'] = $json['mobile'];
            $document['referalCode'] = $json['referalCode']?$json['referalCode']:substr($json['name'],0, 5).rand(1000,9999);
            //$document['referalCode'] = $json['referalCode'];
//            $document['fbId']                       = $json['fbId'];
//            $document['googleId']                   = $json['googleId'];
//            $document['socialType']                 = $json['socialType'];           
            $document['isActive'] = 1;
            $document['createdDate'] = date('Y-m-d H:i:s');
            $isSuccess = false;
            $obj = new parent_model;

            $isMobileAlreadyExist = $obj->isMobileAlreadyExist($json['mobile1']);
            if (!$isMobileAlreadyExist) {
                $insertId = $obj->parentSignUp($document);
                if ($insertId) {
                    $isSuccess = true;
                    $message = "Parent Signup successfull";
                    $data = array();
                } else {
                    $isSuccess = false;
                    $message = "Parent Signup not successfull";
                    $data = array();
                }
            } else {
                $isSuccess = false;
                $message = "Mobile already exist";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }
    
      function parentgetOTP() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;  
            $obj = new parent_model;
            $isMobileAlreadyExist = $obj->isMobileAlreadyExist($json['mobile']);
            if($json['referalBy']){
            $this->load->model('tutor_model');
            $checkReferCode = $this->tutor_model->checkReferCode($json['referalBy']);
            }else{
             $checkReferCode = true;  
            }
            if (!$isMobileAlreadyExist) {
            if ($checkReferCode) {       
            $output = sendSMS($json['mobile']);
            //echo $output;
            if ($output['success'] == 1) {
                $isSuccess = true;
                $data = array(array("otp" => $output['data']));
                $message = "OTP sent to mobile.";
            } else {
                $isSuccess = false;
                $message = "OTP not sent to mobile," . $output['data'];
                $data = array();
            }
            }else {
                $isSuccess = false;
                $message = "Referal code not exists";
                $data = array();
            } 
            }else {
                $isSuccess = false;
                $message = "Mobile number already exists";
                $data = array();
            }            
            
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*
     * Request Peram:
      {
      "id": "",
      "name": "Raman Chauhan",
      "email": "raman@gmail.com",
      "profileImage":"",
      "password": "123456",
      "mobile1": "",
      "phone": "0",
      "address1": "Shimla",
      "address2": "Solan",
      "city": "ios",
      "latitude": "",
      "longitude": "0",
      "referalCode": "0",
      "fbId": "0",
      "googleId": "0",
      "socialType": "0"
      }
     * 
     */

    public function parentSignUp() {
        $json = file_get_contents('php://input');
        //$post                                   = $this->input->post();         
        if (is_json($json)) {
            $document = array();
            $json = json_decode($json, true);
            $document['id'] = $json['id'];
            //$document['email']                      = $json['email'];
            if ($json['id'] == "") {
                $document['mobile1'] = $json['mobile1'];               
               // $document['referalCode'] = $json['referalCode'];               
               // $document['referalCode'] = substr($json['name1'], 0, 4) . rand(1000, 9999);
                $document['referalCode'] = 'HTP'. rand(1000, 99999);
                $document['referalBy']   = $json['referalBy'];
                $document['password']    = md5($json['password']);                
                $document['createdDate'] = date('Y-m-d H:i:s');
                
            }
            $document['name'] = $json['name'];
            $document['profileImage'] = $json['profileImage'];
            $document['phone'] = $json['phone'];
            $document['address1'] = $json['address1'];
            $document['address2'] = $json['address2'];
            $document['city'] = $json['city'];
            $document['state'] = $json['state'];
            $document['pin'] = $json['pin'];
            $document['latitude'] = $json['latitude'];
            $document['longitude'] = $json['longitude'];
            $document['isProfileComplete'] = 0;
//            $document['fbId']                       = $json['fbId'];
//            $document['googleId']                   = $json['googleId'];
//            $document['socialType']                 = $json['socialType'];           

            $isSuccess = false;
            $obj = new parent_model;

            if ($json['id']) {
                $resultTutor = $obj->getParentProfile($json['id']);
                //echo $resultEvent['eventName'].$json['eventName'];
                if ($resultTutor['mobile1'] != $json['mobile1']) {
                    $isMobileAlreadyExist = $obj->isMobileAlreadyExist($json['mobile1']);
                } else {
                    $isMobileAlreadyExist = false;
                }
            } else {
                $isMobileAlreadyExist = $obj->isMobileAlreadyExist($document['mobile1']);
            }
            if (!$isMobileAlreadyExist) {
                $insertId = $obj->parentSignUp($document);
                if ($insertId) {
                    $isSuccess = true;
                    $message = "Parent Signup successfull";                                    
                    $data = array("parentId" => $insertId);
                } elseif ($this->db->affected_rows()) {
                    $isSuccess = true;
                    $message = "Profile has been updated";
                    $data = array();
                } else {
                    $isSuccess = false;
                    $message = "Parent Signup not successfull";
                    $data = array();
                }
            } else {
                $isSuccess = false;
                $message = "Mobile already exist";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*
     * Request Peram:
     * {
      "parentId": "1"
      }
     */

    function getParentMyProfile() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['parentId'] = $json['parentId'];
            $isSuccess = false;
            $obj = new parent_model;
            $existParent = $obj->getParentProfile($document['parentId']);
            if (!empty($existParent)) {
                $isSuccess = true;
                $messageCode = "1";
                $message = "Profile";
                $data = array($obj->getParentMyProfile($document['parentId']));
            } else {
                $isSuccess = false;
                $messageCode = "2";
                $message = "Parent not exist";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $messageCode = "0";
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "messageCode" => $messageCode, "Message" => $message, "Result" => $data));
    }

    /*
     * Request Peram:
     * {
      "parentId": "1"
      }
     */

    /* function editParentProfile() {
      $json = file_get_contents('php://input');
      if (is_json($json)) {
      $json = json_decode($json, true);
      $document = array();
      $document['parentId'] = $json['parentId'];
      $document['name'] = $json['name'];
      $document['profileImage'] = $json['profileImage'];
      $document['phone'] = $json['phone'];
      $document['address1'] = $json['address1'];
      $document['address2'] = $json['address2'];
      $document['city'] = $json['city'];
      $document['state'] = $json['state'];
      $document['pin'] = $json['pin'];
      $isSuccess = false;
      $obj = new parent_model;
      $existParent = $obj->getParentProfile($document['parentId']);
      if (!empty($existParent)) {
      $isSuccess = true;
      $messageCode = "1";
      $message = "Profile Update";
      $data = array($obj->editParentProfile($document));
      } else {
      $isSuccess = false;
      $messageCode = "2";
      $message = "Parent not exist";
      $data = array();
      }
      } else {
      $isSuccess = false;
      $messageCode = "0";
      $message = "Invaid Json Input";
      $data = array();
      }
      echo json_encode(array("isSuccess" => $isSuccess, "messageCode" => $messageCode, "Message" => $message, "Result" => $data));
      } */

    function editStudentProfile() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            //$document['parentId'] = $json['id'];
            //$document['studentId'] = $json['studentId'];
            $document['parentId'] = $json['id'];
            $document['studentId'] = $json['requestId'];
            //$document['fkParentId'] = $json['id'];
            //$document['requestId'] = $json['requestId'];
            $document['name'] = $json['name'];
            $document['board'] = $json['board'];
            $document['class'] = $json['class'];
            $class = explode('_', $json['class']);
            $document['fkClassId'] = $class[1];
            $document['subject'] = $json['subject'];
            $document['schoolName'] = $json['schoolName'];
            $document['studentGender'] = $json['studentGender'];
            $document['tutionType'] = $json['tutionType'];
            $document['tutionPremises'] = $json['tutionPremises'];
            $document['preferredTutionDays'] = $json['preferredTutionDays'];
            $document['latitude'] = $json['latitude'];
            $document['longitude'] = $json['longitude'];
            $document['location'] = $json['location'];
            $document['startTime'] = date('H:i:s', strtotime($json['startTime']));
            $document['endTime'] = date('H:i:s', strtotime($json['endTime']));
            $document['modifiedDate'] = date('Y-m-d H:i:s');
            $document['studentGender'] = $json['gender'];
            $isSuccess = false;
            $obj = new parent_model;
            $existStudent = $obj->getStudentProfile($document);
            if (!empty($existStudent)) {
                $isSuccess = true;
                $messageCode = "1";
                $message = "Profile Update";
                $data = array($obj->editStudentProfile($document));
            } else {
                $isSuccess = false;
                $messageCode = "2";
                $message = "Student not exist";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $messageCode = "0";
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "messageCode" => $messageCode, "Message" => $message, "Result" => $data));
    }

    /*
     * Request Peram:
     * {
      "parentId": "1"
      }
     */

    function getStudentProfile() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['parentId'] = $json['parentId'];
            $document['studentId'] = $json['studentId'];
            $isSuccess = false;
            $obj = new parent_model;
            $existStudent = $obj->getStudentProfile($document);
            // print_r($existStudent);die("hh");
            if (!empty($existStudent)) {
                $isSuccess = true;
                $messageCode = "1";
                $message = "Student List !!";
                $data = array($obj->getStudentProfile($document));
            } else {
                $isSuccess = false;
                $messageCode = "2";
                $message = "Student not exist";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $messageCode = "0";
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "messageCode" => $messageCode, "Message" => $message, "Result" => $data));
    }

    /*
     * Request Peram:
     * {
      "parentId": "1"
      }
     */

    function deleteParentStudent() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['parentId'] = $json['parentId'];
            $document['studentId'] = $json['studentId'];
            $isSuccess = false;
            $obj = new parent_model;
            $existStudent = $obj->getParentProfile($document['parentId']);
            if (!empty($existStudent)) {
                $isSuccess = true;
                $messageCode = "1";
                $message = "Student delete";
                $data = array($obj->deleteParentStudent($document));
            } else {
                $isSuccess = false;
                $messageCode = "2";
                $message = "Student not exist";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $messageCode = "0";
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "messageCode" => $messageCode, "Message" => $message, "Result" => $data));
    }

    /*
     * Request Peram:
     * {
      "emailOrPhone": "raman@gmail.com/8505913155",
      "password": "123456",
      "deviceToken": "klfjdkjw978kj43j4kj34h3kj3",
      "deviceType": "ios",
      "socialType": "0/1/2",
      "fbId": "0",
      "googleId": "0"
      }
     */

    function parentLogin() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['emailOrPhone'] = $json['emailOrPhone'];
            $document['password'] = md5($json['password']);
            $document['deviceToken'] = $json['deviceToken'];
            $document['deviceType'] = $json['deviceType'];
            $document['socialType'] = $json['socialType'];
            $document['fbId'] = $json['fbId'];
            $document['googleId'] = $json['googleId'];
            $isSuccess = false;
            $obj = new parent_model;

            if ($json['socialType'] == 0) {
                $parentLogin = $obj->parentLogin($document);
                if (isset($parentLogin['parentId'])) {

                    $isSuccess = true;
                    $messageCode = "1";
                    $message = "Signin Successfully";
                    $data = array($obj->getParentProfile($parentLogin['parentId']));
                } elseif ($parentLogin == 3) {

                    $isSuccess = false;
                    $messageCode = "3";
                    $message = "User not exist";
                    $data = array();
                } elseif ($parentLogin == 2) {

                    $isSuccess = false;
                    $messageCode = "2";
                    $message = "Wrong credentails";
                    $data = array();
                }
            } else {
                if ($json['fbId']) {
                    $isFbIdExist = $obj->isFbIdExist($json['fbId']);
                    if ($isFbIdExist) {
                        $isSuccess = true;
                        $messageCode = "1";
                        $message = "Signin Successfully";
                        $data = array($obj->getParentProfile($isFbIdExist));
                    } else {
                        $isSuccess = false;
                        $messageCode = "3";
                        $message = "User not exist";
                        $data = array();
                    }
                } else {
                    $isGoogleIdExist = $obj->isGoogleIdExist($json['googleId']);
                    if ($isGoogleIdExist) {
                        $isSuccess = true;
                        $messageCode = "1";
                        $message = "Signin Successfully";
                        $data = array($obj->getParentProfile($isGoogleIdExist));
                    } else {
                        $isSuccess = false;
                        $messageCode = "3";
                        $message = "User not exist";
                        $data = array();
                    }
                }
            }
        } else {
            $isSuccess = false;
            $messageCode = "0";
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "messageCode" => $messageCode, "Message" => $message, "Result" => $data));
    }

    function forgotPasswordParent() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $emailOrPhone = $json['emailOrPhone'];
            $isSuccess = false;
            $data = array();
            $obj = new parent_model;
            $parentId = $obj->isEmailOrMobileAlreadyExist($emailOrPhone);
            if ($parentId) {
                if (filter_var($json['emailOrPhone'], FILTER_VALIDATE_EMAIL)) {
                    $otp = rand(1000, 9999);

                    $this->email->to($json['emailOrPhone']);
                    $this->email->from('hometutor@support.com');
                    $this->email->subject("HomeTutor:Forgot Password");
                    $this->email->message('Hi Here is the info you requested.Your OTP is ' . $otp);
                    if ($this->email->send()) {
                        $data = array(array("otp" => $otp, 'id' => $parentId));
                        $message = "OTP sent to email.";
                        $isSuccess = true;
                    } else {
                        $isSuccess = false;
                        $message = "OTP not sent to email";
                        $data = array();
                    }
                } else {
                    $output = sendSMS($emailOrPhone);
                    if ($output['success'] == 1) {
                        $data = array(array("otp" => $output['data'], 'id' => $parentId));
                        $message = "OTP sent to mobile.";
                        $isSuccess = true;
                    } else {
                        $isSuccess = false;
                        $message = "OTP not sent to mobile," . $output['data'];
                        $data = array();
                    }
                }
            } else {
                $isSuccess = false;
                $message = "User not exist";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    function changePasswordParent() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);

            $document = array();
            $document['id'] = $json['id'];
            $document['password'] = md5($json['newPassword']);
            $document['modifiedDate'] = date('Y-m-d H:i:s');
            $isSuccess = false;
            $obj = new parent_model;
            $obj->parentSignUp($document);
            if ($this->db->affected_rows()) {
                $isSuccess = true;
                $message = "Password changed successfully";
                $data = array();
            } else {
                $isSuccess = false;
                $message = "Password not changed";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*
     * Request Peram
      {
      "id":"1",
      "name":"Sacxhin",
      "class":"1",
      "subject":"1",
      "board":"1",
      "latitude":"1",
      "longitude":"1",
      "location":"1",
      "schoolName":"D.A.V",
      "studentGender":"1",
      "tutionType":"1",
      "preferredTutionDays":"1",
      "startTime":"",
      "endTime":"",
      "tutorCode":"",
      "tutionPremises":"1"
      }
     */

    function addStudent() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $document = array();
            $document['fkParentId'] = $json['id'];
            $document['requestId'] = $json['requestId'];
            $document['name'] = $json['name'];
            $document['board'] = $json['board'];
            $document['class'] = $json['class'];
            $class = explode('_', $json['class']);
            $document['fkClassId'] = $class[1];
            $document['subject'] = $json['subject'];
            $document['schoolName'] = $json['schoolName'];
            $document['studentGender'] = $json['studentGender'];
            $document['tutionType'] = $json['tutionType'];
            $document['tutionPremises'] = $json['tutionPremises'];
            $document['preferredTutionDays'] = $json['preferredTutionDays'];
            $document['latitude'] = $json['latitude'];
            $document['longitude'] = $json['longitude'];
            $document['location'] = $json['location'];
            $document['startTime'] = date('H:i:s', strtotime($json['startTime']));
            $document['endTime'] = date('H:i:s', strtotime($json['endTime']));
            $document['tutorCode'] = $json['tutorCode'];
            $document['isActive'] = 1;
            $document['createdDate'] = date('Y-m-d H:i:s');
            $document['output_counter'] = ($json['output_counter']) ? $json['output_counter'] : '0';
            $document['gender'] = $json['gender'];
            $document['selectmore'] = ($json['selectmore']) ? $json['selectmore'] : '0'; //1=search selectmore button for

            $obj = new parent_model;
            if ($json['tutorCode']) {
                $isTutorCodeExist = $obj->isTutorCodeExist($json['tutorCode']);
                if (!$isTutorCodeExist) {
                    $isSuccess = false;
                    $message = "Tutor code not exists";
                    $data = array();
                    echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
                    // die();
                }
            }
            $results = $obj->addStudent($document);
            //echo $this->db->last_query();
            if (!empty($results)) {
                $isSuccess = true;
                $message = "Tutors List found";
                $data = array($results);
            } else {
                $isSuccess = false;
                $message = "Tutors List not found";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*
      {
      "id":"1"
      }
     */

    function pickAStudent() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;

            $fkParentId = $json['id'];
            $obj = new parent_model;
//            $entryForStudent                = $obj->checkDuplicateEntryForStudent($document);
//            if(empty($entryForStudent))
//            {
            $pickAStudent = $obj->pickAStudent($fkParentId);
            //echo $this->db->last_query();
            if (!empty($pickAStudent)) {
                $isSuccess = true;
                $message = "Pick a student list found";
                $data = $pickAStudent;
            } else {
                $isSuccess = false;
                $message = "Pick a student list not found";
                $data = array();
            }
//            }
//            else
//            {
//                $isSuccess      = false;
//                $message        = "Duplicate Entry for student";
//                $data           = array();
//            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*
     * Request Peram
      {
      "boardId" :"1"
      }
     */

    function parentClasses() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);


            $boardId = $json['boardId'];

            $isSuccess = false;
            $obj = new parent_model;
            $getParentClassesByBoardId = $obj->getParentClassesByBoardId($boardId);
            if (!empty($getParentClassesByBoardId)) {
                $isSuccess = true;
                $message = "Classes list found";
                $data = $getParentClassesByBoardId;
            } else {
                $isSuccess = false;
                $message = "Classes list not found";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*
     * Request Peram
      {
      "classId":"12_4"
      }
     */

    function parentSubjects() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $classId = $json['classId'];
            $isSuccess = false;
            $obj = new parent_model;
            $getParentSubjectsByClassId = $obj->getParentSubjectsByClassId($classId);
            if (!empty($getParentSubjectsByClassId)) {
                $isSuccess = true;
                $message = "Subject list found";
                $data = $getParentSubjectsByClassId;
            } else {
                $isSuccess = false;
                $message = "Subject list not found";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*
     * Request Peram
      {
      "classId":"12_4"
      }
     */

    function parentSendRequest() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $request = $json['request'];
            $isSuccess = false;
            $obj = new parent_model;
            $insert_id = $obj->parentSendRequest($request);
            if (!empty($insert_id)) {
                $isSuccess = true;
                $message = "Request sent successfully";
                $data = array();
            } else {
                $isSuccess = false;
                $message = "Request not send";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*
     *  Request Peram
      {
      "parentId":"",
      "tabName":""
      }
     */

    function getTabsDataForParent() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['fkParentId'] = $json['parentId'];
            $document['tabName'] = $json['tabName'];
            $isSuccess = false;
            $obj = new parent_model;
            $getTabsDataForParent = $obj->getTabsDataForParent($document);
            if (!empty($getTabsDataForParent)) {
                $isSuccess = true;
                $message = "List found";
                $data = $getTabsDataForParent;
            } else {
                $isSuccess = false;
                $message = "List not found";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*
     *  Request Peram
      {
      "parentId": "1",
      "statusType": "1",
      "requestId": "3"
      }
     */

    function updateCancelOrDelete() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $document['fkParentId'] = $json['parentId'];
            $document['statusCancelOrDelete'] = $json['statusType'];
            $document['fkParentRequestId'] = $json['requestId'];
            $document['fkTutorId'] = $json['fkTutorId'];
            $document['statusData1'] = $json['statusData'];
            $document['modifiedDate'] = date('Y-m-d H:i:s');
            $obj = new parent_model;
            $obj->updateCancelOrDelete($document);
            if ($this->db->affected_rows()) {
                $isSuccess = true;
                $message = "Record has been updated ";
                $data = array();
                
             /*******************push notification**********************/
           
            $studentInfo = $obj->getRequestDetail($document['fkParentRequestId']);
            $parentInfo = $obj->getParentProfile($document['fkParentId']);        
         //parent
            $msg1 = '{"type":34,"message":{"StudentName":"' . $studentInfo['name'] . '",'
                    . '"Class"     :"' . $studentInfo['className'] . '",'
                    . '"Subject"   :"' . $studentInfo['subjectName'] . '""}}';
            $msg = '"You have cancelled your tuition request of"' . $studentInfo['name'] . '" has connected with you for your tuition request. Class : '
                    . 'Class   :"' . $studentInfo['className'] . '",'
                    . 'Subject :"' . $studentInfo['subjectName'] . '"Thank You for Using HomeTutor  to search a tutor. "';
            sendAndroidPush($parentInfo['deviceToken'], $msg1, "", "", 34);
            sendPushAsSMS($parentInfo['mobile1'], $msg);
          //  tutor
            $TotaltutorID=  explode(',', $document['fkTutorId']);
           
            foreach($TotaltutorID as $tutorID){
            $tutorInfo = $obj->getTutors($tutorID);
          
            $Tmsg1 = '{"type":35,"message":{"ParentName":"' . $parentInfo['name'] . '",'
                    . '"Class"     :"' . $studentInfo['className'] . '",'
                    . '"Subject"   :"' . $studentInfo['subjectName'] . '""}}';
            $Tmsg = '" Request of "' . $parentInfo['name'] . '"for Class :  '
                    . 'Class   :"' . $studentInfo['className'] . '",'
                    . 'Subject :"' . $studentInfo['subjectName'] . '"  is closed. Keep your profile up to date to get suitable tuition requests."';
            
            sendAndroidPush($tutorInfo['deviceToken'], $Tmsg1, "", "", 35);
            sendPushAsSMS($tutorInfo['mobile1'], $Tmsg);
            }
                /**********************************************************/
                
            } else {
                $isSuccess = false;
                $message = "Record not updated";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*
     *  Request Peram
      {
      "offerId": "1",
      "statusType": "1/2",
      "requestId": "1"
      }
     */

    function updateOfferStatus() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $document['fkTutorId'] = $json['tutorId'];
            $document['statusData1'] = $json['reasonId'];
            $document['statusData'] = $json['offerId'];
            $document['statusOffer'] = $json['statusType'];
            $document['fkParentRequestId'] = $json['requestId'];
            $document['modifiedDate'] = date('Y-m-d H:i:s');
            $obj = new parent_model;
            $res = $obj->updateOfferStatus($document);
           
            if ($res) {
                $isSuccess = true;
                if ($json['statusType'] == 1) {
                    $message = "Offer has been accepted";
                    $result = $obj->getRequestDetail($json['requestId']);
                    $tutorInfo = $obj->getTutors($json['tutorId']);

                    /**************class schedule entry********************* */
                    //print_r($document);
                    $offerdetail = $obj->getOfferDetail($document);
                    $parentInfo = $obj->getParentProfile($offerdetail['fkParentId']);
                   // print_r($offerdetail);die("ggg");
                    $list = array();
                    $startDateCheck = $offerdetail['start'];
                    $curDate=date('Y-m-d');
                    if($startDateCheck>$curDate){
                       $startDate=$startDateCheck; 
                    }else{
                       $startDate=$curDate; 
                    }
                    $start = explode('-', $startDate);
                    for($i=1;$i<=3;$i++){
                    if($i==1){
                    $month = $start[1];
                    $d = $start[2];
                    $year = $start[0];
                    }else{
                    $list=array();
                    $d = 1;
                    $countmonth = $start[1]+$i-1;                    
                    if($countmonth>12){
                    $start[1]=0;   
                    $start[0] = $start[0]+1;
                    $month=01;    
                    $year = $start[0];
                    }else{
                     $month=$start[1]+$i-1;   
                     $year = $start[0];  
                    }
                    } 

                    for ($d = $d; $d <= 31; $d++) {
                        $time = mktime(12, 0, 0, $month, $d, $year);
                        if (date('m', $time) == $month)
                            $list[] = date('Y-m-d', $time);
                    }
                   
                    $classdata = $offerdetail['days'];
                    $classday = explode(',', $classdata);
                   
                    $classDueDate=0;
                    foreach ($list as $classdate) {
                        $timestamp = strtotime($classdate);
                        $day = strtoupper(date('D', $timestamp));
                        if (in_array($day, $classday)) {
                   
                            $data = array(
                                'fkParentID' => $offerdetail['fkParentId'],
                                'fkStudentID' => $offerdetail['fkRequestId'],
                                'fkTutorId' => $offerdetail['fkTutorId'],
                                'class_date' => $classdate,
                                'start_time' => $offerdetail['time'],
                                'class_duration' => $offerdetail['duration'],
                                'subject_id' => $result['subjectId'],
                                'className' => $result['className'],
                                'boardName' => $result['boardName'],
                                'created_date' => date("Y-m-d H:i:s")
                            );
                        $CheckScheduler = $obj->CheckScheduler($data);
                        if(empty($CheckScheduler)){  
                       $this->db->insert('ht_class_schedule', $data);
                        }
                             if($classDueDate==0){//for get first class
                                $classDueDate=$classdate;
                            }
                        }
                    }
               
                }
                  

                    /*********************end class schedule***************** */
                    $start = explode('-', $startDate);                  
                    $month = $start[1];//Start number of count class day in month
                    $d = 01;
                    $year = $start[0];
                     
                    for ($d = $d; $d <= 31; $d++) {
                        $time = mktime(12, 0, 0, $month , $d,  $year);
                        if (date('m', $time) == $month )
                            $list1[] = date('Y-m-d', $time);
                    }
                   
                    $Numberclassdate=0;                     
                    $classdata = $offerdetail['days'];
                    $classday = explode(',', $classdata);
                    foreach ($list1 as $classdate) {
                        $timestamp = strtotime($classdate);
                        $day = strtoupper(date('D', $timestamp));
                        if (in_array($day, $classday)) { 
                           
                          $Numberclassdate=$Numberclassdate+1;                             
                        }
                    }//End number of count class day in month
                  
                    if($offerdetail['mode']==0)
                    {
                        $offerfeemode=1;
                    }else{
                         $offerfeemode=0;
                    }
                    
                     /***************************************payment Due entry**************/
                      $paymentdata = array(
                                'fkTutorId' => $offerdetail['fkTutorId'],
                                'fkParentId' => $offerdetail['fkParentId'],
                                'fkStudentId' => $offerdetail['fkRequestId'],
                                'fkSubjectId' => $result['subjectId'],
                                'fkClassId' => $result['ClassId'],
                                'paymentMode' => $offerfeemode,
                               // 'dueDate' => $classDueDate,
                                'dueDate' => date('Y-m-d', strtotime('-1 day', strtotime($startDate))),
                                'Number_class' => $Numberclassdate
                            );
                          $CheckPaymentDueDate = $obj->CheckPaymentDueDate($paymentdata);
                          if(empty($CheckPaymentDueDate)){ 
                            $this->db->insert('ht_ParentDuePayment', $paymentdata);
                          }
                    /*******************************************ENd************************/

                    /****************************Push Notification************************/
                            //tutor
                    $msg1 = '{"type":5,"message":{"parent":"' . $parentInfo['name'] . '",'
                            . '"Class"     :"' . $result['className'] . '",'
                            . '"Subject"   :"' . $result['subjectName'] . '"}}';

                    $msg = '"Congratulations. Your Offer is accepted by '
                            . '"' . $parentInfo['name'] . '",'
                            . 'for tuition request of  Class   :"' . $result['className'] . '",'
                            . 'Subject :"' . $result['subjectName'] . '" ."';

                    //echo $message1;
                    sendAndroidPush($tutorInfo['deviceToken'], $msg1, "", "", 5);
                    sendPushAsSMS($tutorInfo['mobile1'], $msg);
                    
                    //Parent 
                     $Pmsg1 = '{"type":36,"message":{"parent":"' . $tutorInfo['name'] . '",'
                            . '"student"     :"' . $result['name'] . '",'
                            . '"Class"     :"' . $result['className'] . '",'
                            . '"Subject"   :"' . $result['subjectName'] . '"}}';

                    $Pmsg = '"Thank You for accepting '
                            . '"' . $tutorInfo['name'] . '",'
                            . 'offer for your tuition request of :"' . $result['name'] . '",'
                            . 'class :"' . $result['className'] . '" ,'
                            . 'Subject :"' . $result['subjectName'] . '".Please proceed to Payment to start this tuition."';

                    //echo $message1;
                    sendAndroidPush($parentInfo['deviceToken'], $Pmsg1, "", "", 36);
                    sendPushAsSMS($parentInfo['mobile1'], $Pmsg);
                    
                    //push send other tutor
                    $RequestAllTutor = $obj->RequestAllTutor($document);                   
                    if(!empty($RequestAllTutor)){
                    foreach($RequestAllTutor as $RequestTutor){
                    $OtherTutorInfo = $obj->getTutors($RequestTutor['fkTutorId']);  
                    $Othermsg1 = '{"type":46,"message":{"parent":"' . $parentInfo['name'] . '",'
                                 . '"Class":"' . $result['className'] . '",'
                                 . '"Subject":"' . $result['subjectName'] . '"}}';

                    $Othermsg = 'Request of ' . $parentInfo['name'] . ' '
                                . 'for Class :' . $result['className'] . '",'
                                . 'Subject :' . $result['subjectName'] . ' is closed. Keep your profile up to date to get suitable tuition requests.';
                    sendAndroidPush($OtherTutorInfo['deviceToken'], $Othermsg1, "", "", 46);
                    sendPushAsSMS($OtherTutorInfo['mobile1'], $Othermsg);
                    }                    
                    }
                    
                    /************************END********************************************/
                } elseif ($json['statusType'] == 2) {
                    $message = "Offer has been rejected";
                    $result = $obj->getRequestDetail($json['requestId']);
                    $tutorInfo = $obj->getTutors($json['tutorId']);
                    $reasonName = $obj->getReasonsName($json['reasonId']);
                    $msg1 = '{"type":3,"message":{"location":"' . $result['location'] . '",'
                            . '"Class"     :"' . $result['className'] . '",'
                            . '"Subject"   :"' . $result['subjectName'] . '",'
                            . '"Day"       :"' . $result['preferredTutionDays'] . '",'
                            . '"Reason"    :"' . $reasonName . '",'
                            . '"Time"      :"' . $result['startTime'] . '"}}';

                    $msg = '"Your offer has been rejected by parent due to reason "' . $reasonName . '". Request details: '
                            . 'location:"' . $result['location'] . '",'
                            . 'Class   :"' . $result['className'] . '",'
                            . 'Subject :"' . $result['subjectName'] . '",'
                            . 'Day     :"' . $result['preferredTutionDays'] . '",'
                            . 'Time    :"' . $result['startTime'] . '".Please connect using Hometutor App"';

                    //echo $message1;
                    sendAndroidPush($tutorInfo['deviceToken'], $msg1, "", "", 3);
                    sendPushAsSMS($tutorInfo['mobile1'], $msg);
                }
                $data = array();
            } else {
                $isSuccess = false;
                $message = "Offer not accept";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }
    
    
        /**************************Start GenrateAutoScheduler************************************** */
    function genrateAutoScheduler() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $document = array();
          //  $document['tabName'] = $json['tabName'];
            $obj = new parent_model;
            $genrateAutoScheduler = $obj->genrateAutoScheduler($document);
           
            if (!empty($genrateAutoScheduler)) { 
        
                foreach($genrateAutoScheduler as $AutoSchedulerData )
                {      
                    $result = $obj->getRequestDetail($AutoSchedulerData['requestId']);
                    $tutorInfo = $obj->getTutors($AutoSchedulerData['tutorId']);
                    $AutoScheduler['fkParentRequestId'] = $AutoSchedulerData['requestId'];
                    $AutoScheduler['fkTutorId'] = $AutoSchedulerData['tutorId'];
                    /**************class schedule entry********************* */                   
                    $offerdetail = $obj->getOfferDetail($AutoScheduler);
                    $parentInfo = $obj->getParentProfile($offerdetail['fkParentId']);
                 
                    $list = array();
                    $startDateCheck = $offerdetail['start'];
                    $curDate=date('Y-m-d');
                    if($startDateCheck>$curDate){
                       $startDate=$startDateCheck; 
                    }else{
                       $startDate=$curDate; 
                    }
                    $start = explode('-', $startDate);

                    $month = $start[1];
                    $d = $start[2];
                    $year = $start[0];

                    for ($d = $d; $d <= 31; $d++) {
                        $time = mktime(12, 0, 0, $month, $d, $year);
                        if (date('m', $time) == $month)
                            $list[] = date('Y-m-d', $time);
                    }
                   
                    $classdata = $offerdetail['days'];
                    $classday = explode(',', $classdata);
                    $classDueDate=0;
                    foreach ($list as $classdate) {
                        $timestamp = strtotime($classdate);
                        $day = strtoupper(date('D', $timestamp));
                        if (in_array($day, $classday)) {

                            $data = array(
                                'fkParentID' => $offerdetail['fkParentId'],
                                'fkStudentID' => $offerdetail['fkRequestId'],
                                'fkTutorId' => $offerdetail['fkTutorId'],
                                'class_date' => $classdate,
                                'start_time' => $offerdetail['time'],
                                'class_duration' => $offerdetail['duration'],
                                'subject_id' => $result['subjectId'],
                                'className' => $result['className'],
                                'boardName' => $result['boardName'],
                                'created_date' => date("Y-m-d H:i:s")
                            );
                                                          
                             $CheckScheduler = $obj->CheckScheduler($data);
                             if(empty($CheckScheduler)){
                             $this->db->insert('ht_class_schedule', $data);
                             }
                             if($classDueDate==0){//for get first class
                                $classDueDate=$classdate;
                            }
                        }
                    }
                    
                  

                    /*********************end class schedule***************** */
                    /***************************************payment Due entry**************/
                    $list1=array();
                    $start = explode('-', $startDate);                  
                    $month = $start[1];//Start number of count class day in month
                    $d = 01;
                    $year = $start[0];
                     
                    for ($d = $d; $d <= 31; $d++) {
                        $time = mktime(12, 0, 0, $month , $d,  $year);
                        if (date('m', $time) == $month )
                            $list1[] = date('Y-m-d', $time);
                    }
                 
                    $Numberclassdate=0;                     
                    $classdata = $offerdetail['days'];
                    $classday = explode(',', $classdata);
                    foreach ($list1 as $classdate) {
                        $timestamp = strtotime($classdate);
                        $day = strtoupper(date('D', $timestamp));
                        if (in_array($day, $classday)) {                             
                          
                          $Numberclassdate=$Numberclassdate+1;                             
                        }
                    }//End number of count class day in month                  
                   $paymentdata = array(
                          
                                'Number_class' => $Numberclassdate
                            );
                            $this->db->where('fkTutorId', $offerdetail['fkTutorId']);
                            $this->db->where('fkParentId', $offerdetail['fkParentId']);
                            $this->db->where('fkStudentId', $offerdetail['fkRequestId']);
                            $this->db->update('ht_ParentDuePayment', $paymentdata);
                         // echo $this->db->last_query();die("dd");
                    /*******************************************ENd************************/
                }
                
                $isSuccess = true;
                $message = "Class scheduled successfully!!";
                $data = array();
            } else {
                $isSuccess = false;
                $message = "Class not scheduled.!!";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }
    

    /****************************End GenrateAutoScheduler************************************************* */

    /*
     *  Request Peram
      {
      "requestId":"1",
      "parentId":"1",
      "tutorId":"1",
      "statusData":"1"
      }

     */

    function deSelectTutor() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $data['fkParentRequestId'] = $json['requestId'];
            $data['fkParentId'] = $json['parentId'];
            $data['fkTutorId'] = $json['tutorId'];
            $data['statusData1'] = $json['statusData'];
            $data['modifiedDate'] = date('Y-m-d H:i:s');
            $data['isParentDeselectedTutor'] = 1;
            $obj = new parent_model;
            $obj->deSelectTutor($data);
            //echo $this->db->last_query();
            if ($this->db->affected_rows()) {
                $isSuccess = true;
                $message = "Tutor Deselected";
                $data = array();
                $result = getRequestDetail($json['requestId']);
                $tutorInfo = getTutors($json['tutorId']);
                $reasonName = getReasonsName($json['statusData']);
                $msg1 = '{"type":4,"message":{"location":"' . $result['location'] . '",'
                        . '"Class"     :"' . $result['className'] . '",'
                        . '"Subject"   :"' . $result['subjectName'] . '",'
                        . '"Day"       :"' . $result['preferredTutionDays'] . '",'
                        . '"Reason"    :"' . $reasonName . '",'
                        . '"Time"      :"' . $result['startTime'] . '"}}';

                $msg = '"Parent has deselected you from the request due to reason "' . $reasonName . '". Request details: '
                        . 'location:"' . $result['location'] . '",'
                        . 'Class   :"' . $result['className'] . '",'
                        . 'Subject :"' . $result['subjectName'] . '",'
                        . 'Day     :"' . $result['preferredTutionDays'] . '",'
                        . 'Time    :"' . $result['startTime'] . '".Please connect using Hometutor App"';

                //echo $message1;
                sendAndroidPush($tutorInfo['deviceToken'], $msg1, "", "", 4);
                sendPushAsSMS($tutorInfo['mobile1'], $msg);
            } else {
                $isSuccess = false;
                $message = "Tutor not deselected";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*     * *******santosh 11-8-2016*************************************** */

    /*     * ****************************Get Erolled class*************************************** */
    /*
     * Request
     *
     *      
     * */

    function getStudentEnrolled() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            //   $data['fkStudentID']                      = $json['fkStudentID']; 
            $data['fkParentID'] = $json['fkParentID'];
            $obj = new parent_model;
            $getAllEnrolledClass = $obj->getStudentEnrolled($data);
            if (!empty($getAllEnrolledClass)) {
                $isSuccess = true;
                $message = "class list found";
                $data = $getAllEnrolledClass;
            } else {
                $isSuccess = false;
                $message = "class list not found";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*     * ****************************End Erolled class********************************* */
    /*     * ****************************Start Cancel Erolled class********************************* */
    /*
     * Request
     * {
      "fkTutorId":"179",
      "fkParentID":"73",
      "fkStudentID":"4166",
      "classScheduledID":"1",
      "status":"50"   cancel Reasion Id
      }
     */

    function cancelEnrolledClass() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $data['fkTutorId'] = $json['fkTutorId'];
            $data['fkParentID'] = $json['fkParentID'];
            $data['fkStudentID'] = $json['fkStudentID'];
            $data['classScheduledID'] = $json['classScheduledID'];
            $data['statusData2'] = $json['statusData2']; //Cancel Class Reasion Id
            $obj = new parent_model;
            $getAllEnrolledClass = $obj->cancelEnrolledClass($data);
            if (!empty($getAllEnrolledClass)) {
                
            /************************for tutor notification**************/
            $this->load->model('tutor_model');
            $classScheduled = $this->tutor_model->getStudentScheduledById($data['classScheduledID']);
        
            $msg2 = '{"type":37,"message":{"StudentName":"' . $classScheduled['studentName'] . '",'
                        . '"Date":"' . $classScheduled['class_date'] . '",'
                        . '"Time":"' . date('h:i:A',strtotime($classScheduled['start_time'])) . '"}}';
            
             $Tmsg =  ''. $classScheduled['studentName'] . ' has Cancelled Tuition Session for '
                        . ' at Date/time: ' . $classScheduled['class_date']  . '/' . date('h:i:A',strtotime($classScheduled['start_time'])) . '';
       
            sendAndroidPush($classScheduled['TutorDeviceToken'], $msg2, "", "", 37);
            sendPushAsSMS($classScheduled['TutorMobile'], $Tmsg);
            /***********************END*****************************************/
                
                $isSuccess = true;
                $message = "class cancel succesfully";
                $data = $getAllEnrolledClass;
            } else {
                $isSuccess = false;
                $message = "class not cancel ";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*     * ****************************End Cancel Erolled class********************************* */

    /*     * ****************************Start Terminate Erolled class********************************* */
    /*
     * Request
     *      
     * */

    function terminateEnrolledClass() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $data['fkTutorId'] = $json['fkTutorId'];
            $data['fkParentID'] = $json['fkParentID'];
            $data['fkStudentID'] = $json['fkStudentID'];
            $data['statusData3'] = $json['statusData3']; //Resion Id for class terminate
            $obj = new parent_model;
            $terminateEnrolledClass = $obj->terminateEnrolledClass($data);
            if (!empty($terminateEnrolledClass)) {
                $isSuccess = true;
                $message = "class Terminate succesfully";
                $data = $terminateEnrolledClass;
            } else {
                $isSuccess = false;
                $message = "class not Terminate ";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*     * ****************************End Terminate Erolled class******************************* */


    /*     * *****************************Get Reason parent*********************************** */

    function getAllReasonsParent() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $data['tabName'] = $json['tabName'];
            $data['isParent'] = $json['isParent'];
            $obj = new parent_model;
            $getAllReasons = $obj->getAllReasonsParent($data);
            if (!empty($getAllReasons)) {
                $isSuccess = true;
                $message = "Reasons list found";
                $data = $getAllReasons;
            } else {
                $isSuccess = false;
                $message = "Reasons list not found";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /***************************End Reason parent*******************************************/
    
    /*******************************Get student class Scheduler History*********************************** */

    function getStudSchedulerHist() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;           
            $data['fkStudentID'] = $json['fkStudentID'];
            $obj = new parent_model;
            $getStudSchedulerHist = $obj->getStudSchedulerHist($data);
            if (!empty($getStudSchedulerHist)) {
                $isSuccess = true;
                $message = "Topic list found";
                $data = $getStudSchedulerHist;
            } else {
                $isSuccess = false;
                $message = "Topic list not found";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /***************************End Get student class Scheduler History*******************************************/
    
     /*******************************************Get Scheme Refer & Earn*****************************************/

    function getParentSchemeRefer() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $data['fkFromTutorId'] = $json['fkFromTutorId'];
            $obj = new parent_model;
            $getParentSchemeRefer = $obj->getParentSchemeRefer($data);
            if (!empty($getParentSchemeRefer)) {
                $isSuccess = true;
                $message = "Scheme found";
                $data = $getParentSchemeRefer;
            } else {
                $isSuccess = false;
                $message = "Scheme not found";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /****************************Get Scheme Refer & Earn************************************** */

    /*******************************************Start Only  Add Student*****************************************/

    function addPstudent() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $document = array();
            $document['fkParentId'] = $json['id'];          
            $document['name'] = $json['name'];
            $document['board'] = $json['board'];
            $document['class'] = $json['class'];
            $class = explode('_', $json['class']);
            $document['fkClassId'] = $class[1];
            $document['subject'] = $json['subject'];
            $document['schoolName'] = $json['schoolName'];
            $document['studentGender'] = $json['studentGender'];         
            $obj = new parent_model;
            
            $results = $obj->addPstudent($document);
            //echo $this->db->last_query();
            if (!empty($results)) {
                $isSuccess = true;
                $message = "Student add successfully!!";
                $data = array($results);
            } else {
                $isSuccess = false;
                $message = "Sorry student not add.!!";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    
    }
    
    
    


    /****************************End Only  Add Student ************************************** */
    
    /****************************gettab data count*****************************************/
     function getTabsDataParentCount() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['fkParentId'] = $json['parentId'];            
            $isSuccess = false;
            $obj = new parent_model;
            $getTabsDataParentCount= $obj->getTabsDataParentCount($document);
            if (!empty($getTabsDataParentCount)) {
                $isSuccess = true;
                $message = "List found";
                $data = $getTabsDataParentCount;
            } else {
                $isSuccess = false;
                $message = "List not found";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }
    /******************************End Get Tab Data***************************************/

         

    /***************************santosh work end************************************************* */


    /*****************gaurav 27-7-2016*************** */

    public function assignment() {
        if ($this->input->post()) {
            $request_data = array();
            $request_data['parent_id'] = $this->input->post('parent_id');
            if ($request_data != "") {
                $assignment_data = $this->parent_model->get_assignment($request_data['parent_id']);
                /* status will be true only if transaction is successfull */
                if (!empty($assignment_data)) {
                    $key_array = array();
                    foreach ($assignment_data as $c) {
                        if (!in_array($c['id'], $key_array)) {
                            $key_array[] = $c['id'];
                        }
                    }
                    $final_array = array();
                    foreach ($assignment_data as $pre) {
                        foreach ($key_array as $do) {
                            if ($do == $pre['id']) {
                                $final_array[$do][] = $pre;
                            }
                        }
                    }
                    foreach ($final_array as $j) {
                        $main_array[] = $j;
                    }
                    //print_r($main_array);die;
                    $isSuccess = True;
                    $message = "Data fetched successfully.";
                    $data = $main_array;
                } else {
                    $isSuccess = False;
                    $message = "No record found.";
                    $data = array();
                }
            } else {
                $isSuccess = False;
                $message = "No record found.";
                $data = array();
            }
        } else {
            $isSuccess = False;
            $message = "Request parameters are not valid.";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    public function question() {
        if ($this->input->post()) {
            $request_data = array();
            $request_data['student_id'] = $this->input->post('student_id');
            $request_data['topic_id'] = $this->input->post('topic_id');
            //print_R($request_data);die;
            if ($request_data != "") {
                $question_data = $this->parent_model->get_question($request_data);
                /* status will be true only if transaction is successfull */
                if ($question_data) {
                    $isSuccess = True;
                    $message = "Data fetched successfully.";
                    $data = $question_data;
                } else {
                    $isSuccess = False;
                    $message = "Data Not fetched.";
                    $data = $question_data;
                }
            } else {
                $isSuccess = False;
                $message = "No record found.";
                $data = array();
            }
        } else {
            $isSuccess = False;
            $message = "Request parameters are not valid.";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    public function assignment_actions() {
        if ($this->input->post()) {
            $request_data = array();
            $request_data['assignment_id'] = $this->input->post('assignment_id');
            $request_data['status'] = $this->input->post('status');
            //print_R($request_data);die;
            if ($request_data != "") {
                $request_download = $this->parent_model->set_assignment_download($request_data);
                /* status will be true only if transaction is successfull */
                if ($request_download) {
                    $isSuccess = True;
                    $message = "Assignment Downloaded.";
                    $data = array();
                } else {
                    $isSuccess = False;
                    $message = "Assignment Not Downloaded.";
                    $data = array();
                }
            } else {
                $isSuccess = False;
                $message = "No record found.";
                $data = array();
            }
        } else {
            $isSuccess = False;
            $message = "Request parameters are not valid.";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    /*
     * set assignment complete
     */

    public function set_assignment_complete() {
        if ($this->input->post()) {
            $request_data = array();
            $request_data['assignment_id'] = $this->input->post('assignment_id');
            $request_data['score'] = $this->input->post('score');
            $request_data['completion_time'] = $this->input->post('completion_time');
            $request_data['total_question'] = $this->input->post('total_question');
            $request_data['attempted_question'] = $this->input->post('attempted_question');
            $request_data['correct_answer'] = $this->input->post('correct_answer');
            $request_data['completed_date'] = $this->input->post('completed_date');

            if (count($request_data) > 0) {
                $request_download = $this->parent_model->set_assignment_complete($request_data);
                /* status will be true only if transaction is successfull */
                if ($request_download) {
                    $isSuccess = True;
                    $message = "Assignment Complete.";
                    $data = array();
                } else {
                    $isSuccess = False;
                    $message = "Assignment Not Complete.";
                    $data = array();
                }
            } else {
                $isSuccess = False;
                $message = "ERROR.";
                $data = array();
            }
        } else {
            $isSuccess = False;
            $message = "Request parameters are not valid.";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    public function assignment_detail() {
        if ($this->input->post()) {
            $request_data = array();
            $request_data['parent_id'] = $this->input->post('parent_id');
            $this->db->select('ht_parent_student.id as student_id');
            $this->db->distinct('student_id');
            $this->db->where('ht_parent_student.fkParentId', $request_data['parent_id']);
            $this->db->from('ht_parent_student');
            $this->db->join('ht_assignment_master', 'ht_assignment_master.student_id = ht_parent_student.id');
            $result_data = $this->db->get()->result_array();
            if (!empty($result_data)) {
                foreach ($result_data as $r) {
                    $assignement_record = $this->parent_model->get_assignment_detail($r);
                    if (!empty($assignement_record)) {
                        $request_download[] = $assignement_record;
                    }
                }
                if (!empty($request_download)) {
                    $i = 0;
                    foreach ($request_download as $k) {
                        foreach ($k as $a) {
                            $a['detail'] = $this->parent_model->get_assignment_full_detail($a['id']);
                            $result[$i][] = $a;
                        }
                        $i++;
                    }
                }
                /* status will be true only if transaction is successfull */
                if (!empty($result)) {

                    $isSuccess = True;
                    $message = "Assignment Complete Detail.";
                    $data = $result;
                } else {
                    $isSuccess = False;
                    $message = "Assignment Not Complete.";
                    $data = array();
                }
            } else {
                $isSuccess = False;
                $message = "No record found.";
                $data = array();
            }
        } else {
            $isSuccess = False;
            $message = "Request parameters are not valid.";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    /*     * ***************ENd gaurav 27-7-2016*************** */
}
