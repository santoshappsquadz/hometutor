<?php

class Share_tutor extends MX_Controller {

    function __Construct() {
        parent::__Construct();
        $this->load->model('share_tutor_model');
		$this->load->library('email');
    }

    function student_detail_share_with_tutor() {
        if ($this->input->post()) {
            $request_data = array();
            $request_data['assignment'] = $this->input->post('assignment');
            $request_data['attempt'] = $this->input->post('attempt');
            $request_data['date/time'] = $this->input->post('date/time');
            $request_data['timetaken'] = $this->input->post('timetaken');
            $request_data['%age_correct_answer'] = $this->input->post('%age_correct_answer');
            $request_data['total_question'] = $this->input->post('total_question');
            $request_data['attempt_question'] = $this->input->post('attempt_question');
            $request_data['correct_answer'] = $this->input->post('correct_answer');
            $request_data['student_id'] = $this->input->post('student_id');
            if (!empty($request_data['student_id'])) {
                $tutor_mobile_no = $this->share_tutor_model->get_tutor_mobile_number($request_data['student_id']);
                if ($tutor_mobile_no) {
                    $msg = 'Assignment details: '
                            . 'Assignment:"' . $request_data['assignment'] . '", '
                            . 'Attempt   :"' . $request_data['attempt'] . '",'
                            . 'Date/Time :"' . $request_data['date/time'] . '" ,'
                            . 'Time Taken:"' . $request_data['timetaken'] . '" ,'
                            . '% Age Correct Answer:"' . $request_data['%age_correct_answer'] . '" ,'
                            . 'Total Question:"' . $request_data['total_question'] . '" ,'
                            . 'Attempt Question:"' . $request_data['attempt_question'] . '" ,'
                            . 'Correct Answer:"' . $request_data['correct_answer'] . '" ';
                    $res = sendPushAsSMS($tutor_mobile_no['mobile1'], $msg);
                    if ($res['success']) {
                        $isSuccess = True;
                        $message = "Success.";
                        $data = array();
                    } else {
                        $isSuccess = false;
                        $message = "msg not send.";
                        $data = array();
                    }
                } else {
                    $isSuccess = False;
                    $message = "Tutor not Exist.";
                    $data = array();
                }
            } else {
                $isSuccess = False;
                $message = "ERROR.";
                $data = array();
            }
        } else {
            $isSuccess = False;
            $message = "Request parameters are not valid.";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    public function tutor_offer_show_screen() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $document = array();
            $json = json_decode($json, true);
            $document['tutor_id'] = $json['tutor_id'];
            $obj = new share_tutor_model();
            $data = array();
            $main_data = array();
            $data = $obj->tutor_offer_show_screen($document['tutor_id']);

            if ($data) {
                $isSuccess = True;
                $message = "Success.";
                $data = $data;
            } else {
                $isSuccess = false;
                $message = "offer not found.";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    public function image_for_how_it_work() {
		$json = file_get_contents('php://input');
        if (is_json($json)) {
            $document = array();
            $json = json_decode($json, true);
            $document['status'] = $json['status'];
			$obj = new share_tutor_model();
			$data = array();
			$data = $obj->image_for_how_it_work($document['status']);
			if ($data) {
				$isSuccess = True;
				$message = "Success.";
				$data = $data;
			} else {
				$isSuccess = false;
				$message = "image not found.";
				$data = array();
			}
		}else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }
	
	
	

    public function image_for_why_chooses() {
		$json = file_get_contents('php://input');
      
         if (is_json($json)) {
				$document = array();
				 $json = json_decode($json, true);
				 $document['status'] = $json['status'];
				$obj = new share_tutor_model();
				$data = array();
				$data = $obj->image_for_why_chooses($document['status']);
				if ($data) {
					$isSuccess = True;
					$message = "Success.";
					$data = $data;
				} else {
					$isSuccess = false;
					$message = "image not found.";
					$data = array();
				}
			}else {
				$isSuccess = false;
				$message = "Invaid Json Input";
				$data = array();
			}
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data)); 
    }

}

?>