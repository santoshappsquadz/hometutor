<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Version_detail extends MX_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        
        $this->load->model('Version_detail_model');

        $this->load->library('email');
        //$this->load->helper('aes'); 
    }


    
    public function version()
    {
        $json = file_get_contents('php://input');
        if (is_json($json)) 
            {
            $json                        = json_decode($json, true);
            $document                    = array();
            $document['user_id']         = $json['user_id'];
            $document['user_type']       = $json['user_type'];
            $document['version']          = $json['version'];           
            $isSuccess                   = false;
            $data                        = array();
            $obj                         = new Version_detail_model;

            $checkUser                    =$obj->checkuser($document);
           // pre($checkUser);die;
            if($checkUser){
            $version                      = $obj->addVersion($document);            
            if(!empty($version))
                {
                    $isSuccess                   = true;
                    $message                     = "Version Added Successfully";
                    $messageCode                 = "";
                    $data                        = $version;    

            }else{
                    $isSuccess                   = false;
                    $message                     = "User does not exist!!";
                    $messageCode                 =  "";
                    $data                        = array();
                        

                } 
            }else{            
            $isSuccess                   = false;
            $message                     = "User does not exist!!";
            $messageCode                 =  "";
            $data                        = array();
                
            }
        }           
        else
        {
            $isSuccess                   = false;
            $message                     = "Invaid Json Input";
            $messageCode                 =  "";
            $data                        = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
     
    }
    
   
}
