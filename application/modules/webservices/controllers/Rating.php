<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Rating extends MX_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        
        $this->load->model('rating_model');

        $this->load->library('email');
        //$this->load->helper('aes'); 
    }


    
    public function addRating()
    {
        $json = file_get_contents('php://input');
        if (is_json($json)) 
            {
            $json                        = json_decode($json, true);
            $document                    = array();
            $document['parentId']        = $json['parentId'];
            $document['tutorId']         = $json['tutorId'];
            $document['rating']          = $json['rating'];
            $document['review']          = $json['review'];
            $document['modified']        = date('Y-m-d h:i:s');
            $isSuccess                   = false;
            $data                        = array();
            $obj                         = new rating_model;

            $rating                      = $obj->addRating($document);
            
                if(!empty($rating))
                {
                    $isSuccess                   = true;
                    $message                     = "Rating Added Successfully";
                    $messageCode                 = "";
                    $data                        = $rating;    

                }
                
            } 
           
        else
        {
            $isSuccess                   = false;
            $message                     = "Invaid Json Input";
            $messageCode                 =  "";
            $data                        = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
     
    }
    
    
    public function getRating()
    {
        $json = file_get_contents('php://input');
        if (is_json($json)) 
            {
            $json                        = json_decode($json, true);
            $document                    = array();
            $document['tutorId']         = $json['tutorId'];
            $isSuccess                   = false;
            $data                        = array();
            $obj                         = new rating_model;

            $rating                      = $obj->getRating($document);
            
                if(!empty($rating))
                {
                    $isSuccess                   = true;
                    $message                     = "Rating List Displayed Successfully";
                    $messageCode                 = "";
                    $data                        = $rating;    
                }
                else
                {
                    $isSuccess                   = false;
                    $message                     = "No Record Found";
                    $messageCode                 = "";
                    $data                        = array();
                }
                
            } 
           
        else
        {
            $isSuccess                   = false;
            $message                     = "Invaid Json Input";
            $messageCode                 =  "";
            $data                        = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
     
    }
    
    
    public function avgRating()
    {
        $json = file_get_contents('php://input');
        if (is_json($json)) 
            {
            $json                        = json_decode($json, true);
            $document                    = array();
            $document['tutorId']         = $json['tutorId'];
            $isSuccess                   = false;
            $data                        = array();
            $obj                         = new rating_model;

            $rating                      = $obj->avgRating($document);
            
                if(!empty($rating))
                {
                    $isSuccess                   = true;
                    $message                     = "Average Rating Displayed Successfully";
                    $messageCode                 = "";
                    $data                        = $rating;    
                }
                else
                {
                    $isSuccess                   = false;
                    $message                     = "No Record Found";
                    $messageCode                 = "";
                    $data                        = array();
                }
                
            } 
           
        else
        {
            $isSuccess                   = false;
            $message                     = "Invaid Json Input";
            $messageCode                 =  "";
            $data                        = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
     
    }
    
    
    
    
     public function getRatingByParentId()
    {
        $json = file_get_contents('php://input');
        if (is_json($json)) 
            {
            $json                        = json_decode($json, true);
            $document                    = array();
            $document['parentId']         = $json['parentId'];
            $isSuccess                   = false;
            $data                        = array();
            $obj                         = new rating_model;

            $rating                      = $obj->getRatingByParentId($document);
            
                if(!empty($rating))
                {
                    $isSuccess                   = true;
                    $message                     = "Rating List Displayed Successfully";
                    $messageCode                 = "";
                    $data                        = $rating;    

                }
                
            } 
           
        else
        {
            $isSuccess                   = false;
            $message                     = "Invaid Json Input";
            $messageCode                 =  "";
            $data                        = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
     
    }
    
    
    public function getAllRating()
    {
        $json = file_get_contents('php://input');
        if (is_json($json)) 
            {
            $json                        = json_decode($json, true);
            $document                    = array();
            $document['parentId']         = $json['parentId'];
            $document['tutorId']         = $json['tutorId'];
            $isSuccess                   = false;
            $data                        = array();
            $obj                         = new rating_model;

            $rating                      = $obj->getAllRating($document);
            
                if(!empty($rating))
                {
                    $isSuccess                   = true;
                    $message                     = "Rating List Displayed Successfully";
                    $messageCode                 = "";
                    $data                        = $rating;    

                }
                
            } 
           
        else
        {
            $isSuccess                   = false;
            $message                     = "Invaid Json Input";
            $messageCode                 =  "";
            $data                        = array();
        }
        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
     
    }
   
}
