<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tutors extends MX_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->model('tutor_model');
        $this->load->library('email');
        //$this->load->helper('aes'); 
    }

    /*
     * Request Peram:
      {
      "id": "",
      "name": "raman@gmail.com",
      "profileImage": "123456",
      "email": "klfjdkjw978kj43j4kj34h3kj3",
      "password": "ios",
      "mobile1": "",
      "mobile2": "0",
      "dob": "raman@gmail.com/8505913155",
      "gender": "123456",
      "qualification": "klfjdkjw978kj43j4kj34h3kj3",
      "photoId": "ios",
      "qualificationCertificate": "",
      "video": "0",
      "studentTaught": "0",
      "teachingExp": "0",
      "physicalDisablity": "0",
      "aboutMe": "raman@gmail.com/8505913155",
      "address1": "123456",
      "address2": "klfjdkjw978kj43j4kj34h3kj3",
      "city": "ios",
      "state": "",
      "pincode": "0",
      "referalCode": "0",
      "fbId": "0",
      "googleId": "0",
      "socialType": "0",
      "coachingDetail": [{
      "board": "",
      "class": "0",
      "subject": "0",
      "feesPerHour": "0",
      "feesPerMonth": 0
      }, {
      "board": "",
      "class": "0",
      "subject": "0",
      "feesPerHour": "0",
      "feesPerMonth": ""
      }],
      "availablity": [{
      "typeOfTution": "",
      "premise": "0",
      "days": "0",
      "startTime": "",
      "endTime": ""
      }, {
      "typeOfTution": "",
      "premise": "0",
      "days": "0",
      "startTime": "",
      "endTime": ""
      }],
      "coachingLocation": [{
      "location": "",
      "radius": "0",
      "latitude": "",
      "longitude": "0"
      }, {
      "location": "",
      "radius": "0",
      "latitude": "",
      "longitude": "0"
      }]
      }
     */

    public function tutorSignUp() {
        $json = file_get_contents('php://input');
        //$post                                   = $this->input->post();         
        if (is_json($json)) {
            $document = array();
            $json = json_decode($json, true);
            $document['id'] = $json['id'];
            $document['name'] = $json['name'];
            $document['profileImage'] = $json['profileImage'];
            $document['email'] = $json['email'];
            $document['password'] = md5($json['password']);
            $document['mobile1'] = $json['mobile1'];
            $document['mobile2'] = $json['mobile2'];
            $document['dob'] = $json['dob'];
            $document['gender'] = $json['gender'];
            $document['qualification'] = $json['qualification'];
            $document['other_qualification'] = $json['other_qualification'];
            $document['photoId'] = $json['photoId'];
            $document['qualificationCertificate'] = $json['qualificationCertificate'];
            $document['video'] = $json['video'];
            $document['studentTaught'] = $json['studentTaught'];
            $document['teachingExp'] = $json['teachingExp'];
            $document['physicalDisablity'] = $json['physicalDisablity'];
            $document['aboutMe'] = $json['aboutMe'];
            $document['address1'] = $json['address1'];
            $document['address2'] = $json['address2'];
            $document['city'] = $json['city'];
            $document['state'] = $json['state'];
            $document['pincode'] = $json['pincode'];
            $document['fbId'] = $json['fbId'];
            $document['googleId'] = $json['googleId'];
            $document['socialType'] = $json['socialType'];
            $document['coachingDetail'] = $json['coachingDetail'];
            $document['availablity'] = $json['availablity'];
            $document['coachingLocation'] = $json['coachingLocation'];
            //$document['referalCode']                = $json['referalCode'];
            //$document['referalCode'] = substr($json['name'], 0, 5) . rand(1000, 9999);
             $document['referalCode'] = 'HTT'. rand(1000, 99999);
            $document['referalBy']                = $json['referalBy'];
            $document['isActive'] = 0;
            $document['createdDate'] = date('Y-m-d H:i:s');
            $isSuccess = false;
            $obj = new tutor_model;


            if ($json['id']) {
                $resultTutor = $obj->getTutorProfile($json['id']);
                //echo $resultEvent['eventName'].$json['eventName'];
                if ($resultTutor['email'] != $json['email']) {
                    $isEmailAlreadyExist = $obj->isEmailAlreadyExist($json['email']);
                } else {
                    $isEmailAlreadyExist = false;
                }
            } else {
                $isEmailAlreadyExist = $obj->isEmailAlreadyExist($json['email']);
            }
            if (!$isEmailAlreadyExist) {
                if ($json['id']) {
                    $resultTutor = $obj->getTutorProfile($json['id']);
                    //echo $resultEvent['eventName'].$json['eventName'];
                    if ($resultTutor['mobile1'] != $json['mobile1']) {
                        $isMobile1AlreadyExist = $obj->isMobileAlreadyExist($json['mobile1']);
                    } else {
                        $isMobile1AlreadyExist = false;
                    }
                } else {
                    $isMobile1AlreadyExist = $obj->isMobileAlreadyExist($document['mobile1']);
                }
                if (!$isMobile1AlreadyExist) {

                    if ($json['id']) {
                        $resultTutor = $obj->getTutorProfile($json['id']);
                        //echo $resultEvent['eventName'].$json['eventName'];
                        if ($resultTutor['mobile2'] != $json['mobile2']) {
                            $isMobile2AlreadyExist = $obj->isMobileAlreadyExist($json['mobile2']);
                        } else {
                            $isMobile2AlreadyExist = false;
                        }
                    } else {
                        $isMobile2AlreadyExist = $obj->isMobileAlreadyExist($document['mobile2']);
                    }
                    if (!$isMobile2AlreadyExist) {
                        
                          /*****************************Check manaditary field**************************/  
                            /*
                             * check for empty coachingLocation
                             */
                           foreach($document['coachingLocation'] as $checkcoachingLocation){
                               
                               foreach ($checkcoachingLocation as $key => $value)
                                { 
                                    if (array_key_exists($key, $checkcoachingLocation)) 
                                    {   
                                        
                                        if($checkcoachingLocation[$key]=='')
                                        {   
                                           $isSuccess = false;
                                           $message = "{$key} is empty in coaching Location";
                                           $data = array();
                                           echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
                                           return ;
                                        }
                                        
                                    }
                                    
                                }
                            } 
                            /*
                             * check for empty availablity
                             */
                               foreach($document['availablity'] as $checkavailablity){                               
                               foreach ($checkavailablity as $key => $value)
                                {  
                                    if (array_key_exists($key, $checkavailablity)) 
                                    {   
                                       if(empty($checkavailablity[$key]))
                                        {
                                           $isSuccess = false;
                                           $message = "{$key} is empty in availablity";
                                           $data = array();
                                           echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
                                           return ;
                                        }
                                    }
                                    
                                }
                            }
                            
                            /*
                             * check for empty coachingDetail
                             */
                            foreach($document['coachingDetail'] as $checkcoachingDetail){                             
                               foreach ($checkcoachingDetail as $key => $value)
                                { 
                                    if (array_key_exists($key, $checkcoachingDetail)) 
                                    {   
                                       if($key=='feesPerHour' || $key=='feesPerMonth')
                                        {
                                         if(empty($checkcoachingDetail['feesPerHour']) && empty($checkcoachingDetail['feesPerMonth']))
                                          {
                                           $isSuccess = false;
                                           $message = "{$key} is empty in coachingDetail";
                                           $data = array();
                                           echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
                                           return ;
                                        }
                                        }else{                                            
                                          if(empty($checkcoachingDetail[$key]))
                                          {
                                           $isSuccess = false;
                                           $message = "{$key} is empty in coachingDetail";
                                           $data = array();
                                           echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
                                           return ;
                                        }
                                            
                                        }
                                    }
                                    
                                }
                            }
                            //die("hh");
                      /*****************************Check manaditary field**************************/      
                        
                        $insertId = $obj->tutorSignUp($document);
                        if ($insertId) {
                            $isSuccess = true;
                            $message = "Signup Successful";
                            $data = $obj->getTutorProfile($insertId);
                        } else if ($this->db->affected_rows()) {
                            $isSuccess = true;
                            $message = "Profile updated Successfully";
                            $data = $obj->getTutorProfile($insertId);
                        } else {

                            $isSuccess = false;
                            $message = "Signup not successful";
                            $data = array();
                        }
                    } else {
                        $isSuccess = false;
                        $message = "Mobile2 already exist";
                        $data = array();
                    }
                } else {
                    $isSuccess = false;
                    $message = "Mobile No already exist";
                    $data = array();
                }
            } else {
                $isSuccess = false;
                $message = "Email already exist";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "message" => $message, "Result" => $data));
    }

    /*
     * Request Peram:
     * 
      "emailOrPhone": "raman@gmail.com/8505913155",
      "password": "123456",
      "deviceToken": "klfjdkjw978kj43j4kj34h3kj3",
      "deviceType": "ios",
      "socialType": "0/1/2",
      "fbId": "0",
      "googleId": "0"
     */

    function tutorLogin() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $document = array();
            $document['emailOrPhone'] = $json['emailOrPhone'];
            $document['password'] = md5($json['password']);
            $document['deviceToken'] = $json['deviceToken'];
            $document['deviceType'] = $json['deviceType'];
            $document['socialType'] = $json['socialType'];
            $document['fbId'] = $json['fbId'];
            $document['googleId'] = $json['googleId'];
            $isSuccess = false;
            $obj = new tutor_model;

            if ($json['socialType'] == 0) {
                $tutorLogin = $obj->tutorLogin($document);
                if (isset($tutorLogin['tutorId'])) {
                    $isSuccess = true;
                    $messageCode = "1";
                    $message = "Signin Successful";
                    $data = array($obj->getTutorProfile($tutorLogin['tutorId']));
                } elseif ($tutorLogin == 3) {

                    $isSuccess = false;
                    $messageCode = "3";
                    $message = "User not exist";
                    $data = array();
                } elseif ($tutorLogin == 2) {

                    $isSuccess = false;
                    $messageCode = "2";
                    $message = "Wrong credentails";
                    $data = array();
                }
            } else {
                if ($json['fbId']) {
                    $isFbIdExist = $obj->isFbIdExist($json['fbId']);
                    if ($isFbIdExist) {
                        $isSuccess = true;
                        $messageCode = "1";
                        $message = "Signin Successfull";
                        $data = array($obj->getTutorProfile($isFbIdExist));
                    } else {
                        $isSuccess = false;
                        $messageCode = "3";
                        $message = "User not exist";
                        $data = array();
                    }
                } else {
                    $isGoogleIdExist = $obj->isGoogleIdExist($json['googleId']);
                    if ($isGoogleIdExist) {
                        $isSuccess = true;
                        $messageCode = "1";
                        $message = "Signin Successfull";
                        $data = array($obj->getTutorProfile($isGoogleIdExist));
                    } else {
                        $isSuccess = false;
                        $messageCode = "3";
                        $message = "User not exist";
                        $data = array();
                    }
                }
            }
        } else {
            $isSuccess = false;
            $messageCode = "0";
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "messageCode" => $messageCode, "Message" => $message, "Result" => $data));
    }

    function getOTP() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $output = sendSMS($json['mobile']);
            //echo $output;
            if ($output['success'] == 1) {
                $isSuccess = true;
                $data = array(array("otp" => $output['data']));
                $message = "OTP sent to mobile.";
            } else {
                $isSuccess = false;
                $message = "OTP not sent to mobile," . $output['data'];
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    function forgotPassword() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $emailOrPhone = $json['emailOrPhone'];
            $isSuccess = false;
            $data = array();
            $obj = new tutor_model;
            $tutorId = $obj->isEmailOrMobileAlreadyExist($emailOrPhone);
            if ($tutorId) {
                if (filter_var($json['emailOrPhone'], FILTER_VALIDATE_EMAIL)) {
                    $otp = rand(1000, 9999);

                    $this->email->to($json['emailOrPhone']);
                    $this->email->from('hometutor@support.com');
                    $this->email->subject("HomeTutor:Forgot Password");
                    $this->email->message('Hi Here is the info you requested.Your OTP is ' . $otp);
                    if ($this->email->send()) {
                        $data = array(array("otp" => $otp, 'id' => $tutorId));
                        $message = "OTP sent to email.";
                        $isSuccess = true;
                    } else {
                        $isSuccess = false;
                        $message = "OTP not sent to email";
                        $data = array();
                    }
                } else {
                    $output = sendSMS($emailOrPhone);
                    if ($output['success'] == 1) {
                        $data = array(array("otp" => $output['data'], 'id' => $tutorId));
                        $message = "OTP sent to mobile.";
                        $isSuccess = true;
                    } else {
                        $isSuccess = false;
                        $message = "OTP not sent to mobile," . $output['data'];
                        $data = array();
                    }
                }
            } else {
                $isSuccess = false;
                $message = "User not exist";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    function changePassword() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);

            $document = array();
            $document['id'] = $json['id'];
            $document['password'] = md5($json['newPassword']);
            $document['modifiedDate'] = date('Y-m-d H:i:s');
            $isSuccess = false;
            $obj = new tutor_model;
            $obj->changePassword($document);
            if ($this->db->affected_rows()) {
                $isSuccess = true;
                $message = "Password changed successfully";
                $data = array();
            } else {
                $isSuccess = false;
                $message = "Password not changed";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    function getAllBoards() {
        $data = array();
        $isSuccess = false;
        $obj = new tutor_model;
        $getAllBoards = $obj->getAllBoards();
        if (!empty($getAllBoards)) {
            $isSuccess = true;
            $message = "Boards list found";
            $data = $getAllBoards;
        } else {
            $isSuccess = false;
            $message = "Boards list not found";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    function getAllClassesByBoardId() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);


            $boardId = $json['boardId'];

            $isSuccess = false;
            $obj = new tutor_model;
            $getAllClassesByBoardId = $obj->getAllClassesByBoardId($boardId);
            if (!empty($getAllClassesByBoardId)) {
                $isSuccess = true;
                $message = "Classes list found";
                $data = $getAllClassesByBoardId;
            } else {
                $isSuccess = false;
                $message = "Classes list not found";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    function getAllSubjectsByClassId() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $classId = $json['classId'];
            $isSuccess = false;
            $obj = new tutor_model;
            $getAllSubjectsByClassId = $obj->getAllSubjectsByClassId($classId);
            if (!empty($getAllSubjectsByClassId)) {
                $isSuccess = true;
                $message = "Subject list found";
                $data = $getAllSubjectsByClassId;
            } else {
                $isSuccess = false;
                $message = "Subject list not found";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    function getAllStates() {
        $data = array();
        $isSuccess = false;
        $obj = new tutor_model;
        $getAllStates = $obj->getAllStates();
        if (!empty($getAllStates)) {
            $isSuccess = true;
            $message = "State list found";
            $data = $getAllStates;
        } else {
            $isSuccess = false;
            $message = "State list not found";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    function getAllCityByStateName() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $stateName = $json['stateName'];
            $isSuccess = false;
            $obj = new tutor_model;
            $getAllCityByStateName = $obj->getAllCityByStateName($stateName);
            if (!empty($getAllCityByStateName)) {
                $isSuccess = true;
                $message = "City list found";
                $data = $getAllCityByStateName;
            } else {
                $isSuccess = false;
                $message = "City list not found";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    function getAllQualifications() {
        $data = array();
        $isSuccess = false;
        $obj = new tutor_model;
        $getAllQualifications = $obj->getAllQualifications();
        if (!empty($getAllQualifications)) {
            $isSuccess = true;
            $message = "Qualifications list found";
            $data = $getAllQualifications;
        } else {
            $isSuccess = false;
            $message = "Qualifications list not found";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*
     * Request Peram
      {
      "tabName":"2",
      "id":"1",
      "board": "",
      "class": "0",
      "subject": "0",
      "feesPerHour": "0",
      "feesPerMonth": 0
      }or
      {
      "tabName":"3",
      "id":"1",
      "typeOfTution": "",
      "premise": "0",
      "days": "0",
      "startTime": "",
      "endTime": ""
      }or
      {
      "tabName":"4",
      "id":"1",
      "location": "",
      "radius": "0"
      }
     */

    function updateTutorProfile() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $json['modifiedDate'] = date('Y-m-d H:i:s');
            $obj = new tutor_model;
            $obj->updateTutorProfile($json);
            if ($this->db->affected_rows()) {
                $isSuccess = true;
                $message = "Tutor profile updated";
                $data = array();
            } else {
                $isSuccess = false;
                $message = "Tutor profile not updated";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*
     * Request Peram
      {
      "id":"1";
      "tabName" :"2"
      }
     */

    function deleteRecordFromTab() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $json['isActive'] = 0;
            $json['modifiedDate'] = date('Y-m-d H:i:s');
            $obj = new tutor_model;
            $obj->deleteRecordFromTab($json);
            if ($this->db->affected_rows()) {
                $isSuccess = true;
                $message = "Tutor profile updated";
                $data = array();
            } else {
                $isSuccess = false;
                $message = "Tutor profile not updated";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*
     * Request Peram
      {
      "id":"1",
      "tabName" :"2"
      }
     */

    function getTabsData() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $document = array();
            $document['id'] = $json['id'];
            $document['tabName'] = $json['tabName'];
            $obj = new tutor_model;
            $getTabsData = $obj->getTabsData($document);
            //echo $this->db->last_query();
            if (!empty($getTabsData)) {
                $isSuccess = true;
                $message = "List found";
                $data = $getTabsData;
            } else {
                $isSuccess = false;
                $message = "List not found";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*
     *  Request Peram
      {
      "tabName":"1",
      "requestId" :"1",
      "parentId":"1",
      "tutorId" :"9",
      "mode":"Monthly",
      "fees" :"200",
      "duration":"2",
      "start" :"2016-07-01",
      "time":"Mon,Wed,Fri/06:00 AM",
      "location" :"Student",
      "type":"1"
      }

     */

    function tutorOfferToParent() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $document = array();
            $document['tabName'] = $json['tabName'];
            $document['fkRequestId'] = $json['requestId'];
            $document['fkParentId'] = $json['parentId'];
            $document['fkTutorId'] = $json['tutorId'];
            $document['mode'] = $json['mode'];
            $document['fees'] = $json['fees'];
            $document['duration'] = $json['duration'];
            $document['start'] = $json['start'];
            $document['days'] = $json['days'];
            $document['time'] = date('H:i:s', strtotime($json['time']));
            $document['location'] = $json['location'];
            $document['type'] = $json['type'];
            $document['isActive'] = 1;
            $document['createdDate'] = date('Y-m-d H:i:s');
            $obj = new tutor_model;
            $insertId = $obj->tutorOfferToParent($document);
            //echo $this->db->last_query();
            if (!empty($insertId)) {
                $isSuccess = true;
                $message = "Offer saved";
                $data = array();
            } else {
                $isSuccess = false;
                $message = "Offer not saved";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*
     *  Request Peram
      {
      "requestId" :"1",
      "parentId":"1",
      "tutorId" :"9",
      "status":"4/3/2/5"
      "statusData":{
      "tabName":"1",
      "requestId" :"1",
      "parentId":"1",
      "tutorId" :"9",
      "mode":"Monthly",
      "fees" :"200",
      "duration":"2",
      "start" :"2016-07-01",
      "time":"Mon,Wed,Fri/06:00 AM",
      "location" :"Student",
      "type":"1"
      }    or 2017-09-09 or reasonId
      }

     */

    function tutorUpdateStatus() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $document = array();
            $document['id'] = $json['parentRequestTutorId'];
            $document['fkParentRequestId'] = $json['requestId'];
            $document['fkParentId'] = $json['parentId'];
            $document['fkTutorId'] = $json['tutorId'];
            $document['status'] = $json['status'];
            $document['statusData'] = $json['statusData'];
            $document['modifiedDate'] = date('Y-m-d H:i:s');
            $obj = new tutor_model;
            $obj->tutorUpdateStatus($document);
            if ($this->db->affected_rows()) {
                $isSuccess = true;
                $message = "Status updated";
                $data = array();
            } else {
                $isSuccess = false;
                $message = "Status not updated";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*
     *  Request Peram
      {
      "tutorId":"1"
      }

     */

    function getTutorProfile() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $tutorId = $json['tutorId'];
            $obj = new tutor_model;
            $getTutorProfile = $obj->getTutorProfile($tutorId);
            if (!empty($getTutorProfile)) {
                $isSuccess = true;
                $message = "Profile detail found";
                $data = $getTutorProfile;
            } else {
                $isSuccess = false;
                $message = "Profile detail not found";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*
     *  Request Peram
      {
      "tabName":"1"
      }

     */

    function getAllReasons() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $data['tabName'] = $json['tabName'];
            $data['isParent'] = $json['isParent'];
            $obj = new tutor_model;
            $getAllReasons = $obj->getAllReasons($data);
            if (!empty($getAllReasons)) {
                $isSuccess = true;
                $message = "Reasons list found";
                $data = $getAllReasons;
            } else {
                $isSuccess = false;
                $message = "Reasons list not found";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*     * ***********************santosh08-08-2016************************************* */

    /*     * ***********************Start Class done************************************* */
    /*
     * Request:
     *             
     * {
      "class_id":"2",
      "fkTutorId":"2",
      "fkParentID":"3",
      "fkStudentID":"4,2",
      "subject_id":"4",
      "class_date":"20-08-2016",
      "class_duration":"1",
      "topic":"1,2",
      "topic_status":"0,2",
      "class_done":"1"      class scheduled Id
     * 
      }
     *
     */

    function tutorClassDone() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);

            $data = array();
            $isSuccess = false;
            $data['fkTutorId'] = $json['fkTutorId'];
            $data['fkParentID'] = $json['fkParentID'];
            $data['fkStudentID'] = $json['fkStudentID'];
            $data['subject_id'] = $json['subject_id'];
            $data['class_date'] = $json['class_date'];
            $data['class_duration'] = $json['class_duration'];
            $data['topic'] = $json['topic'];
            $data['topic_status'] = $json['topic_status'];
            $data['classScheduledID'] = $json['classScheduledID'];
            $obj = new tutor_model;
            $res=$obj->tutorClassDone($data);
            if ($res) {
                $isSuccess = true;
                $message = "Class done";
                $data = array();
            } else {
                $isSuccess = false;
                $message = "Class not done";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*     * ***********************End Class done************************************* */

    /*     * ****************************Get Topic*************************************** */
    /*
     * Request
     *
     *      
     * */

    function getSubjectTopic() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $data['class_id'] = $json['class_id'];
            $data['subject_id'] = $json['subject_id'];
            $obj = new tutor_model;
            $getAlltopic = $obj->getSubjectTopic($data);
            if (!empty($getAlltopic)) {
                $isSuccess = true;
                $message = "Topic list found";
                $data = $getAlltopic;
            } else {
                $isSuccess = false;
                $message = "Topic list not found";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*     * ****************************End Get Topic********************************************* */


    /*     * ****************************Get Erolled class*************************************** */
    /*
     * Request
     *
     *      
     * */
            
    function getEnrolledClass() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $data['fkTutorId'] = $json['fkTutorId'];
            $obj = new tutor_model;
            $getAllEnrolledClass = $obj->getEnrolledClass($data);
            if (!empty($getAllEnrolledClass)) {              
                $i=0;             
                foreach($getAllEnrolledClass as $getAllEnrolledClass){
                    $this->db->select('parent_student.name,parent_student.location,parent_student.latitude,'
                . 'parent_student.longitude,parent_student.class classID,ht_subject.name as subjectName,ht_class_schedule.*,'
                . 'DATE_FORMAT( ht_class_schedule.class_date, "%D %b") as classDate,'
                . 'DATE_FORMAT( ht_class_schedule.start_time, "%h:%i %p") as start_time');
        $this->db->where('ht_class_schedule.fkStudentID', $getAllEnrolledClass['fkStudentID']);
        $this->db->where('ht_class_schedule.class_date>="'.$getAllEnrolledClass['class_date'].'"');
        $this->db->where("(ht_class_schedule.class_status=0");
        $this->db->or_where("ht_class_schedule.class_status=4)");       
        $this->db->join('parent_student', 'ht_class_schedule.fkStudentID=parent_student.id');
        $this->db->join('ht_subject', 'ht_subject.id=ht_class_schedule.subject_id');
        $this->db->order_by('ht_class_schedule.class_date ASC');
        $this->db->order_by('ht_class_schedule.start_time ASC');
        $this->db->limit(5, 0);
        $getAllEnrolledClass1[$i]=$this->db->get('ht_class_schedule')->result_array();
        $i = $i+1;
                } 
      //  print_r($getAllEnrolledClass1);
        for($i=0;$i<count($getAllEnrolledClass1);$i++)
        {
            if($i==0){
            $temp = $getAllEnrolledClass1[$i];
            }else{
                $temp = array_merge($temp,$getAllEnrolledClass1[$i]);
            }
        }
        //pre($temp);
        foreach ($temp as $key => $row) {
            $class_date[$key]  = $row['class_date'];
            $start_time[$key] = date("H:i:s", strtotime($row['start_time']));
            //$start_time[$key] = $row['start_time'];
            
        }
        array_multisort($class_date, SORT_ASC, $start_time, SORT_ASC, $temp);

        foreach($temp as $val)
        {
            $dated = $val['class_date'];
            $myarray[$dated][] = $val;
        }    
         $myarray=array_values($myarray);
                $isSuccess = true;
                $message = "class list found";
                $data = $myarray;
            } else {
                $isSuccess = false;
                $message = "class list not found";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*     * ****************************End Erolled class********************************* */


    /*     * ****************************Rescheduled Erolled class*************************************** */
    /*
     * Request
     *
     *      
     * */

    function tutorClassRescheduled() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $data['fkTutorId'] = $json['fkTutorId'];
            $data['fkParentID'] = $json['fkParentID'];
            $data['fkStudentID'] = $json['fkStudentID'];
            $data['class_date'] = $json['class_date'];
            $data['start_time'] = date('H:i:s', strtotime($json['start_time']));
            $data['classScheduledID'] = $json['classScheduledID']; //Class Scheduled Enrolled Id
            $data['status'] = $json['status']; //Resion Id
            $obj = new tutor_model;
            $getClassRescheduled = $obj->tutorClassRescheduled($data);
            if (!empty($getClassRescheduled)) {
                $isSuccess = true;
                $message = "class Rescheduled";
                $data = $getClassRescheduled;
            } else {
                $isSuccess = false;
                $message = "class Not Rescheduled";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*     * ****************************End Rescheduled Erolled class********************************* */

    /*     * ****************************Cancel Erolled class*************************************** */
    /*
     * Request
     *
     *      
     * */

    function tutorClassCancel() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $data['fkTutorId'] = $json['fkTutorId'];
            $data['fkParentID'] = $json['fkParentID'];
            $data['fkStudentID'] = $json['fkStudentID'];
            $data['classScheduledID'] = $json['classScheduledID'];
            $data['statusData2'] = $json['statusData2']; //Cancel class Resion Id
            $obj = new tutor_model;
            $getClassCancel = $obj->tutorClassCancel($data);
            if (!empty($getClassCancel)) {
                $isSuccess = true;
                $message = "Class Cancelled";
                $data = $getClassCancel;
            } else {
                $isSuccess = false;
                $message = "Class Cancelled";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*     * ****************************End Rescheduled Erolled class********************************* */


    /*     * ****************************My Erolled class History********************************* */
    /*
     * Request Peram
      {
      "id":"1",
      "tabName" :"2"
      }
     */

    function myEnrolledClassHistory() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $document = array();
            $document['id'] = $json['id'];
            $document['tabName'] = $json['tabName'];
            $obj = new tutor_model;
            $getTabsData = $obj->myEnrolledClass($document);
            //echo $this->db->last_query();
            if (!empty($getTabsData)) {
                $isSuccess = true;
                $message = "List found";
                $data = $getTabsData;
            } else {
                $isSuccess = false;
                $message = "List not found";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*     * ****************************End My Erolled class********************************* */



    /*     * ****************************View Erolled class History Detail********************************* */
    /*
     * Request Peram
      {
      
      "parentRequestTutorId" :"4613"
      }
     */

    function viewEnrolledClassHistory() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $document = array();
            $document['parentRequestTutorId'] = $json['parentRequestTutorId'];
            $obj = new tutor_model;
            $getTabsData['ProfileDetail'] = $obj->viewEnrolledClassHistory($document['parentRequestTutorId']);
            $assignement_record = $obj->get_assignment_detail($document['parentRequestTutorId']);

            if (!empty($assignement_record)) {
                $request_download[] = $assignement_record;
                //print_r($request_download);
            }

            if (!empty($request_download)) {
                $i = 0;
                foreach ($request_download as $k) {
                                      //  print_r($k);
                    foreach ($k as $a) {// print_r($a);echo $a['id'];
                        $a['detail'] = $obj->get_assignment_full_detail($a['id']);
                        $result[$i][] = $a;
                    }
                    $i++;
                }
            }
            //print_r($result);die;
            if(!empty($result)){
            $getTabsData['topicDetail']=$result;
            }else
            {
                $getTabsData['topicDetail']='';
            }
            //echo $this->db->last_query();
            if (!empty($getTabsData)) {
                $isSuccess = true;
                $message = "List found";
                $data = $getTabsData;
            } else {
                $isSuccess = false;
                $message = "List not found";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*     * ****************************End View Erolled class History Detail********************************* */


    /*     * ****************************Send Message*************************************** */
    /*
     * Request
     *
     *      
     * */

    function sendMessagesBy() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $data['fkFromUserId'] = $json['fkFromUserId'];
            $data['fkToUserId'] = $json['fkToUserId'];
            $data['fkToStuId'] = $json['fkToStuId'];
            $data['isParent'] = $json['isParent'];
            $data['messgae'] = $json['messgae'];
            $data['createdDate'] = date("Y-m-d H:i:s");
            $obj = new tutor_model;
            $sendMessagesBy = $obj->sendMessagesBy($data);
            if (!empty($sendMessagesBy)) {
                $isSuccess = true;
                $message = "Message Send";
                $data = $sendMessagesBy;
            } else {
                $isSuccess = false;
                $message = "Message not Send";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*     * ****************************End Send Message********************************** */



    /*     * ****************************Get Message*************************************** */
    /*
     * Request for Inbox
     *
     *      
     * */

    function getMessagesBy() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $data['fkToUserId'] = $json['fkToUserId'];
            $data['isParent'] = $json['isParent'];
            $obj = new tutor_model;
            $getMessagesBy = $obj->getMessagesBy($data);
            if (!empty($getMessagesBy)) {
                $isSuccess = true;
                $message = "Message list found";
                $data = $getMessagesBy;
            } else {
                $isSuccess = false;
                $message = "Message list not found";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*     * ****************************End Send Message*************************************** */

    /*     * ****************************Get Send Message*************************************** */
    /*
     * Request for Send box
     *
     *      
     * */

    function getSendMessagesBy() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $data['fkFromUserId'] = $json['fkFromUserId'];
            $data['isParent'] = $json['isParent'];
            $obj = new tutor_model;
            $getSendMessagesBy = $obj->getSendMessagesBy($data);
            if (!empty($getSendMessagesBy)) {
                $isSuccess = true;
                $message = " Send Message list found";
                $data = $getSendMessagesBy;
            } else {
                $isSuccess = false;
                $message = "Send Message list not found";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*     * ****************************End Get Send Message********************************* */

    /*     * ****************************Get Send Message*************************************** */
    /*
     * Request for Send box
     *
     *      
     * */

    function deletedMessages() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            // $data['messageId']                      = $json['messageId'];  
            $data['fkFromUserId'] = $json['fkFromUserId'];
            $data['isParent'] = $json['isParent'];
            $obj = new tutor_model;

            $deletedMessages = $obj->deletedMessages($data);
            if (!empty($deletedMessages)) {
                $isSuccess = true;
                $message = "Delete";
                $data = $deletedMessages;
            } else {
                $isSuccess = false;
                $message = "not Delete";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*     * ****************************End Get Send Message********************************* */


    /*     * ****************************Send Push Notification for offline Parent*************************************** */
    /*
     * Request
     */

    function sendPushOfflineMessage() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $data['fkFromUserId'] = $json['fkFromUserId'];
            $data['fkToUserId'] = $json['fkToUserId'];
            $data['isParent'] = $json['isParent'];
            $data['chatID'] = $json['chatID'];
            $data['chatID2'] = $json['chatID2']; 
            $data['fkToStuId'] = $json['fkToStuId'];
            $obj = new tutor_model;
            $sendPushOfflineMessage = $obj->sendPushOfflineMessage($data);
            if (!empty($sendPushOfflineMessage)) {
                $isSuccess = true;
                $message = "Notification Send";
                $data = $sendPushOfflineMessage;
            } else {
                $isSuccess = false;
                $message = "Notification not Send";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*     * ****************************End Push Notification for offline Parent********************************** */

    /**********************************Start delete Current Erolled class********************************* */
    /*
     * Request
     *      
     * */

    function deletEnrolledClassTutor() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $data['fkTutorId'] = $json['fkTutorId'];
            $data['fkParentID'] = $json['fkParentID'];
            $data['fkStudentID'] = $json['fkStudentID'];
            $data['statusData3'] = $json['statusData3']; //Resion Id for class terminate
            $obj = new tutor_model;
            $deletEnrolledClassTutor = $obj->deletEnrolledClassTutor($data);
            if (!empty($deletEnrolledClassTutor)) {
                $isSuccess = true;
                $message = "Tuition Termination Request Registered. Admin will contact you soon!!";
                $data = $deletEnrolledClassTutor;
            } else {
                $isSuccess = false;
                $message = "Enrolled not delete.";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*     * *****************************End delete Current Erolled class******************************* */


    /*     * **********************************Start delete Previous Erolled class********************************* */
    /*
     * Request
     *      
     * */

    function deletPreEnrolledClassTutor() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $data['fkTutorId'] = $json['fkTutorId'];
            $data['fkParentID'] = $json['fkParentID'];
            $data['fkStudentID'] = $json['fkStudentID'];
            //$data['statusData3'] = $json['statusData3']; //Resion Id for class terminate
            $obj = new tutor_model;
            $deletPreEnrolledClassTutor = $obj->deletPreEnrolledClassTutor($data);
            if (!empty($deletPreEnrolledClassTutor)) {
                $isSuccess = true;
                $message = "Previous Enrolled class deleted succesfully!!";
                $data = $deletPreEnrolledClassTutor;
            } else {
                $isSuccess = false;
                $message = "Enrolled not delete.";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*     * *****************************End delete Previous Erolled class******************************* */

    /******************************MyErolled class Update offer******************************* */
     function updateOfferEnrolledTutor() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $document = array();
            //$document['tabName']                    = $json['tabName'];
            $document['fkRequestId'] = $json['requestId'];
            $document['fkParentId'] = $json['parentId'];
            $document['fkTutorId'] = $json['tutorId'];            
            $document['mode'] = $json['mode'];
            $document['fees'] = $json['fees'];
            $document['duration'] = $json['duration'];
            $document['start'] = $json['start'];
            $document['days'] = $json['days'];
            $document['time'] = date('H:i:s', strtotime($json['time']));
            $document['location'] = $json['location'];
            $document['type'] = $json['type'];
            $document['isActive'] = 1;
            $document['createdDate'] = date('Y-m-d H:i:s');           
            $obj = new tutor_model;            
        
            $getPstudent=$obj->getPstudent($document['fkRequestId']);//Get current student detail
          
            $getOldRequestStudent=$obj->getRequestStudent($document); //check current offer accept by parent     
           
            $getNewRequestStudent=$obj->getNewRequestStudent($getPstudent['update_enrolled']);//check new update offer accept by parent
            
           if(!empty($getNewRequestStudent) && $getNewRequestStudent['statusOffer']==0)//update new offer when new offer not accept by parent
           {   $document['fkRequestId']=$getNewRequestStudent['fkParentRequestId'];
               $insertId = $obj->updateOfferEnrolledTutor($document);
           }
           else{ 
            if($getOldRequestStudent['statusOffer']==1){ // new enrollment genrate when tutor update new offer
            $getPstudent=$obj->getPstudent($document['fkRequestId']); 
            if(!empty($getPstudent)){           
             unset($getPstudent['id']);
             $getPstudent['createdDate']= date('Y-m-d H:i:s');         
             $getPstudent['update_enrolled']=$document['fkRequestId'];             
             $this->db->insert('ht_parent_student', $getPstudent); 
             $student_id=$this->db->insert_id(); 
             
             $updatePstudent['update_enrolled']=$student_id;
             $this->db->where('id', $document['fkRequestId']); 
             $this->db->update('ht_parent_student', $updatePstudent); 
              //$student_id=1233; 
            }
            /***********Insert Offer**************/
            $oldRequestId=$document['fkRequestId'];
            $document['fkRequestId']=$student_id;
            $insertId = $obj->tutorOfferToParent($document);
            /***************End*****************************/
            $document['fkRequestId']=$oldRequestId;
           //  print_r($document);
            $getRequestStudent=$obj->getRequestStudent($document); 
            // print_r($getRequestStudent);die("hh");
             if(!empty($getRequestStudent)){            
             $updateStudentID =$getRequestStudent['fkParentRequestId']; 
             $updateStatusData =$insertId;
             unset($getRequestStudent['id']);
             unset($getRequestStudent['statusData']);
             $getRequestStudent['fkParentRequestId']=$student_id;
             $getRequestStudent['statusOffer']=0;          
             $getRequestStudent['update_enrolled']=$updateStudentID;
             $getRequestStudent['statusData']=$updateStatusData;
             $getRequestStudent['createdDate']= date('Y-m-d H:i:s');
             $this->db->insert('ht_parentRequestTutor', $getRequestStudent); 
            }

            }
            else{//old offer update when request not update by parent
              $insertId = $obj->updateOfferEnrolledTutor($document);
            }
           
           }
            if (!empty($insertId)) {
                $isSuccess = true;
                $message = "Offer Update";
                $data = array();
            } else {
                $isSuccess = false;
                $message = "Offer not Update";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }
    /******************************End MyErolled class Update offer******************************* */
    
      /********************Auto Terminate Offer Update************************/
    function autoTerminateScheduler() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false; 
            $obj = new tutor_model;
            $getUpdateRequest=$obj->getUpdateRequest();  
           
            if (!empty($getUpdateRequest)) {                
                foreach($getUpdateRequest as $UpdateRequest){
                  $getPstudentOffer=$obj->getPstudentOffer($UpdateRequest['fkParentRequestId']);
                  if(date("m", strtotime($getPstudentOffer['start'])) == date('m')){                    
                  $udateReq['status']=6; 
                  $udateReq['terminate_enrolled']=2;
                  $this->db->where('fkParentId',$UpdateRequest['fkParentId']);  
                  $this->db->where('fkTutorId',$UpdateRequest['fkTutorId']);  
                  $this->db->where('fkParentRequestId',$UpdateRequest['update_enrolled']);  
                  $check=$this->db->update('ht_parentRequestTutor',$udateReq);
                  if($check){
                  $udateSchedule['class_status']=6;                  
                  $this->db->where('fkParentID',$UpdateRequest['fkParentId']);  
                  $this->db->where('fkTutorId',$UpdateRequest['fkTutorId']);  
                  $this->db->where('fkStudentID',$UpdateRequest['update_enrolled']);  
                  $this->db->where('class_status',0); 
                  $this->db->update('ht_class_schedule',$udateSchedule);  
                  }
                  
                 }                 
                }                
                $isSuccess = true;
                $message = "Class terminate successfully.!!";
                $data = array();
            } else {
                $isSuccess = false;
                $message = "Class not terminate.!!";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }
      /********************End Auto Terminate Offer Update************************/
    
    
    
    /*************************************Reoffer by tutor************************************/
     function reOfferEnrolledTutor() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $document = array();
            //$document['tabName']                    = $json['tabName'];
            $document['fkRequestId'] = $json['requestId'];
            $document['fkParentId'] = $json['parentId'];
            $document['fkTutorId'] = $json['tutorId'];
            $document['mode'] = $json['mode'];
            $document['fees'] = $json['fees'];
            $document['duration'] = $json['duration'];
            $document['start'] = $json['start'];
            $document['days'] = $json['days'];
            $document['time'] = date('H:i:s', strtotime($json['time']));
            $document['location'] = $json['location'];
            $document['type'] = $json['type'];
            $document['isActive'] = 1;
            $document['modifiedDate'] = date('Y-m-d H:i:s');
            $obj = new tutor_model;
            $insertId = $obj->reOfferEnrolledTutor($document);            
            if (!empty($insertId)) {
                $isSuccess = true;
                $message = "Re-Offer successfully!!";
                $data = array();
            } else {
                $isSuccess = false;
                $message = "Offer not send!!";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }
    /************************************End**************************************************/


    /***************************Get Scheme Refer & Earn************************************** */

    function getSchemeRefer() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $data['fkFromTutorId'] = $json['fkFromTutorId'];
            $data['reffer'] = $json['reffer'];//for check tutor or parent 0 for tutre, 1 for parent
            $obj = new tutor_model;
            $getSchemeRefer = $obj->getSchemeRefer($data);
            if (!empty($getSchemeRefer)) {
                $isSuccess = true;
                $message = "Scheme found";
                $data = $getSchemeRefer;
            } else {
                $isSuccess = false;
                $message = "Scheme not found";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /*     * **************************Get Scheme Refer & Earn************************************** */
    
     /***************************Check Email Exist************************************* */

   function checkEmailAlreadyExist() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $data['email'] = $json['email'];
            $data['mobile1'] = $json['mobile1'];
            $data['referalBy'] = $json['referalBy'];
            $data['deviceToken'] = $json['deviceToken'];
            $obj = new tutor_model;
            $isEmailAlreadyExist = $obj->isEmailAlreadyExist($data['email']);
            $isMobile1AlreadyExist = $obj->isMobileAlreadyExist($json['mobile1']);
            if($data['referalBy']){
            $checkReferCode = $obj->checkReferCode($data['referalBy']);
            if($checkReferCode){
           // for tutor notification  
            $checkReferCodeData = $obj->checkReferCodeData($data['referalBy']);   
            $getscheme = $obj->getRefferPrice();           
            $msg1 = '{"type":38,"message":{"ReferalName":"' .$checkReferCodeData['name'] . '",'
                   . '"amount":"' . $getscheme['price'] . '"}}';
 
            $Tmsg = 'Congratulations. Your referral '.$checkReferCodeData['name']  . ' '
                   . 'has registered.';    
       
            sendAndroidPush($data['deviceToken'], $msg1, "", "", 38);
            sendPushAsSMS($data['mobile1'], $Tmsg);
            }
            }else{
             $checkReferCode = true;  
            }
            
            if (!empty($isEmailAlreadyExist) ) { 
                $isSuccess = false;
                $message = "Email already exists";
                $data = $isEmailAlreadyExist;
            } 
           else if(!empty($isMobile1AlreadyExist))
           {
                $isSuccess = false;
                $message = "Mobile already exists";
                $data = $isMobile1AlreadyExist;
           }
           
            else if(!$checkReferCode)
           {
                $isSuccess = false;
                $message = "Referal code not exists";
                $data = $checkReferCode;
           }
           
            else {
                $isSuccess = true;
                $message = "Email or Mobile not found";
                $data = array();
            }
          
           
            
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }
    /***************************End Check Email Exist************************************** */
    
    
     /***************************Check Mobile No. Exist************************************* */

    function checkMobileAlreadyExist() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $data['mobile1'] = $json['mobile1'];
            $obj = new tutor_model;
            $isMobile1AlreadyExist = $obj->isMobileAlreadyExist($json['mobile1']);
            if (!empty($isMobile1AlreadyExist)) {
                $isSuccess = true;
                $message = "Mobile already exist";
                $data = $isMobile1AlreadyExist;
            } else {
                $isSuccess = false;
                $message = "Mobile not found";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }

    /***************************End Check Email Exist************************************** */
    
      /***************************Start getTabsDataCount************************************** */
     function getTabsDataCount() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $document = array();
            $document['id'] = $json['id'];          
            $obj = new tutor_model;
            $getTabsDataCount = $obj->getTabsDataCount($document);
            //echo $this->db->last_query();
            if (!empty($getTabsDataCount)) {
                $isSuccess = true;
                $message = "List found";
                $data = $getTabsDataCount;
            } else {
                $isSuccess = false;
                $message = "List not found";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }
    /***************************End getTabsDataCount************************************** */
    
    
     /***************************Start sms Send Assignment Complete************************************** */
     function smsSendAssignmentCom() {
        $json = file_get_contents('php://input');
        if (is_json($json)) {
            $json = json_decode($json, true);
            $data = array();
            $isSuccess = false;
            $document = array();
            $document['fkTutorId'] = $json['fkTutorId'];
            $document['Assignment'] = $json['Assignment'];
            $document['Attempt'] = $json['Attempt'];  
            $document['Date_Time'] = $json['Date_Time'];  
            $document['Time_taken'] = $json['Time_taken'];  
            $document['Correct_Answer_per'] = $json['Correct_Answer_per'];  
            $document['Total_Questions'] = $json['Total_Questions'];
            $document['Attempted_Questions'] = $json['Attempted_Questions'];  
            $document['Correct_Answers'] = $json['Correct_Answers']; 
            $obj = new tutor_model;
            $smsSendAssignmentCom = $obj->smsSendAssignmentCom($document);
            //echo $this->db->last_query();
            if (!empty($smsSendAssignmentCom)) {
                $isSuccess = true;
                $message = "Message Send Successfully!!";
                $data = $smsSendAssignmentCom;
            } else {
                $isSuccess = false;
                $message = "Message Not Send Successfully!!";
                $data = array();
            }
        } else {
            $isSuccess = false;
            $message = "Invaid Json Input";
            $data = array();
        }
        echo json_encode(array("isSuccess" => $isSuccess, "Message" => $message, "Result" => $data));
    }
    /***************************End sms Send Assignment Complete*************************************** */
    

    /****************************End************************************************* */
}
