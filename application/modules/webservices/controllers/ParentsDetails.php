<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Parents
 *
 * @author appsquadz
 */
class ParentsDetails extends MX_Controller {
    
    function __construct()
    {
       // error_reporting(0);
       parent::__construct();
        $this->load->model('parentsDetails_Model'); 
        $this->load->library('email');
        // date_default_timezone_set("Asia/Bankok");
    } 
    
    
    
      
    public function classesAmountDue()
    {            
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();
            $document['parentId']               = $json['parentId']; 
          
          
            $isSuccess                      = false;
            $obj                            = new ParentsDetails_Model;
           
               
            $results                        = $obj->getClassesAmountDetails($document);
                    
                    if($results)
                    {                                          
                        $isSuccess      = true;
                        $message        = "Details Shown Successfully";
                        $data           = $results;
                    }
                    else
                    {
                        $isSuccess      = false;
                        $message        = "Details Couldnt Show ";
                        $data           = array();
                    }

        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }

        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }

       
   public function addPayment()
    {            
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();
            $document1                      = array();
            $document2                      = array();
            $document['paymentData']        = $json['paymentData'];
            $document1['incentiveAmount']   = $json['incentiveAmount'];
            $document1['fkParentId']        = $json['parentId'];
	    $document2['totalAmount']       = $json['totalAmount'];
          
            $isSuccess                      = false;
            $obj                            = new ParentsDetails_Model;
           
            foreach($json['paymentData'] as $data) 
            {  
                 $results                        = $obj->addPayment($data);
            }
            
               
            
            if($results)
            {   
                $results1                        = $obj->parentIncentiveUsed($document1);
                
                $document2['parentId']              = $json['parentId'];
                $parentDetails                      = $obj->getParentDeviceToken($document2);
                $parentDeviceToken                  = $parentDetails['deviceToken'];
                $parentPhone                        = $parentDetails['mobile1'];
              
                $msg3push = '{"type":"62","message":{"Amount:"' . $document2['totalAmount'] .'"}}' ;
              
               sendAndroidPush($parentDeviceToken, $msg3push, "", "", 62);			
				
		$msg1 = '"You have made a payment of Rs "' . $document2['totalAmount'] . '". Please open Account section in App to view details. "';
    		sendPushAsSMS($parentPhone, $msg1); 
                $isSuccess      = true;
                $message        = "Details Shown Successfully";
                $data           = $results;
            }
            else
            {
                $isSuccess      = false;
                $message        = "Details Couldnt Show ";
                $data           = array();
            }

        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }

        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }
    
    
     public function parentsAccountDetails()
    {           
         
         
         
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();
            $document['parentId']           = $json['parentId'];
            //$document['tutorId']            = $json['tutorId'];
           
          
          
            $isSuccess                      = false;
            $obj                            = new ParentsDetails_Model;
           
               
            $results                        = $obj->getAccountDetails($document);
                    
                    if($results)
                    { 
                        
                        foreach($results as $val)
                        {
                            $box = $val['fkStudentId'].$val['fkSubjectId'].$val['fkTutorId'];
                            if(isset($final[$box])and !empty($final[$box]))
                            {
                                $final[$box][] = $val;
                            }
                            else
                            {
                                $final[$box] = array();
                                $final[$box][] = $val;
                            }
                        }
                        
                        
                        
                        $isSuccess      = true;
                        $message        = "Details Shown Successfully";
                        $data           = array_values($final);
                    }
                    else
                    {
                        $isSuccess      = false;
                        $message        = "Details Couldnt Show ";
                        $data           = array();
                    }

        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }

        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }
    
    
    
      public function parentsPaymentDetails()
    {           
         
         
         
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();
            $document['parentId']           = $json['parentId'];
            //$document['tutorId']            = $json['tutorId'];
           
          
          
            $isSuccess                      = false;
            $obj                            = new ParentsDetails_Model;
           
               
            $results                        = $obj->parentsPaymentDetails($document);
                    
                    if($results)
                    { 
                        
                        foreach($results as $val)
                        {
                            $box = $val['fkStudentId'].$val['fkSubjectId'].$val['fkTutorId'];
                            if(isset($final[$box])and !empty($final[$box]))
                            {
                                $final[$box][] = $val;
                            }
                            else
                            {
                                $final[$box] = array();
                                $final[$box][] = $val;
                            }
                        }
                        
                        
                        
                        $isSuccess      = true;
                        $message        = "Details Shown Successfully";
                        $data           = array_values($final);
                    }
                    else
                    {
                        $isSuccess      = false;
                        $message        = "Details Couldnt Show ";
                        $data           = array();
                    }

        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }

        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }
    

	/*************************Tutor-Account************************/


	   
    public function tutorAmountDue()
    {            
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();
            $document['fkTutorId']               = $json['fkTutorId']; 
          
          
            $isSuccess                      = false;
            $obj                            = new ParentsDetails_Model;
           
               
            $results                        = $obj->getTutorAmountDetails($document);
                    
                    if($results)
                    {                                          
                        $isSuccess      = true;
                        $message        = "Details Shown Successfully";
                        $data           = $results;
                    }
                    else
                    {
                        $isSuccess      = false;
                        $message        = "Details Couldnt Show ";
                        $data           = array();
                    }

        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }

        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }
    
    
    
     public function tutorPaymentRecieved()
    {            
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();
            $document['fkTutorId']               = $json['fkTutorId']; 
          
          
            $isSuccess                      = false;
            $obj                            = new ParentsDetails_Model;
           
               
            $results                        = $obj->tutorPaymentRecieved($document);
                    
                    if($results)
                    {                                          
                        foreach($results as $val)
                        {
                            $box = $val['fkStudentId'].$val['fkSubjectId'].$val['fkTutorId'];
                            if(isset($final[$box])and !empty($final[$box]))
                            {
                                $final[$box][] = $val;
                            }
                            else
                            {
                                $final[$box] = array();
                                $final[$box][] = $val;
                            }
                        }
                        
                        
                        
                        $isSuccess      = true;
                        $message        = "Details Shown Successfully";
                        $data           = array_values($final);
                    }
                    else
                    {
                        $isSuccess      = false;
                        $message        = "Details Couldnt Show ";
                        $data           = array();
                    }

        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }

        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }


	  
    /************Admin Account Side ************/
    
    
     public function tutorPaymentsByAdmin()
    {            
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();
            $document['fkTutorId']          = $json['fkTutorId']; 
          
          
            $isSuccess                      = false;
            $obj                            = new ParentsDetails_Model;
           
               
            $results                        = $obj->tutorPaymentsByAdmin($document);
                    
                    if($results)
                    {                                          
                        $isSuccess      = true;
                        $message        = "Details Shown Successfully";
                        $data           = $results;
                    }
                    else
                    {
                        $isSuccess      = false;
                        $message        = "Details Couldnt Show ";
                        $data           = array();
                    }

        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }

        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }
    
    
    
    
     public function tutorIncentives()
    {            
        $json                               = file_get_contents('php://input');       
        if(is_json($json))
        {
            $json                           = json_decode($json,true);
            $document                       = array();
            $document['fkTutorId']          = $json['fkTutorId']; 
          
          
            $isSuccess                      = false;
            $obj                            = new ParentsDetails_Model;
           
               
            $results                        = $obj->tutorIncentives($document);
                    
                    if($results)
                    {                                          
                        $isSuccess      = true;
                        $message        = "Details Shown Successfully";
                        $data           = $results;
                    }
                    else
                    {
                        $isSuccess      = false;
                        $message        = "Details Couldnt Show ";
                        $data           = array();
                    }

        }
        else
        {
            $isSuccess      = false;
            $message        = "Invaid Json Input";
            $data           = array();
        }

        echo json_encode(array("isSuccess"=>$isSuccess,"message"=>$message,"Result"=>$data));
    }
    
    
    
}
