<?php

defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/*
 * Webservice HomeTutor Start
 */
$route['tutors'] = 'webservices/tutors';
$route['tutorLogin'] = 'webservices/tutors/tutorLogin';
$route['tutorSignUp'] = 'webservices/tutors/tutorSignUp';
$route['getTutorProfile'] = 'webservices/tutors/getTutorProfile';
$route['getOTP'] = 'webservices/tutors/getOTP';
$route['forgotPassword'] = 'webservices/tutors/forgotPassword';
$route['changePassword'] = 'webservices/tutors/changePassword';
$route['getAllBoards'] = 'webservices/tutors/getAllBoards';
$route['getAllClassesByBoardId'] = 'webservices/tutors/getAllClassesByBoardId';
$route['getAllSubjectsByClassId'] = 'webservices/tutors/getAllSubjectsByClassId';
$route['getAllStates'] = 'webservices/tutors/getAllStates';
$route['getAllCityByStateName'] = 'webservices/tutors/getAllCityByStateName';
$route['getAllQualifications'] = 'webservices/tutors/getAllQualifications';
$route['updateTutorProfile'] = 'webservices/tutors/updateTutorProfile';
$route['deleteRecordFromTab'] = 'webservices/tutors/deleteRecordFromTab';
$route['getTabsData'] = 'webservices/tutors/getTabsData';
$route['tutorOfferToParent'] = 'webservices/tutors/tutorOfferToParent';
$route['tutorUpdateStatus'] = 'webservices/tutors/tutorUpdateStatus';
$route['getAllReasons'] = 'webservices/tutors/getAllReasons';

/* * ****************santosh 02-08-2016*************** */
$route['tutorClassDone'] = 'webservices/tutors/tutorClassDone';
$route['getSubjectTopic'] = 'webservices/tutors/getSubjectTopic';
$route['tutorClassRescheduled'] = 'webservices/tutors/tutorClassRescheduled';
$route['tutorClassCancel'] = 'webservices/tutors/tutorClassCancel';
$route['getEnrolledClass'] = 'webservices/tutors/getEnrolledClass';
$route['myEnrolledClassHistory'] = 'webservices/tutors/myEnrolledClassHistory';
$route['viewEnrolledClassHistory'] = 'webservices/tutors/viewEnrolledClassHistory';
$route['sendMessagesBy'] = 'webservices/tutors/sendMessagesBy';
$route['getMessagesBy'] = 'webservices/tutors/getMessagesBy';
$route['getSendMessagesBy'] = 'webservices/tutors/getSendMessagesBy';
$route['deletedMessages'] = 'webservices/tutors/deletedMessages';
$route['sendPushOfflineMessage'] = 'webservices/tutors/sendPushOfflineMessage';
$route['deletEnrolledClassTutor'] = 'webservices/tutors/deletEnrolledClassTutor';
$route['deletPreEnrolledClassTutor'] = 'webservices/tutors/deletPreEnrolledClassTutor';
$route['updateOfferEnrolledTutor'] = 'webservices/tutors/updateOfferEnrolledTutor';
$route['getSchemeRefer'] = 'webservices/tutors/getSchemeRefer';
$route['checkEmailAlreadyExist'] = 'webservices/tutors/checkEmailAlreadyExist';
$route['checkMobileAlreadyExist'] = 'webservices/tutors/checkMobileAlreadyExist';
$route['getTabsDataCount'] = 'webservices/tutors/getTabsDataCount';
$route['autoTerminateScheduler'] = 'webservices/tutors/autoTerminateScheduler';
$route['reOfferEnrolledTutor'] = 'webservices/tutors/reOfferEnrolledTutor';
$route['smsSendAssignmentCom'] = 'webservices/tutors/smsSendAssignmentCom';
/* * ****************End santosh******************** */

/*
 * Webservice HomeTutor End
 */

/*
 * Webservice Parent Start
 */
$route['parentLogin'] = 'webservices/parents/parentLogin';
$route['parentSignUp'] = 'webservices/parents/parentSignUp';
$route['parentgetOTP'] = 'webservices/parents/parentgetOTP';
$route['getParentProfile'] = 'webservices/parents/getParentProfile';
$route['forgotPasswordParent'] = 'webservices/parents/forgotPasswordParent';
$route['changePasswordParent'] = 'webservices/parents/changePasswordParent';
$route['addStudent'] = 'webservices/parents/addStudent';
$route['pickAStudent'] = 'webservices/parents/pickAStudent';
$route['parentClasses'] = 'webservices/parents/parentClasses';
$route['parentSubjects'] = 'webservices/parents/parentSubjects';
$route['parentSendRequest'] = 'webservices/parents/parentSendRequest';
$route['getTabsDataForParent'] = 'webservices/parents/getTabsDataForParent';
$route['updateCancel'] = 'webservices/parents/updateCancelOrDelete';
$route['updateOfferStatus'] = 'webservices/parents/updateOfferStatus';
$route['deSelectTutor'] = 'webservices/parents/deSelectTutor';
$route['sendMessages'] = 'webservices/parents/sendMessages';

/* * ****************santosh 02-08-2016*************** */
$route['getParentMyProfile'] = 'webservices/parents/getParentMyProfile';
$route['editParentProfile'] = 'webservices/parents/editParentProfile';
$route['getStudentProfile'] = 'webservices/parents/getStudentProfile';
$route['deleteParentStudent'] = 'webservices/parents/deleteParentStudent';
$route['editStudentProfile'] = 'webservices/parents/editStudentProfile';
$route['getStudentEnrolled'] = 'webservices/parents/getStudentEnrolled';
$route['cancelEnrolledClass'] = 'webservices/parents/cancelEnrolledClass';
$route['terminateEnrolledClass'] = 'webservices/parents/terminateEnrolledClass';
$route['getAllReasonsParent'] = 'webservices/parents/getAllReasonsParent';
$route['getStudSchedulerHist'] = 'webservices/parents/getStudSchedulerHist';
$route['getParentSchemeRefer'] = 'webservices/parents/getParentSchemeRefer';
$route['addPstudent'] = 'webservices/parents/addPstudent';
$route['getTabsDataParentCount'] = 'webservices/parents/getTabsDataParentCount';
$route['genrateAutoScheduler'] = 'webservices/parents/genrateAutoScheduler';
/* * ****************End santosh******************** */

/* * ****************gaurav 27-7-2016*************** */
$route['assignment'] = 'webservices/parents/assignment';
$route['question'] = 'webservices/parents/question';
$route['assignment_actions'] = 'webservices/parents/assignment_actions';
$route['set_assignment_complete'] = 'webservices/parents/set_assignment_complete';
$route['assignment_detail'] = 'webservices/parents/assignment_detail';
$route['student_detail_share_with_tutor']= 'webservices/Share_tutor/student_detail_share_with_tutor';
$route['tutor_offer_show_screen']= 'webservices/Share_tutor/tutor_offer_show_screen';

$route['how_it_work']= 'webservices/Share_tutor/image_for_how_it_work';
$route['why_chooses']= 'webservices/Share_tutor/image_for_why_chooses';
/* * ****************gaurav 27-7-2016*************** */
/*
 * Webservice Parent End
 */

/*
 * Webservice Version Start santosh
 */
$route['version']= 'webservices/Version_detail/version';
/*
 * Webservice Version End
 */

/*
 * Admin HomeTutor Start
 */
$route['admin'] = 'admin_panel/admin';

$route['login'] = 'admin_panel/login';
$route['adminLogin'] = 'admin_panel/login/logins';

//$route['adminLogin'] = 'admin_panel/admin/login';
$route['logout'] = 'admin_panel/admin/logout';
$route['dashboard'] = 'admin_panel/admin/dashboard';
$route['tutors'] = 'admin_panel/admin/manageTutors';
$route['changeStatus/(:num)/(:num)'] = 'admin_panel/admin/changeStatus/$1/$2';
$route['getTutorDetailsById/(:num)'] = 'admin_panel/admin/getTutorDetailsById/$1';
$route['saveRating/(:num)'] = 'admin_panel/admin/saveRating/$1/';
/******** Gaurav**********/
$route['csv_upload']                  = 'admin_panel/admin/csv_upload';
/******Gaurav End**********************/

/**************************jaidev Admin panel****************************/
$route['dashboardNew']                   = 'admin_panel/admin/dashboardNew';
$route['parentsRecord']                  = 'admin_panel/admin/parentsRecord';
$route['approveAmount/(:num)']                  = 'admin_panel/admin/approveAmount/$1';
$route['parentsIncentives/(:num)']           = 'admin_panel/admin/parentsIncentives';
$route['tutorIncentiveView/(:num)']          = 'admin_panel/admin/tutorIncentiveView/$1';
$route['tutorIncentive/(:num)']              = 'admin_panel/admin/tutorIncentive/$1';
$route['addIncentives/(:num)']               = 'admin_panel/admin/addIncentives/$1';
$route['tutorPaymentsRecord/(:num)']         = 'admin_panel/admin/tutorPaymentsRecord/$1';
$route['addTutorIncentive/(:num)']           = 'admin_panel/admin/addTutorIncentive/$1';
$route['addPayment/(:num)']                  = 'admin_panel/admin/addPayment/$1';
$route['addTutorPayment/(:num)']             = 'admin_panel/admin/addTutorPayment/$1';
$route['schemeView']                         = 'admin_panel/admin/schemeView';
$route['schemeActionPage/(:num)']            = 'admin_panel/admin/schemeActionPage/$1';
$route['updateScheme/(:num)']                = 'admin_panel/admin/updateScheme/$1';
$route['classesTermination']                 = 'admin_panel/admin/classesTerminationView';
$route['changeClassTerminationStatus/(:num)/(:num)/(:num)/(:num)/(:num)/(:num)']  = 'admin_panel/admin/changeClassTerminationStatus/$1/$2/$3/$4/$5/$6';
$route['getParentDetailsById/(:num)']    = 'admin_panel/admin/getParentDetailsById/$1';

//Parent Incentives
$route['parentIncentive/(:num)']             = 'admin_panel/admin/parentIncentive/$1';
$route['addParentIncentives/(:num)']         = 'admin_panel/admin/addParentIncentives/$1';
$route['addParentIncentive/(:num)']           = 'admin_panel/admin/addParentIncentive/$1';
/***************************End Admin panel*****************************/
/*
 * Admin HomeTutor End
 */

/* * *************Parent Account -Jaidev ************** */
$route['parentsAccount'] = 'webservices/parentsAccount';
$route['getClassesAmountDetails'] = 'webservices/parentsAccount/getClassesAmountDetails';
$route['updateTutorAmount'] = 'webservices/parentsAccount/updateTutorAmountAfterPayment';
$route['getAccountDetails'] = 'webservices/parentsAccount/getAccountDetails';

/* * **********************Updated************************ */

$route['parentsDetails'] = 'webservices/parentsDetails';
$route['classesAmountDue'] = 'webservices/parentsDetails/classesAmountDue';
$route['addPayment'] = 'webservices/parentsDetails/addPayment';
$route['parentsAccountDetails'] = 'webservices/parentsDetails/parentsAccountDetails';

////////////////////////tutor//////////////////////////////////////////////
$route['tutorAmountDue'] = 'webservices/parentsDetails/tutorAmountDue';
$route['tutorPaymentRecieved'] = 'webservices/parentsDetails/tutorPaymentRecieved';

////////////////////////Admin Side Work/////////////////////////////////////////
$route['tutorPaymentsByAdmin']   = 'webservices/parentsDetails/tutorPaymentsByAdmin';
$route['tutorIncentives']        = 'webservices/parentsDetails/tutorIncentives';
/* * *************Parent Account -Jaidev ************** */


/* * **********************Udeet (16-08-2016)**************************************** */
$route['addRating'] = 'webservices/Rating/addRating';
$route['getRating'] = 'webservices/Rating/getRating';
$route['getRatingByParentId'] = 'webservices/Rating/getRatingByParentId';
$route['getAllRating']         = 'webservices/Rating/getAllRating';
$route['avgRating']            = 'webservices/Rating/avgRating';
/**************************End Work*********************************************************/


