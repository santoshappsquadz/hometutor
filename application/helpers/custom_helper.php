<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('pre'))
{
    function pre($array)
    {
        echo "<pre>";
        print_r($array);
        echo "</pre>";
    }   
}
if ( ! function_exists('is_json'))
{
    function is_json($string,$return_data = false) 
    {
        $data = json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE) ? ($return_data ? $data : TRUE) : FALSE;
    } 
}
if ( ! function_exists('sendSMS'))
{
    function sendSMS($mobile)
    {
        //$authKey           = "114496AEk1n7jgam574c511d";
        $authKey           = "115082ACwt7SSQ57555698";
        
        //Multiple mobiles numbers separated by comma
        $mobileNumber      = $mobile;

        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId          = "HTUTOR";
        $otp               = rand(1000,9999);
        //Your message to send, Add URL encoding here.
        $msg               = "Your OTP is ".$otp.". Thanks for using Home Tutor.";
        $message           = urlencode($msg);

        //Define route 
        //$route             = "default";
        $route             = '4';
        //Prepare you post parameters
        $postData          = array(
                                    'authkey' => $authKey,
                                    'mobiles' => $mobileNumber,
                                    'message' => $message,
                                    'sender'  => $senderId,
                                    'route'   => $route
                                );

        //API URL
        $url                ="https://control.msg91.com/api/sendhttp.php";

        // init the resource
        $ch                 = curl_init();
        curl_setopt_array($ch, array(
        CURLOPT_URL            => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POST           => true,
        CURLOPT_POSTFIELDS     => $postData

        ));


        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


        //get response
        $output             = curl_exec($ch);

        //Print error if any
        if(curl_errno($ch))
        {
            echo 'error:' . curl_error($ch);
        }
        curl_close($ch);
        if(preg_match("/^[a-zA-Z0-9]*$/", $output, $match))
        {
            return array('success'=>1,"data"=>$otp);
            
        }
        else
        {
            return array('success'=>0,"data"=>$output);
        }
    }
}

if ( ! function_exists('sendPushAsSMS'))
{
    function sendPushAsSMS($mobile,$message)
    {
        //$authKey           = "114496AEk1n7jgam574c511d";
        $authKey           = "115082ACwt7SSQ57555698";
        
        //Multiple mobiles numbers separated by comma
        $mobileNumber      = $mobile;

        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId          = "HTUTOR";
        $otp               = rand(1000,9999);
        //Your message to send, Add URL encoding here.
        $msg               = $message;
        $message           = urlencode($msg);

        //Define route 
        //$route             = "default";
        $route             = '4';
        //Prepare you post parameters
        $postData          = array(
                                    'authkey' => $authKey,
                                    'mobiles' => $mobileNumber,
                                    'message' => $message,
                                    'sender'  => $senderId,
                                    'route'   => $route
                                );

        //API URL
        $url                ="https://control.msg91.com/api/sendhttp.php";

        // init the resource
        $ch                 = curl_init();
        curl_setopt_array($ch, array(
        CURLOPT_URL            => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POST           => true,
        CURLOPT_POSTFIELDS     => $postData

        ));


        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


        //get response
        $output             = curl_exec($ch);

        //Print error if any
        if(curl_errno($ch))
        {
            echo 'error:' . curl_error($ch);
        }
        curl_close($ch);
        if(preg_match("/^[a-zA-Z0-9]*$/", $output, $match))
        {
            return array('success'=>1,"data"=>$otp);
            
        }
        else
        {
            return array('success'=>0,"data"=>$output);
        }
    }
    
}

if ( ! function_exists('getTutors'))
{
    function getTutors($tutorId)
    {
       $CI     = & get_instance();
       $CI->load->model('parent_model'); 
       $obj    = new parent_model;
       $result = $obj->getTutors($tutorId);
       return $result;
    }   
}
if ( ! function_exists('getTutorProfile'))
{
    function getTutorProfile($tutorId)
    {
       
       $CI     = & get_instance();
       $CI->load->model('webservices/tutor_model'); 

       $obj    = new tutor_model;
       $result = $obj->getTutorProfile($tutorId);
       return $result;
    }   
}
if ( ! function_exists('getParents'))
{
    function getParents($parentId)
    {
       $CI     = & get_instance();
       $CI->load->model('parent_model');    
       $obj    = new parent_model;
       $result = $obj->getParentProfile($parentId);
       return $result;
    }   
}

if ( ! function_exists('getRequestDetail'))
{
    function getRequestDetail($requestId)
    {
       $CI     = & get_instance();
       $CI->load->model('parent_model'); 
       $obj    = new parent_model;
       $result = $obj->getRequestDetail($requestId);
       return $result;
    }   
}
if ( ! function_exists('getReasonsName'))
{
    function getReasonsName($reasonId)
    {
       $CI     = & get_instance();
       $CI->load->model('parent_model'); 
       $obj    = new parent_model;
       $result = $obj->getReasonsName($reasonId);
       return $result;
    }   
}